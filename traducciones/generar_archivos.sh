#!/bin/bash

#Agrega nuevos idiomas aqui
idiomas=(pt_BR en_US fr_FR it_IT ru_RU)

if [[ $# < 1 ]]; then
    echo "Genera archivos, use uno de los siguientes parametros"
    echo "--generar_po      Genera los archivos .po para un nuevo idioma"
    echo "--generar_mo      Compila los archisos .po creando la version compilada .mo"
    exit 0
fi

if [ $1 = "--generar_po" ]; then
    echo "Generando archivos .po"
elif [ $1 = "--generar_mo" ]; then
    echo "Generando archivos .mo"
else
    echo "Opcion invalida, use uno de los siguientes parametros"
    echo "--generar_po      Genera los archivos .po para un nuevo idioma"
    echo "--generar_mo      Compila los archisos .po creando la version compilada .mo"
    exit 0
fi

if [ $1 = "--generar_po" ]; then
    xgettext --package-name="concertistamidi" --package-version="1.4.0" --msgid-bugs-address="carlosbarrazaes@gmail.com" --no-wrap --sort-by-file --add-location=file --keyword="T" --keyword="T_" --keyword="N" --add-comments="TR" --language=C++ --output="./concertistamidi.pot" --from-code=utf-8 `find ../codigo -name '*.c++' -o -name '*.h++'`
fi
for idioma in ${idiomas[@]}
do
    echo -e "$idioma\c "
    carpeta="${idioma}/"
    archivo="${idioma}/concertistamidi.po"

    if [ $1 = "--generar_po" ]; then
        #Crea la carpeta si no existe
        if ! [ -d $carpeta ]; then
            echo "Creando carpeta: " $carpeta
            mkdir $carpeta
        fi

        #Crea el archivo o lo actualiza
        if [ -f $archivo ]; then
            echo -e "\tActualizando archivo .po \c"
            msgmerge "${archivo}" "concertistamidi.pot" --output-file="${archivo}"
        else
            echo -e "\t\c";
            msginit --no-wrap --no-translator --input="concertistamidi.pot" --output-file="${archivo}" --locale="${idioma}"
        fi
    fi

    if [ $1 = "--generar_mo" ]; then
        carpeta_compilado="${idioma}/LC_MESSAGES"
        if ! [ -d $carpeta_compilado ]; then
            mkdir $carpeta_compilado;
        fi

        if [ -f $archivo ]; then
            msgfmt "${archivo}" --output-file="${carpeta_compilado}/concertistamidi.mo"
            echo -e "\tArchivo .mo creado"
        else
            echo -e "\tNo existe el archivo, debe crearlo primero con el parametro --generar_po"
        fi
    fi
done
