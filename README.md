# Concertista MIDI

Concertista MIDI es un juego para aprender a tocar piano facilmente usando el teclado o el ratón de tu computador o un teclado MIDI.

### Caracteristicas
* Selector de archivos
	* Navegación a traves de las carpetas configuradas
	* Musica de entrenamiento
* Selector de pistas
	* Se muestra informacion del archivo
		* Formato del archivo MIDI
		* Selector de secuencia MIDI (solo en archivos MIDI 1.0 Tipo 2)
		* Numero de pistas
		* Especificaion MIDI
	* Modo de juego
		* Fondo: Reproduce de forma automatica
		* Tocar: Tocado por el jugador
		* Aprender: Espera a que el jugador toque la nota correcta
	* 24 colores diferentes para las pistas
	* Silenciar pista
	* Vista previa de la pista
* Ventana del Juego
	* Teclado MIDI completo de 128 teclas
		* Teclado de 37, 49, 61, 76, 88 teclas
		* Teclado personalizable desde 24 a 128 teclas
		* Se destaca el teclado habilitado para tocar de acuerdo a la configuración
	* Marcadores en tablero de notas y barra de progreso
		* Marcadores MIDI
		* Puntos de Referencia MIDI
		* Cambios de Armadura
	* Etiquetas en las notas y teclas, configurables
		* Etiquetas Do Fijo
		* Etiquetas Do Variable
		* Etiquetas Notas Inglesas
		* Etiquetas Númericas
		* Etiqueta de Dinamicas (solo en Notas)
	* Se muestra Pulsos por minuto y compás
* Entrada y Salida
	* Admite multiples dispositivos de entrada y salida, con configuración independiente de:
		* Volumen de entrada
		* Volumen de salida
		* Habilitar o no sensibilidad de teclado MIDI
		* Rango de teclado
		* Control de teclas luminosas
			* Canal MIDI 1 al 16
			* Casio LK-S250 SysEx
	* Teclado y ratón
* Libreria MIDI
	* Compatibilidad con especificación MIDI (detectar instrumentos y percusión):
		* Nombres de 256 instrumentos
		* Nombres de 10 percusiones
		* General MIDI Nivel 1 o GM
		* General MIDI Nivel 2 o GM2
		* Roland General Estandar o Roland GS
		* Yamaha General Extendido o Yamaha XG
		* Multimedia PC o MPC
	* Compatibilidad con MIDI 1.0
		* Tipo 0
		* Tipo 1
		* Tipo 2
	* Compatibilidad con MIDI 2.0
		* Archivos SMF2CLIP (.midi2)
* Grabación de Archivos MIDI
* Disponible parcialmente en 6 idiomas
	* Español 100% (original)
	* Ingles 84%
	* Portugués 4%
	* Francés 3%
	* Italiano 3%
	* Ruso 3%

### Capturas

![Menú](extra/imagenes/1.png "Menú")
![Selector de Archivos](extra/imagenes/2.png "Selector de Archivos")
![Selector de Pistas](extra/imagenes/3.png "Selector de Pistas")
![Organo](extra/imagenes/4.png "Organo")

-----------

### Atajos de Teclado

|Tecla                 | Función                                                                     |
|:---------------------|:----------------------------------------------------------------------------|
|F2                    |Alterna entre opciones de etiquetas en las Notas                             |
|F3                    |Alterna entre opciones de etiquetas en el Teclado                            |
|F4                    |Mostrar y ocultar Subtitulos                                                 |
|F5                    |Muestra las notas tocadas anteriormente por el jugador al repetir la canción |
|F9                    |Alterna numero de teclas 88, 76, 61, 49, 37                                  |
|F10                   |Modo Desarrollo                                                              |
|F11                   |Pantalla Completa                                                            |
|F12                   |Modo Alambre                                                                 |
|Flecha Arriba         |Aumentar velocidad, Subir en el navegador de archivos                        |
|Flecha Abajo          |Reducir velocidad, Bajar en el navegador de archivos                         |
|Flecha Izquierda      |Retroceder un compás, Atras en el navegador de archivos                      |
|Flecha Derecha        |Avanzar un compás notas, Adelante en el navegador de archivos                |
|Insertar              |Desplazar Organo a la Derecha                                                |
|Suprimir              |Desplazar Organo a la Izquierda                                              |
|Inicio                |Estirar notas                                                                |
|Fin                   |Encoger notas                                                                |
|Re Pág                |Agregar una tecla al organo                                                  |
|Av Pág                |Eliminar una tecla al organo                                                 |
|Sumar (+)             |Subir volumen                                                                |
|Resta (-)             |Bajar volumen                                                                |
|M                     |Silencio                                                                     |
|V                     |Reiniciar Canción                                                            |
|Espacio               |Pausar                                                                       |
|Escape                |Atras o Salir                                                                |
|Enter                 |Entrar o Continuar                                                           |
|Numeros al Borrar     |Teclas negras                                                                |
|Tabulador al Enter    |Teclas blancas en el organos                                                 |


## Traduciones

Los siguientes idiomas estan disponibles parcialmente

|Idioma              | Completado|
|:-------------------|:----------|
|Ingles (en_US)      |84%        |
|Portugués (pt_BR)   |4%         |
|Francés (fr_FR)     |3%         |
|Italiano (it_IT)    |3%         |
|Ruso (ru_RU)        |3%         |

Para agregar un nuevo idioma debes editar el archivo

	/traducciones/generar_archivos.sh

agregar el idioma en la linea 4, separado por un espacio

	idiomas=(pt_BR en_US fr_FR it_IT ru_RU)

luego para crear el archivo po debes ejecutar el comando siguiente

	sh generar_archivos.sh --generar_po

ahora puedes traducir, puedes usar algun programa como Poedit, una vez terminado puedes compilar el archivo con el comando

	sh generar_archivos.sh --generar_mo

y ver los resultados ejecutando el programa, para forza una configuracion puede usar el comando:

	#Intrucciones para compilar abajo
	cd ../binario
	LANG=en_US.utf-8 ./concertistamidi

## Requerimientos

Se requieren los siguientes paquetes para compilar concertista midi.

|Libreria    | Uso                                   |
|:-----------|:--------------------------------------|
|cmake       |Construccion                           |
|g++         |Compilación                            |
|glew        |Opengl                                 |
|libsdl2     |Sdl2 para OpenGl                       |
|glm         |Libreria de matematica para opengl     |
|freetype    |Tipografias                            |
|icu         |String Unicode                         |
|sqlite      |Base de datos                          |
|gettext     |Libreria para traducciones             |

### Instalar paquetes en Ubuntu

	sudo apt install g++ cmake libglew-dev libsdl2-dev libglm-dev libfreetype6-dev libicu-dev libsqlite3-dev gettext timidity

### Instalar paquetes en Fedora

	sudo yum install g++ cmake glew-devel SDL2-devel glm-devel alsa-lib-devel freetype-devel libicu-devel sqlite-devel gettext timidity++

### Instalar paquetes en Gentoo

	emerge --ask media-libs/glew media-libs/libsdl2 USE:opengl media-libs/glm media-libs/freetype dev-libs/icu dev-db/sqlite sys-devel/gettext media-sound/timidity++ USE:alsa media-sound/fluid-soundfont

#### Configura Timidity en Gentoo

Edita el archivo

	nano /etc/timidity.cfg

Agrega lo siguiente:

	dir /usr/share/sounds/sf2
	soundfont FluidR3_GM.sf2

-----------------

## Compilación

	mkdir binario
	cd binario
	cmake .. -DTIPO_CONSTRUCCION=Liberar
	make

### Habilita las traducciones (opcional)
	cd ../traducciones
	sh generar_archivos.sh --generar_mo

## Instalación

	sudo make install

### Opciones Adicionales

|Opción                      | Descripción                               |
|:---------------------------|:------------------------------------------|
|-DTIPO_CONSTRUCCION=Depurar |Permite construir la versión de depuración habilitando advertencias y la informacion de depuración para usarla con gdb o valgrind|
|-DOPTIMIZAR_NATIVO=true     |Habilita la optimización -march=native     |
|-DCONSTRUCCION_LOCAL=true   |Habilita la construccion local (no puede instalarse)|
|-PREFIJO_INSTALACION=/usr   |Cambia la ruta de instalación              |

--------------

## Ejecutar

Ya es posible ejecutar concertista midi con:

	./concertistamidi

Aviso: En linux existe un dispositivo llamado **Midi Through** que no produce sonido (lo puedes encontrar en el nucleo como SND_SEQ_DUMMY).

