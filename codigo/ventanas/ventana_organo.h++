#ifndef VENTANA_ORGANO_H
#define VENTANA_ORGANO_H

#include "ventana.h++"
#include "../elementos_graficos/barra_progreso.h++"
#include "../elementos_graficos/boton.h++"
#include "../elementos_graficos/etiqueta.h++"
#include "../elementos_graficos/imagen.h++"
#include "../elementos_graficos/tablero_notas.h++"
#include "../elementos_graficos/organo.h++"
#include "../elementos_graficos/partitura.h++"
#include "../elementos_graficos/titulo.h++"
#include "../elementos_graficos/puntuacion.h++"
#include "../control/pista.h++"
#include "../control/rango_organo.h++"
#include "../control/configuracion.h++"
#include "../control/datos_musica.h++"
#include "../control/tecla_organo.h++"
#include "../libreria_midi/midi.h++"
#include "../libreria_midi/funciones_midi.h++"
#include "../util/texto.h++"

#include <array>
#include <mutex>

class VentanaOrgano : public Ventana
{
private:
	bool m_cambio_dispositico_midi;

protected:
	//Recursos
	Rectangulo *m_rectangulo;
	Textura2D *m_textura_subtitulo;
	Textura2D *m_sonido_0;
	Textura2D *m_sonido_1;
	Textura2D *m_sonido_2;
	Textura2D *m_sonido_3;

	//Componentes
	Barra_Progreso *m_barra;
	Tablero_Notas *m_tablero;
	Organo *m_organo;
	Partitura *m_partitura;

	Etiqueta *m_texto_velocidad_ppm;
	Etiqueta *m_texto_velocidad_porcentaje;
	Boton *m_boton_ppm_menos;
	Boton *m_boton_ppm_mas;

	Imagen *m_icono_volumen;
	Etiqueta *m_texto_volumen;

	Etiqueta *m_texto_compas;
	Etiqueta *m_texto_pausa;
	Etiqueta *m_subtitulos;

	//Controles
	std::mutex m_bloqueo;
	bool m_solo_reproduciendo;
	unsigned int m_ppm_actual;
	double m_velocidad_musica;
	double m_volumen;
	double m_volumen_antes_mute;
	bool m_cambio_velocidad;
	bool m_cambio_volumen;
	bool m_pausa;
	bool m_retorno_carro;
	bool m_mostrar_subtitulo;
	unsigned short m_duracion_nota;
	unsigned char m_etiqueta_tablero_notas;
	unsigned char m_etiqueta_organo;
	int m_cambiar_compas;
	unsigned char m_teclado_seleccionado;
	std::string m_subtitulo_texto;

	//Datos
	Controlador_Midi *m_controlador_midi;
	Rango_Organo m_teclado_visible, m_teclado_util;
	Configuracion *m_configuracion;

	std::array<Tecla_Organo, NUMERO_TECLAS_ORGANO> m_teclas;

	bool hay_cambio_dispositivo_midi();
	void cambiar_texto_ppm();
	void actualizar_velocidad();
	void actualizar_volumen();
	void actualizar_textura_boton_volumen();

public:
	VentanaOrgano(Configuracion *configuracion, Administrador_Recursos *recursos);
	~VentanaOrgano();

	void actualizar_comun(unsigned int diferencia_tiempo);
	void actualizar_midi_comun(unsigned int diferencia_tiempo);
	void dibujar_comun();

	void evento_raton_comun(Raton *raton);
	void evento_teclado_comun(Tecla tecla, bool estado);
	void evento_pantalla_comun(float ancho, float alto);

	void pulsos_por_minuto(unsigned int nuevo_ppm);
	void compas(unsigned char numerador, unsigned char denominador);
	int cambiar_compas();
};

#endif
