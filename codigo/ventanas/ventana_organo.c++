#include "ventana_organo.h++"

VentanaOrgano::VentanaOrgano(Configuracion *configuracion, Administrador_Recursos *recursos) : Ventana()
{
	this->fps_dinamico(false);
	this->protector_pantalla(false);

	m_configuracion = configuracion;

	m_rectangulo = recursos->figura(FiguraGeometrica::Rectangulo);
	m_textura_subtitulo = recursos->textura(Textura::Nota);
	m_sonido_0 = recursos->textura(Textura::Sonido_0);
	m_sonido_1 = recursos->textura(Textura::Sonido_1);
	m_sonido_2 = recursos->textura(Textura::Sonido_2);
	m_sonido_3 = recursos->textura(Textura::Sonido_3);

	m_controlador_midi = m_configuracion->controlador_midi();

	m_volumen = m_configuracion->volumen();
	m_velocidad_musica = m_configuracion->velocidad();
	m_duracion_nota = m_configuracion->duracion_nota();
	m_mostrar_subtitulo = m_configuracion->subtitulos();
	m_teclado_visible = m_configuracion->teclado_visible();
	m_teclado_util = m_configuracion->teclado_util();
	m_etiqueta_tablero_notas = m_configuracion->etiqueta_tablero_nota();
	m_etiqueta_organo = m_configuracion->etiqueta_organo();

	m_teclado_seleccionado = 0;
	if(m_teclado_visible.numero_teclas() == 88)
		m_teclado_seleccionado = 0;
	else if(m_teclado_visible.numero_teclas() == 76)
		m_teclado_seleccionado = 1;
	else if(m_teclado_visible.numero_teclas() == 61)
		m_teclado_seleccionado = 2;
	else if(m_teclado_visible.numero_teclas() == 49)
		m_teclado_seleccionado = 3;
	else if(m_teclado_visible.numero_teclas() == 37)
		m_teclado_seleccionado = 4;

	//Actualiza el rango util si hay cambios en los dispositivos
	if(m_configuracion->controlador_midi()->hay_cambios_de_dispositivos())
	{
		m_configuracion->actualizar_rango_util_organo();
		m_teclado_util.cambiar(m_configuracion->teclado_util().tecla_inicial(), m_configuracion->teclado_util().numero_teclas());
	}

	if(m_volumen > 0.0)
		m_volumen_antes_mute = m_volumen;
	else
		m_volumen_antes_mute = 1.0;

	m_barra = new Barra_Progreso(0, 40, Pantalla::Ancho, 40, 0, recursos);
	m_organo = new Organo(0, Pantalla::Alto, Pantalla::Ancho, &m_teclado_visible, &m_teclado_util, Armadura_0, Escala_Mayor, recursos);
	m_tablero = new Tablero_Notas(0, m_barra->alto()+40, Pantalla::Ancho, Pantalla::Alto - (m_organo->alto() + m_barra->alto() + 40), &m_teclado_visible, &m_teclado_util, nullptr, recursos);
	m_partitura = new Partitura(0, m_barra->alto() + 70, Pantalla::Ancho, 120, recursos);

	m_texto_velocidad_ppm = new Etiqueta(recursos);
	m_texto_velocidad_ppm->tipografia(recursos->tipografia(ModoLetra::Grande));
	m_texto_velocidad_ppm->color(Color(1.0f, 1.0f, 1.0f));
	m_texto_velocidad_ppm->posicion((Pantalla::Ancho / 2.0f) - 60, 0.0f);
	m_texto_velocidad_ppm->dimension(120.0f, 40.0f);
	m_texto_velocidad_ppm->margen(2);
	m_texto_velocidad_ppm->alineacion_horizontal(Alineacion_H::Centrado);
	m_texto_velocidad_ppm->alineacion_vertical(Alineacion_V::Arriba);

	std::string valor_1 = std::to_string(m_velocidad_musica*100);
	m_texto_velocidad_porcentaje = new Etiqueta(recursos);
	m_texto_velocidad_porcentaje->texto(valor_1.substr(0, valor_1.find(".")) + "%");
	m_texto_velocidad_porcentaje->tipografia(recursos->tipografia(ModoLetra::Chica));
	m_texto_velocidad_porcentaje->posicion((Pantalla::Ancho / 2.0f) - 60, 0.0f);
	m_texto_velocidad_porcentaje->dimension(120.0f, 40.0f);
	m_texto_velocidad_porcentaje->color(Color(1.0f, 1.0f, 1.0f));
	m_texto_velocidad_porcentaje->alineacion_horizontal(Alineacion_H::Centrado);
	m_texto_velocidad_porcentaje->alineacion_vertical(Alineacion_V::Abajo);
	m_texto_velocidad_porcentaje->margen(2);

	m_boton_ppm_menos = new Boton((Pantalla::Ancho / 2.0f)-41.0f, 24.0f, 16.0f, 16.0f, "", recursos);
	m_boton_ppm_menos->textura(recursos->textura(Textura::FlechaIzquierda));
	m_boton_ppm_menos->color_boton(Color(0.141f, 0.624f, 0.933f));

	m_boton_ppm_mas = new Boton((Pantalla::Ancho / 2.0f)+25.0f, 24.0f, 16.0f, 16.0f, "", recursos);
	m_boton_ppm_mas->textura(recursos->textura(Textura::FlechaDerecha));
	m_boton_ppm_mas->color_boton(Color(0.141f, 0.624f, 0.933f));

	m_icono_volumen = new Imagen(Pantalla::Ancho - 40.0f, 0.0f, true, m_sonido_0, recursos);
	m_icono_volumen->dimension(40.0f, 40.0f);
	this->actualizar_textura_boton_volumen();

	std::string valor_2 = std::to_string(m_volumen*100);
	m_texto_volumen = new Etiqueta(recursos);
	m_texto_volumen->tipografia(recursos->tipografia(ModoLetra::MuyChica));
	m_texto_volumen->color(Color(1.0f, 1.0f, 1.0f));
	m_texto_volumen->posicion(Pantalla::Ancho - 40.0f, 0.0f);
	m_texto_volumen->dimension(40.0f, 40.0f);
	m_texto_volumen->margen(2);
	m_texto_volumen->alineacion_horizontal(Alineacion_H::Derecha);
	m_texto_volumen->alineacion_vertical(Alineacion_V::Abajo);
	m_texto_volumen->texto(valor_2.substr(0, valor_2.find(".")) + "%");

	m_texto_compas = new Etiqueta(recursos);
	m_texto_compas->tipografia(recursos->tipografia(ModoLetra::Mediana));
	m_texto_compas->color(Color(1.0f, 1.0f, 1.0f));
	m_texto_compas->posicion(0, 0);
	m_texto_compas->dimension(100, 40);
	m_texto_compas->margen(5);
	m_texto_compas->alineacion_horizontal(Alineacion_H::Izquierda);
	m_texto_compas->alineacion_vertical(Alineacion_V::Centrado);

	m_texto_pausa = new Etiqueta(recursos);
	m_texto_pausa->texto(T("Pausa"));
	m_texto_pausa->tipografia(recursos->tipografia(ModoLetra::Grande));
	m_texto_pausa->color(Color(0.0f, 0.0f, 0.0f));
	m_texto_pausa->posicion(0, 200);
	m_texto_pausa->dimension(Pantalla::Ancho, 40);
	m_texto_pausa->alineacion_horizontal(Alineacion_H::Centrado);

	m_subtitulos = new Etiqueta(recursos);
	m_subtitulos->tipografia(recursos->tipografia(ModoLetra::Grande));
	m_subtitulos->color(Color(0.0f, 0.0f, 0.0f));
	m_subtitulos->posicion(0, 100);
	m_subtitulos->dimension(Pantalla::Ancho, 20);
	m_subtitulos->alineacion_horizontal(Alineacion_H::Centrado);

	m_tablero->modificar_duracion_nota(m_duracion_nota);
	m_tablero->opcion_etiqueta(m_etiqueta_tablero_notas);

	m_organo->teclas(&m_teclas);
	m_organo->opcion_etiqueta(m_etiqueta_organo);

	//Elimina las notas tocadas antes de esta ventana
	m_controlador_midi->restablecer();

	m_ppm_actual = 120;
	m_cambio_velocidad = false;
	m_cambio_volumen = false;
	m_pausa = false;
	m_retorno_carro = false;
	m_solo_reproduciendo = false;
	m_cambiar_compas = 0;
	m_cambio_dispositico_midi= false;
}

VentanaOrgano::~VentanaOrgano()
{
	m_controlador_midi->restablecer();

	delete m_barra;
	delete m_tablero;
	delete m_organo;
	delete m_partitura;
	delete m_texto_velocidad_ppm;
	delete m_texto_velocidad_porcentaje;
	delete m_boton_ppm_menos;
	delete m_boton_ppm_mas;
	delete m_icono_volumen;
	delete m_texto_volumen;
	delete m_texto_compas;
	delete m_texto_pausa;
	delete m_subtitulos;
}

bool VentanaOrgano::hay_cambio_dispositivo_midi()
{
	bool estado = m_cambio_dispositico_midi;
	m_cambio_dispositico_midi = false;
	return estado;
}

void VentanaOrgano::cambiar_texto_ppm()
{
	std::string valor = std::to_string(static_cast<double>(m_ppm_actual)*m_velocidad_musica);

	//TR PPM es una abreviatura de pulsos por minuto
	m_texto_velocidad_ppm->texto(T_("{0} PPM", valor.substr(0, valor.find("."))));
}

void VentanaOrgano::actualizar_velocidad()
{
	if(m_velocidad_musica < 0.01)
		m_velocidad_musica = 0.01;
	else if(m_velocidad_musica > 2.0)
		m_velocidad_musica = 2.0;

	m_configuracion->velocidad(m_velocidad_musica);
	m_cambio_velocidad = true;
}

void VentanaOrgano::actualizar_volumen()
{
	if(m_volumen < 0.0)
		m_volumen = 0.0;
	else if(m_volumen > 1.5)
		m_volumen = 1.5;

	if(m_volumen > 0.0)
		m_volumen_antes_mute = m_volumen;

	m_configuracion->volumen(m_volumen);
	m_cambio_volumen = true;
}

void VentanaOrgano::actualizar_textura_boton_volumen()
{
	if(m_volumen <= 0.0)
		m_icono_volumen->textura(m_sonido_0);
	else if(m_volumen > 0.0 && m_volumen < (1.0/3.0))
		m_icono_volumen->textura(m_sonido_1);
	else if(m_volumen > (1.0/3.0) && m_volumen < (2.0/3.0))
		m_icono_volumen->textura(m_sonido_2);
	else if(m_volumen > (2.0/3.0))
		m_icono_volumen->textura(m_sonido_3);
}

void VentanaOrgano::actualizar_comun(unsigned int diferencia_tiempo)
{
	//Actualiza el rango util si hay cambios en los dispositivos
	if(!m_solo_reproduciendo && m_configuracion->controlador_midi()->hay_cambios_de_dispositivos())
	{
		m_configuracion->actualizar_rango_util_organo();
		m_teclado_util.cambiar(m_configuracion->teclado_util().tecla_inicial(), m_configuracion->teclado_util().numero_teclas());
		m_cambio_dispositico_midi = true;
	}

	//Desactiva las teclas activas por la cancion porque se volvera a calcular
	for(unsigned char x=0; x<NUMERO_TECLAS_ORGANO; x++)
	{
		Tecla_Organo &tecla = m_teclas[x];
		if(tecla.activa_automatica)
			tecla.activa_automatica = false;
	}

	m_boton_ppm_menos->actualizar(diferencia_tiempo);
	m_boton_ppm_mas->actualizar(diferencia_tiempo);

	//Cambio de velocidad de la musica
	if(m_cambio_velocidad)
	{
		m_cambio_velocidad = false;

		std::string valor = std::to_string(m_velocidad_musica*100);
		m_texto_velocidad_porcentaje->texto(valor.substr(0, valor.find(".")) + "%");

		this->cambiar_texto_ppm();
	}

	if(m_cambio_volumen)
	{
		m_cambio_volumen = false;
		std::string valor = std::to_string(m_volumen*100);
		m_texto_volumen->texto(valor.substr(0, valor.find(".")) + "%");
		this->actualizar_textura_boton_volumen();
	}

	//Se actualizan los componentes
	m_barra->actualizar(diferencia_tiempo);
	m_tablero->actualizar(diferencia_tiempo);
	m_organo->actualizar(diferencia_tiempo);
	m_partitura->actualizar(diferencia_tiempo);
}

void VentanaOrgano::actualizar_midi_comun(unsigned int /*diferencia_tiempo*/)
{
}

void VentanaOrgano::dibujar_comun()
{
	m_tablero->dibujar();
	if(m_mostrar_subtitulo && m_subtitulo_texto.length() > 0)
	{
		//Dibuja el fondo
		m_textura_subtitulo->activar();
		m_rectangulo->textura(true);
		m_rectangulo->extremos_fijos(true, true);
		m_rectangulo->color(Color(0.9f, 0.9f, 0.9f));
		m_rectangulo->dibujar_estirable(Pantalla::Centro_horizontal()-((m_subtitulos->largo_texto()+40)/2), 90, m_subtitulos->largo_texto()+40, m_subtitulos->alto_texto()+20, 15.0f, 12.0f);
		m_subtitulos->dibujar();
		m_rectangulo->extremos_fijos(false, false);
	}

	m_barra->dibujar();
	m_organo->dibujar();

	if(Pantalla::ModoDesarrollo)
		m_partitura->dibujar();

	m_rectangulo->textura(false);
	m_rectangulo->dibujar(0, 0, Pantalla::Ancho, 40, Color(0.141f, 0.624f, 0.933f));
	m_texto_velocidad_ppm->dibujar();
	m_boton_ppm_menos->dibujar();
	m_boton_ppm_mas->dibujar();
	m_texto_compas->dibujar();

	m_texto_velocidad_porcentaje->dibujar();
	m_icono_volumen->dibujar();
	m_texto_volumen->dibujar();

	if(m_pausa)
		m_texto_pausa->dibujar();
}

void VentanaOrgano::evento_raton_comun(Raton *raton)
{
	m_barra->evento_raton(raton);
	m_tablero->evento_raton(raton);
	m_organo->evento_raton(raton);
	m_partitura->evento_raton(raton);
	m_boton_ppm_menos->evento_raton(raton);
	m_boton_ppm_mas->evento_raton(raton);

	int dy = raton->dy();
	if(dy != 0)
	{
		if(raton->esta_sobre((Pantalla::Ancho / 2.0f) - 60.0f, 0, 120.0f, 40.0f))
		{
			//Cambio de PPM
			if(dy > 0)
				m_velocidad_musica+=0.01;
			else
				m_velocidad_musica-=0.01;
			this->actualizar_velocidad();
		}
		else if(raton->esta_sobre(Pantalla::Ancho - 40.0f, 0, 40.0f, 40.0f))
		{
			//Cambio de volumen
			if(dy > 0)
				m_volumen+=0.01;
			else
				m_volumen-=0.01;
			this->actualizar_volumen();
		}
	}

	if(m_boton_ppm_mas->esta_activado())
	{
		m_velocidad_musica+=0.1;
		this->actualizar_velocidad();
	}
	else if(m_boton_ppm_menos->esta_activado())
	{
		m_velocidad_musica-=0.1;
		this->actualizar_velocidad();
	}

	while(m_organo->hay_eventos())
	{
		std::pair<unsigned char, bool> evento = m_organo->obtener_evento();
		m_controlador_midi->enviar_nota(evento.first, evento.second);
	}
}

void VentanaOrgano::evento_teclado_comun(Tecla tecla, bool estado)
{
	bool cambio_teclado = false;
	if(tecla == Tecla::FlechaArriba && estado)
	{
		m_velocidad_musica+=0.01;
		this->actualizar_velocidad();
	}
	else if(tecla == Tecla::FlechaAbajo && estado)
	{
		m_velocidad_musica-=0.01;
		this->actualizar_velocidad();
	}
	else if(tecla == Tecla::FlechaIzquierda && estado)
	{
		m_cambiar_compas = -1;
	}
	else if(tecla == Tecla::FlechaDerecha && estado)
	{
		m_cambiar_compas = 1;
	}
	else if(tecla == Tecla::RestaNumerico && estado)
	{
		m_volumen-=0.1;
		this->actualizar_volumen();
	}
	else if(tecla == Tecla::SumaNumerico && estado)
	{
		m_volumen+=0.1;
		this->actualizar_volumen();
	}
	else if(tecla == Tecla::M && estado)
	{
		if(m_volumen > 0)
			m_volumen = 0;
		else
			m_volumen = m_volumen_antes_mute;
		this->actualizar_volumen();
	}
	else if(tecla == Tecla::Espacio && !estado)
	{
		m_pausa = !m_pausa;
		if(m_pausa)
			m_controlador_midi->desactivar_notas();
	}
	else if(tecla == Tecla::F2 && estado)
	{
		m_etiqueta_tablero_notas ++;
		if(m_etiqueta_tablero_notas == m_tablero->numero_etiquetas())
			m_etiqueta_tablero_notas = 0;
		//Si registra una opcion mayor en la base de datos no mostrara nada, entonces al cambiar no debe seguir
		//mostrando nada, por eso se salta la opcion 0
		else if(m_etiqueta_tablero_notas > m_tablero->numero_etiquetas())
			m_etiqueta_tablero_notas = 1;
		m_tablero->opcion_etiqueta(m_etiqueta_tablero_notas);
		m_configuracion->etiqueta_tablero_nota(m_etiqueta_tablero_notas);
	}
	else if(tecla == Tecla::F3 && estado)
	{
		m_etiqueta_organo ++;
		if(m_etiqueta_organo == m_organo->numero_etiquetas())
			m_etiqueta_organo = 0;
		//Si registra una opcion mayor en la base de datos no mostrara nada, entonces al cambiar no debe seguir
		//mostrando nada, por eso se salta la opcion 0
		else if(m_etiqueta_organo > m_organo->numero_etiquetas())
			m_etiqueta_organo = 1;
		m_organo->opcion_etiqueta(m_etiqueta_organo);
		m_configuracion->etiqueta_organo(m_etiqueta_organo);
	}
	else if(tecla == Tecla::F4 && estado)
	{
		m_mostrar_subtitulo = !m_mostrar_subtitulo;
		m_configuracion->subtitulos(m_mostrar_subtitulo);
		//Actualiza el subtitulo
		if(m_mostrar_subtitulo)
			m_subtitulos->texto(Texto::quitar_espacios_en_extremos(m_subtitulo_texto));
	}
	else if(tecla == Tecla::F9 && estado)
	{
		m_teclado_seleccionado++;
		if(m_teclado_seleccionado > 4)
			m_teclado_seleccionado = 0;
		switch(m_teclado_seleccionado)
		{
			case 0: m_teclado_visible.cambiar(21, 88);
					break;
			case 1: m_teclado_visible.cambiar(28, 76);
					break;
			case 2: m_teclado_visible.cambiar(36, 61);
					break;
			case 3: m_teclado_visible.cambiar(36, 49);
					break;
			case 4: m_teclado_visible.cambiar(48, 37);
					break;
			default: m_teclado_visible.cambiar(21, 88);
		}
		cambio_teclado = true;
	}
	else if(tecla == Tecla::Insertar && estado)
	{
		m_teclado_visible.tecla_inicial(m_teclado_visible.tecla_inicial()+1);
		cambio_teclado = true;
	}
	else if(tecla == Tecla::Suprimir && estado)
	{
		m_teclado_visible.tecla_inicial(m_teclado_visible.tecla_inicial()-1);
		cambio_teclado = true;
	}
	else if(tecla == Tecla::Inicio && estado)
	{
		m_tablero->duracion_nota(1);
		m_duracion_nota = m_tablero->duracion_nota();
		m_configuracion->duracion_nota(m_duracion_nota);
	}
	else if(tecla == Tecla::Fin && estado)
	{
		m_tablero->duracion_nota(-1);
		m_duracion_nota = m_tablero->duracion_nota();
		m_configuracion->duracion_nota(m_duracion_nota);
	}
	else if(tecla == Tecla::Repag && estado)
	{
		m_teclado_visible.numero_teclas(m_teclado_visible.numero_teclas()+1);
		cambio_teclado = true;
	}
	else if(tecla == Tecla::Avpag && estado)
	{
		m_teclado_visible.numero_teclas(m_teclado_visible.numero_teclas()-1);
		cambio_teclado = true;
	}

	if(cambio_teclado)
	{
		m_organo->calcular_tamannos();
		//El organo puede cambiar de tamaño al cambiar el numero de teclas, entonces hay que
		//ajustar el tamaño del tablero de notas
		m_tablero->dimension(Pantalla::Ancho, Pantalla::Alto - (m_organo->alto() + m_barra->alto()+40));
		m_tablero->reiniciar();
		m_configuracion->teclado_visible(m_teclado_visible.tecla_inicial(), m_teclado_visible.numero_teclas());
	}
}

void VentanaOrgano::evento_pantalla_comun(float ancho, float alto)
{
	m_texto_velocidad_ppm->posicion((ancho / 2.0f) - 60.0f, 0);
	m_texto_velocidad_porcentaje->posicion((ancho / 2.0f) - 60.0f, 0);
	m_boton_ppm_menos->posicion((ancho / 2.0f) - 41.0f, 24.0f);
	m_boton_ppm_mas->posicion((ancho / 2.0f) + 25.0f, 24.0f);

	m_icono_volumen->posicion(ancho - 40.0f, 0.0f);
	m_texto_volumen->posicion(ancho - 40.0f, 0.0f);

	m_barra->dimension(ancho, m_barra->alto());
	m_organo->posicion(m_organo->x(), alto);
	m_organo->dimension(ancho, m_organo->alto());
	m_tablero->dimension(ancho, alto - (m_organo->alto() + m_barra->alto()+40));
	m_partitura->dimension(ancho, 200);

	m_texto_pausa->dimension(ancho, 40);
	m_subtitulos->dimension(Pantalla::Ancho, 20);
}

void VentanaOrgano::pulsos_por_minuto(unsigned int nuevo_ppm)
{
	m_ppm_actual = nuevo_ppm;
	this->cambiar_texto_ppm();
}

void VentanaOrgano::compas(unsigned char numerador, unsigned char denominador)
{
	unsigned int compas_numerador = static_cast<unsigned int>(numerador);
	unsigned int compas_denominador = static_cast<unsigned int>(denominador);
	m_texto_compas->texto(std::to_string(compas_numerador) + "/" + std::to_string(compas_denominador));
}

int VentanaOrgano::cambiar_compas()
{
	int valor = m_cambiar_compas;
	m_cambiar_compas = 0;
	return valor;
}
