#ifndef VENTANAORGANO_H
#define VENTANAORGANO_H

#include "ventana.h++"
#include "ventana_organo.h++"
#include "../elementos_graficos/barra_progreso.h++"
#include "../elementos_graficos/etiqueta.h++"
#include "../elementos_graficos/tablero_notas.h++"
#include "../elementos_graficos/organo.h++"
#include "../elementos_graficos/titulo.h++"
#include "../elementos_graficos/puntuacion.h++"
#include "../control/pista.h++"
#include "../control/rango_organo.h++"
#include "../control/configuracion.h++"
#include "../control/datos_musica.h++"
#include "../control/tecla_organo.h++"
#include "../libreria_midi/midi.h++"

class VentanaReproduccion : public VentanaOrgano
{
private:
	//Componentes
	Titulo *m_titulo_musica;
	Puntuacion *m_puntaje;
	Etiqueta m_texto_combos;

	//Datos
	Datos_Musica *m_musica;
	std::vector<Pista_Midi*> *m_pistas_midi;
	Evaluacion *m_evaluacion;
	TiempoTocado *m_tiempo_tocado;
	std::vector<std::pair<unsigned char, unsigned short>> m_notas_requeridas;
	std::vector<unsigned int> m_primera_nota;//Primera nota por cada pista
	std::vector<Pista> *m_pistas;
	std::vector<std::int64_t> *m_barras_compas;
	bool m_cambio_tiempo_compas;
	bool m_cambio_tiempo_barra;
	std::int64_t m_tiempo_actual_midi;
	std::int64_t m_maximo_tiempo_actualizar;
	bool m_reinicio_cancion;

	void inicializar();
	void recalcular_puntaje();
	void reproducir_eventos(unsigned int diferencia_tiempo_microsegundos);
	void escuchar_eventos();
	void calcular_subtitulos(Evento_Midi *evento);

	void calcular_teclas_activas(unsigned int diferencia_tiempo);
	void reiniciar(bool restablecer_controles);
	bool hay_nota_nueva(unsigned char id_nota);
	unsigned int encontrar_nota_tocada(unsigned short pista, unsigned char id_nota);
	void bloquear_nota(unsigned short pista, unsigned int numero_nota);
	void desbloquear_nota(unsigned short pista, unsigned int numero_nota);
	void desbloquear_notas(bool desbloquear_todas);

	void agregar_nota_requerida(unsigned char id_nota, unsigned short pista);
	void borrar_notas_requeridas();
	bool hay_notas_requeridas();
	void cerrar_notas_evaluacion();

public:
	VentanaReproduccion(Configuracion *configuracion, Datos_Musica *musica, Administrador_Recursos *recursos);
	~VentanaReproduccion();

	void actualizar(unsigned int diferencia_tiempo) override;
	void actualizar_midi(unsigned int diferencia_tiempo) override;
	void dibujar() override;

	void evento_raton(Raton *raton) override;
	void evento_teclado(Tecla tecla, bool estado) override;
	void evento_pantalla(float ancho, float alto) override;
};
#endif
