#ifndef VENTANA_RESULTADOS_H
#define VENTANA_RESULTADOS_H

#include "ventana.h++"
#include "../recursos/administrador_recursos.h++"
#include "../elementos_graficos/etiqueta.h++"
#include "../elementos_graficos/boton.h++"
#include "../control/datos_musica.h++"

class VentanaResultados : public Ventana
{
private:
	Rectangulo *m_rectangulo;
	Etiqueta m_texto_titulo;
	Boton *m_boton_atras;
	Boton *m_boton_repetir;

	Etiqueta *m_texto_resultado_total;
	Etiqueta *m_texto_notas_totales;
	Etiqueta *m_texto_notas_totales_valor;
	Etiqueta *m_texto_tiempo_inferior;
	Etiqueta *m_texto_tiempo_inferior_valor;
	Etiqueta *m_texto_tiempo_superior;
	Etiqueta *m_texto_tiempo_superior_valor;
	Etiqueta *m_texto_tiempo_desface;
	Etiqueta *m_texto_tiempo_desface_valor;
	Etiqueta *m_texto_errores;
	Etiqueta *m_texto_errores_valor;

public:
	VentanaResultados(Datos_Musica *musica, Administrador_Recursos *recursos);
	~VentanaResultados();

	void actualizar(unsigned int diferencia_tiempo) override;
	void actualizar_midi(unsigned int diferencia_tiempo) override;
	void dibujar() override;

	void evento_raton(Raton *raton) override;
	void evento_teclado(Tecla tecla, bool estado) override;
	void evento_pantalla(float ancho, float alto) override;
};

#endif
