#include "ventana_configuracion.h++"

VentanaConfiguracion::VentanaConfiguracion(Configuracion *configuracion, Administrador_Recursos *recursos) : Ventana(), m_texto_titulo(recursos)
{
	this->fps_dinamico(true);
	this->protector_pantalla(true);
	this->consumir_eventos(true);

	m_configuracion = configuracion;

	m_recursos = recursos;
	m_rectangulo = recursos->figura(FiguraGeometrica::Rectangulo);

	m_texto_titulo.texto(T("Configuración"));
	m_texto_titulo.tipografia(recursos->tipografia(ModoLetra::Grande));
	m_texto_titulo.color(Color(1.0f, 1.0f, 1.0f));
	m_texto_titulo.posicion(0, 0);
	m_texto_titulo.dimension(Pantalla::Ancho, 40);
	m_texto_titulo.alineacion_horizontal(Alineacion_H::Centrado);
	m_texto_titulo.alineacion_vertical(Alineacion_V::Centrado);

	m_boton_atras = new Boton(10, Pantalla::Alto - 36, 120, 32, T("Atrás"), ModoLetra::Chica, recursos);
	m_boton_atras->color_boton(Color(0.9f, 0.9f, 0.9f));
	m_selector_archivos = nullptr;

	//Pestaña de configuracion general
	m_solapa = new Panel_Solapa(0, 40, 250, Pantalla::Alto, recursos);
	m_solapa->agregar_solapa(T("General"));
	m_solapa1_titulo = new Etiqueta(260, 50, Pantalla::Ancho-270, 20, T("General"), ModoLetra::Grande, recursos);
	m_solapa1_titulo->alineacion_horizontal(Alineacion_H::Centrado);

	m_solapa1_texto_restablecer = new Etiqueta(260, 100, Pantalla::Ancho-430, 30, T("Volver a la configuración predeterminada"), ModoLetra::Mediana, recursos);
	m_solapa1_texto_restablecer->alineacion_vertical(Alineacion_V::Centrado);

	m_solapa1_texto_limpiar = new Etiqueta(260, 140, Pantalla::Ancho-430, 30, T("Limpiar la base de datos"), ModoLetra::Mediana, recursos);
	m_solapa1_texto_limpiar->alineacion_vertical(Alineacion_V::Centrado);

	m_solapa1_texto_borrar = new Etiqueta(260, 180, Pantalla::Ancho-430, 30, T("Borrar la base de datos"), ModoLetra::Mediana, recursos);
	m_solapa1_texto_borrar->alineacion_vertical(Alineacion_V::Centrado);

	m_solapa1_restablecer = new Boton(Pantalla::Ancho - 160, 100, 150, 30, T("Restablecer"), ModoLetra::Mediana, recursos);
	m_solapa1_restablecer->margen(0);

	m_solapa1_limpiar_bd = new Boton(Pantalla::Ancho - 160, 140, 150, 30, T("Limpiar"), ModoLetra::Mediana, recursos);
	m_solapa1_limpiar_bd->margen(0);

	m_solapa1_borrar_db = new Boton(Pantalla::Ancho - 160, 180, 150, 30, T("Borrar"), ModoLetra::Mediana, recursos);
	m_solapa1_borrar_db->margen(0);

	m_solapa1_casilla_midi2 = new Casilla_Verificacion(260, 220, Pantalla::Ancho-270, 30, T("Habilitar MIDI 2.0 en dispositivos compatibles"), recursos);
	if(m_configuracion->habilitar_midi_2())
		m_solapa1_casilla_midi2->estado(true);
	else
		m_solapa1_casilla_midi2->estado(false);

	m_solapa1_desarrollo = new Etiqueta(260, 270, Pantalla::Ancho-270, 20, T("Desarrollo"), ModoLetra::Grande, recursos);
	m_solapa1_desarrollo->alineacion_horizontal(Alineacion_H::Centrado);

	m_solapa1_casilla_desarrollo = new Casilla_Verificacion(260, 320, Pantalla::Ancho-270, 30, T("Modo Desarrollo (F10)"), recursos);
	m_solapa1_casilla_modo_alambre = new Casilla_Verificacion(260, 360, Pantalla::Ancho-270, 30, T("Modo Alambre (F12)"), recursos);

	m_solapa->agregar_elemento_solapa(0, m_solapa1_titulo);
	m_solapa->agregar_elemento_solapa(0, m_solapa1_texto_restablecer);
	m_solapa->agregar_elemento_solapa(0, m_solapa1_texto_limpiar);
	m_solapa->agregar_elemento_solapa(0, m_solapa1_texto_borrar);
	m_solapa->agregar_elemento_solapa(0, m_solapa1_restablecer);
	m_solapa->agregar_elemento_solapa(0, m_solapa1_limpiar_bd);
	m_solapa->agregar_elemento_solapa(0, m_solapa1_borrar_db);
	m_solapa->agregar_elemento_solapa(0, m_solapa1_casilla_midi2);
	m_solapa->agregar_elemento_solapa(0, m_solapa1_desarrollo);
	m_solapa->agregar_elemento_solapa(0, m_solapa1_casilla_desarrollo);
	m_solapa->agregar_elemento_solapa(0, m_solapa1_casilla_modo_alambre);

	//Pestaña de configuracion de carpetas midi
	m_solapa->agregar_solapa(T("Archivos"));

	m_solapa2_grabacion = new Etiqueta(260, 50, Pantalla::Ancho-270, 20, T("Grabación"), ModoLetra::Grande, recursos);
	m_solapa2_grabacion->alineacion_horizontal(Alineacion_H::Centrado);

	m_solapa2_texto_grabacion = new Etiqueta(260, 100, Pantalla::Ancho-530, 30, T_("Carpeta: {0}", m_configuracion->carpeta_grabacion()), ModoLetra::Mediana, recursos);
	m_solapa2_texto_grabacion->alineacion_vertical(Alineacion_V::Centrado);

	m_solapa2_cambiar_carpeta = new Boton(Pantalla::Ancho - 260, 100, 250, 30, T("Cambiar"), ModoLetra::Mediana, recursos);
	m_solapa2_cambiar_carpeta->margen(0);

	m_solapa2_texto_version_midi = new Etiqueta(260, 140, Pantalla::Ancho-530, 30, T_("Version del archivos MIDI:"), ModoLetra::Mediana, recursos);
	m_solapa2_texto_version_midi->alineacion_vertical(Alineacion_V::Centrado);

	m_solapa2_opcion_version_midi = new Lista_Opciones(Pantalla::Ancho - 260, 140, 250, 30, true, recursos);
	m_solapa2_opcion_version_midi->tipografia(recursos->tipografia(ModoLetra::Mediana));
	std::vector<std::string> opciones_version_midi;
	opciones_version_midi.push_back(T("Versión 1.0 (*.mid)"));
	opciones_version_midi.push_back(T("Versión 2.0 (*.midi2)"));
	m_solapa2_opcion_version_midi->opciones_textos(opciones_version_midi);
	m_solapa2_opcion_version_midi->opcion_predeterminada(m_configuracion->grabacion_version_midi()-1);

	m_solapa2_reproduccion = new Etiqueta(260, 190, Pantalla::Ancho-270, 20, T("Carpetas Reproducción"), ModoLetra::Grande, recursos);
	m_solapa2_reproduccion->alineacion_horizontal(Alineacion_H::Centrado);

	m_solapa2_tabla = new Tabla(260, 240, Pantalla::Ancho-270, Pantalla::Alto-330, 32, recursos);
	m_solapa2_tabla->agregar_columna(TipoCelda::Etiqueta, T("Nombre"), false, 1);
	m_solapa2_tabla->agregar_columna(TipoCelda::Etiqueta, T("Ruta"), false, 3);
	this->cargar_tabla_carpetas();
	m_solapa2_agregar = new Boton(Pantalla::Ancho-320, Pantalla::Alto-80, 150, 30, T("Agregar"), ModoLetra::Mediana, recursos);
	m_solapa2_agregar->margen(0);

	m_solapa2_eliminar = new Boton(Pantalla::Ancho-160, Pantalla::Alto-80, 150, 30, T("Eliminar"), ModoLetra::Mediana, recursos);
	m_solapa2_eliminar->margen(0);
	m_solapa2_eliminar->habilitado(false);//Se habilita al seleccionar una fila de la tabla

	m_opcion_selector_archivos = 0;

	m_solapa->agregar_elemento_solapa(1, m_solapa2_grabacion);
	m_solapa->agregar_elemento_solapa(1, m_solapa2_texto_grabacion);
	m_solapa->agregar_elemento_solapa(1, m_solapa2_cambiar_carpeta);
	m_solapa->agregar_elemento_solapa(1, m_solapa2_texto_version_midi);
	m_solapa->agregar_elemento_solapa(1, m_solapa2_opcion_version_midi);
	m_solapa->agregar_elemento_solapa(1, m_solapa2_reproduccion);
	m_solapa->agregar_elemento_solapa(1, m_solapa2_tabla);
	m_solapa->agregar_elemento_solapa(1, m_solapa2_agregar);
	m_solapa->agregar_elemento_solapa(1, m_solapa2_eliminar);

	//Pestaña de configuracion de dispositivos midis
	m_solapa->agregar_solapa(T("Dispositivos"));
	m_solapa3_titulo = new Etiqueta(260, 50, Pantalla::Ancho-270, 20, T("Dispositivos"), ModoLetra::Grande, recursos);
	m_solapa3_titulo->alineacion_horizontal(Alineacion_H::Centrado);

	m_solapa3_panel = new Panel_Desplazamiento(260, 100, Pantalla::Ancho-270, Pantalla::Alto-150, 10, recursos);

	Controlador_Midi *controlador = m_configuracion->controlador_midi();
	for(unsigned int x=0; x<controlador->lista_dispositivos().size(); x++)
	{
		m_solapa3_lista_dispositivos.push_back(new Configuracion_Dispositivo(0, 0, Pantalla::Ancho-270, *controlador->lista_dispositivos()[x], recursos));
		m_solapa3_panel->agregar_elemento(m_solapa3_lista_dispositivos[x]);
	}

	m_solapa->agregar_elemento_solapa(2, m_solapa3_titulo);
	m_solapa->agregar_elemento_solapa(2, m_solapa3_panel);

	m_actualizar_lista_dispositivos = false;

	//Pestaña de configuracion de videos
	m_solapa->agregar_solapa(T("Video"));
	m_solapa4_titulo = new Etiqueta(260, 50, Pantalla::Ancho-270, 20, T("Video"), ModoLetra::Grande, recursos);
	m_solapa4_titulo->alineacion_horizontal(Alineacion_H::Centrado);

	m_solapa4_casilla_pantalla_completa = new Casilla_Verificacion(260, 100, Pantalla::Ancho-270, 30, T("Pantalla Completa (F11)"), recursos);

	m_solapa->agregar_elemento_solapa(3, m_solapa4_titulo);
	m_solapa->agregar_elemento_solapa(3, m_solapa4_casilla_pantalla_completa);

	//Actualiza segun el estado de la pantalla
	if(m_solapa4_casilla_pantalla_completa->activado() != Pantalla::PantallaCompleta)
		m_solapa4_casilla_pantalla_completa->estado(Pantalla::PantallaCompleta);
	if(m_solapa1_casilla_desarrollo->activado() != Pantalla::ModoDesarrollo)
		m_solapa1_casilla_desarrollo->estado(Pantalla::ModoDesarrollo);
	if(m_solapa1_casilla_modo_alambre->activado() != Pantalla::ModoAlambre)
		m_solapa1_casilla_modo_alambre->estado(Pantalla::ModoAlambre);
}

VentanaConfiguracion::~VentanaConfiguracion()
{
	delete m_boton_atras;
	if(m_selector_archivos != nullptr)
		delete m_selector_archivos;

	delete m_solapa1_titulo;
	delete m_solapa1_texto_restablecer;
	delete m_solapa1_texto_limpiar;
	delete m_solapa1_texto_borrar;
	delete m_solapa1_restablecer;
	delete m_solapa1_limpiar_bd;
	delete m_solapa1_borrar_db;
	delete m_solapa1_casilla_midi2;
	delete m_solapa1_desarrollo;
	delete m_solapa1_casilla_desarrollo;
	delete m_solapa1_casilla_modo_alambre;

	delete m_solapa2_grabacion;
	delete m_solapa2_texto_grabacion;
	delete m_solapa2_texto_version_midi;
	delete m_solapa2_cambiar_carpeta;
	delete m_solapa2_opcion_version_midi;
	delete m_solapa2_reproduccion;
	delete m_solapa2_tabla;
	delete m_solapa2_agregar;
	delete m_solapa2_eliminar;

	delete m_solapa3_titulo;
	//Elimina todos los elementos de configuracion de dispositivos
	for(unsigned int x=0; x<m_solapa3_lista_dispositivos.size(); x++)
		delete m_solapa3_lista_dispositivos[x];
	delete m_solapa3_panel;

	delete m_solapa4_titulo;
	delete m_solapa4_casilla_pantalla_completa;

	delete m_solapa;
}

void VentanaConfiguracion::cargar_tabla_carpetas()
{
	std::vector<std::vector<std::string>> carpetas = m_configuracion->base_de_datos()->carpetas();
	for(unsigned int c=0; c < carpetas.size(); c++)
	{
		std::vector<CeldaContenido> fila;
		for(unsigned int f=0; f < carpetas[c].size(); f++)
		{
			fila.push_back(CeldaContenido{.texto = carpetas[c][f]});
		}
		m_solapa2_tabla->insertar_fila(fila);
	}
}

unsigned int VentanaConfiguracion::limpiar_base_de_datos()
{
	unsigned int registros_eliminados = 0;
	m_configuracion->base_de_datos()->iniciar_transaccion();
	std::vector<std::string> archivos = m_configuracion->base_de_datos()->lista_archivos();
	for(unsigned int x=0; x<archivos.size(); x++)
	{
		//Archivos que ya no existen, movidos o renombrados
		if(!std::ifstream(archivos[x]))
		{
			//NOTE Agregar las demas tablas una vez que esten implementadas
			m_configuracion->base_de_datos()->borrar_archivo(archivos[x]);
			Registro::Nota(T_("El archivo \"{0}\" no existe", archivos[x]));
			registros_eliminados++;
		}
	}
	std::vector<std::string> carpetas = m_configuracion->base_de_datos()->lista_seleccion();
	for(unsigned int x=0; x<carpetas.size(); x++)
	{
		if(!std::ifstream(carpetas[x]) && carpetas[x] != "-")
		{
			m_configuracion->base_de_datos()->borrar_seleccion(carpetas[x]);
			Registro::Nota(T_("La carpeta \"{0}\" no existe", carpetas[x]));
			registros_eliminados++;
		}
	}

	std::vector<std::vector<std::string>> carpetas_de_busqueda = m_configuracion->base_de_datos()->carpetas();
	bool cambio_lista = false;
	for(unsigned int x=0; x<carpetas_de_busqueda.size(); x++)
	{
		if(!std::ifstream(carpetas_de_busqueda[x][1]))
		{
			m_configuracion->base_de_datos()->eliminar_carpeta(carpetas_de_busqueda[x][1]);
			Registro::Nota(T_("La carpeta de busqueda \"{0}\" no existe", carpetas_de_busqueda[x][1]));
			registros_eliminados++;
			cambio_lista = true;
		}
	}
	if(cambio_lista)
	{
		//Se vuelve a cargar la tabla de carpetas de busqueda
		this->m_solapa2_tabla->vaciar();
		this->cargar_tabla_carpetas();
	}
	m_configuracion->base_de_datos()->finalizar_transaccion();
	return registros_eliminados;
}

void VentanaConfiguracion::guardar_configuracion_dispositivos()
{
	m_configuracion->base_de_datos()->iniciar_transaccion();
	bool hay_cambios = false;
	for(unsigned int x=0; x<m_solapa3_lista_dispositivos.size(); x++)
	{

		Configuracion_Dispositivo *actual = m_solapa3_lista_dispositivos[x];
		if(actual->dispositivo_cambiado())
		{
			hay_cambios = true;
			Dispositivo_Midi datos = actual->configuracion();
			Dispositivo_Midi *dispositivo = m_configuracion->controlador_midi()->obtener_dispositivo(datos.cliente(), datos.puerto(), datos.nombre());

			if(dispositivo != nullptr)
			{
				if(dispositivo->conectado())
					m_configuracion->controlador_midi()->desconectar_dispositivo(dispositivo);
				else if(!datos.habilitado())//Si el dispositivo no estaba conectado y se deshabilita se elimina del controlador MIDI
				{
					m_actualizar_lista_dispositivos = true;
					dispositivo->habilitado(false);
					m_configuracion->controlador_midi()->eliminar_dispositivo(dispositivo);
				}
				dispositivo->copiar_configuracion(datos);//Carga la nueva configuracion
			}
			if(datos.habilitado())
			{
				//Si existe, se borra la configuración anterior para guardar la nueva configuracion
				if(m_configuracion->base_de_datos()->existe_dispositivo(datos))
					m_configuracion->base_de_datos()->eliminar_dispositivo(datos);
				m_configuracion->base_de_datos()->agregar_dispositivo(datos);
				if(dispositivo != nullptr)
					m_configuracion->controlador_midi()->conectar_dispositivo(dispositivo);
			}
			else
			{
				m_configuracion->base_de_datos()->eliminar_dispositivo(datos);
			}
		}
	}
	m_configuracion->base_de_datos()->finalizar_transaccion();
	if(hay_cambios)
		m_configuracion->actualizar_rango_util_organo();
}

void VentanaConfiguracion::actualizar(unsigned int diferencia_tiempo)
{
	if(m_selector_archivos != nullptr)
		m_selector_archivos->actualizar(diferencia_tiempo);
	m_solapa->actualizar(diferencia_tiempo);

	if(m_solapa->solapa_activa() == 2)
	{
		Controlador_Midi *controlador = m_configuracion->controlador_midi();
		if(controlador->hay_cambios_de_dispositivos() || m_actualizar_lista_dispositivos)
		{
			m_actualizar_lista_dispositivos = false;
			//Guarda los cambios de la configuracion, porque se borrara todo y se cargaran nuevamente
			this->guardar_configuracion_dispositivos();

			//Elimina todos los elementos de configuracion de dispositivos
			for(unsigned int x=0; x<m_solapa3_lista_dispositivos.size(); x++)
				delete m_solapa3_lista_dispositivos[x];
			m_solapa3_lista_dispositivos.clear();
			m_solapa3_panel->vaciar();

			//Crea nuevamente la lista de dispositivos

			for(unsigned int x=0; x<controlador->lista_dispositivos().size(); x++)
			{
				m_solapa3_lista_dispositivos.push_back(new Configuracion_Dispositivo(0, 0, Pantalla::Ancho-270, *controlador->lista_dispositivos()[x], m_recursos));
				m_solapa3_panel->agregar_elemento(m_solapa3_lista_dispositivos[x]);
			}

			//Actualiza las posiciones, reenvia el evento porque se creeo todo de nuevo, normalmente esto lo hace
			//el panel m_solapa
			m_solapa3_panel->actualizar(diferencia_tiempo);
		}
		else
		{
			bool cambiar_altura = false;
			for(unsigned int x=0; x<m_solapa3_lista_dispositivos.size() && !cambiar_altura; x++)
			{
				if(m_solapa3_lista_dispositivos[x]->cambio_altura())
					cambiar_altura = true;
			}
			if(cambiar_altura)
				m_solapa3_panel->actualizar_dimension();
		}
	}
	m_boton_atras->actualizar(diferencia_tiempo);
}

void VentanaConfiguracion::actualizar_midi(unsigned int /*diferencia_tiempo*/)
{
}

void VentanaConfiguracion::dibujar()
{
	m_rectangulo->textura(false);
	m_rectangulo->dibujar(0, 0, Pantalla::Ancho, 40, Color(0.141f, 0.624f, 0.933f));//Borde arriba
	m_rectangulo->dibujar(0, Pantalla::Alto - 40, Pantalla::Ancho, 40, Color(0.761f, 0.887f, 0.985f));//Borde abajo
	m_texto_titulo.dibujar();

	m_solapa->dibujar();
	m_boton_atras->dibujar();

	if(m_selector_archivos != nullptr)
		m_selector_archivos->dibujar();
}

void VentanaConfiguracion::evento_raton(Raton *raton)
{
	if(m_selector_archivos != nullptr)//Cancela los demas eventos
	{
		m_selector_archivos->evento_raton(raton);
		if(m_selector_archivos->dialogo() == Dialogo::Cancelar)
		{
			delete m_selector_archivos;
			m_selector_archivos = nullptr;
		}
		else if(m_selector_archivos->dialogo() == Dialogo::Aceptar)
		{
			if(m_opcion_selector_archivos == 0)
			{
				std::string ruta_nueva = m_selector_archivos->ruta_seleccionada();
				Registro::Depurar("Carpeta de grabación en: " + ruta_nueva);
				m_configuracion->carpeta_grabacion(ruta_nueva);
				m_solapa2_texto_grabacion->texto(T_("Carpeta: {0}", ruta_nueva));
				delete m_selector_archivos;
				m_selector_archivos = nullptr;
			}
			else if(m_opcion_selector_archivos == 1)
			{
				std::string ruta_nueva = m_selector_archivos->ruta_seleccionada();
				std::string nombre_carpeta = Funciones::nombre_archivo(ruta_nueva, true);
				if(ruta_nueva == "/")
					nombre_carpeta = "/";
				Registro::Depurar("Agregando la Carpeta: \"" + nombre_carpeta + "\" Ruta: \"" + ruta_nueva + "\"");
				m_configuracion->base_de_datos()->agregar_carpeta(nombre_carpeta, ruta_nueva);
				delete m_selector_archivos;
				m_selector_archivos = nullptr;

				//Se recarga la tabla de carpetas
				m_solapa2_eliminar->habilitado(false);
				this->m_solapa2_tabla->vaciar();
				this->cargar_tabla_carpetas();
			}
		}
		else
			return;
	}
	m_solapa->evento_raton(raton);
	m_boton_atras->evento_raton(raton);
	if(m_boton_atras->esta_activado())
	{
		//Se guardan los cambios en los dispositivos
		this->guardar_configuracion_dispositivos();
		m_accion = Accion::CambiarATitulo;
	}

	if(m_solapa->solapa_activa() == 0)
	{
		if(m_solapa1_restablecer->esta_activado())
		{
			m_configuracion->velocidad(1.0);
			m_configuracion->duracion_nota(6500);
			m_configuracion->teclado_visible(21,88);
			m_configuracion->subtitulos(true);
			Notificacion::Nota(T("Configuración restablecida"), 5);
		}
		if(m_solapa1_limpiar_bd->esta_activado())
		{
			Notificacion::Nota(T("Limpiando base de datos..."), 1);
			unsigned int registros_eliminados = this->limpiar_base_de_datos();
			if(registros_eliminados == 1)
				Notificacion::Nota(T("Se borro 1 registro huerfano"), 5);
			else if(registros_eliminados > 1)
				Notificacion::Nota(T_("Se borraron {0} registros huerfanos", registros_eliminados), 5);
			else
				Notificacion::Nota(T("Base de datos limpia"), 5);
		}
		if(m_solapa1_borrar_db->esta_activado())
		{
			m_configuracion->base_de_datos()->borrar_archivos();
			m_configuracion->base_de_datos()->borrar_selecciones();
			Notificacion::Nota(T("Base de datos borrada"), 5);
		}
		if(m_solapa1_casilla_midi2->cambio_estado())
		{
			if(m_solapa1_casilla_midi2->activado())
				m_configuracion->habilitar_midi_2(true);
			else
				m_configuracion->habilitar_midi_2(false);
		}
		if(m_solapa1_casilla_desarrollo->cambio_estado())
		{
			if(m_solapa1_casilla_desarrollo->activado())
				m_accion = Accion::EntrarModoDesarrollo;
			else
				m_accion = Accion::SalirModoDesarrollo;
		}
		if(m_solapa1_casilla_modo_alambre->cambio_estado())
		{
			if(m_solapa1_casilla_modo_alambre->activado())
				m_accion = Accion::EntrarModoAlambre;
			else
				m_accion = Accion::SalirModoAlambre;
		}
	}
	else if(m_solapa->solapa_activa() == 1)
	{
		if(m_solapa2_tabla->seleccion())
			m_solapa2_eliminar->habilitado(true);

		bool mostrar_selector_archivo = false;
		if(m_solapa2_cambiar_carpeta->esta_activado())
		{
			mostrar_selector_archivo = true;
			m_opcion_selector_archivos = 0;
		}
		if(m_solapa2_opcion_version_midi->cambio_opcion_seleccionada())
			m_configuracion->grabacion_version_midi(static_cast<unsigned char>(m_solapa2_opcion_version_midi->opcion_seleccionada()+1));
		if(m_solapa2_agregar->esta_activado())
		{
			mostrar_selector_archivo = true;
			m_opcion_selector_archivos = 1;
		}
		if(mostrar_selector_archivo)
			m_selector_archivos = new Selector_Archivos(Pantalla::Ancho/2 - 600/2, Pantalla::Alto/2 - 450/2, 600, 450, T("Seleccione una carpeta"), Usuario::carpeta_personal(), false, m_recursos);
		else if(m_solapa2_eliminar->esta_activado())
		{
			//Borra seleccion actual y recarga la tabla
			std::vector<std::vector<std::string>> carpetas = m_configuracion->base_de_datos()->carpetas();
			unsigned int seleccion = m_solapa2_tabla->obtener_seleccion();
			if(seleccion < carpetas.size())
			{
				Registro::Depurar("Eliminando de la lista la Carpeta: \"" + carpetas[seleccion][0] + "\" Ruta: \"" + carpetas[seleccion][1] + "\"");
				m_configuracion->eliminar_carpeta(carpetas[seleccion][1]);

				//Se recarga la tabla de carpetas
				m_solapa2_eliminar->habilitado(false);
				this->m_solapa2_tabla->vaciar();
				this->cargar_tabla_carpetas();
			}
		}
	}
	else if(m_solapa->solapa_activa() == 2)
	{
		if(raton->tipo_evento() == EventoRaton::Clic && !raton->activado(BotonRaton::Izquierdo))
		{
			//Cuando suelta el clic se revisa si cambio el estado del dispositivo a habilitado
			for(unsigned int x=0; x<m_solapa3_lista_dispositivos.size(); x++)
			{
				Configuracion_Dispositivo *actual = m_solapa3_lista_dispositivos[x];
				if(actual->cambio_estado_conexion())
				{
					//Se guarda todo en caso que sea desconectado
					//Esto se hace de forma inmediata para no perder informacion, si justo despues de habilitarlo
					//es desconectado, el controlador midi eliminaria el dispositivo y no se podrian guardar los cambios
					//NO se guarda por cada cambio porque seria muy lento.
					this->guardar_configuracion_dispositivos();
				}
			}
		}
	}
	else if(m_solapa->solapa_activa() == 3)
	{
		if(m_solapa4_casilla_pantalla_completa->cambio_estado())
		{
			if(m_solapa4_casilla_pantalla_completa->activado())
				m_accion = Accion::EntrarPantallaCompleta;
			else
				m_accion = Accion::SalirPantallaCompleta;
		}
	}
}

void VentanaConfiguracion::evento_teclado(Tecla tecla, bool estado)
{
	if(tecla == Tecla::Escape && !estado)
	{
		//Se guardan los cambios en los dispositivos
		this->guardar_configuracion_dispositivos();
		m_accion = Accion::CambiarATitulo;
	}
	//Modo desarrollo activado desde teclado
	if(m_solapa1_casilla_desarrollo->activado() != Pantalla::ModoDesarrollo)
		m_solapa1_casilla_desarrollo->estado(Pantalla::ModoDesarrollo);
	if(m_solapa1_casilla_modo_alambre->activado() != Pantalla::ModoAlambre)
		m_solapa1_casilla_modo_alambre->estado(Pantalla::ModoAlambre);
	//Actualiza cuando se activa desde el teclado
	if(m_solapa4_casilla_pantalla_completa->activado() != Pantalla::PantallaCompleta)
		m_solapa4_casilla_pantalla_completa->estado(Pantalla::PantallaCompleta);

	if(m_selector_archivos != nullptr)//Cancela los demas eventos
		return;

	if(m_solapa->solapa_activa() == 1)
	{
		if(tecla == Tecla::FlechaAbajo && !estado)
			m_solapa2_tabla->cambiar_seleccion(1);
		else if(tecla == Tecla::FlechaArriba && !estado)
			m_solapa2_tabla->cambiar_seleccion(-1);

		if(m_solapa2_tabla->seleccion())
			m_solapa2_eliminar->habilitado(true);
	}
}

void VentanaConfiguracion::evento_pantalla(float ancho, float alto)
{
	m_texto_titulo.dimension(ancho, 40);
	m_boton_atras->posicion(m_boton_atras->x(), alto - 36);
	if(m_selector_archivos != nullptr)
		m_selector_archivos->posicion(ancho/2 - 600/2, alto/2 - 450/2);

	m_solapa->dimension(250, alto);

	m_solapa1_titulo->dimension(Pantalla::Ancho-270, 20);
	m_solapa1_restablecer->posicion(Pantalla::Ancho - 160, 100);
	m_solapa1_texto_restablecer->dimension(Pantalla::Ancho-430, 30);
	m_solapa1_texto_borrar->dimension(Pantalla::Ancho-430, 30);
	m_solapa1_texto_limpiar->dimension(Pantalla::Ancho-430, 30);
	m_solapa1_limpiar_bd->posicion(Pantalla::Ancho - 160, 140);
	m_solapa1_borrar_db->posicion(Pantalla::Ancho - 160, 180);
	m_solapa1_casilla_midi2->dimension(Pantalla::Ancho-270, 30);
	m_solapa1_desarrollo->dimension(Pantalla::Ancho-270, 20);
	m_solapa1_casilla_desarrollo->dimension(Pantalla::Ancho-270, 30);
	m_solapa1_casilla_modo_alambre->dimension(Pantalla::Ancho-270, 30);

	m_solapa2_grabacion->dimension(Pantalla::Ancho-270, 20);
	m_solapa2_texto_grabacion->dimension(Pantalla::Ancho-530, 30);
	m_solapa2_cambiar_carpeta->posicion(Pantalla::Ancho - 260, 100);
	m_solapa2_texto_version_midi->dimension(Pantalla::Ancho-530, 30);
	m_solapa2_opcion_version_midi->posicion(Pantalla::Ancho - 260, 140);
	m_solapa2_reproduccion->dimension(Pantalla::Ancho-270, 20);
	m_solapa2_tabla->dimension(Pantalla::Ancho-270, Pantalla::Alto-330);
	m_solapa2_agregar->posicion(Pantalla::Ancho-320, Pantalla::Alto-80);
	m_solapa2_eliminar->posicion(Pantalla::Ancho-160, Pantalla::Alto-80);

	m_solapa3_titulo->dimension(Pantalla::Ancho-270, 20);
	m_solapa3_panel->dimension(Pantalla::Ancho-270, Pantalla::Alto-150);
	for(Configuracion_Dispositivo *d : m_solapa3_lista_dispositivos)
		d->dimension(Pantalla::Ancho-270, 0);

	m_solapa4_titulo->dimension(Pantalla::Ancho-270, 20);
	m_solapa4_casilla_pantalla_completa->dimension(Pantalla::Ancho-270, 30);
}
