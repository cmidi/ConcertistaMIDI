#include "ventana_reproduccion.h++"

#include "../libreria_midi/funciones_midi.h++"
#include "../util/texto.h++"

VentanaReproduccion::VentanaReproduccion(Configuracion *configuracion, Datos_Musica *musica, Administrador_Recursos *recursos) : VentanaOrgano(configuracion, recursos), m_texto_combos(recursos)
{
	this->fps_dinamico(false);
	this->protector_pantalla(false);
	this->consumir_eventos(false);

	m_musica = musica;
	m_evaluacion = m_musica->evaluacion();
	m_tiempo_tocado = m_musica->tiempo_tocado();

	Secuencia_Midi *secuencia = m_musica->midi()->secuencia_activa();

	m_titulo_musica = new Titulo(0, m_barra->alto()+40, Pantalla::Ancho, Pantalla::Alto - (m_organo->alto() + m_barra->alto() + 40), recursos);
	m_titulo_musica->datos(musica);

	m_puntaje = new Puntuacion(20, 100, 200, 64, m_teclado_util, recursos);

	this->pulsos_por_minuto(secuencia->inicial_ppm());
	this->compas(secuencia->inicial_compas_numerador(), secuencia->inicial_compas_denominador());

	m_texto_combos.texto("");
	m_texto_combos.tipografia(recursos->tipografia(ModoLetra::Grande));
	m_texto_combos.color(Color(1.0f, 0.5f, 0.0f));
	m_texto_combos.posicion(0, 150);
	m_texto_combos.dimension(Pantalla::Ancho, 20);
	m_texto_combos.alineacion_horizontal(Alineacion_H::Centrado);
	m_texto_combos.alineacion_vertical(Alineacion_V::Centrado);

	m_pistas_midi = m_musica->midi()->secuencia_activa()->pistas();
	m_pistas = m_musica->pistas();
	m_barras_compas = m_musica->midi()->secuencia_activa()->barras_compas();

	m_cambio_tiempo_compas = false;
	m_cambio_tiempo_barra = false;

	//Se cambia la posicion para que el primer compas quede al inicio
	float desplazamiento = m_titulo_musica->posicion_y_dibujo() - m_tablero->y();
	m_tiempo_actual_midi = -(static_cast<std::int64_t>((m_tablero->alto() - desplazamiento) * m_duracion_nota) + 4000000);
	m_maximo_tiempo_actualizar = std::numeric_limits<std::int64_t>::max();

	m_musica->midi()->secuencia_activa()->cambiar_a(m_tiempo_actual_midi);

	m_barra->tiempo_total(secuencia->duracion_en_microsegundos());
	m_barra->indicadores_compas(m_musica->midi()->secuencia_activa()->barras_compas());
	m_barra->marcadores(m_musica->midi()->secuencia_activa()->marcadores());
	m_barra->tiempo(m_tiempo_actual_midi);

	m_tablero->tiempo_tocado(m_tiempo_tocado);
	m_tablero->pistas(m_pistas);
	m_tablero->pistas_midi(m_pistas_midi);
	m_tablero->lineas(m_musica->midi()->secuencia_activa()->barras_compas());
	m_tablero->marcadores(m_musica->midi()->secuencia_activa()->marcadores());
	m_tablero->mosrar_reproduccion_previa(m_configuracion->mosrar_reproduccion_previa());
	m_tablero->tiempo(m_tiempo_actual_midi);

	m_organo->cambiar_armadura(secuencia->inicial_armadura(), secuencia->inicial_escala());

	m_partitura->pista(m_musica->midi()->secuencia_activa()->pistas()->at(0));
	m_partitura->tempos(m_musica->midi()->secuencia_activa()->tempo());
	m_partitura->compas(m_musica->midi()->secuencia_activa()->compas());
	m_partitura->armadura(m_musica->midi()->secuencia_activa()->armadura());
	m_partitura->barras_compas(m_barras_compas);
	m_partitura->tiempo_total(secuencia->duracion_en_microsegundos());
	m_partitura->tiempo(m_tiempo_actual_midi);

	//Se inician todas las pistas en 0
	for(unsigned short i=0; i<m_pistas_midi->size(); i++)
		m_primera_nota.push_back(0);

	m_evaluacion->notas_totales = 0;
	m_evaluacion->notas_tocadas = 0;
	m_evaluacion->errores = 0;

	//Cuenta el numero de notas totales que seran jugadas
	m_solo_reproduciendo = true;
	unsigned int notas_jugables = 0;
	for(unsigned short i=0; i<m_pistas_midi->size(); i++)
	{
		if(m_pistas->at(i).modo() != Modo::Fondo)
		{
			m_solo_reproduciendo = false;
			//La pista puede haber sido cargada en una reproducción anterior
			TiempoTocado::iterator pista_buscada = m_tiempo_tocado->find(i);

			bool extisten_datos = false;
			if(pista_buscada != m_tiempo_tocado->end() && pista_buscada->second.size() > 0)
				extisten_datos = true;

			std::vector<Nota_Midi*> &notas = (*m_pistas_midi)[i]->notas();
			for(unsigned int j=0; j<notas.size(); j++)
			{
				//Cuenta solo las notas que se pueden tocar con el teclado_util actual
				if(notas[j]->id_nota() >= m_teclado_util.tecla_inicial() &&
					notas[j]->id_nota() <= m_teclado_util.tecla_final() &&
					notas[j]->fin->microsegundos() > notas[j]->inicio->microsegundos()
				)
					notas_jugables ++;

				//Crea la evaluacion si no existe para todas las notas, hasta las que estan fuera de rango
				if(!extisten_datos)
				{
					Tiempos_Nota nota_nueva;
					nota_nueva.id_nota = notas[j]->id_nota();
					nota_nueva.inicio = notas[j]->inicio->microsegundos();
					nota_nueva.fin = notas[j]->fin->microsegundos();
					(*m_tiempo_tocado)[i].push_back(nota_nueva);
				}
			}
		}
	}

	if(m_solo_reproduciendo)
	{
		m_puntaje->visible(false);
		//Se muestran todas las teclas si solo esta reproduciendo
		m_teclado_util.cambiar(0, NUMERO_TECLAS_ORGANO);
	}
	m_puntaje->notas_totales(notas_jugables);
	m_reinicio_cancion = false;
}

VentanaReproduccion::~VentanaReproduccion()
{
	delete m_titulo_musica;
	delete m_puntaje;
}

void VentanaReproduccion::actualizar(unsigned int diferencia_tiempo)
{
	m_bloqueo.lock();
	//Cuando termina la cancion se retrocede a la ventana anterior o muestra resultados segun corresponda
	if(m_musica->midi()->secuencia_activa()->fin_reproduccion())
	{
		if(m_solo_reproduciendo)
			m_accion = Accion::CambiarASeleccionPista;
		else
		{
			m_accion = Accion::CambiarAResultados;
			m_evaluacion->notas_totales = m_puntaje->notas_totales();
			m_evaluacion->notas_tocadas = m_puntaje->notas_tocadas();
			m_evaluacion->errores = m_puntaje->errores();
			//Ya termino de reproducir, se finalizan las notas que el jugador aun no suelta
			this->cerrar_notas_evaluacion();
		}
	}

	//Cambia el compas desde el teclado
	int cambio_compas = this->cambiar_compas();
	if(cambio_compas > 0)
	{
		//Avanza un compás
		if(m_barras_compas->size() > 0)
		{
			unsigned int compas_nuevo = 0;
			for(unsigned int x=0; x<m_barras_compas->size(); x++)
			{
				if((*m_barras_compas)[x] > m_tiempo_actual_midi && x > 0)
				{
					compas_nuevo = x;
					break;
				}
			}
			this->cerrar_notas_evaluacion();
			this->reiniciar(false);//Solo apaga las notas manteniendo los controles
			m_tiempo_actual_midi = (*m_barras_compas)[compas_nuevo];
			m_musica->midi()->secuencia_activa()->cambiar_a(m_tiempo_actual_midi);
			m_cambio_tiempo_compas = true;
		}
	}
	else if(cambio_compas < 0)
	{
		//Retrocede un compás
		if(m_tiempo_actual_midi > 0 && m_barras_compas->size() > 0)
		{
			unsigned int compas_actual = 0;
			for(unsigned int x=0; x<m_barras_compas->size(); x++)
			{
				if((*m_barras_compas)[x] >= m_tiempo_actual_midi && x > 0)
				{
					compas_actual = x-1;
					break;
				}
			}

			std::int64_t tiempo_inicio = (*m_barras_compas)[compas_actual];
			std::int64_t tiempo_final;
			if(compas_actual+1 < m_barras_compas->size())
				tiempo_final = (*m_barras_compas)[compas_actual+1];
			else
				tiempo_final = m_musica->midi()->duracion_en_microsegundos();

			double duracion_compas = static_cast<double>(tiempo_final - tiempo_inicio);
			double progreso_en_compas = static_cast<double>(m_tiempo_actual_midi - tiempo_inicio);

			//Cierra las notas antes de cambiar al tiempo
			this->cerrar_notas_evaluacion();
			if(compas_actual > 0 && progreso_en_compas / duracion_compas <= 0.50)
				m_tiempo_actual_midi = (*m_barras_compas)[compas_actual-1];
			else
				m_tiempo_actual_midi = (*m_barras_compas)[compas_actual];

			this->reiniciar(true);//Apaga las notas borrando los controles
			m_controlador_midi->restablecer();//Restablece todos los eventos
			m_musica->midi()->secuencia_activa()->cambiar_a(m_tiempo_actual_midi);
			m_cambio_tiempo_compas = true;
		}
	}

	//Si selecciono un nuevo tiempo en la barra de progreso, se cambia la posicion.
	m_cambio_tiempo_barra = m_barra->cambiado_tiempo();
	if(m_barra->cambiado_tiempo())
	{
		this->cerrar_notas_evaluacion();
		this->reiniciar(false);//Solo apaga las notas manteniendo los controles
		m_tiempo_actual_midi = m_barra->tiempo();
	}

	int direccion_cambio = m_barra->direccion_cambio();
	if(!m_barra->cambiado_tiempo() && direccion_cambio != 0)
	{
		if(direccion_cambio < 0)
			m_controlador_midi->restablecer();//Restablece todos los eventos
		m_musica->midi()->secuencia_activa()->cambiar_a(m_tiempo_actual_midi);
	}

	//Actualiza el rango util si hay cambios en los dispositivos
	if(!m_solo_reproduciendo && this->hay_cambio_dispositivo_midi())
		this->recalcular_puntaje();

	//Actualiza la etiqueta de combos
	m_puntaje->actualizar(diferencia_tiempo);
	if(m_puntaje->combo() > COMBO_MINIMO_MOSTRAR)
		m_texto_combos.texto(T_("¡Combo {0}!", m_puntaje->combo()));

	m_titulo_musica->actualizar(diferencia_tiempo);

	//Se crean los subtitulos
	if(m_mostrar_subtitulo)
	{
		std::string texto = Texto::quitar_espacios_en_extremos(m_subtitulo_texto);
		if(m_subtitulos->texto() != texto)
			m_subtitulos->texto(texto);
	}

	m_barra->tiempo(m_tiempo_actual_midi);
	m_tablero->tiempo(m_tiempo_actual_midi);
	m_partitura->tiempo(m_tiempo_actual_midi);
	this->actualizar_comun(diferencia_tiempo);

	this->calcular_teclas_activas(diferencia_tiempo);
	m_bloqueo.unlock();
}

void VentanaReproduccion::actualizar_midi(unsigned int diferencia_tiempo)
{
	m_bloqueo.lock();
	//Se calculan los microsegundos entre fotogramas para actualizar el midi
	unsigned int ms_actualizar = 0;

	if(!m_pausa && !m_cambio_tiempo_compas && !m_cambio_tiempo_barra)
		ms_actualizar = static_cast<unsigned int>((static_cast<double>(diferencia_tiempo) / 1000.0) * m_velocidad_musica);
	m_cambio_tiempo_compas = false;

	if(!m_cambio_tiempo_barra)
	{
		if(!this->hay_notas_requeridas())
			this->reproducir_eventos(ms_actualizar);
		else
			this->desbloquear_notas(false);
	}

	this->escuchar_eventos();
	this->actualizar_midi_comun(diferencia_tiempo);
	m_bloqueo.unlock();
}

void VentanaReproduccion::dibujar()
{
	m_bloqueo.lock();
	this->dibujar_comun();
	if(m_puntaje->combo() > COMBO_MINIMO_MOSTRAR)
		m_texto_combos.dibujar();
	m_puntaje->dibujar();
	m_titulo_musica->dibujar();
	m_bloqueo.unlock();
}

void VentanaReproduccion::recalcular_puntaje()
{
	//Cuenta el numero de notas totales que seran jugadas
	unsigned int notas_jugables = 0;
	std::map<unsigned short, std::vector<Tiempos_Nota>>::iterator pista = m_tiempo_tocado->begin();
	while(pista != m_tiempo_tocado->end())
	{
		if(m_pistas->at(pista->first).modo() != Modo::Fondo)
		{
			for(unsigned int t=0; t<pista->second.size(); t++)
			{
				//Cuenta solo las notas que se pueden tocar con el teclado_util actual
				if(pista->second[t].id_nota >= m_teclado_util.tecla_inicial() &&
					pista->second[t].id_nota <= m_teclado_util.tecla_final() &&
					pista->second[t].fin > pista->second[t].inicio)
				{
					notas_jugables ++;
				}
				else if(pista->second[t].tocada && m_notas_requeridas.size() > 0)
				{
					//Desbloquea la nota que quedo fuera de rango y la descuenta
					pista->second[t].tocada = false;
				}
			}
		}
		pista++;
	}
	//Borra las notas requeridas fura de rango
	for(unsigned char x=0; x<m_notas_requeridas.size(); x++)
	{
		if(m_notas_requeridas[x].first < m_teclado_util.tecla_inicial() ||
			m_notas_requeridas[x].first > m_teclado_util.tecla_final())
		{
			//Apagando luces
			m_controlador_midi->tecla_luninosa(m_notas_requeridas[x].first, false);
			m_teclas[m_notas_requeridas[x].first].es_requerida = false;

			std::iter_swap(m_notas_requeridas.begin() + x, m_notas_requeridas.end()-1);
			m_notas_requeridas.pop_back();

			x--;
		}
	}

	m_puntaje->cambiar_rango(m_teclado_util);
	m_puntaje->notas_totales(notas_jugables);
}

void VentanaReproduccion::reproducir_eventos(unsigned int diferencia_tiempo)
{
	if(m_pausa)
		return;

	//Solo avanza hasta el comienzo de la proxima nota a tocar si esta en modo aprender alguna pista
	if(diferencia_tiempo > m_maximo_tiempo_actualizar)
	{
		diferencia_tiempo = static_cast<unsigned int>(m_maximo_tiempo_actualizar);
		m_maximo_tiempo_actualizar = std::numeric_limits<std::int64_t>::max();//Se quita el limite porque ya se utilizó
	}

	m_musica->midi()->actualizar(diferencia_tiempo);
	m_tiempo_actual_midi = m_musica->midi()->secuencia_activa()->posicion_en_microsegundos();

	//Se escriben las notas
	bool notas_requeridas_nuevas = false;
	unsigned short pista = 0;
	Evento_Midi* evento = m_musica->midi()->siguiente_evento(pista);
	while(evento != nullptr)
	{
		//Cambios en compas y tempo
		if(evento->tipo_mensaje() == TipoMensaje_DatosFlexibles)
		{
			if(evento->tipo_dato_flexible() == DatosFlexibles_Compas)
				this->compas(evento->compas_numerador(), evento->compas_denominador());
			else if(evento->tipo_dato_flexible() == DatosFlexibles_Tempo)
				this->pulsos_por_minuto(Funciones_Midi::us_a_ppm(static_cast<unsigned int>(evento->tempo_en_microsegundos())));
			else if(evento->tipo_dato_flexible() == DatosFlexibles_Armadura)
				m_organo->cambiar_armadura(evento->armadura(), evento->escala());
			else if(evento->contiene_texto())
				this->calcular_subtitulos(evento);

			//Hasta aqui llegan los metaeventos, no se envian a los dispositivos MIDI.
			evento = m_musica->midi()->siguiente_evento(pista);
			continue;
		}

		if(	m_pistas->at(pista).modo() != Modo::Fondo && evento->es_nota() &&
			(evento->id_nota() >= m_teclado_util.tecla_inicial() &&
			evento->id_nota() <= m_teclado_util.tecla_final()))
		{
			if(evento->tipo_voz_de_canal() == EventoMidi_NotaEncendida)
			{
				//Agrega la nota actual a notas requeridas excepto si esta fuera de rango
				if(m_pistas->at(pista).modo() == Modo::Aprender)
				{
					this->agregar_nota_requerida(evento->id_nota(), pista);
					notas_requeridas_nuevas = true;
				}
				else if(m_pistas->at(pista).modo() == Modo::Tocar)
				{
					//Activa la nota luminosa
					m_controlador_midi->tecla_luninosa(evento->id_nota(), true);
				}
			}
			else if(evento->tipo_voz_de_canal() == EventoMidi_NotaApagada)
			{
				//Si nota activa y llega una EventoMidi_NotaApagada apagar y cambiar a plomo
				unsigned int nota_tocada = this->encontrar_nota_tocada(pista, evento->id_nota());
				if(nota_tocada < std::numeric_limits<unsigned int>::max())
				{
					//Verifica si se paso de largo en una nota tocada correctamente
					if(!this->hay_nota_nueva(evento->id_nota()))
					{
						Tecla_Organo &tecla = m_teclas[evento->id_nota()];
						tecla.color_tecla = Pista::Colores_pista[0];
						tecla.correcta = false;//Deja de ser correcta
					}
					this->desbloquear_nota(pista, nota_tocada);
				}
				else
					//No toco la nota por lo que pierde el combo
					m_puntaje->reiniciar_combo();
				if(m_pistas->at(pista).modo() == Modo::Tocar)
					m_controlador_midi->tecla_luninosa(evento->id_nota(), false);
			}
		}
		else if(evento->es_nota())
		{
			//Envia las notas que no toca el jugador
			if(m_pistas->at(pista).sonido())
			{
				//Prende las luces si solo esta reproduciendo en todas las pistas
				if(m_solo_reproduciendo && m_pistas->at(pista).visible())
				{
					if(evento->tipo_voz_de_canal() == EventoMidi_NotaEncendida)
						m_controlador_midi->tecla_luninosa(evento->id_nota(), true);
					else if(evento->tipo_voz_de_canal() == EventoMidi_NotaApagada)
						m_controlador_midi->tecla_luninosa(evento->id_nota(), false);
				}

				//Se copia el evento para poder editarlo y escalar el volumen
				Evento_Midi evento_salida = *evento;
				if(evento_salida.tipo_mensaje() == TipoMensaje_VozDeCanalMidi1)
					evento_salida.velocidad_nota_7(static_cast<unsigned char>(evento_salida.velocidad_nota_7() * m_volumen));
				else
					evento_salida.velocidad_nota_16(static_cast<unsigned short>(evento_salida.velocidad_nota_16() * m_volumen));

				m_controlador_midi->escribir(evento_salida);
			}
		}
		else
		{
			//Envia cualquier otro evento independientemente si la pista esta silenciada o no,
			//porque los controladores pueden afectar a multiples pistas que usen el mismo canal
			m_controlador_midi->escribir(*evento);
		}

		//Obtiene el siguiente evento
		evento = m_musica->midi()->siguiente_evento(pista);
	}

	//Borra las notas requeridas si todas son tocadas a la vez
	if(notas_requeridas_nuevas)
		this->borrar_notas_requeridas();
}

void VentanaReproduccion::escuchar_eventos()
{
	//Lee todos los eventos
	bool nuevas_notas_tocadas = false;
	while(m_controlador_midi->hay_eventos())
	{
		Evento_Midi evento = m_controlador_midi->leer();

		//Omitir eventos que no son EventoMidi_NotaEncendida o EventoMidi_NotaApagada
		if(!evento.es_nota())
			continue;

		//Cancela la pausa al recibir un evento
		m_pausa = false;

		Tecla_Organo &tecla = m_teclas[evento.id_nota()];
		if(evento.tipo_voz_de_canal() == EventoMidi_NotaEncendida)
		{
			//Si la nota esta activa no puede activarse de nuevo,
			//solo suma uno al contador de clic
			if(!tecla.activa)
			{
				//Eventos EventoMidi_NotaEncendida
				Nota_Midi *nota_encontrada = nullptr;
				unsigned short pista_encontrada = 0;
				unsigned int posicion_encontrada = 0;
				for(unsigned short pista = 0; pista < m_pistas_midi->size(); pista++)
				{
					//Se salta las pistas que no toca el jugador
					if(m_pistas->at(pista).modo() == Modo::Fondo)
						continue;

					bool detener_proximo_ciclo = false;
					std::int64_t menor_tiempo = std::numeric_limits<std::int64_t>::max();

					std::vector<Nota_Midi*> &notas = (*m_pistas_midi)[pista]->notas();
					for(unsigned int n=m_primera_nota[pista]; n<notas.size() && !detener_proximo_ciclo; n++)
					{
						Nota_Midi *nota_actual = notas[n];

						//Se salta las notas que no corresponden con el evento
						if(nota_actual->id_nota() == evento.id_nota())
						{
							std::int64_t tiempo_inicio = nota_actual->inicio->microsegundos() - TIEMPO_DETECCION_INICIAL;
							std::int64_t tiempo_final = nota_actual->inicio->microsegundos() + TIEMPO_DETECCION_FINAL;

							//El tiempo de deteccion no debe superar el largo de la nota
							if(tiempo_final > nota_actual->fin->microsegundos())
								tiempo_final = nota_actual->fin->microsegundos();

							//Termina con esta pista si la nota aun no llega
							if(tiempo_inicio > m_tiempo_actual_midi)
								break;

							//El tiempo para esta nota se ha excedido, por lo que se salta
							if(tiempo_final < m_tiempo_actual_midi)
								continue;

							//Nota dentro del rango para tocar y que no se haya tocado anteriormente
							TiempoTocado::iterator pista_buscada = m_tiempo_tocado->find(pista);
							if(pista_buscada != m_tiempo_tocado->end())
							{
								if(!pista_buscada->second[n].tocada)
								{
									//Se queda con el tiempo de la primera nota que aun no se ha tocado
									if(menor_tiempo == std::numeric_limits<std::int64_t>::max())
										menor_tiempo = nota_actual->inicio->microsegundos();
									//Verifica que sea la primera que debe tocarse, para que no se salte notas,
									//con una pequeña tolerancia que haga mas facil tocar muchas notas cortas simultaneas
									else if(menor_tiempo < nota_actual->inicio->microsegundos() && menor_tiempo+TIEMPO_TOLERANCIA < nota_actual->inicio->microsegundos())
										break;

									//Primera coincidencia detectada
									if(nota_encontrada == nullptr)
									{
										nota_encontrada = notas[n];
										pista_encontrada = pista;//Guarda la pista
										posicion_encontrada = n;//Guarda la posicion
									}
									else
									{
										std::int64_t distancia_actual = abs(m_tiempo_actual_midi - nota_encontrada->inicio->microsegundos());
										std::int64_t distancia_anterior = abs(m_tiempo_actual_midi - nota_encontrada->inicio->microsegundos());

										//Se encuentra una nota mas cercana al evento
										if(distancia_actual < distancia_anterior)
										{
											nota_encontrada = notas[n];
											pista_encontrada = pista;//Guarda la pista
											posicion_encontrada = n;//Guarda la posicion
										}
									}
									break;//Termina la pista actual porque ya encontro la mas cercana
								}
							}
						}
					}
				}

				//Se guarda la nota tocada por el jugador
				if(nota_encontrada != nullptr)
				{
					//Nota correcta
					tecla.activa = true;
					tecla.activa_automatica = false;
					tecla.grupo = nota_encontrada->grupo();
					tecla.canal = nota_encontrada->canal();
					tecla.pista = pista_encontrada;
					tecla.posicion = posicion_encontrada;
					tecla.sonido = m_pistas->at(pista_encontrada).sonido();
					tecla.valida = true;
					tecla.correcta = true;
					tecla.mostrar_particula = true;
					tecla.contador_clic = 1;
					tecla.color_tecla = m_pistas->at(pista_encontrada).color();

					//Guarda el tiempo en el que se toco la nota para evaluar
					//Si nota_encontrada no es nulo entonces esta dentro del rango m_tiempo_tocado
					Tiempos_Nota &nota_evaluada = (*m_tiempo_tocado)[pista_encontrada][posicion_encontrada];
					nota_evaluada.inicio_tocado = m_tiempo_actual_midi;
					nota_evaluada.fin_tocado = std::numeric_limits<std::int64_t>::min();
					this->bloquear_nota(pista_encontrada, posicion_encontrada);

					//Aumenta el contador de combos
					//El modo aprender cuenta el puntaje cuando todas las notas son tocadas correctamente
					//en el metodo borrar_notas_requeridas()
					if(m_pistas->at(pista_encontrada).modo() != Modo::Aprender)
					{
						m_puntaje->sumar_combo(1);//Suma 1
						m_puntaje->nota_correcta(nota_encontrada->id_nota(), m_tiempo_actual_midi);
					}

					nuevas_notas_tocadas = true;

					//Se envia el evento
					if(m_pistas->at(pista_encontrada).sonido())
					{
						//Se cambia el canal y escala el volumen
						if(evento.version_midi() == VersionMidi::Version_2)
							evento.grupo(nota_encontrada->grupo());

						evento.canal(nota_encontrada->canal());

						if(evento.tipo_mensaje() == TipoMensaje_VozDeCanalMidi1)
							evento.velocidad_nota_7(static_cast<unsigned char>(evento.velocidad_nota_7() * m_volumen));
						else
							evento.velocidad_nota_16(static_cast<unsigned short>(evento.velocidad_nota_16() * m_volumen));

						m_controlador_midi->escribir(evento);
					}
				}
				else
				{
					//Notas plomas son notas erroneas
					tecla.activa = true;
					tecla.activa_automatica = false;
					tecla.grupo = evento.grupo();
					tecla.canal = evento.canal();
					tecla.pista = 0;
					tecla.posicion = 0;
					tecla.sonido = true;
					tecla.valida = false;
					tecla.correcta = false;
					tecla.mostrar_particula = false;
					tecla.contador_clic = 1;
					tecla.color_tecla = Pista::Colores_pista[0];

					//Escala el volumen
					if(evento.tipo_mensaje() == TipoMensaje_VozDeCanalMidi1)
						evento.velocidad_nota_7(static_cast<unsigned char>(evento.velocidad_nota_7() * m_volumen));
					else
						evento.velocidad_nota_16(static_cast<unsigned short>(evento.velocidad_nota_16() * m_volumen));

					//Se envia el evento
					m_controlador_midi->escribir(evento);

					//Pierde el combo
					m_puntaje->reiniciar_combo();
					if(m_tiempo_actual_midi >= 0)
						m_puntaje->sumar_error();//Solo cuenta el error cuando comienza la canción
				}
			}
			else
				tecla.contador_clic++;
		}
		else
		{
			//Eventos EventoMidi_NotaApagada
			//Si la tecla no esta activa, no hay nada que hacer aqui
			if(tecla.activa && tecla.contador_clic == 1)
			{
				//Guarda el tiempo en el que se solto la nota para evaluar solo en modo tocar o aprender
				if(tecla.valida && m_pistas->at(tecla.pista).modo() != Modo::Fondo)
				{
					TiempoTocado::iterator pista_buscada = m_tiempo_tocado->find(tecla.pista);
					if(pista_buscada != m_tiempo_tocado->end() && pista_buscada->second.size() > tecla.posicion)
					{
						Tiempos_Nota &nota_evaluada = pista_buscada->second[tecla.posicion];
						nota_evaluada.fin_tocado = m_tiempo_actual_midi;

						//Desbloquea la nota, cuando se pasa de largo
						if(nota_evaluada.tocada && nota_evaluada.fin_tocado >= nota_evaluada.fin)
							this->desbloquear_nota(tecla.pista, tecla.posicion);
					}
				}

				//Se envia el evento de apagado
				if(tecla.sonido)
				{
					//Se selecciona el grupo y el canal
					if(evento.version_midi() == VersionMidi::Version_2)
						evento.grupo(tecla.grupo);

					evento.canal(tecla.canal);

					m_controlador_midi->escribir(evento);
				}

				tecla.desactivar_nota(false);
			}
			else
				tecla.contador_clic--;
		}
	}
	//Borra las notas requeridas si todas son tocadas a la vez
	if(nuevas_notas_tocadas)
		this->borrar_notas_requeridas();
}

void VentanaReproduccion::calcular_subtitulos(Evento_Midi *evento)
{
	//Solo llegan eventos MIDI de datos flexibles
	if(	evento->tipo_dato_flexible() == DatosFlexibles_Letra ||
		evento->tipo_dato_flexible() == DatosFlexibles_TextoLetraDesconocido ||
		evento->tipo_dato_flexible() == DatosFlexibles_Ruby ||
		(evento->version_midi() == VersionMidi::Version_1 && evento->tipo_metaevento_midi1() == MetaEventoMidi_Texto))
	{
		std::string nuevo_texto = evento->texto();
		std::replace(nuevo_texto.begin(), nuevo_texto.end(), '_', ' ');

		//Retorno de carro para la proxima linea generalmente en MetaEventoMidi_Letra
		if(nuevo_texto.length() > 0 && (nuevo_texto[0] == '\r' || nuevo_texto[0] == '\n'))
			m_retorno_carro = true;
		else if(nuevo_texto.length() > 0 && (nuevo_texto[0] == '\\' || nuevo_texto[0] == '/'))
		{
			//Midi karaoke generalmente en MetaEventoMidi_Texto
			std::string recortado = nuevo_texto.substr(1);//Quita el primer caracter
			m_subtitulo_texto = recortado;
		}
		else
		{
			if(m_subtitulo_texto.length() >= 80 || m_retorno_carro)
			{
				m_subtitulo_texto = nuevo_texto;
				m_retorno_carro = false;
			}
			else
				m_subtitulo_texto += nuevo_texto;
		}
	}
	//Limpia la cadena si contiene caracteres invisibles
	if(Texto::esta_vacio(m_subtitulo_texto) && m_subtitulo_texto.length() > 0)
		m_subtitulo_texto = "";
}

void VentanaReproduccion::calcular_teclas_activas(unsigned int diferencia_tiempo)
{
	float posicion_y = 0;
	float largo_nota = 0;
	unsigned char id_nota = 0;//Id de la nota desde 0 hasta 127
	std::int64_t maximo_tiempo_actualizar = 0;
	for(unsigned short pista=0; pista<m_pistas_midi->size(); pista++)
	{
		std::vector<Nota_Midi*> &notas = (*m_pistas_midi)[pista]->notas();
		//Dibuja solo las pistas que tienen notas, hay pistas vacias
		if(notas.size() > 0 && m_pistas->at(pista).visible())
		{
			for(unsigned int n=m_primera_nota[pista]; n<notas.size(); n++)
			{
				//Numero_nota incluye blancas y negras
				id_nota = notas[n]->id_nota();

				//Se salta las notas fuera de la pantalla
				if(id_nota < m_teclado_visible.tecla_inicial() || id_nota > m_teclado_visible.tecla_final())
					continue;

				//Calcula el tiempo maximo que puede desplazarce al actualizar para evitar saltarse notas muy cortas
				//al detener la cancion en el modo Aprender
				if(m_pistas->at(pista).modo() == Modo::Aprender)
				{
					//Menor a 0 indica una nota aun no tocada
					if(notas[n]->inicio->microsegundos() > m_tiempo_actual_midi)
					{
						maximo_tiempo_actualizar = notas[n]->inicio->microsegundos() - m_tiempo_actual_midi;
						if(maximo_tiempo_actualizar < m_maximo_tiempo_actualizar)//Se queda con la proxima nota mas cercana
							m_maximo_tiempo_actualizar = maximo_tiempo_actualizar;
					}
				}

				//Solo se actualiza el color de las pistas de Fondo, a menos que esten
				//fuera del rango del teclado util
				if(m_pistas->at(pista).modo() != Modo::Fondo &&
					id_nota >= m_teclado_util.tecla_inicial() &&
					id_nota <= m_teclado_util.tecla_final())
					continue;

				posicion_y = static_cast<float>(m_tiempo_actual_midi - notas[n]->inicio->microsegundos()) / static_cast<float>(m_duracion_nota);

				//Si la nota no esta sonando termina el recorrido por la pista actual
				if(posicion_y < -5)//-5 igual al tiempo de espera
					break;

				largo_nota = static_cast<float>(notas[n]->fin->microsegundos() - notas[n]->inicio->microsegundos()) / static_cast<float>(m_duracion_nota);

				//El alto minimo de la nota es de 20 pixeles
				if((posicion_y-largo_nota > 0 && largo_nota >= 20) || (posicion_y > 20 && largo_nota < 20))//La nota n salio de la pantalla
				{
					//Almacena la posicion de la primera nota visible desde la posicion actual para no tener que recorrer todo de nuevo
					if(n == m_primera_nota[pista])
						m_primera_nota[pista] = n+1;
					//No se dibujan las notas que ya salieron de la pantalla
					continue;
				}

				Tecla_Organo &tecla = m_teclas[id_nota];
				//Cambia el tiempo de espera de las notas
				if(posicion_y >= -5 && posicion_y < 0 && tecla.tiempo_espera <= 0)
					tecla.tiempo_espera = (-posicion_y)-1;
				else if(posicion_y >= 0 && tecla.tiempo_espera <= 0)
				{
					//Almacena el color si cumplio el tiempo de espera
					if(!tecla.activa)
					{
						tecla.activa_automatica = true;
						tecla.color_tecla = m_pistas->at(pista).color();

						if(m_solo_reproduciendo)
							tecla.mostrar_particula = true;
					}
				}
			}
		}
	}

	float tiempo = static_cast<float>(diferencia_tiempo)/1000000000.0f*(1.0f/0.0166f);
	for(unsigned char i=0; i<NUMERO_TECLAS_ORGANO; i++)
	{
		Tecla_Organo &tecla = m_teclas[i];
		if(tecla.tiempo_espera > 0)
			tecla.tiempo_espera -= tiempo;
	}
}

void VentanaReproduccion::reiniciar(bool restablecer_controles)
{
	//Borra los subtitulos
	m_subtitulo_texto.clear();
	m_subtitulos->texto("");

	//Reinicia el tablero
	m_tablero->reiniciar();

	//Reinicia el contador de combos
	m_puntaje->reiniciar_combo();
	m_puntaje->cambiar_a(m_tiempo_actual_midi);

	//Desactiva las notas requeridas
	for(unsigned char x=0; x<NUMERO_TECLAS_ORGANO; x++)
		m_teclas[x].desactivar_nota(true);

	//Elimina todas las notas requeridas
	m_notas_requeridas.clear();

	//Desbloquea las notas que puedan haber quedado como tocadas
	this->desbloquear_notas(true);

	//Reinicia la primera nota de cada pista a 0
	for(unsigned short i=0; i<m_primera_nota.size(); i++)
		m_primera_nota[i] = 0;

	//Limpia los eventos incompletos
	if(restablecer_controles)
		m_controlador_midi->restablecer();
	else
		m_controlador_midi->desactivar_notas();

	if(m_tiempo_actual_midi <= 0)
	{
		Secuencia_Midi *secuencia = m_musica->midi()->secuencia_activa();
		this->pulsos_por_minuto(secuencia->inicial_ppm());
		this->compas(secuencia->inicial_compas_numerador(), secuencia->inicial_compas_denominador());
	}
}

bool VentanaReproduccion::hay_nota_nueva(unsigned char id_nota)
{
	//Retorna verdadero si hay una nota activa en la posición id_nota
	for(unsigned short pista = 0; pista < m_pistas->size(); pista++)
	{
		if(m_pistas->at(pista).modo() == Modo::Fondo)
			continue;

		//Si no extiste la evaluacion se salta la pista
		TiempoTocado::iterator pista_buscada = m_tiempo_tocado->find(pista);
		if(pista_buscada == m_tiempo_tocado->end())
			continue;

		std::vector<Tiempos_Nota> &pista_actual = pista_buscada->second;

		for(unsigned int n=m_primera_nota[pista]; n<pista_actual.size(); n++)
		{
			if(	pista_actual[n].tocada && pista_actual[n].id_nota == id_nota &&
				pista_actual[n].fin_tocado == std::numeric_limits<std::int64_t>::min() &&
				pista_actual[n].fin > m_tiempo_actual_midi)
				return true;

			//Detiene el ciclo porque las notas aun no llegan
			if(pista_actual[n].inicio - TIEMPO_DETECCION_INICIAL > m_tiempo_actual_midi)
				break;
		}
	}
	return false;
}

unsigned int VentanaReproduccion::encontrar_nota_tocada(unsigned short pista, unsigned char id_nota)
{
	//Retorna la posicion de las nota que estan bloqueadas y pasaron de largo
	//Si no la encuentra retorna std::numeric_limits<unsigned int>::max()
	//Las pista modo Fondo no se pueden tocar
	if(m_pistas->at(pista).modo() == Modo::Fondo)
		return std::numeric_limits<unsigned int>::max();

	//No existe la evaluacion
	TiempoTocado::iterator pista_buscada = m_tiempo_tocado->find(pista);
	if(pista_buscada == m_tiempo_tocado->end())
		return std::numeric_limits<unsigned int>::max();

	std::vector<Tiempos_Nota> &pista_actual = pista_buscada->second;
	for(unsigned int n=m_primera_nota[pista]; n<pista_actual.size(); n++)
	{
		if(pista_actual[n].tocada && pista_actual[n].id_nota == id_nota && pista_actual[n].fin <= m_tiempo_actual_midi)
			return n;

		//Detiene el ciclo porque las notas aun no llegan
		if(pista_actual[n].inicio > m_tiempo_actual_midi)
			break;
	}

	return std::numeric_limits<unsigned int>::max();//No encontrado
}

void VentanaReproduccion::bloquear_nota(unsigned short pista, unsigned int numero_nota)
{
	TiempoTocado::iterator pista_buscada = m_tiempo_tocado->find(pista);
	if(pista_buscada != m_tiempo_tocado->end())
	{
		if(pista_buscada->second.size() > numero_nota && !pista_buscada->second[numero_nota].tocada)
			pista_buscada->second[numero_nota].tocada = true;
	}
}

void VentanaReproduccion::desbloquear_nota(unsigned short pista, unsigned int numero_nota)
{
	TiempoTocado::iterator pista_buscada = m_tiempo_tocado->find(pista);
	if(pista_buscada != m_tiempo_tocado->end())
	{
		if(pista_buscada->second.size() > numero_nota && pista_buscada->second[numero_nota].tocada)
			pista_buscada->second[numero_nota].tocada = false;
	}
}

void VentanaReproduccion::desbloquear_notas(bool desbloquear_todas)
{
	//2 modos, desbloquear todo y desbloquear solo algunas (para el modo aprender)
	//Para el modo de "NO desbloquear todo" es para desbloquear las notas
	//que se bloquearon por tocarla antes de tiempo y son requeridas para continuar
	//en el modo aprender, si la nota esta siendo tocada no se desbloquea.

	TiempoTocado::iterator pista_actual = m_tiempo_tocado->begin();
	while(pista_actual != m_tiempo_tocado->end())
	{
		//Se salta las pistas que no toca el jugador
		if(m_pistas->at(pista_actual->first).modo() != Modo::Fondo)
		{
			std::vector<Tiempos_Nota> &actual = pista_actual->second;
			unsigned int inicio = 0;

			//Se comienza solo desde la posicion actual
			if(!desbloquear_todas)
				inicio = m_primera_nota[pista_actual->first];

			for(unsigned int n = inicio; n < actual.size(); n++)
			{
				if(actual[n].tocada)
				{
					if(desbloquear_todas)
						actual[n].tocada = false;
					else
					{
						//Se desbloquea solo si es requerida, std::numeric_limits<std::int64_t>::min() indica que esta siendo tocada
						if(m_teclas[actual[n].id_nota].es_requerida &&
							actual[n].fin_tocado > std::numeric_limits<std::int64_t>::min())
						{
							//Primero se asegura que sea de la pista correcta
							//puede ocurrir una superposicion de notas
							bool pista_correcta = false;
							for(unsigned int x=0; x<m_notas_requeridas.size() && !pista_correcta; x++)
							{
								if(m_notas_requeridas[x].first == actual[n].id_nota && m_notas_requeridas[x].second == pista_actual->first)
									pista_correcta = true;
							}

							if(pista_correcta)
								actual[n].tocada = false;
						}
					}
				}
			}
		}
		pista_actual++;
	}
}

void VentanaReproduccion::agregar_nota_requerida(unsigned char id_nota, unsigned short pista)
{
	Tecla_Organo &tecla = m_teclas[id_nota];

	tecla.es_requerida = true;
	tecla.color_punto = m_pistas->at(pista).color();

	m_notas_requeridas.push_back(std::pair<unsigned char, unsigned short>(id_nota, pista));
	m_controlador_midi->tecla_luninosa(id_nota, true);
}

void VentanaReproduccion::borrar_notas_requeridas()
{
	//Borra las notas requerida solo si todas estan activas al mismo tiempo
	if(m_notas_requeridas.size() > 0)
	{
		bool aun_falta = false;
		for(unsigned char x=0; x<m_notas_requeridas.size(); x++)
		{
			//Si falta alguna nota o no se toco a tiempo, entonces no se borra nada
			Tecla_Organo &tecla = m_teclas[m_notas_requeridas[x].first];
			if(!tecla.correcta || (tecla.correcta && tecla.pista != m_notas_requeridas[x].second))
				aun_falta = true;//La nota se toco antes de tiempo (ploma) o no fue tocada
		}

		if(!aun_falta)
		{
			for(unsigned char x=0; x<m_notas_requeridas.size(); x++)
			{
				//Apagando luces
				m_controlador_midi->tecla_luninosa(m_notas_requeridas[x].first, false);

				//Quita las notas requerida del organo
				m_teclas[m_notas_requeridas[x].first].es_requerida = false;

				m_puntaje->nota_correcta(m_notas_requeridas[x].first, m_tiempo_actual_midi);
			}
			//Todas son tocadas correctamente por lo que se suman al combo
			m_puntaje->sumar_combo(static_cast<unsigned int>(m_notas_requeridas.size()));
			m_notas_requeridas.clear();
		}
	}
}

bool VentanaReproduccion::hay_notas_requeridas()
{
	if(m_notas_requeridas.size() > 0)
		return true;
	return false;
}

void VentanaReproduccion::evento_raton(Raton *raton)
{
	this->evento_raton_comun(raton);
	m_titulo_musica->evento_raton(raton);
}

void VentanaReproduccion::cerrar_notas_evaluacion()
{
	if(!m_solo_reproduciendo)
	{
		std::map<unsigned short, std::vector<Tiempos_Nota>>::iterator pista = m_tiempo_tocado->begin();
		while(pista != m_tiempo_tocado->end())
		{
			std::vector<Tiempos_Nota> &notas = pista->second;
			for(unsigned int n=0; n<notas.size(); n++)
			{
				if (notas[n].fin_tocado == std::numeric_limits<std::int64_t>::min())
				{
					notas[n].tocada = false;
					notas[n].fin_tocado = m_tiempo_actual_midi;
				}
			}
			pista++;
		}
	}
}

void VentanaReproduccion::evento_teclado(Tecla tecla, bool estado)
{
	m_bloqueo.lock();
	if(tecla == Tecla::Escape && !estado)
		m_accion = Accion::CambiarASeleccionPista;
	else if(tecla == Tecla::F5 && !estado)
	{
		bool actual = m_tablero->mosrar_reproduccion_previa();
		actual = !actual;
		m_tablero->mosrar_reproduccion_previa(actual);
		m_configuracion->mosrar_reproduccion_previa(actual);
	}
	else if(tecla == Tecla::V && estado)
	{
		//Va al inicio de la canción
		if(m_tiempo_actual_midi >= 0)
		{
			std::int64_t tiempo_inicio = -(static_cast<std::int64_t>(m_tablero->alto() * m_duracion_nota));
			m_musica->midi()->secuencia_activa()->cambiar_a(tiempo_inicio);
			m_tiempo_actual_midi = m_musica->midi()->secuencia_activa()->posicion_en_microsegundos();
			this->cerrar_notas_evaluacion();
			this->reiniciar(true);
		}
	}
	else
		this->evento_teclado_comun(tecla, estado);
	m_bloqueo.unlock();
}

void VentanaReproduccion::evento_pantalla(float ancho, float alto)
{
	m_bloqueo.lock();
	bool actualizar_tiempo = false;
	float desplazamiento = m_titulo_musica->posicion_y_dibujo() - m_tablero->y();
	float proporcion = 0;
	if(m_musica->midi()->secuencia_activa()->posicion_en_microsegundos() <= 0 && !m_reinicio_cancion)
	{
		float tiempo_actual = -static_cast<float>(m_musica->midi()->secuencia_activa()->posicion_en_microsegundos());
		proporcion = (tiempo_actual / m_duracion_nota) / (m_tablero->alto()-desplazamiento);
		actualizar_tiempo = true;
	}

	this->evento_pantalla_comun(ancho, alto);
	m_titulo_musica->dimension(Pantalla::Ancho, Pantalla::Alto - (m_organo->alto() + m_barra->alto() + 40));
	m_texto_combos.dimension(Pantalla::Ancho, 20);

	//evento_pantalla_comun actualiza el tamaño de m_tablero
	if(actualizar_tiempo)
	{
		//Se calcula de nuevo el desplazamiento
		desplazamiento = m_titulo_musica->posicion_y_dibujo() - m_tablero->y();
		std::int64_t tiempo_inicio = -static_cast<std::int64_t>((m_tablero->alto()-desplazamiento) * proporcion * m_duracion_nota);
		m_musica->midi()->secuencia_activa()->cambiar_a(tiempo_inicio);
	}
	if(m_reinicio_cancion)
	{
		m_reinicio_cancion = false;
		std::int64_t tiempo_inicio = -static_cast<std::int64_t>(m_tablero->alto() * m_duracion_nota);
		if(m_tiempo_actual_midi < tiempo_inicio)
			m_musica->midi()->secuencia_activa()->cambiar_a(tiempo_inicio);
	}
	m_bloqueo.unlock();
}
