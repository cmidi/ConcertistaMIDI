#ifndef VENTANASELECCIONMUSICA_H
#define VENTANASELECCIONMUSICA_H

#include <filesystem>
#include <vector>
#include <unicode/translit.h>
#include <thread>
#include <mutex>

#include "ventana.h++"
#include "../elementos_graficos/boton.h++"
#include "../elementos_graficos/etiqueta.h++"
#include "../elementos_graficos/tabla.h++"
#include "../elementos_graficos/notificacion.h++"
#include "../elementos_graficos/ruta_exploracion.h++"
#include "../elementos_graficos/barra_carga.h++"
#include "../elementos_graficos/reproductor.h++"
#include "../control/configuracion.h++"
#include "../control/base_de_datos.h++"
#include "../control/datos_musica.h++"
#include "../control/datos_archivos.h++"

class VentanaSeleccionMusica : public Ventana
{
private:
	//Recursos
	Rectangulo *m_rectangulo;

	//Hilo para la carga de archivos
	std::thread *m_hilo_carga;
	bool m_hilo_activo;
	std::mutex m_bloqueo;
	bool m_datos_listos;
	bool m_saltar_actualizacion;
	unsigned int m_seleccion_actual;
	bool m_nueva_carpeta_cargada;

	//Datos
	Configuracion *m_configuracion;
	Datos_Musica *m_musica;
	Base_de_Datos *m_datos;

	std::vector<Datos_Archivos> m_lista_archivos;
	std::string m_carpeta_inicial;
	std::string m_carpeta_activa;
	bool m_es_carpeta_inicial;
	bool m_es_carpeta_nueva;

	//Componentes
	Boton *m_boton_atras;
	Boton *m_boton_continuar;
	Etiqueta *m_texto_titulo;
	Barra_Carga *m_barra_carga;
	Tabla *m_tabla_archivos;
	Ruta_Exploracion *m_ruta_exploracion;
	Reproductor *m_reproductor;

	void iniciar_hilo_cargar_datos(const std::string &nueva_ruta);
	void detener_hilo_cargar_datos();
	static void hilo_carga_datos(VentanaSeleccionMusica *ventana, const std::string &nueva_ruta);

	void cargar_lista_carpetas();
	void cargar_contenido_carpeta();

	void cargar_datos(const std::string &nueva_ruta);
	void crear_tabla();
	void guardar_carpeta_actual();
	bool abrir_archivo_seleccionado();
	void cargar_reproductor();

public:
	VentanaSeleccionMusica(Configuracion *configuracion, Datos_Musica *musica, Administrador_Recursos *recursos);
	~VentanaSeleccionMusica();

	void actualizar(unsigned int diferencia_tiempo) override;
	void actualizar_midi(unsigned int diferencia_tiempo) override;
	void dibujar() override;

	void evento_raton(Raton *raton) override;
	void evento_teclado(Tecla tecla, bool estado) override;
	void evento_pantalla(float ancho, float alto) override;
};
#endif
