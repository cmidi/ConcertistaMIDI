#include "ventana_seleccion_musica.h++"

VentanaSeleccionMusica::VentanaSeleccionMusica(Configuracion *configuracion, Datos_Musica *musica, Administrador_Recursos *recursos) : Ventana()
{
	this->fps_dinamico(true);
	this->protector_pantalla(true);
	this->consumir_eventos(true);

	m_configuracion = configuracion;
	m_musica = musica;
	m_datos = configuracion->base_de_datos();

	m_rectangulo = recursos->figura(FiguraGeometrica::Rectangulo);

	m_texto_titulo = new Etiqueta(recursos);
	m_texto_titulo->texto(T("Seleccione una canción para tocar"));
	m_texto_titulo->tipografia(recursos->tipografia(ModoLetra::Grande));
	m_texto_titulo->color(Color(1.0f, 1.0f, 1.0f));
	m_texto_titulo->posicion(0, 0);
	m_texto_titulo->dimension(Pantalla::Ancho, 40);
	m_texto_titulo->alineacion_horizontal(Alineacion_H::Centrado);
	m_texto_titulo->alineacion_vertical(Alineacion_V::Centrado);

	m_ruta_exploracion = new Ruta_Exploracion(10, 50, Pantalla::Ancho-20, 32, recursos);
	m_ruta_exploracion->carpeta_extra(T("Raiz"), "-");

	//Fila titulo
	m_tabla_archivos = new Tabla(10, 92, Pantalla::Ancho-20, Pantalla::Alto-142, 40, recursos);
	m_tabla_archivos->agregar_columna(TipoCelda::Imagen, "", false, 1);
	m_tabla_archivos->agregar_columna(TipoCelda::Etiqueta, ("Nombre Archivo"), false, 10);
	m_tabla_archivos->agregar_columna(TipoCelda::Etiqueta, T("Duracion"), true, 2);
	m_tabla_archivos->agregar_columna(TipoCelda::Etiqueta, T("Visitas"), true, 2);
	m_tabla_archivos->agregar_columna(TipoCelda::Etiqueta, T("Tamaño"), true, 2);
	m_tabla_archivos->agregar_columna(TipoCelda::Etiqueta, T("Fecha"), true, 4);
	m_tabla_archivos->texto_tabla_vacia(T("La carpeta está vacía"));

	m_barra_carga = new Barra_Carga(160.0f, Pantalla::Alto - 30.0f, Pantalla::Ancho - 320.0f, 20.0f, recursos);
	m_barra_carga->texto(T("Revisando archivo:"));

	m_reproductor = new Reproductor(160.0f, Pantalla::Alto - 35.0f, Pantalla::Ancho - 320.0f, 30.0f, recursos);
	m_reproductor->controlador_midi(configuracion->controlador_midi());

	m_boton_atras = new Boton(10.0f, Pantalla::Alto - 36.0f, 120.0f, 32.0f, T("Atrás"), ModoLetra::Chica, recursos);
	m_boton_atras->color_boton(Color(0.9f, 0.9f, 0.9f));

	m_boton_continuar = new Boton(Pantalla::Ancho - 130.0f, Pantalla::Alto - 36.0f, 120.0f, 32.0f, T("Continuar"), ModoLetra::Chica, recursos);
	m_boton_continuar->color_boton(Color(0.9f, 0.9f, 0.9f));

	m_es_carpeta_inicial = false;
	m_es_carpeta_nueva = true;
	m_carpeta_inicial = m_configuracion->carpeta_inicial();
	m_carpeta_activa = m_configuracion->carpeta_activa();

	if(m_carpeta_inicial == "-")
		m_carpeta_activa = "-";
	else
	{
		//Cargar el nombre de la carpeta inicial
		std::vector<std::vector<std::string>> ruta_carpetas = m_datos->carpetas();
		for(unsigned int i=0; i<ruta_carpetas.size(); i++)
		{
			if(m_carpeta_inicial == ruta_carpetas[i][1])
				m_ruta_exploracion->carpeta_extra(ruta_carpetas[i][0], ruta_carpetas[i][1]);
		}
	}

	m_hilo_carga = nullptr;
	m_hilo_activo = false;
	m_datos_listos = false;
	m_saltar_actualizacion = false;
	m_seleccion_actual = 0;
	m_nueva_carpeta_cargada = false;

	this->iniciar_hilo_cargar_datos(m_carpeta_activa);
}

VentanaSeleccionMusica::~VentanaSeleccionMusica()
{
	this->detener_hilo_cargar_datos();

	delete m_texto_titulo;
	delete m_ruta_exploracion;
	delete m_tabla_archivos;
	delete m_barra_carga;
	delete m_reproductor;

	delete m_boton_atras;
	delete m_boton_continuar;
}

void VentanaSeleccionMusica::actualizar(unsigned int diferencia_tiempo)
{
	m_bloqueo.lock();
	if(m_saltar_actualizacion)
	{
		m_saltar_actualizacion = false;
		m_bloqueo.unlock();
		return;
	}

	if(m_hilo_carga != nullptr && m_datos_listos)
	{
		this->detener_hilo_cargar_datos();
		this->crear_tabla();
		m_saltar_actualizacion = true;
	}

	m_barra_carga->actualizar(diferencia_tiempo);
	m_reproductor->actualizar(diferencia_tiempo);
	m_ruta_exploracion->actualizar(diferencia_tiempo);
	m_tabla_archivos->actualizar(diferencia_tiempo);
	m_boton_atras->actualizar(diferencia_tiempo);
	m_boton_continuar->actualizar(diferencia_tiempo);
	m_bloqueo.unlock();
}

void VentanaSeleccionMusica::actualizar_midi(unsigned int diferencia_tiempo)
{
	m_reproductor->actualizar_midi(diferencia_tiempo);
}

void VentanaSeleccionMusica::dibujar()
{
	m_bloqueo.lock();
	m_rectangulo->textura(false);
	m_rectangulo->dibujar(0, 0, Pantalla::Ancho, 40, Color(0.141f, 0.624f, 0.933f));
	m_rectangulo->dibujar(0, Pantalla::Alto - 40, Pantalla::Ancho, 40, Color(0.761f, 0.887f, 0.985f));
	m_texto_titulo->dibujar();

	m_ruta_exploracion->dibujar();
	m_tabla_archivos->dibujar();
	m_barra_carga->dibujar();

	if(!m_barra_carga->visible() && (m_reproductor->archivo_abierto() || m_reproductor->hay_error()))
		m_reproductor->dibujar();

	m_boton_atras->dibujar();
	m_boton_continuar->dibujar();
	m_bloqueo.unlock();
}

void VentanaSeleccionMusica::iniciar_hilo_cargar_datos(const std::string &nueva_ruta)
{
	m_reproductor->cerrar_archivo();
	//No permite iniciar un nuevo hilo hasta que termine el actual
	if(m_hilo_activo)
		this->detener_hilo_cargar_datos();

	m_datos_listos = false;
	m_barra_carga->valor_actual(0);
	m_barra_carga->valor_maximo(Funciones::numero_de_archivos(nueva_ruta));

	if(nueva_ruta != "" && std::ifstream(nueva_ruta))
	{
		//Almacena la carpeta raiz para retroceder despues
		if(m_es_carpeta_inicial)
		{
			m_carpeta_inicial = nueva_ruta;
			m_configuracion->carpeta_inicial(nueva_ruta);
			m_es_carpeta_inicial = false;

			for(unsigned int m = 0; m<m_lista_archivos.size(); m++)
			{
				if(nueva_ruta == m_lista_archivos[m].ruta)
					m_ruta_exploracion->carpeta_extra(m_lista_archivos[m].nombre, nueva_ruta);
			}
		}
	}

	m_hilo_activo = true;
	m_hilo_carga = new std::thread(this->hilo_carga_datos, this, nueva_ruta);
}

void VentanaSeleccionMusica::detener_hilo_cargar_datos()
{
	if(m_hilo_activo)
	{
		m_hilo_activo = false;
		m_hilo_carga->join();
		delete m_hilo_carga;
		m_hilo_carga = nullptr;
	}
}

void VentanaSeleccionMusica::hilo_carga_datos(VentanaSeleccionMusica *ventana, const std::string &nueva_ruta)
{
	ventana->cargar_datos(nueva_ruta);
}

void VentanaSeleccionMusica::cargar_lista_carpetas()
{
	UErrorCode estado = U_ZERO_ERROR;
	//NFD: descompone la cadena
	//[:Nonspacing Mark:] Remove: Elimina acentos
	//NFC: recompone nuevamente la cadena
	icu::Transliterator *transformador = icu::Transliterator::createInstance("NFD; [:Nonspacing Mark:] Remove; NFC", UTRANS_FORWARD, estado);

	std::vector<std::vector<std::string>> ruta_carpetas = m_datos->carpetas();
	m_barra_carga->valor_maximo(static_cast<unsigned int>(ruta_carpetas.size()));
	for(unsigned int i=0; i<ruta_carpetas.size(); i++)
	{
		Datos_Archivos actual;

		actual.nombre = ruta_carpetas[i][0];
		actual.nombre_unicode = ruta_carpetas[i][0].c_str();
		transformador->transliterate(actual.nombre_unicode);
		actual.ruta = ruta_carpetas[i][1];
		actual.tamanno = Funciones::numero_de_archivos(actual.ruta);
		actual.es_carpeta = true;

		m_bloqueo.lock();
		m_lista_archivos.push_back(actual);

		//Permite finalizar la carga de archivos antes de tiempo
		if(!m_hilo_activo)
		{
			m_datos->finalizar_transaccion();
			m_bloqueo.unlock();
			return;
		}

		m_barra_carga->valor_actual(i+1);
		m_bloqueo.unlock();
	}
}

void VentanaSeleccionMusica::cargar_contenido_carpeta()
{
	m_datos->iniciar_transaccion();
	if(!std::ifstream(m_carpeta_activa))
	{
		Registro::Error(T_("Permiso denegado, no se puede acceder a \"{0}\"", m_carpeta_activa));
		return;
	}

	UErrorCode estado = U_ZERO_ERROR;
	icu::Transliterator *transformador = icu::Transliterator::createInstance("NFD; [:Nonspacing Mark:] Remove; NFC", UTRANS_FORWARD, estado);

	m_es_carpeta_nueva = true;
	unsigned int contador = 0;
	for(const std::filesystem::directory_entry &elemento : std::filesystem::directory_iterator(m_carpeta_activa))
	{
		try
		{
			std::string ruta = std::string(elemento.path());
			std::string nombre_archivo = Funciones::nombre_archivo(ruta, elemento.is_directory());
			std::string extencion_archivo = Funciones::extencion_archivo(ruta);

			bool oculto = false;
			if(nombre_archivo.length() > 0 && nombre_archivo[0] == '.')
				oculto = true;

			if((elemento.is_directory() && !oculto) || (!elemento.is_directory() && Funciones::es_midi(extencion_archivo)))
			{
				Datos_Archivos actual;
				actual.ruta = elemento.path();
				actual.es_carpeta = elemento.is_directory();
				actual.fecha_acceso = "-";

				if(!elemento.is_directory())
				{
					actual.nombre = nombre_archivo;
					actual.nombre_unicode = nombre_archivo.c_str();
					transformador->transliterate(actual.nombre_unicode);
					std::vector<std::string> datos_midi = m_datos->datos_archivo(actual.ruta);
					actual.tamanno = elemento.file_size();
					if(datos_midi.size() > 0)
					{
						actual.visitas = static_cast<unsigned int>(std::stoi(datos_midi[0]));
						actual.duracion = std::stol(datos_midi[1]);
						if(datos_midi[2] != "")
							actual.fecha_acceso = datos_midi[2];
						m_es_carpeta_nueva = false;
					}
					else
					{
						//Archivo nuevo
						actual.duracion = Midi::duracion_midi(actual.ruta);//Tiempo en microsegundos
						actual.es_nuevo = true;
						m_datos->agregar_archivo(actual.ruta, actual.duracion);
					}
				}
				else
				{
					actual.nombre = nombre_archivo;
					actual.nombre_unicode = nombre_archivo.c_str();
					transformador->transliterate(actual.nombre_unicode);
					actual.tamanno = Funciones::numero_de_archivos(actual.ruta);//Numero de archivos
				}

				m_bloqueo.lock();
				m_lista_archivos.push_back(actual);

				//Permite finalizar la carga de archivos antes de tiempo
				if(!m_hilo_activo)
				{
					m_datos->finalizar_transaccion();
					m_bloqueo.unlock();
					return;
				}

				contador++;
				m_barra_carga->valor_actual(contador);
				m_bloqueo.unlock();
			}
		}
		catch(std::filesystem::filesystem_error &e)
		{
			Registro::Error(T_("No se puede leer en: {0}", std::string(elemento.path())));
			Registro::Error(std::string(e.what()));
		}
	}
	m_datos->finalizar_transaccion();
	delete transformador;
}

void VentanaSeleccionMusica::cargar_datos(const std::string &nueva_ruta)
{
	if(nueva_ruta == "-")
	{
		//Limpia la lista de archivos
		m_bloqueo.lock();
		m_lista_archivos.clear();

		m_es_carpeta_inicial = true;
		m_carpeta_inicial = "-";
		m_carpeta_activa = "-";
		m_configuracion->carpeta_inicial(m_carpeta_inicial);
		m_configuracion->carpeta_activa(m_carpeta_activa);
		m_bloqueo.unlock();

		this->cargar_lista_carpetas();
	}
	else if(nueva_ruta != "" && std::ifstream(nueva_ruta))
	{
		Registro::Depurar("Abriendo la carpeta: " + nueva_ruta);
		m_bloqueo.lock();
		m_carpeta_activa = nueva_ruta;
		m_configuracion->carpeta_activa(m_carpeta_activa);

		//Limpia la lista de archivos
		m_lista_archivos.clear();
		m_bloqueo.unlock();

		this->cargar_contenido_carpeta();
	}
	else
		Notificacion::Aviso(T("No se puede acceder a la carpeta"), 5);

	m_bloqueo.lock();
	m_datos_listos = true;
	m_nueva_carpeta_cargada = true;
	m_bloqueo.unlock();
}

void VentanaSeleccionMusica::crear_tabla()
{
	m_ruta_exploracion->ruta(m_carpeta_inicial, m_carpeta_activa);

	if(m_lista_archivos.size() == 0)
	{
		if(m_es_carpeta_inicial)
		{
			Notificacion::Aviso(T("No hay carpetas donde buscar archivos MIDI"), 5);
			Notificacion::Nota(T("Agrega carpetas en la configuración"), 15);
		}
		m_ruta_exploracion->siguiente_habilitado(false);
	}
	else
		m_ruta_exploracion->siguiente_habilitado(true);

	m_tabla_archivos->vaciar();//Se vacia la tabla

	//Ordenar Lista
	std::sort(m_lista_archivos.begin(), m_lista_archivos.end());

	//Crear Tabla
	for(unsigned int i=0; i<m_lista_archivos.size(); i++)
	{
		std::vector<CeldaContenido> fila_nueva;

		if(m_lista_archivos[i].es_carpeta)
			fila_nueva.push_back(CeldaContenido{.imagen = Textura::Carpeta});
		else
		{
			std::string extencion_archivo = Funciones::extencion_archivo(m_lista_archivos[i].ruta);
			TipoArchivo tipo_archivo = Funciones::tipo_archivo(extencion_archivo);
			if(tipo_archivo == TipoArchivo::MIDI_1)
				fila_nueva.push_back(CeldaContenido{.imagen = Textura::ArchivoMIDI_1});
			else if(tipo_archivo == TipoArchivo::MIDI_2)
				fila_nueva.push_back(CeldaContenido{.imagen = Textura::ArchivoMIDI_2});
			else if(tipo_archivo == TipoArchivo::RMID)
				fila_nueva.push_back(CeldaContenido{.imagen = Textura::ArchivoRMI});
			else if(tipo_archivo == TipoArchivo::KAR)
				fila_nueva.push_back(CeldaContenido{.imagen = Textura::ArchivoKAR});
			else if(tipo_archivo == TipoArchivo::STY)
				fila_nueva.push_back(CeldaContenido{.imagen = Textura::ArchivoSTY});
		}
		fila_nueva.push_back(CeldaContenido{.texto = m_lista_archivos[i].nombre});

		if(m_lista_archivos[i].es_carpeta)
		{
			fila_nueva.push_back(CeldaContenido{.texto = "-"});//Las carpetas no tienen duracion
			fila_nueva.push_back(CeldaContenido{.texto = "-"});//No se cuentan las visitas a las carpetas

			if(m_lista_archivos[i].tamanno == 0)
				fila_nueva.push_back(CeldaContenido{.texto = T("Sin Archivos")});
			else if(m_lista_archivos[i].tamanno == 1)
				fila_nueva.push_back(CeldaContenido{.texto = T_("{0} archivo", std::to_string(m_lista_archivos[i].tamanno))});//Singular
			else
				fila_nueva.push_back(CeldaContenido{.texto = T_("{0} archivos", std::to_string(m_lista_archivos[i].tamanno))});//Plural
		}
		else
		{
			fila_nueva.push_back(CeldaContenido{.texto = Funciones::microsegundo_a_texto(m_lista_archivos[i].duracion, false, false)});
			fila_nueva.push_back(CeldaContenido{.texto = std::to_string(m_lista_archivos[i].visitas)});
			fila_nueva.push_back(CeldaContenido{.texto = Texto::bytes_a_texto(m_lista_archivos[i].tamanno)});
		}
		fila_nueva.push_back(CeldaContenido{.texto = m_lista_archivos[i].fecha_acceso});

		if(m_lista_archivos[i].es_nuevo && !m_es_carpeta_nueva)
			m_tabla_archivos->insertar_fila(fila_nueva, Color(0.804f, 0.883f, 0.993f));
		else
			m_tabla_archivos->insertar_fila(fila_nueva);
	}
	//Recupera la fila seleccionada anteriormente si fue guardada
	std::vector<std::string> seleccionado_anterior = m_datos->leer_ultima_seleccion(m_carpeta_activa);
	if(seleccionado_anterior.size() == 2)
	{
		unsigned int seleccion_guardada = static_cast<unsigned int>(std::stoi(seleccionado_anterior[0]));
		if(seleccion_guardada < m_lista_archivos.size() && m_lista_archivos[seleccion_guardada].ruta == seleccionado_anterior[1])
		{
			//El archivo seleccionado no cambio de posicion en la tabla
			m_tabla_archivos->seleccionar(seleccion_guardada);
		}
		else
		{
			//El archivo seleccionado cambio de posicion en la tabla por lo que se busca donde quedo
			//si aun existe
			bool posicion_encontrada = false;
			unsigned int x=0;
			while(x<m_lista_archivos.size() && !posicion_encontrada)
			{
				if(m_lista_archivos[x].ruta == seleccionado_anterior[1])
				{
					//Archivo encontrado
					m_tabla_archivos->seleccionar(x);
					posicion_encontrada = true;
				}
				x++;
			}
		}
		this->cargar_reproductor();
	}
}

void VentanaSeleccionMusica::guardar_carpeta_actual()
{
	unsigned int seleccion_actual = m_tabla_archivos->obtener_seleccion();
	if(seleccion_actual < m_lista_archivos.size())
	{
		//Guarda el archivo seleccionado en esta carpeta
		m_datos->guardar_ultima_seleccion(m_carpeta_activa, seleccion_actual, m_lista_archivos[seleccion_actual].ruta);
	}
}

bool VentanaSeleccionMusica::abrir_archivo_seleccionado()
{
	m_bloqueo.lock();
	m_reproductor->reproducir(false);
	unsigned int seleccion_actual = m_tabla_archivos->obtener_seleccion();
	if(seleccion_actual < m_lista_archivos.size())
	{
		//Guarda el archivo seleccionado en esta carpeta
		m_datos->guardar_ultima_seleccion(m_carpeta_activa, seleccion_actual, m_lista_archivos[seleccion_actual].ruta);
		if(m_lista_archivos[seleccion_actual].es_carpeta)
		{
			//Abre la carpeta seleccionada
			std::string ruta_nueva = m_lista_archivos[seleccion_actual].ruta;

			m_bloqueo.unlock();
			//Carga la lista de archivos de la carpeta seleccionada
			this->iniciar_hilo_cargar_datos(ruta_nueva);
			return false;
		}
		else
		{
			//Abre el archivo seleccionado
			Datos_Archivos &archivo_abierto = m_lista_archivos[seleccion_actual];
			Registro::Depurar("Abriendo archivo: " + archivo_abierto.ruta);
			std::string estado = m_musica->cargar_midi(archivo_abierto.ruta);
			if(estado.size() == 0)
			{
				//Se suma la visita y se actualiza la fecha
				m_datos->sumar_visita_archivo(archivo_abierto.ruta);

				//Se actualiza la duracion si el archivo cambia
				if(m_musica->midi()->duracion_en_microsegundos() != archivo_abierto.duracion)
					m_datos->actualizar_archivo(archivo_abierto.ruta, m_musica->midi()->duracion_en_microsegundos());
				m_bloqueo.unlock();
				return true;
			}
			else
			{
				Notificacion::Error(T("Error al abrir el archivo"), 5);
				Notificacion::Error(estado, 5);
			}
			m_bloqueo.unlock();
			return false;
		}
	}
	m_bloqueo.unlock();
	return false;
}

void VentanaSeleccionMusica::cargar_reproductor()
{
	unsigned int seleccion_actual = m_tabla_archivos->obtener_seleccion();
	if(m_seleccion_actual != seleccion_actual || m_nueva_carpeta_cargada)
	{
		m_seleccion_actual = seleccion_actual;
		m_nueva_carpeta_cargada = false;
		if(seleccion_actual < m_lista_archivos.size())
		{
			//Guarda el archivo seleccionado en esta carpeta
			if(!m_lista_archivos[seleccion_actual].es_carpeta)
			{
				m_reproductor->abrir_archivo(m_lista_archivos[seleccion_actual].ruta);
			}
			else
				m_reproductor->cerrar_archivo();
		}
	}
}

void VentanaSeleccionMusica::evento_raton(Raton *raton)
{
	m_bloqueo.lock();
	m_ruta_exploracion->evento_raton(raton);
	bool siguiente = m_ruta_exploracion->siguiente();
	m_bloqueo.unlock();

	if(!m_hilo_activo && siguiente)
	{
		if(this->abrir_archivo_seleccionado())
			m_accion = Accion::CambiarASeleccionPista;
	}

	m_bloqueo.lock();
	bool cambiar_carpeta = m_ruta_exploracion->cambiar_carpeta();
	m_bloqueo.unlock();

	if(cambiar_carpeta)
	{
		m_bloqueo.lock();
		this->guardar_carpeta_actual();
		std::string nueva_ruta = m_ruta_exploracion->nueva_ruta();
		m_bloqueo.unlock();

		this->iniciar_hilo_cargar_datos(nueva_ruta);
	}

	m_bloqueo.lock();
	m_tabla_archivos->evento_raton(raton);
	bool seleccion_activada = m_tabla_archivos->seleccion_activada();
	m_bloqueo.unlock();

	//Abrir el archivo con doble clic
	if(!m_hilo_activo && raton->tipo_evento() == EventoRaton::Clic && raton->activado(BotonRaton::Izquierdo) && seleccion_activada)
	{
		if(this->abrir_archivo_seleccionado())
			m_accion = Accion::CambiarASeleccionPista;
	}

	if(!m_hilo_activo && raton->tipo_evento() == EventoRaton::Clic && raton->boton_activado() == BotonRaton::Ninguno)
		this->cargar_reproductor();

	m_reproductor->evento_raton(raton);
	m_boton_atras->evento_raton(raton);
	m_boton_continuar->evento_raton(raton);

	if(m_boton_atras->esta_activado())
	{
		this->guardar_carpeta_actual();
		m_accion = Accion::CambiarATitulo;
	}
	else if(!m_hilo_activo && m_boton_continuar->esta_activado())
	{
		if(this->abrir_archivo_seleccionado())
			m_accion = Accion::CambiarASeleccionPista;
	}
}

void VentanaSeleccionMusica::evento_teclado(Tecla tecla, bool estado)
{
	if(tecla == Tecla::Escape && !estado)
	{
		this->guardar_carpeta_actual();
		m_accion = Accion::CambiarATitulo;
	}
	else if(!m_hilo_activo && (tecla == Tecla::Entrar || tecla == Tecla::FlechaDerecha) && !estado)
	{
		if(this->abrir_archivo_seleccionado())
			m_accion = Accion::CambiarASeleccionPista;
	}
	else if((tecla == Tecla::Borrar || tecla == Tecla::FlechaIzquierda) && !estado)
	{
		m_bloqueo.lock();
		this->guardar_carpeta_actual();
		m_ruta_exploracion->atras();
		std::string nueva_ruta = m_ruta_exploracion->nueva_ruta();
		m_bloqueo.unlock();

		this->iniciar_hilo_cargar_datos(nueva_ruta);

	}
	else if(!m_hilo_activo && tecla == Tecla::FlechaAbajo && !estado)
	{
		m_tabla_archivos->cambiar_seleccion(1);
		this->cargar_reproductor();
	}
	else if(!m_hilo_activo && tecla == Tecla::FlechaArriba && !estado)
	{
		m_tabla_archivos->cambiar_seleccion(-1);
		this->cargar_reproductor();
	}
}

void VentanaSeleccionMusica::evento_pantalla(float ancho, float alto)
{
	m_bloqueo.lock();
	float alto_tabla_anterior = m_tabla_archivos->alto();
	bool fila_seleccionada_visible = m_tabla_archivos->seleccion_visible();
	m_texto_titulo->dimension(ancho, 40);
	m_ruta_exploracion->dimension(ancho-20, 32);
	m_tabla_archivos->dimension(ancho-20, alto-142);

	m_barra_carga->posicion(160.0f, alto - 30.0f);
	m_barra_carga->dimension(ancho - 320.0f, 20.0f);

	m_reproductor->posicion(160.0f, alto - 35.0f);
	m_reproductor->dimension(ancho - 320.0f, 30.0f);

	//Actualiza la seleccion en caso que al achicar el tamaño de la ventana
	//vaya a quedar fuera de la tabla.
	if(alto_tabla_anterior > m_tabla_archivos->alto() && fila_seleccionada_visible)
		m_tabla_archivos->seleccionar(m_tabla_archivos->obtener_seleccion());

	m_boton_atras->posicion(m_boton_atras->x(), alto - 36);
	m_boton_continuar->posicion(ancho - 130, alto - 36);
	m_bloqueo.unlock();
}
