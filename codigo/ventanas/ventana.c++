#include "ventana.h++"

Ventana::Ventana()
{
	m_accion = Accion::Ninguna;
	m_fps_dinamico = false;
	m_protector_pantalla = false;
	m_consumir_eventos = false;
}

Ventana::~Ventana()
{
}

void Ventana::fps_dinamico(bool estado)
{
	m_fps_dinamico = estado;
}

void Ventana::protector_pantalla(bool estado)
{
	m_protector_pantalla = estado;
}

void Ventana::consumir_eventos(bool estado)
{
	m_consumir_eventos = estado;
}

Accion Ventana::obtener_accion()
{
	Accion accion = m_accion;
	m_accion = Accion::Ninguna;
	return accion;
}

bool Ventana::fps_dinamico()
{
	return m_fps_dinamico;
}

bool Ventana::protector_pantalla()
{
	return m_protector_pantalla;
}

bool Ventana::consumir_eventos()
{
	return m_consumir_eventos;
}
