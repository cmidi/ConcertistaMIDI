#include "ventana_grabacion.h++"

VentanaGrabacion::VentanaGrabacion(Configuracion *configuracion, Administrador_Recursos *recursos) : VentanaOrgano(configuracion, recursos), m_texto_instrumento(recursos)
{
	this->fps_dinamico(false);
	this->protector_pantalla(false);
	this->consumir_eventos(false);

	m_solo_reproduciendo = false;
	m_tablero->desplazamiento(Direccion::Subir);

	m_midi = new Midi1(1, 480);
	//m_midi = new Midi2();
	//m_midi->secuencia_activa()->crear_pista_vacia_midi2(0, 0, 0, 0, 0);//grupo 0, canal 0, programa 0, banco bei 0, banco bed 0
	m_midi->secuencia_activa()->crear_pista_vacia_midi1(0, 0, 0, 0);//Canal 0, programa 0, banco bei 0, banco bed 0
	m_pistas.push_back(Pista((*m_midi->secuencia_activa()->pistas())[0], Pista::Colores_pista[1], Modo::Tocar, true, true));

	m_microsegundos_acumulado = 0;
	m_tiempo_por_tics = m_midi->tiempo_por_tics();
	m_tics_acumulado = 0;
	m_tics_delta = 0.0;
	m_id_pista_actual = 0;

	m_barra->indicadores_compas(m_midi->secuencia_activa()->barras_compas());
	m_barra->marcadores(m_midi->secuencia_activa()->marcadores());

	m_tablero->pistas(&m_pistas);
	m_tablero->pistas_midi(m_midi->secuencia_activa()->pistas());
	m_tablero->lineas(m_midi->secuencia_activa()->barras_compas());
	m_tablero->marcadores(m_midi->secuencia_activa()->marcadores());

	m_texto_instrumento.texto(Nombre_Instrumento(0, 0, 0) + " (0, 0, 0)");
	m_texto_instrumento.tipografia(recursos->tipografia(ModoLetra::Mediana));
	m_texto_instrumento.color(Color(0.0f, 0.0f, 0.0f));
	m_texto_instrumento.posicion(10, 100);
	m_texto_instrumento.dimension(300, 40);

	m_programa = new Evento_Midi(TipoMensaje_VozDeCanalMidi2, EventoMidi_Programa);
	m_programa->programa(0);
	m_programa->programa_banco_izquierdo(0);
	m_programa->programa_banco_derecho(0);

	this->pulsos_por_minuto(120);
	this->compas(4, 4);

	m_pausa = true;
	m_cambio_programa = false;
}

VentanaGrabacion::~VentanaGrabacion()
{
	delete m_midi;
}

void VentanaGrabacion::insertar_nota(unsigned char id_nota)
{
	Tecla_Organo &tecla = m_teclas[id_nota];

	if(!tecla.activa)
	{
		tecla.color_tecla = m_pistas[0].color();
		tecla.activa = true;
		tecla.contador_clic = 0;
	}
	tecla.contador_clic++;
}

void VentanaGrabacion::eliminar_nota(unsigned char id_nota)
{
	Tecla_Organo &tecla = m_teclas[id_nota];
	if(tecla.activa)
	{
		if(tecla.contador_clic == 1)
			tecla.activa = false;

		if(tecla.contador_clic > 0)
			tecla.contador_clic--;
	}
}

void VentanaGrabacion::actualizar(unsigned int diferencia_tiempo)
{
	m_bloqueo.lock();
	if(m_cambio_programa)
	{
		m_texto_instrumento.texto(Nombre_Instrumento(m_programa->programa(), m_programa->programa_banco_izquierdo(), m_programa->programa_banco_derecho()) + " "
			"("+ std::to_string(static_cast<unsigned int>(m_programa->programa_banco_izquierdo())) +", "
			""+ std::to_string(static_cast<unsigned int>(m_programa->programa_banco_derecho())) + ", "
			""+ std::to_string(static_cast<unsigned int>(m_programa->programa())) +")");
	}

	m_barra->tiempo_total(m_midi->secuencia_activa()->duracion_en_microsegundos());
	m_barra->tiempo(m_microsegundos_acumulado);
	m_tablero->tiempo(m_microsegundos_acumulado);
	m_bloqueo.unlock();

	this->actualizar_comun(diferencia_tiempo);
}

void VentanaGrabacion::actualizar_midi(unsigned int diferencia_tiempo)
{
	m_bloqueo.lock();
	unsigned int microsegundos_actualizar;
	if(m_pausa)
		microsegundos_actualizar = 0;
	else
		microsegundos_actualizar = static_cast<unsigned int>((static_cast<double>(diferencia_tiempo) / 1000.0) * m_velocidad_musica);

	m_microsegundos_acumulado += microsegundos_actualizar;
	m_tics_delta_temporal += static_cast<double>(microsegundos_actualizar) / m_tiempo_por_tics;
	unsigned int tics_delta = static_cast<unsigned int>(m_tics_delta_temporal);
	m_tics_acumulado += tics_delta;
	m_tics_delta += tics_delta;//Reiniciado cuando ocurre un evento
	//Se hace esto para conservar los decimales
	m_tics_delta_temporal -= tics_delta;

	m_midi->secuencia_activa()->actualizar_tiempo_actual(m_tics_acumulado, m_microsegundos_acumulado);

	//Lee todos los eventos
	while(m_controlador_midi->hay_eventos())
	{
		Evento_Midi evento = m_controlador_midi->leer();

		//Se saltan los eventos especiales creados solo para este programa
		if((evento.version_midi() == VersionMidi::Version_1 && evento.tipo_evento_midi1() < EventoMidi_NotaApagada))
			continue;

		m_pausa = false;
		Datos_Evento *evento_datos = new Datos_Evento(0, m_id_pista_actual, m_tics_acumulado, m_microsegundos_acumulado, new Evento_Midi(evento));
		m_midi->secuencia_activa()->agregar_nuevo_evento(evento_datos);

		if(	evento.tipo_mensaje() == TipoMensaje_VozDeCanalMidi1 ||
			evento.tipo_mensaje() == TipoMensaje_VozDeCanalMidi2)
		{
			if(evento.tipo_voz_de_canal() == EventoMidi_NotaEncendida)
				this->insertar_nota(evento.id_nota());
			else if(evento.tipo_voz_de_canal() == EventoMidi_NotaApagada)
				this->eliminar_nota(evento.id_nota());
			else if(evento.tipo_voz_de_canal() == EventoMidi_Controlador)
			{
				if(evento.tipo_mensaje() == TipoMensaje_VozDeCanalMidi1)
				{
					if(evento.controlador_mensaje() == MensajeControlador_SeleccionDeBanco_BEI)
					{
						m_programa->programa_banco_izquierdo(evento.controlador_valor());
						m_cambio_programa = true;
					}
					else if(evento.controlador_mensaje() == MensajeControlador_SeleccionDeBanco_BED)
					{
						m_programa->programa_banco_derecho(evento.controlador_valor());
						m_cambio_programa = true;
					}
				}
			}
			else if(evento.tipo_voz_de_canal() == EventoMidi_Programa)
			{
				if(evento.tipo_mensaje() == TipoMensaje_VozDeCanalMidi2 && evento.programa_banco_valido() == 1)
				{
					m_programa->programa_banco_izquierdo(evento.programa_banco_izquierdo());
					m_programa->programa_banco_derecho(evento.programa_banco_derecho());
				}

				m_programa->programa(evento.programa());
				m_cambio_programa = true;
			}

			if(evento.es_nota())
			{
				if(evento.tipo_mensaje() == TipoMensaje_VozDeCanalMidi1)
					evento.velocidad_nota_7(static_cast<unsigned char>(evento.velocidad_nota_7() * m_volumen));
				else
					evento.velocidad_nota_16(static_cast<unsigned short>(evento.velocidad_nota_16() * m_volumen));
			}
		}

		m_controlador_midi->escribir(evento);
	}
	m_bloqueo.unlock();

	this->actualizar_midi_comun(diferencia_tiempo);
}

void VentanaGrabacion::dibujar()
{
	this->dibujar_comun();
	m_bloqueo.lock();
	m_texto_instrumento.dibujar();
	m_bloqueo.unlock();
}

void VentanaGrabacion::evento_raton(Raton *raton)
{
	this->evento_raton_comun(raton);
}

void VentanaGrabacion::evento_teclado(Tecla tecla, bool estado)
{
	if(tecla == Tecla::Escape && !estado)
	{
		m_accion = Accion::CambiarATitulo;
		m_bloqueo.lock();
		try
		{
			m_midi->guardar(m_configuracion->carpeta_grabacion() + "/" + Funciones::fecha_actual()+".mid");
		}
		catch(const Excepcion_Midi &e)
		{
			//TR {0} es la descripcion del error
			Registro::Error(T_("Error al guardar el archivo MIDI: {0}", e.descripcion_error()));
		}
		m_bloqueo.unlock();
	}
	else
		this->evento_teclado_comun(tecla, estado);
}

void VentanaGrabacion::evento_pantalla(float ancho, float alto)
{
	this->evento_pantalla_comun(ancho, alto);
}
