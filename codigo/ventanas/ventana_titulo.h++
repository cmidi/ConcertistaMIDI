#ifndef VENTANATITULO_H
#define VENTANATITULO_H

#include "ventana.h++"
#include "../elementos_graficos/etiqueta.h++"
#include "../elementos_graficos/boton.h++"
#include "../control/configuracion.h++"
#include "version.h++"

class VentanaTitulo : public Ventana
{
private:
	//Recursos
	Rectangulo *m_rectangulo;
	Textura2D *m_textura_fondo, *m_textura_titulo;
	Etiqueta m_texto_dispositivo_entrada, m_texto_dispositivo_salida;
	Etiqueta m_texto_version;

	//Componetes
	Boton *m_boton_tocar_cancion;
	Boton *m_boton_grabar_cancion;
	Boton *m_boton_configurar;
	Boton *m_boton_salir;

	//Control
	std::mutex m_bloqueo;
	bool m_cambio_dispositivo;

	//Datos
	Configuracion *m_configuracion;
	Color m_color_boton_normal;
	Color m_color_letra_normal;
	Color m_color_boton_seleccion;
	Color m_color_letra_seleccion;
	char m_opcion_seleccionada;
	std::string m_texto_entrada;
	std::string m_texto_salida;

public:
	VentanaTitulo(Configuracion *configuracion, Administrador_Recursos *recursos);
	~VentanaTitulo();

	void actualizar(unsigned int diferencia_tiempo) override;
	void actualizar_midi(unsigned int diferencia_tiempo) override;
	void dibujar() override;

	void evento_raton(Raton *raton) override;
	void evento_teclado(Tecla tecla, bool estado) override;
	void evento_pantalla(float ancho, float alto) override;
};
#endif
