#ifndef VENTANA_H
#define VENTANA_H

#include "../dispositivos/pantalla.h++"
#include "../dispositivos/teclas.h++"
#include "../dispositivos/raton.h++"
#include "../util/traduccion.h++"
#include "../registro.h++"
#include <mutex>

enum class Accion : unsigned char
{
	Ninguna,
	CambiarATitulo,
	CambiarAConfiguracion,
	CambiarASeleccionMusica,
	CambiarASeleccionPista,
	CambiarAReproduccion,
	CambiarAGrabacion,
	CambiarAResultados,
	Salir,
	EntrarPantallaCompleta,
	SalirPantallaCompleta,
	EntrarModoAlambre,
	SalirModoAlambre,
	EntrarModoDesarrollo,
	SalirModoDesarrollo,
};

class Ventana
{
private:
	bool m_fps_dinamico;
	bool m_protector_pantalla;
	bool m_consumir_eventos;

protected:
	Accion m_accion;

	void fps_dinamico(bool estado);
	void protector_pantalla(bool estado);
	void consumir_eventos(bool estado);

public:
	Ventana();
	virtual ~Ventana();

	virtual void actualizar(unsigned int diferencia_tiempo) = 0;
	virtual void actualizar_midi(unsigned int diferencia_tiempo) = 0;
	virtual void dibujar() = 0;
	virtual void evento_raton(Raton *raton) = 0;
	virtual void evento_teclado(Tecla tecla, bool estado) = 0;
	virtual void evento_pantalla(float ancho, float alto) = 0;

	Accion obtener_accion();
	bool fps_dinamico();
	bool protector_pantalla();
	bool consumir_eventos();
};
#endif
