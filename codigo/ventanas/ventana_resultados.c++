#include "ventana_resultados.h++"
#include <cmath>

VentanaResultados::VentanaResultados(Datos_Musica *musica, Administrador_Recursos *recursos) : Ventana(), m_texto_titulo(recursos)
{
	this->fps_dinamico(true);
	this->protector_pantalla(true);
	this->consumir_eventos(true);

	m_rectangulo = recursos->figura(FiguraGeometrica::Rectangulo);

	m_texto_titulo.texto(T("Resultados"));
	m_texto_titulo.tipografia(recursos->tipografia(ModoLetra::Grande));
	m_texto_titulo.color(Color(1.0f, 1.0f, 1.0f));
	m_texto_titulo.posicion(0, 0);
	m_texto_titulo.dimension(Pantalla::Ancho, 40);
	m_texto_titulo.alineacion_horizontal(Alineacion_H::Centrado);
	m_texto_titulo.alineacion_vertical(Alineacion_V::Centrado);

	Evaluacion *evaluacion = musica->evaluacion();
	TiempoTocado *tiempo_tocado = musica->tiempo_tocado();

	double tiempo_inferior = 0;
	unsigned int muestras_inferior = 0;
	double tiempo_superiror = 0;
	unsigned int muestras_superior = 0;
	double error_al_iniciar_nota = 0;
	std::map<unsigned short, std::vector<Tiempos_Nota>>::iterator pista = tiempo_tocado->begin();
	while(pista != tiempo_tocado->end())
	{
		std::vector<Tiempos_Nota> &notas = pista->second;
		for(unsigned int n=0; n<notas.size(); n++)
		{
			//El calculo solo es con las notas tocadas
			if(notas[n].inicio_tocado == 0 && notas[n].fin_tocado == 0)
				continue;

			double tiempo_esperado = static_cast<double>(notas[n].fin - notas[n].inicio);
			if(tiempo_esperado > 0)
			{
				double tiempo_real = static_cast<double>(notas[n].fin_tocado - notas[n].inicio_tocado);
				if(tiempo_esperado >= tiempo_real)
				{
					tiempo_inferior += tiempo_real / tiempo_esperado;
					muestras_inferior++;
				}
				else
				{
					tiempo_superiror += (tiempo_real / tiempo_esperado) - 1.0;
					muestras_superior++;
				}
				double diferencia_inicio = std::abs(static_cast<double>(notas[n].inicio - notas[n].inicio_tocado));
				error_al_iniciar_nota += diferencia_inicio / tiempo_esperado;
			}
		}
		pista++;
	}
	double tiempo_que_falta = 0;
	double tiempo_que_sobra = 0;
	double proporcion_desface = 0;
	if(muestras_inferior > 0)
		tiempo_que_falta = 1 - (tiempo_inferior / muestras_inferior);
	if(tiempo_que_falta < 0)
		tiempo_que_falta = 0;
	else if(tiempo_que_falta > 1.0)
		tiempo_que_falta = 1.0;

	if(muestras_superior > 0)
		tiempo_que_sobra = (tiempo_superiror / muestras_superior);
	if(tiempo_que_sobra < 0)
		tiempo_que_sobra = 0;

	if(muestras_inferior + muestras_superior > 0)
		proporcion_desface = (error_al_iniciar_nota / (muestras_inferior + muestras_superior));
	if(proporcion_desface < 0.0)
		proporcion_desface = 0.0;
	else if(proporcion_desface > 1.0)
		proporcion_desface = 1.0;

	double porcentaje_tocado = 0;
	double porcentaje_errores = 0;
	if(evaluacion->notas_totales > 0)
	{
		porcentaje_tocado = static_cast<double>(evaluacion->notas_tocadas) / static_cast<double>(evaluacion->notas_totales);
		porcentaje_errores = static_cast<double>(evaluacion->errores) / static_cast<double>(evaluacion->notas_totales);
	}
	if(porcentaje_tocado > 1.0)
		porcentaje_tocado = 1.0;
	if(porcentaje_errores > 1.0)
		porcentaje_errores = 1.0;

	double porcentaje_previo = (porcentaje_tocado - (porcentaje_errores * 0.5));
	if(porcentaje_previo < 0)
		porcentaje_previo = 0;

	//Calculo de Porcentaje Final
	double porcentaje_final = 0;
	porcentaje_final += porcentaje_previo * 0.7;
	porcentaje_final += (1.0 - tiempo_que_falta) * porcentaje_tocado * 0.1;
	porcentaje_final += (1.0 - tiempo_que_sobra) * porcentaje_tocado * 0.1;
	porcentaje_final += (1.0 - proporcion_desface) * porcentaje_tocado * 0.1;
	porcentaje_final = porcentaje_final * 100.0;

	float color = (static_cast<float>(porcentaje_final) * 118.0f) / 100.0f;

	std::string nota;
	if(porcentaje_final < 14)
		//TR Ordenado por calidad en orden: Atroz=0% Pésimo=14% Mal=28% Bien=42% Estupendo=58% Excelente=72% Perfecto=100%
		nota = N("Atroz");
	else if(porcentaje_final >= 14 && porcentaje_final < 28)
		//TR Ordenado por calidad en orden: Atroz=0% Pésimo=14% Mal=28% Bien=42% Estupendo=58% Excelente=72% Perfecto=100%
		nota = N("Pésimo");
	else if(porcentaje_final >= 28 && porcentaje_final < 42)
		//TR Ordenado por calidad en orden: Atroz=0% Pésimo=14% Mal=28% Bien=42% Estupendo=58% Excelente=72% Perfecto=100%
		nota = N("Mal");
	else if(porcentaje_final >= 42 && porcentaje_final < 58)
		//TR Ordenado por calidad en orden: Atroz=0% Pésimo=14% Mal=28% Bien=42% Estupendo=58% Excelente=72% Perfecto=100%
		nota = N("Bien");
	else if(porcentaje_final >= 58 && porcentaje_final < 72)
		//TR Ordenado por calidad en orden: Atroz=0% Pésimo=14% Mal=28% Bien=42% Estupendo=58% Excelente=72% Perfecto=100%
		nota = N("Estupendo");
	else if(porcentaje_final >= 72 && porcentaje_final < 100)
		//TR Ordenado por calidad en orden: Atroz=0% Pésimo=14% Mal=28% Bien=42% Estupendo=58% Excelente=72% Perfecto=100%
		nota = N("Excelente");
	else if(porcentaje_final >= 100)
		//TR Ordenado por calidad en orden: Atroz=0% Pésimo=14% Mal=28% Bien=42% Estupendo=58% Excelente=72% Perfecto=100%
		nota = N("Perfecto");

	std::string porcentaje = std::to_string(porcentaje_final);
	std::string porcentaje_falta = std::to_string(tiempo_que_falta*100.0);
	std::string porcentaje_sobra = std::to_string(tiempo_que_sobra*100.0);
	std::string porcentaje_desface = std::to_string(proporcion_desface*100.0);
	porcentaje = porcentaje.substr(0, porcentaje.find("."));
	porcentaje_falta = porcentaje_falta.substr(0, porcentaje_falta.find("."));
	porcentaje_sobra = porcentaje_sobra.substr(0, porcentaje_sobra.find("."));
	porcentaje_desface = porcentaje_desface.substr(0, porcentaje_desface.find("."));

	//TR Resultados: Perfecto 100%
	m_texto_resultado_total = new Etiqueta(0, 80, Pantalla::Ancho, 35, T_("{0} {1}%", nota, porcentaje), ModoLetra::MuyGrandeNegrita, recursos);
	m_texto_resultado_total->color(Color::MSL_a_RVA(color, 0.8f, 0.4f, 1.0f));
	m_texto_resultado_total->alineacion_horizontal(Alineacion_H::Centrado);

	m_texto_notas_totales = new Etiqueta(0, 140, Pantalla::Ancho, 20, T("Total de notas"), ModoLetra::Mediana, recursos);
	m_texto_notas_totales->alineacion_horizontal(Alineacion_H::Centrado);

	m_texto_notas_totales_valor = new Etiqueta(0, 170, Pantalla::Ancho, 20, std::to_string(evaluacion->notas_tocadas) + "/" + std::to_string(evaluacion->notas_totales), ModoLetra::Mediana, recursos);
	m_texto_notas_totales_valor->alineacion_horizontal(Alineacion_H::Centrado);

	m_texto_tiempo_inferior = new Etiqueta(0, 200, Pantalla::Ancho, 20, T("Promedio de tiempo que tocas de menos"), ModoLetra::Mediana, recursos);
	m_texto_tiempo_inferior->alineacion_horizontal(Alineacion_H::Centrado);

	m_texto_tiempo_inferior_valor = new Etiqueta(0, 230, Pantalla::Ancho, 20, porcentaje_falta + "%", ModoLetra::Mediana, recursos);
	m_texto_tiempo_inferior_valor->alineacion_horizontal(Alineacion_H::Centrado);

	m_texto_tiempo_superior = new Etiqueta(0, 260, Pantalla::Ancho, 20, T("Promedio de tiempo que tocas de más"), ModoLetra::Mediana, recursos);
	m_texto_tiempo_superior->alineacion_horizontal(Alineacion_H::Centrado);

	m_texto_tiempo_superior_valor = new Etiqueta(0, 290, Pantalla::Ancho, 20, porcentaje_sobra + "%", ModoLetra::Mediana, recursos);
	m_texto_tiempo_superior_valor->alineacion_horizontal(Alineacion_H::Centrado);

	m_texto_tiempo_desface = new Etiqueta(0, 320, Pantalla::Ancho, 20, T("Desface al iniciar la nota"), ModoLetra::Mediana, recursos);
	m_texto_tiempo_desface->alineacion_horizontal(Alineacion_H::Centrado);

	m_texto_tiempo_desface_valor = new Etiqueta(0, 350, Pantalla::Ancho, 20, porcentaje_desface + "%", ModoLetra::Mediana, recursos);
	m_texto_tiempo_desface_valor->alineacion_horizontal(Alineacion_H::Centrado);

	m_texto_errores = new Etiqueta(0, 380, Pantalla::Ancho, 20, T("Numero errores"), ModoLetra::Mediana, recursos);
	m_texto_errores->alineacion_horizontal(Alineacion_H::Centrado);

	m_texto_errores_valor = new Etiqueta(0, 410, Pantalla::Ancho, 20, std::to_string(evaluacion->errores), ModoLetra::Mediana, recursos);
	m_texto_errores_valor->alineacion_horizontal(Alineacion_H::Centrado);

	m_boton_atras = new Boton(10, Pantalla::Alto - 36, 120, 32, T("Atrás"), ModoLetra::Chica, recursos);
	m_boton_atras->color_boton(Color(0.9f, 0.9f, 0.9f));

	m_boton_repetir = new Boton(Pantalla::Ancho - 130, Pantalla::Alto - 36, 120, 32, T("Repetir"), ModoLetra::Chica, recursos);
	m_boton_repetir->color_boton(Color(0.9f, 0.9f, 0.9f));
}

VentanaResultados::~VentanaResultados()
{
	delete m_texto_resultado_total;
	delete m_texto_notas_totales;
	delete m_texto_notas_totales_valor;
	delete m_texto_tiempo_inferior;
	delete m_texto_tiempo_inferior_valor;
	delete m_texto_tiempo_superior;
	delete m_texto_tiempo_superior_valor;
	delete m_texto_tiempo_desface;
	delete m_texto_tiempo_desface_valor;
	delete m_texto_errores;
	delete m_texto_errores_valor;

	delete m_boton_atras;
	delete m_boton_repetir;
}

void VentanaResultados::actualizar(unsigned int diferencia_tiempo)
{
	m_boton_atras->actualizar(diferencia_tiempo);
	m_boton_repetir->actualizar(diferencia_tiempo);
}

void VentanaResultados::actualizar_midi(unsigned int /*diferencia_tiempo*/)
{
}

void VentanaResultados::dibujar()
{
	m_rectangulo->textura(false);
	m_rectangulo->dibujar(0, 0, Pantalla::Ancho, 40, Color(0.141f, 0.624f, 0.933f));//Borde arriba
	m_rectangulo->dibujar(0, Pantalla::Alto - 40, Pantalla::Ancho, 40, Color(0.761f, 0.887f, 0.985f));//Borde abajo

	m_texto_titulo.dibujar();

	m_texto_resultado_total->dibujar();
	m_texto_notas_totales->dibujar();
	m_texto_notas_totales_valor->dibujar();
	m_texto_tiempo_inferior->dibujar();
	m_texto_tiempo_inferior_valor->dibujar();
	m_texto_tiempo_superior->dibujar();
	m_texto_tiempo_superior_valor->dibujar();
	m_texto_tiempo_desface->dibujar();
	m_texto_tiempo_desface_valor->dibujar();
	m_texto_errores->dibujar();
	m_texto_errores_valor->dibujar();

	m_boton_atras->dibujar();
	m_boton_repetir->dibujar();
}

void VentanaResultados::evento_raton(Raton *raton)
{
	m_boton_atras->evento_raton(raton);
	if(m_boton_atras->esta_activado())
		m_accion = Accion::CambiarASeleccionPista;

	m_boton_repetir->evento_raton(raton);
	if(m_boton_repetir->esta_activado())
		m_accion = Accion::CambiarAReproduccion;
}

void VentanaResultados::evento_teclado(Tecla tecla, bool estado)
{
	if(tecla == Tecla::Escape && !estado)
		m_accion = Accion::CambiarASeleccionPista;
	else if(tecla == Tecla::Entrar && !estado)
		m_accion = Accion::CambiarAReproduccion;
}

void VentanaResultados::evento_pantalla(float ancho, float alto)
{
	m_texto_titulo.dimension(ancho, 40);

	m_texto_resultado_total->dimension(ancho, 35);
	m_texto_notas_totales->dimension(ancho, 20);
	m_texto_notas_totales_valor->dimension(ancho, 20);
	m_texto_tiempo_inferior->dimension(ancho, 20);
	m_texto_tiempo_inferior_valor->dimension(ancho, 20);
	m_texto_tiempo_superior->dimension(ancho, 20);
	m_texto_tiempo_superior_valor->dimension(ancho, 20);
	m_texto_tiempo_desface->dimension(ancho, 20);
	m_texto_tiempo_desface_valor->dimension(ancho, 20);
	m_texto_errores->dimension(ancho, 20);
	m_texto_errores_valor->dimension(ancho, 20);

	m_boton_atras->posicion(m_boton_atras->x(), alto - 36);
	m_boton_repetir->posicion(ancho - 130, alto - 36);
}
