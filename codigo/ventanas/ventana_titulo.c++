#include "ventana_titulo.h++"

VentanaTitulo::VentanaTitulo(Configuracion *configuracion, Administrador_Recursos *recursos) : Ventana(), m_texto_dispositivo_entrada(recursos), m_texto_dispositivo_salida(recursos), m_texto_version(recursos)
{
	this->fps_dinamico(true);
	this->protector_pantalla(true);
	this->consumir_eventos(true);

	m_rectangulo = recursos->figura(FiguraGeometrica::Rectangulo);

	m_textura_fondo = recursos->textura(Textura::FondoTitulo);
	m_textura_titulo = recursos->textura(Textura::Titulo);

	m_color_boton_normal = Color(0.9f, 0.9f, 0.9f);
	m_color_letra_normal = Color(0.0f, 0.0f, 0.0f);

	m_color_boton_seleccion = Color(0.145f, 0.707f, 1.0f);
	m_color_letra_seleccion = Color(1.0f, 1.0f, 1.0f);

	m_opcion_seleccionada = 0;

	float y_centro = Pantalla::Centro_horizontal() - 250.0f / 2.0f;
	m_boton_tocar_cancion = new Boton(y_centro, 220, 250, 50, T("Tocar una canción"), recursos);
	m_boton_tocar_cancion->color_boton(m_color_boton_seleccion);
	m_boton_tocar_cancion->color_texto(m_color_letra_seleccion);

	m_boton_grabar_cancion = new Boton(y_centro, 280, 250, 50, T("Grabar una canción"), recursos);
	m_boton_grabar_cancion->color_boton(m_color_boton_normal);
	m_boton_grabar_cancion->color_texto(m_color_letra_normal);

	m_boton_configurar = new Boton(y_centro, 340, 250, 50, T("Configuración"), recursos);
	m_boton_configurar->color_boton(m_color_boton_normal);
	m_boton_configurar->color_texto(m_color_letra_normal);

	m_boton_salir = new Boton(y_centro, 400, 250, 50, T("Salir"), recursos);
	m_boton_salir->color_boton(m_color_boton_normal);
	m_boton_salir->color_texto(m_color_letra_normal);

	m_cambio_dispositivo = false;
	m_configuracion = configuracion;
	m_texto_entrada = m_configuracion->controlador_midi()->dispositivos_conectados(ENTRADA);
	m_texto_salida = m_configuracion->controlador_midi()->dispositivos_conectados(SALIDA);

	m_texto_dispositivo_entrada.texto(T_("Entrada: {0}", m_texto_entrada));
	m_texto_dispositivo_entrada.tipografia(recursos->tipografia(ModoLetra::Chica));
	m_texto_dispositivo_entrada.color(Color(1.0f, 1.0f, 1.0f));
	m_texto_dispositivo_entrada.posicion(20, Pantalla::Alto - 35);
	m_texto_dispositivo_entrada.ancho_automatico(true);

	m_texto_dispositivo_salida.texto(T_("Salida: {0}", m_texto_salida));
	m_texto_dispositivo_salida.tipografia(recursos->tipografia(ModoLetra::Chica));
	m_texto_dispositivo_salida.color(Color(1.0f, 1.0f, 1.0f));
	m_texto_dispositivo_salida.posicion(20, Pantalla::Alto - 17);
	m_texto_dispositivo_salida.ancho_automatico(true);

	std::string tipo_version;
	if(VERSION_ALFA)
		tipo_version = T("Alfa");
	m_texto_version.texto(T_("Versión: {0}.{1}.{2} {3}", CONCERTISTAMIDI_VERSION_MAYOR, CONCERTISTAMIDI_VERSION_MENOR, CONCERTISTAMIDI_VERSION_PARCHE, tipo_version));
	m_texto_version.tipografia(recursos->tipografia(ModoLetra::Chica));
	m_texto_version.color(Color(1.0f, 1.0f, 1.0f));
	m_texto_version.posicion(Pantalla::Ancho - (m_texto_version.largo_texto()+20), Pantalla::Alto - 26);
}

VentanaTitulo::~VentanaTitulo()
{
	delete m_boton_tocar_cancion;
	delete m_boton_grabar_cancion;
	delete m_boton_configurar;
	delete m_boton_salir;
}

void VentanaTitulo::actualizar(unsigned int diferencia_tiempo)
{
	m_boton_tocar_cancion->actualizar(diferencia_tiempo);
	m_boton_grabar_cancion->actualizar(diferencia_tiempo);
	m_boton_configurar->actualizar(diferencia_tiempo);
	m_boton_salir->actualizar(diferencia_tiempo);

	m_bloqueo.lock();
	if(m_cambio_dispositivo)
	{
		m_texto_dispositivo_entrada.texto(T_("Entrada: {0}", m_texto_entrada));
		m_texto_dispositivo_salida.texto(T_("Salida: {0}", m_texto_salida));
		m_cambio_dispositivo = false;
	}
	m_bloqueo.unlock();
}

void VentanaTitulo::actualizar_midi(unsigned int /*diferencia_tiempo*/)
{
	if(m_configuracion->controlador_midi()->hay_cambios_de_dispositivos())
	{
		m_bloqueo.lock();
		m_cambio_dispositivo = true;
		m_texto_entrada = m_configuracion->controlador_midi()->dispositivos_conectados(ENTRADA);
		m_texto_salida = m_configuracion->controlador_midi()->dispositivos_conectados(SALIDA);
		m_bloqueo.unlock();

		m_configuracion->actualizar_rango_util_organo();
	}
}

void VentanaTitulo::dibujar()
{
	m_textura_fondo->activar();
	m_rectangulo->textura(true);
	m_rectangulo->color(Color(1.0f, 1.0f, 1.0f));
	m_rectangulo->dibujar(0, 0, Pantalla::Ancho, 150);
	m_rectangulo->dibujar(0, Pantalla::Alto - 40, Pantalla::Ancho, 40);

	m_textura_titulo->activar();
	m_rectangulo->dibujar(Pantalla::Centro_horizontal() - 256, 30, 512, 128);

	m_boton_tocar_cancion->dibujar();
	m_boton_grabar_cancion->dibujar();
	m_boton_configurar->dibujar();
	m_boton_salir->dibujar();

	m_texto_dispositivo_entrada.dibujar();
	m_texto_dispositivo_salida.dibujar();
	m_texto_version.dibujar();
}

void VentanaTitulo::evento_raton(Raton *raton)
{
	m_boton_tocar_cancion->evento_raton(raton);
	m_boton_grabar_cancion->evento_raton(raton);
	m_boton_configurar->evento_raton(raton);
	m_boton_salir->evento_raton(raton);

	if(m_boton_tocar_cancion->esta_activado())
		m_accion = Accion::CambiarASeleccionMusica;
	else if(m_boton_grabar_cancion->esta_activado())
		m_accion = Accion::CambiarAGrabacion;
	else if(m_boton_configurar->esta_activado())
		m_accion = Accion::CambiarAConfiguracion;
	else if(m_boton_salir->esta_activado())
		m_accion = Accion::Salir;
}

void VentanaTitulo::evento_teclado(Tecla tecla, bool estado)
{
	if(tecla == Tecla::Escape && estado)
		m_accion = Accion::Salir;
	else if(tecla == Tecla::Entrar && !estado)
	{
		if(m_opcion_seleccionada == 0)
			m_accion = Accion::CambiarASeleccionMusica;
		else if(m_opcion_seleccionada == 1)
			m_accion = Accion::CambiarAGrabacion;
		else if(m_opcion_seleccionada == 2)
			m_accion = Accion::CambiarAConfiguracion;
		else if(m_opcion_seleccionada == 3)
			m_accion = Accion::Salir;
	}
	else if(tecla == Tecla::FlechaArriba && estado)
	{
		m_opcion_seleccionada--;
		if(m_opcion_seleccionada < 0)
			m_opcion_seleccionada = 3;

		if(m_opcion_seleccionada == 0)
		{
			m_boton_grabar_cancion->color_boton(m_color_boton_normal);
			m_boton_grabar_cancion->color_texto(m_color_letra_normal);
			m_boton_tocar_cancion->color_boton(m_color_boton_seleccion);
			m_boton_tocar_cancion->color_texto(m_color_letra_seleccion);
		}
		else if(m_opcion_seleccionada == 1)
		{
			m_boton_configurar->color_boton(m_color_boton_normal);
			m_boton_configurar->color_texto(m_color_letra_normal);
			m_boton_grabar_cancion->color_boton(m_color_boton_seleccion);
			m_boton_grabar_cancion->color_texto(m_color_letra_seleccion);
		}
		else if(m_opcion_seleccionada == 2)
		{
			m_boton_salir->color_boton(m_color_boton_normal);
			m_boton_salir->color_texto(m_color_letra_normal);
			m_boton_configurar->color_boton(m_color_boton_seleccion);
			m_boton_configurar->color_texto(m_color_letra_seleccion);
		}
		else if(m_opcion_seleccionada == 3)
		{
			m_boton_tocar_cancion->color_boton(m_color_boton_normal);
			m_boton_tocar_cancion->color_texto(m_color_letra_normal);
			m_boton_salir->color_boton(m_color_boton_seleccion);
			m_boton_salir->color_texto(m_color_letra_seleccion);
		}
	}
	else if(tecla == Tecla::FlechaAbajo && estado)
	{
		m_opcion_seleccionada++;
		if(m_opcion_seleccionada > 3)
			m_opcion_seleccionada = 0;

		if(m_opcion_seleccionada == 0)
		{
			m_boton_salir->color_boton(m_color_boton_normal);
			m_boton_salir->color_texto(m_color_letra_normal);
			m_boton_tocar_cancion->color_boton(m_color_boton_seleccion);
			m_boton_tocar_cancion->color_texto(m_color_letra_seleccion);
		}
		else if(m_opcion_seleccionada == 1)
		{
			m_boton_tocar_cancion->color_boton(m_color_boton_normal);
			m_boton_tocar_cancion->color_texto(m_color_letra_normal);
			m_boton_grabar_cancion->color_boton(m_color_boton_seleccion);
			m_boton_grabar_cancion->color_texto(m_color_letra_seleccion);
		}
		else if(m_opcion_seleccionada == 2)
		{
			m_boton_grabar_cancion->color_boton(m_color_boton_normal);
			m_boton_grabar_cancion->color_texto(m_color_letra_normal);
			m_boton_configurar->color_boton(m_color_boton_seleccion);
			m_boton_configurar->color_texto(m_color_letra_seleccion);
		}
		else if(m_opcion_seleccionada == 3)
		{
			m_boton_configurar->color_boton(m_color_boton_normal);
			m_boton_configurar->color_texto(m_color_letra_normal);
			m_boton_salir->color_boton(m_color_boton_seleccion);
			m_boton_salir->color_texto(m_color_letra_seleccion);
		}
	}
}

void VentanaTitulo::evento_pantalla(float ancho, float alto)
{
	//Todos los botones tienen el mismo ancho
	float y_centro = Pantalla::Centro_horizontal() - m_boton_tocar_cancion->ancho()/2;

	m_boton_tocar_cancion->posicion(y_centro, m_boton_tocar_cancion->y());
	m_boton_grabar_cancion->posicion(y_centro, m_boton_grabar_cancion->y());
	m_boton_configurar->posicion(y_centro, m_boton_configurar->y());
	m_boton_salir->posicion(y_centro, m_boton_salir->y());
	m_texto_dispositivo_entrada.posicion(20, alto - 35);
	m_texto_dispositivo_salida.posicion(20, alto - 17);
	m_texto_version.posicion(ancho - (m_texto_version.largo_texto()+20), alto - 26);
}
