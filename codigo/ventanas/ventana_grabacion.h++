#ifndef VENTANAORGANOLIBRE_H
#define VENTANAORGANOLIBRE_H

#include "ventana.h++"
#include "ventana_organo.h++"
#include "../elementos_graficos/etiqueta.h++"
#include "../elementos_graficos/barra_progreso.h++"
#include "../elementos_graficos/tablero_notas.h++"
#include "../elementos_graficos/organo.h++"
#include "../control/configuracion.h++"
#include "../control/rango_organo.h++"
#include "../control/tecla_organo.h++"
#include "../control/pista.h++"
#include "../libreria_midi/evento_midi.h++"
#include "../libreria_midi/midi1.h++"
#include "../libreria_midi/midi2.h++"

#include <vector>

class VentanaGrabacion : public VentanaOrgano
{
private:
	//Componentes
	Etiqueta m_texto_instrumento;

	Midi *m_midi;
	double m_tiempo_por_tics;
	std::uint64_t m_tics_acumulado;
	double m_tics_delta_temporal;
	unsigned int m_tics_delta;
	std::int64_t m_microsegundos_acumulado;

	unsigned short m_id_pista_actual;

	//Datos Musica
	std::vector<Pista> m_pistas;
	Evento_Midi *m_programa;
	bool m_cambio_programa;

	void insertar_nota(unsigned char id_nota);
	void eliminar_nota(unsigned char id_nota);

public:
	VentanaGrabacion(Configuracion *configuracion, Administrador_Recursos *recursos);
	~VentanaGrabacion();

	void actualizar(unsigned int diferencia_tiempo) override;
	void actualizar_midi(unsigned int diferencia_tiempo) override;
	void dibujar() override;

	void evento_raton(Raton *raton) override;
	void evento_teclado(Tecla tecla, bool estado) override;
	void evento_pantalla(float ancho, float alto) override;
};
#endif
