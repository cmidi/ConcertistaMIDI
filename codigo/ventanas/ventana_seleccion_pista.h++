#ifndef VENTANASELECCIONPISTA_H
#define VENTANASELECCIONPISTA_H

#include "ventana.h++"
#include "../libreria_midi/reproductor_midi.h++"
#include "../elementos_graficos/panel_desplazamiento.h++"
#include "../elementos_graficos/boton.h++"
#include "../elementos_graficos/etiqueta.h++"
#include "../elementos_graficos/lista_opciones.h++"
#include "../elementos_graficos/configuracion_pista.h++"
#include "../control/configuracion.h++"
#include "../control/datos_musica.h++"

class VentanaSeleccionPista : public Ventana
{
private:
	//Recursos
	Administrador_Recursos *m_recursos;
	Rectangulo *m_rectangulo;
	Textura2D *m_textura_fondo_datos;
	Etiqueta m_texto_titulo;
	Etiqueta m_texto_midi_nombre;
	Etiqueta m_texto_midi_formato;
	Etiqueta m_texto_midi_secuencias;
	Etiqueta m_texto_midi_pistas;
	Etiqueta m_texto_midi_especificacion;
	Lista_Opciones *m_lista_secuencias_midi;

	//Componentes
	Boton *m_boton_atras;
	Boton *m_boton_continuar;
	Panel_Desplazamiento *m_panel_desplazamiento;
	std::vector<Configuracion_Pista*> m_configuracion_pistas;

	//Control
	std::mutex m_bloqueo;

	//Datos
	Configuracion *m_configuracion;
	Datos_Musica *m_musica;

	//Reproductor
	Reproductor_Midi m_reproductor;

	void crear_configuracion(Administrador_Recursos *recursos);
	void cargar_configuracion(Administrador_Recursos *recursos);
 	void guardar_configuracion();

	unsigned short pistas_con_notas(Midi *archivo_midi);
	std::string especificacion_midi(Midi *archivo_midi);
public:
	VentanaSeleccionPista(Configuracion *configuracion, Datos_Musica *musica, Administrador_Recursos *recursos);
	~VentanaSeleccionPista();

	void actualizar(unsigned int diferencia_tiempo) override;
	void actualizar_midi(unsigned int diferencia_tiempo) override;
	void dibujar() override;

	void evento_raton(Raton *raton) override;
	void evento_teclado(Tecla tecla, bool estado) override;
	void evento_pantalla(float ancho, float alto) override;
};
#endif
