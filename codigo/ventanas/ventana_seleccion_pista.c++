#include "ventana_seleccion_pista.h++"

VentanaSeleccionPista::VentanaSeleccionPista(Configuracion *configuracion, Datos_Musica *musica, Administrador_Recursos *recursos) : Ventana(),
m_texto_titulo(recursos), m_texto_midi_nombre(recursos) ,m_texto_midi_formato(recursos) ,m_texto_midi_secuencias(recursos) ,
m_texto_midi_pistas(recursos) ,m_texto_midi_especificacion(recursos)
{
	this->fps_dinamico(true);
	this->protector_pantalla(true);
	this->consumir_eventos(true);

	m_configuracion = configuracion;
	m_musica = musica;

	m_recursos = recursos;
	m_rectangulo = recursos->figura(FiguraGeometrica::Rectangulo);
	m_textura_fondo_datos = recursos->textura(Textura::Circulo);

	m_boton_atras = new Boton(10, Pantalla::Alto - 36, 120, 32, T("Atrás"), ModoLetra::Chica, recursos);
	m_boton_atras->color_boton(Color(0.9f, 0.9f, 0.9f));

	m_boton_continuar = new Boton(Pantalla::Ancho - 130, Pantalla::Alto - 36, 120, 32, T("Continuar"), ModoLetra::Chica, recursos);
	m_boton_continuar->color_boton(Color(0.9f, 0.9f, 0.9f));
	if(this->pistas_con_notas(m_musica->midi()) == 0)
		m_boton_continuar->habilitado(false);

	m_texto_titulo.texto(T("Seleccione pistas para tocar"));
	m_texto_titulo.tipografia(recursos->tipografia(ModoLetra::Grande));
	m_texto_titulo.color(Color(1.0f, 1.0f, 1.0f));
	m_texto_titulo.posicion(0, 0);
	m_texto_titulo.dimension(Pantalla::Ancho, 40);
	m_texto_titulo.alineacion_horizontal(Alineacion_H::Centrado);
	m_texto_titulo.alineacion_vertical(Alineacion_V::Centrado);

	m_texto_midi_nombre.texto(T_("Nombre: {0}", m_musica->nombre_musica()));
	m_texto_midi_nombre.tipografia(recursos->tipografia(ModoLetra::Chica));
	m_texto_midi_nombre.posicion(20, 60);
	m_texto_midi_nombre.ancho_maximo(Pantalla::Ancho - 40);

	std::string version_midi = std::to_string(static_cast<unsigned int>(m_musica->midi()->version_midi())) + ".0";
	m_texto_midi_formato.texto(T_("Formato: MIDI {0} Tipo {1}", version_midi, m_musica->midi()->formato_midi()));
	m_texto_midi_formato.tipografia(recursos->tipografia(ModoLetra::Chica));
	m_texto_midi_formato.posicion(20, 82);

	m_lista_secuencias_midi = nullptr;
	if(m_musica->midi()->formato_midi() == 2)
	{
		m_texto_midi_secuencias.texto(T("Secuencia:"));
		m_texto_midi_secuencias.tipografia(recursos->tipografia(ModoLetra::Chica));
		m_texto_midi_secuencias.posicion((Pantalla::Ancho/2) - (m_texto_midi_secuencias.largo_texto() + 1 + 110 + 5), 82);//5 es el margen

		m_lista_secuencias_midi = new Lista_Opciones((Pantalla::Ancho/2) - (110 + 5), 82, 110, 12, true, recursos);
		m_lista_secuencias_midi->tipografia(recursos->tipografia(ModoLetra::Chica));
		std::vector<std::string> lista_secuencias;
		for(unsigned short n=0; n < m_musica->midi()->numero_secuencias(); n++)
			lista_secuencias.push_back(T_("{0} de {1}", n+1, m_musica->midi()->numero_secuencias()));
		if(lista_secuencias.size() == 0)
			lista_secuencias.push_back(T("0 de 0"));
		m_lista_secuencias_midi->opciones_textos(lista_secuencias);
		m_lista_secuencias_midi->opcion_predeterminada(m_musica->midi()->id_secuencia_activa());
	}

	m_texto_midi_pistas.texto(T_("Pistas: {0}", this->pistas_con_notas(m_musica->midi())));
	m_texto_midi_pistas.tipografia(recursos->tipografia(ModoLetra::Chica));
	m_texto_midi_pistas.posicion(Pantalla::Ancho/2 + 5, 82);//5 es el margen

	m_texto_midi_especificacion.texto(T_("Especificación MIDI: {0}", this->especificacion_midi(m_musica->midi())));
	m_texto_midi_especificacion.tipografia(recursos->tipografia(ModoLetra::Chica));
	m_texto_midi_especificacion.posicion(Pantalla::Ancho - (20 + m_texto_midi_especificacion.ancho()), 82);
	if(Pantalla::Ancho < 1000)
		m_texto_midi_especificacion.ancho_maximo((Pantalla::Ancho-40)*0.3f);
	else
		m_texto_midi_especificacion.ancho_maximo((Pantalla::Ancho-40) / 2.0f);

	m_panel_desplazamiento = new Panel_Desplazamiento(0, 114, Pantalla::Ancho, Pantalla::Alto - (114 + 50), 350, 150, 10, 10, recursos);//0,50

	if(m_musica->secuencia_cargada(m_musica->midi()->id_secuencia_activa()))
		this->cargar_configuracion(recursos);
	else
		this->crear_configuracion(recursos);

	m_reproductor.controlador_midi(m_configuracion->controlador_midi());
	m_reproductor.cargar_midi(m_musica->midi());
	m_reproductor.reproducir_en_bucle(true);
	m_reproductor.habilitar_pistas(false);
}

VentanaSeleccionPista::~VentanaSeleccionPista()
{
	delete m_boton_atras;
	delete m_boton_continuar;
	if(m_lista_secuencias_midi != nullptr)
		delete m_lista_secuencias_midi;
	delete m_panel_desplazamiento;
	for(Elemento* e : m_configuracion_pistas)
		delete e;
}

void VentanaSeleccionPista::crear_configuracion(Administrador_Recursos *recursos)
{
	unsigned char color_usado = 0;
	Configuracion_Pista *configuracion;
	Color color_pista;
	bool visible = true;
	bool sonido = true;
	std::vector<Pista_Midi*> *pistas_midi = m_musica->midi()->secuencia_activa()->pistas();
	for(unsigned short i=0; i<pistas_midi->size(); i++)
	{
		if((*pistas_midi)[i]->numero_notas() > 0)
		{
			sonido = true;
			if((*pistas_midi)[i]->percusion() != Percusion::Desactivado)
			{
				color_pista = Pista::Colores_pista[0];
				visible = false;
			}
			else
			{
				color_pista = Pista::Colores_pista[1+color_usado % NUMERO_COLORES_PISTA];
				visible = true;
				color_usado++;
			}

			//La espesificación Multimedia PC divide los canales en dos grupos
			//Desde el 1 a 10: extendido
			//Desde el 13 al 16: base (canal 11 y 12 sin uso)
			//Los dos grupos contienen la misma canción solo que el grupo base
			//estaba pensado para equipos de pocos recursos
			//Solo se usara el grupo extendido
			bool mpc = m_musica->midi()->secuencia_activa()->contiene_especificacion_midi((*pistas_midi)[i]->grupo(), EspecificacionMidi::MPC);
			if(mpc && (*pistas_midi)[i]->canal() >= 12)
			{
				color_pista = Pista::Colores_pista[0];
				visible = false;
				sonido = false;
			}

			Pista pista((*pistas_midi)[i], color_pista, Modo::Fondo, visible, sonido);
			configuracion = new Configuracion_Pista(0, 0, 350, 150, pista, recursos);
			m_configuracion_pistas.push_back(configuracion);
			m_panel_desplazamiento->agregar_elemento(configuracion);
		}
		else
		{
			Pista pista((*pistas_midi)[i], Color(0.0f, 0.0f, 0.0f), Modo::Fondo, false, false);
			configuracion = new Configuracion_Pista(0, 0, 350, 150, pista, recursos);
			m_configuracion_pistas.push_back(configuracion);
		}
	}
}

void VentanaSeleccionPista::cargar_configuracion(Administrador_Recursos *recursos)
{
	std::vector<Pista> *pistas = m_musica->pistas();
	Configuracion_Pista *configuracion;
	for(unsigned short i=0; i<pistas->size(); i++)
	{
		configuracion = new Configuracion_Pista(0, 0, 350, 150, pistas->at(i), recursos);
		m_configuracion_pistas.push_back(configuracion);
		if(pistas->at(i).pista_midi()->numero_notas() > 0)
			m_panel_desplazamiento->agregar_elemento(configuracion);
	}
}

void VentanaSeleccionPista::guardar_configuracion()
{
	std::vector<Pista> *pistas = new std::vector<Pista>();


	if(m_reproductor.reproduciendo())
	{
		std::vector<bool> pistas_habilitadas = m_reproductor.pistas_reproduciendo();
		//Se configuran para tocar las pistas que estan sonando
		for(unsigned short i=0; i<m_configuracion_pistas.size(); i++)
		{
			Pista p = m_configuracion_pistas[i]->pista();
			if(pistas_habilitadas[i])
			{
				p.sonido(true);
			}
			else
			{
				p.sonido(false);
				if(p.visible())
					p.color(Pista::Colores_pista[0]);
				p.visible(false);
			}
			pistas->push_back(p);
		}
	}
	else
	{
		for(unsigned short i=0; i<m_configuracion_pistas.size(); i++)
			pistas->push_back(m_configuracion_pistas[i]->pista());
	}
	m_musica->pistas(m_musica->midi()->id_secuencia_activa(), pistas);
}

unsigned short VentanaSeleccionPista::pistas_con_notas(Midi *archivo_midi)
{
	unsigned short total = 0;
	std::vector<Pista_Midi*> *pistas = archivo_midi->secuencia_activa()->pistas();
	for(unsigned short p = 0; p < pistas->size(); p++)
	{
		if((*pistas)[p]->numero_notas() > 0)
			total++;
	}
	return total;
}

std::string VentanaSeleccionPista::especificacion_midi(Midi *archivo_midi)
{
	std::string texto;
	std::vector<EspecificacionMidi> lista_especificacion_midi = archivo_midi->secuencia_activa()->lista_especificacion_midi();
	if(lista_especificacion_midi.size() > 0)
	{
		for(unsigned char s=0; s<lista_especificacion_midi.size(); s++)
		{
			if(texto.length() > 0)
				texto += ", ";
			texto += Nombre_EspecificacionMidi(lista_especificacion_midi[s]);
		}
	}
	if(texto.length() == 0)
		texto = T("No definido");

	return texto;
}

void VentanaSeleccionPista::actualizar(unsigned int diferencia_tiempo)
{
	m_boton_atras->actualizar(diferencia_tiempo);
	m_boton_continuar->actualizar(diferencia_tiempo);
	m_panel_desplazamiento->actualizar(diferencia_tiempo);

	if(m_lista_secuencias_midi != nullptr)
		m_lista_secuencias_midi->actualizar(diferencia_tiempo);
}

void VentanaSeleccionPista::actualizar_midi(unsigned int diferencia_tiempo)
{
	m_bloqueo.lock();
	m_reproductor.actualizar(static_cast<unsigned int>((static_cast<double>(diferencia_tiempo) / 1000.0)));
	m_bloqueo.unlock();
}

void VentanaSeleccionPista::dibujar()
{
	m_rectangulo->textura(false);
	m_rectangulo->dibujar(0, 0, Pantalla::Ancho, 40, Color(0.141f, 0.624f, 0.933f));
	m_rectangulo->dibujar(0, Pantalla::Alto - 40, Pantalla::Ancho, 40, Color(0.761f, 0.887f, 0.985f));
	m_texto_titulo.dibujar();

	//Informacion del archivo MIDI
	m_rectangulo->textura(true);
	m_textura_fondo_datos->activar();
	m_rectangulo->color(Color(1.0f, 1.0f, 1.0f));
	m_rectangulo->extremos_fijos(true, true);
	m_rectangulo->dibujar_estirable(10, 50, Pantalla::Ancho-20, 54, 10, 10);
	m_rectangulo->extremos_fijos(false, false);
	m_texto_midi_nombre.dibujar();
	m_texto_midi_formato.dibujar();
	m_texto_midi_pistas.dibujar();

	m_texto_midi_especificacion.dibujar();
	if(m_lista_secuencias_midi != nullptr)
	{
		m_texto_midi_secuencias.dibujar();
		m_lista_secuencias_midi->dibujar();
	}

	//Panel de desplazamiento que contiene la lista de pistas
	m_panel_desplazamiento->dibujar();

	m_boton_atras->dibujar();
	m_boton_continuar->dibujar();
}

void VentanaSeleccionPista::evento_raton(Raton *raton)
{
	m_bloqueo.lock();
	if(m_lista_secuencias_midi != nullptr)
	{
		m_lista_secuencias_midi->evento_raton(raton);

		if(m_lista_secuencias_midi->cambio_opcion_seleccionada())
		{
			m_reproductor.detener();
			m_reproductor.habilitar_pistas(false);
			this->guardar_configuracion();
			m_musica->midi()->seleccionar_secuencia(static_cast<unsigned short>(m_lista_secuencias_midi->opcion_seleccionada()));

			m_panel_desplazamiento->vaciar();
			for(Elemento* e : m_configuracion_pistas)
				delete e;

			m_configuracion_pistas.clear();

			m_texto_midi_pistas.texto(T_("Pistas: {0}", this->pistas_con_notas(m_musica->midi())));
			m_texto_midi_especificacion.texto(T_("Especificación MIDI: {0}", this->especificacion_midi(m_musica->midi())));

			if(m_musica->secuencia_cargada(m_musica->midi()->id_secuencia_activa()))
				this->cargar_configuracion(m_recursos);
			else
				this->crear_configuracion(m_recursos);

			if(this->pistas_con_notas(m_musica->midi()) == 0)
				m_boton_continuar->habilitado(false);
			else
				m_boton_continuar->habilitado(true);
		}
	}
	m_panel_desplazamiento->evento_raton(raton);
	if(raton->tipo_evento() == EventoRaton::Clic && !raton->activado(BotonRaton::Izquierdo))
	{
		bool reproducir = false;
		bool hay_cambios = false;
		unsigned short pista = 0;
		for(unsigned short cp = 0; cp<m_configuracion_pistas.size(); cp++)
		{
			if(m_configuracion_pistas[cp]->vista_previa())
				reproducir = true;

			if(m_configuracion_pistas[cp]->cambio_estado_vista_previa())
			{
				m_reproductor.habilitar_pista(cp, m_configuracion_pistas[cp]->vista_previa());
				hay_cambios = true;
				pista = cp;
			}
		}
		if(hay_cambios)
		{
			if(!m_reproductor.reproduciendo() && reproducir)
			{
				std::int64_t t = (*m_musica->midi()->secuencia_activa()->pistas())[pista]->tiempo_primera_nota();
				//Cambia el tiempo justo 1 milisegundo antes de la primera nota
				m_reproductor.cambiar_a(t - 1);
				m_reproductor.reproducir();
			}
			else if(m_reproductor.reproduciendo() && !reproducir)
			{
				m_reproductor.detener();
			}
		}
	}

	m_boton_atras->evento_raton(raton);
	m_boton_continuar->evento_raton(raton);

	if(m_boton_atras->esta_activado())
	{
		m_accion = Accion::CambiarASeleccionMusica;
		m_reproductor.detener();
	}

	if(m_boton_continuar->esta_activado())
	{
		m_accion = Accion::CambiarAReproduccion;
		this->guardar_configuracion();
		m_reproductor.detener();
	}
	m_bloqueo.unlock();
}

void VentanaSeleccionPista::evento_teclado(Tecla tecla, bool estado)
{
	m_bloqueo.lock();
	if(tecla == Tecla::Escape && !estado)
	{
		m_accion = Accion::CambiarASeleccionMusica;
		m_reproductor.detener();
	}
	else if(tecla == Tecla::Entrar && !estado && this->pistas_con_notas(m_musica->midi()))
	{
		m_accion = Accion::CambiarAReproduccion;
		this->guardar_configuracion();
		m_reproductor.detener();
	}
	else if(tecla == Tecla::RestaNumerico && estado)
	{
		m_reproductor.bajar_volumen();
	}
	else if(tecla == Tecla::SumaNumerico && estado)
	{
		m_reproductor.subir_volumen();
	}
	m_bloqueo.unlock();
}

void VentanaSeleccionPista::evento_pantalla(float ancho, float alto)
{
	m_texto_titulo.dimension(Pantalla::Ancho, 40);

	m_texto_midi_nombre.ancho_maximo(Pantalla::Ancho-40);
	if(m_lista_secuencias_midi != nullptr)
	{
		m_texto_midi_secuencias.posicion((Pantalla::Ancho/2) - (m_texto_midi_secuencias.largo_texto() + 1 + 110 + 5), 82);
		m_lista_secuencias_midi->posicion((Pantalla::Ancho/2) - (110 + 5), 82);
	}
	m_texto_midi_pistas.posicion(Pantalla::Ancho/2 + 5, 82);
	if(Pantalla::Ancho < 1000)
		m_texto_midi_especificacion.ancho_maximo((Pantalla::Ancho-40)*0.3f);
	else
		m_texto_midi_especificacion.ancho_maximo((Pantalla::Ancho-40) / 2.0f);
	m_texto_midi_especificacion.posicion(Pantalla::Ancho - (20 + m_texto_midi_especificacion.ancho()), 82);

	m_panel_desplazamiento->dimension(ancho, alto-(114 + 50));
	m_boton_atras->posicion(m_boton_atras->x(), alto - 36);
	m_boton_continuar->posicion(ancho - 130, alto - 36);
}
