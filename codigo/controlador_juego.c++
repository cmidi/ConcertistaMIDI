#include "controlador_juego.h++"

Controlador_Juego::Controlador_Juego()
{
	m_notificaciones = nullptr;
	m_texto_fps = nullptr;
	m_informacion = nullptr;
	m_ventana_actual = nullptr;
	m_ventana_lista_archivos = nullptr;

	m_hilo = nullptr;
	m_accion_nueva = Accion::Ninguna;
	//Control dinamico de fps
	m_tiempo_fotograma = 1000000000 / 60;
	m_fps_dinamico = 0;
	m_contador_inactividad = 0;

	//Grabar pantalla
	m_fotograma = -1;

	m_modo_alambre = false;
	m_terminar = false;
	m_hilo_activo = false;

	m_aviso_fps_mostrado = false;

	//Se habilita el protector de pantalla en principal.c++ justo despues de crear la ventana
	m_protector_pantalla = true;
	m_tiempo_espera_aviso = 0;

	m_soltando = false;

	m_pantalla_completa = m_configuracion.pantalla_completa();
}

Controlador_Juego::~Controlador_Juego()
{
	if(m_notificaciones != nullptr)
		delete m_notificaciones;
	if(m_texto_fps != nullptr)
		delete m_texto_fps;
	if(m_informacion != nullptr)
		delete m_informacion;
	if(m_ventana_actual != nullptr)
		delete m_ventana_actual;
}

void Controlador_Juego::asignar_administrador_recursos(Administrador_Recursos * recursos)
{
	m_recursos = recursos;
	m_rectangulo = recursos->figura(FiguraGeometrica::Rectangulo);

	m_notificaciones = new Notificacion(m_recursos);
	m_notificaciones->posicion(Pantalla::Centro_horizontal() - m_notificaciones->ancho()/2, 165);

	m_texto_fps = new Etiqueta(m_recursos);
	m_texto_fps->tipografia(recursos->tipografia(ModoLetra::Chica));
	m_texto_fps->posicion(10, 0);
	m_texto_fps->dimension(0, m_texto_fps->alto_texto());
	m_texto_fps->alineacion_vertical(Alineacion_V::Centrado);
	m_texto_fps->ancho_automatico(true);

	m_informacion = new Etiqueta(m_recursos);
	m_informacion->tipografia(recursos->tipografia(ModoLetra::Chica));
	m_informacion->ancho_automatico(true);

	m_accion_nueva = Accion::CambiarATitulo;
}

void Controlador_Juego::crear_ventana_inicial()
{
	if(m_accion_nueva == Accion::CambiarASeleccionPista && m_nuevo_midi.size() > 0 && this->abrir_midi_seleccionado())
		m_ventana_actual = new VentanaSeleccionPista(&m_configuracion, &m_musica, m_recursos);
	else
		m_ventana_actual = new VentanaTitulo(&m_configuracion, m_recursos);
	this->iniciar_hilo_midi();
}

Administrador_Recursos *Controlador_Juego::obtener_administrador_recursos()
{
	return m_recursos;
}

void Controlador_Juego::actualizar()
{
	unsigned int fps = Fps::Calcular_tiempo();
	unsigned int diferencia_tiempo = Fps::Obtener_nanosegundos();
	//unsigned int diferencia_tiempo = (1.0/60.0)*1000000000;

	if(m_nuevo_midi.size() > 0)
		this->abrir_midi_seleccionado();

	Accion accion_actual = m_ventana_actual->obtener_accion();
	if(m_accion_nueva != Accion::Ninguna)
	{
		accion_actual = m_accion_nueva;
		m_accion_nueva = Accion::Ninguna;
	}
	if(accion_actual == Accion::CambiarATitulo)
	{
		this->detener_hilo_midi();
		delete m_ventana_actual;
		if(m_ventana_lista_archivos != nullptr)
			m_ventana_lista_archivos = nullptr;

		m_ventana_actual = new VentanaTitulo(&m_configuracion, m_recursos);
		m_ventana_actual->evento_raton(&m_raton);
		this->iniciar_hilo_midi();
	}
	else if(accion_actual == Accion::CambiarASeleccionMusica)
	{
		this->detener_hilo_midi();
		delete m_ventana_actual;
		if(m_ventana_lista_archivos == nullptr)
		{
			//Se guarda la ventana de VentanaSeleccionMusica porque
			//puede tardar en crearse si hay miles de archivos
			m_ventana_actual = new VentanaSeleccionMusica(&m_configuracion, &m_musica, m_recursos);
			m_ventana_lista_archivos = m_ventana_actual;
		}
		else
		{
			m_ventana_actual = m_ventana_lista_archivos;
			m_ventana_actual->evento_pantalla(Pantalla::Ancho, Pantalla::Alto);
		}
		m_ventana_actual->evento_raton(&m_raton);
		this->iniciar_hilo_midi();
	}
	else if(accion_actual == Accion::CambiarASeleccionPista)
	{
		this->detener_hilo_midi();
		if(m_ventana_actual != m_ventana_lista_archivos)
			delete m_ventana_actual;
		m_ventana_actual = new VentanaSeleccionPista(&m_configuracion, &m_musica, m_recursos);
		m_ventana_actual->evento_raton(&m_raton);

		m_fotograma = -1;
		this->iniciar_hilo_midi();
	}
	else if(accion_actual == Accion::CambiarAReproduccion)
	{
		this->detener_hilo_midi();
		delete m_ventana_actual;
		m_ventana_actual = new VentanaReproduccion(&m_configuracion, &m_musica, m_recursos);
		m_ventana_actual->evento_raton(&m_raton);

		m_fotograma++;
		this->iniciar_hilo_midi();
	}
	else if(accion_actual == Accion::CambiarAGrabacion)
	{
		this->detener_hilo_midi();
		delete m_ventana_actual;
		m_ventana_actual = new VentanaGrabacion(&m_configuracion, m_recursos);
		m_ventana_actual->evento_raton(&m_raton);
		this->iniciar_hilo_midi();
	}
	else if(accion_actual == Accion::CambiarAConfiguracion)
	{
		this->detener_hilo_midi();
		delete m_ventana_actual;
		m_ventana_actual = new VentanaConfiguracion(&m_configuracion, m_recursos);
		m_ventana_actual->evento_raton(&m_raton);
		this->iniciar_hilo_midi();
	}
	else if(accion_actual == Accion::CambiarAResultados)
	{
		this->detener_hilo_midi();
		delete m_ventana_actual;
		m_ventana_actual = new VentanaResultados(&m_musica, m_recursos);
		m_ventana_actual->evento_raton(&m_raton);
		this->iniciar_hilo_midi();
	}
	else if(accion_actual == Accion::Salir)
		this->evento_salir();
	else if(accion_actual == Accion::EntrarPantallaCompleta)
	{
		if(!m_pantalla_completa)
			m_pantalla_completa = true;
	}
	else if(accion_actual == Accion::SalirPantallaCompleta)
	{
		if(m_pantalla_completa)
			m_pantalla_completa = false;
	}
	else if(accion_actual == Accion::EntrarModoAlambre)
	{
		if(!m_modo_alambre)
			m_modo_alambre = true;
	}
	else if(accion_actual == Accion::SalirModoAlambre)
	{
		if(m_modo_alambre)
			m_modo_alambre = false;
	}
	else if(accion_actual == Accion::EntrarModoDesarrollo)
	{
		if(!Pantalla::ModoDesarrollo)
			Pantalla::ModoDesarrollo = true;
	}
	else if(accion_actual == Accion::SalirModoDesarrollo)
	{
		if(Pantalla::ModoDesarrollo)
			Pantalla::ModoDesarrollo = false;
	}

	m_ventana_actual->actualizar(diferencia_tiempo);
	m_notificaciones->actualizar(diferencia_tiempo);
	m_ventana_actual->dibujar();
	m_notificaciones->dibujar();

	if(Pantalla::ModoDesarrollo)
	{
		if(Fps::Actualizar_fps())
			m_texto_fps->texto(T_("FPS: {0}", fps));
		m_texto_fps->dibujar();

		//Dibuja Raton
		m_rectangulo->textura(false);
		m_rectangulo->dibujar(static_cast<float>(m_raton.x())-11, static_cast<float>(m_raton.y()), 21, 1, Color(1.0f, 0.0f, 0.0f));
		m_rectangulo->dibujar(static_cast<float>(m_raton.x()), static_cast<float>(m_raton.y())-11, 1, 21, Color(1.0f, 0.0f, 0.0f));

		m_informacion->posicion(static_cast<float>(m_raton.x()+2), static_cast<float>(m_raton.y()-15));
		m_informacion->texto("X: " + std::to_string(m_raton.x()) + " Y: " + std::to_string(m_raton.y()));
		m_informacion->dibujar();
	}

	//Restablece el tiempo si sale de la ventana de reproduccion
	if(m_ventana_actual->fps_dinamico() && m_tiempo_espera_aviso > 0)
		m_tiempo_espera_aviso = 0;
	//Espera un tiempo a que se estabilicen los fps antes de mostrar el mensaje
	if(!m_ventana_actual->fps_dinamico() && m_tiempo_espera_aviso < 5)
		m_tiempo_espera_aviso += (static_cast<float>(diferencia_tiempo)/1000000000.0f);
	if(!m_ventana_actual->fps_dinamico() && !m_aviso_fps_mostrado && Fps::Actualizar_fps() && fps < 30 && m_tiempo_espera_aviso >= 5)
	{
		Notificacion::Aviso(T_("Hay {0} fps actualmente", fps), 5);
		Notificacion::Aviso(T("Pueden haber problemas con la reproducción"), 5);
		m_aviso_fps_mostrado = true;
	}

	//Desactiva el protector de pantalla solo cuando esta jugando
	if(m_ventana_actual->protector_pantalla())
	{
		//Habilita el protector de pantalla
		if(!m_protector_pantalla)
		{
			SDL_EnableScreenSaver();
			m_protector_pantalla = true;
		}
	}
	else
	{
		//Desabilita el protector de pantalla
		if(m_protector_pantalla)
		{
			SDL_DisableScreenSaver();
			m_protector_pantalla = false;
		}
	}

	if(m_fotograma >= 0)
	{
		m_fotograma++;
		if(m_fotograma > 1)
		{
			//Se omite el primer fotograma porque aun muestra la ventana anterior
			/*float *pixeles = new float[static_cast<int>(Pantalla::Ancho*Pantalla::Alto*4)];
			glReadPixels(0, 0, Pantalla::Ancho, Pantalla::Alto, GL_RGBA, GL_FLOAT, pixeles);
			Archivo::Tga::Escribir("../fotogramas/" + std::to_string(m_fotograma-1) + ".tga", pixeles, Pantalla::Ancho, Pantalla::Alto);
			delete[] pixeles;*/
		}
	}

	if(m_notificaciones->mostrando_notificaciones())
		this->reiniciar_contador_inactividad();

	if(m_fps_dinamico < 4)
		m_contador_inactividad += (static_cast<float>(diferencia_tiempo)/1000000000.0f);

	if(m_ventana_actual->fps_dinamico() && m_fps_dinamico < 4)
	{
		if(m_contador_inactividad > 2.0f && m_fps_dinamico < 1)
		{
			m_tiempo_fotograma = 1000000000 / 30;
			m_fps_dinamico++;
		}
		else if(m_contador_inactividad > 4.0f && m_fps_dinamico < 2)
		{
			m_tiempo_fotograma = 1000000000 / 15;
			m_fps_dinamico++;
		}
		else if(m_contador_inactividad > 8.0f && m_fps_dinamico < 3)
		{
			m_tiempo_fotograma = 1000000000 / 7;
			m_fps_dinamico++;
		}
		else if(m_contador_inactividad > 16.0f && m_fps_dinamico < 4)
		{
			m_tiempo_fotograma = 1000000000 / 1;
			m_fps_dinamico++;
		}
	}
}

void Controlador_Juego::actualizar_midi(unsigned int diferencia_tiempo)
{
	m_configuracion.controlador_midi()->actualizar(diferencia_tiempo, m_ventana_actual->consumir_eventos());

	while(m_configuracion.controlador_midi()->hay_mensajes())
		Notificacion::Nota(m_configuracion.controlador_midi()->siguiente_mensaje(), 5);

	if(m_configuracion.controlador_midi()->hay_cambio_cliente())
		m_configuracion.actualizar_dispositivo_en_registro();

	m_ventana_actual->actualizar_midi(diferencia_tiempo);
}

std::int64_t Controlador_Juego::tiempo_fotograma()
{
	return m_tiempo_fotograma;
}

bool Controlador_Juego::es_pantalla_completa()
{
	return m_pantalla_completa;
}

bool Controlador_Juego::modo_alambre_activado()
{
	return m_modo_alambre;
}

bool Controlador_Juego::terminar()
{
	return m_terminar;
}

bool Controlador_Juego::hilo_activo()
{
	return m_hilo_activo;
}

Raton *Controlador_Juego::raton()
{
	return &m_raton;
}

void Controlador_Juego::eventos_raton()
{
	m_ventana_actual->evento_raton(&m_raton);
	this->reiniciar_contador_inactividad();
}

void Controlador_Juego::eventos_teclado(Tecla tecla, bool estado)
{
	this->reiniciar_contador_inactividad();
	if(tecla == Tecla::F10 && estado)
		Pantalla::ModoDesarrollo = !Pantalla::ModoDesarrollo;
	else if(tecla == Tecla::F11 && estado)
		m_pantalla_completa = !m_pantalla_completa;
	else if(tecla == Tecla::F12 && estado)
		m_modo_alambre = !m_modo_alambre;
	else
	{
		m_ventana_actual->evento_teclado(tecla, estado);
		unsigned char id_nota = Teclado::Tecla_a_nota(tecla);
		if(id_nota < 128)
		{
			if(estado)
			{
				if(m_teclas_pulsadas.count(id_nota) == 0)
				{
					m_teclas_pulsadas.insert(id_nota);
					m_configuracion.controlador_midi()->enviar_nota(id_nota, true);
				}
			}
			else
			{
				if(m_teclas_pulsadas.count(id_nota) > 0)
				{
					m_teclas_pulsadas.erase(id_nota);
					m_configuracion.controlador_midi()->enviar_nota(id_nota, false);
				}
			}
		}
	}
}

void Controlador_Juego::evento_ventana(float ancho, float alto)
{
	this->reiniciar_contador_inactividad();
	Pantalla::Ancho = ancho;
	Pantalla::Alto = alto;
	m_recursos->actualizar_pantalla(ancho, alto);
	m_ventana_actual->evento_pantalla(ancho, alto);
	m_notificaciones->posicion(Pantalla::Centro_horizontal() - m_notificaciones->ancho()/2, 165);
}

void Controlador_Juego::evento_salir()
{
	this->detener_hilo_midi();
	m_terminar = true;
	m_configuracion.pantalla_completa(m_pantalla_completa);
	m_configuracion.guardar_configuracion();
}

void Controlador_Juego::evento_soltar_contenido(bool es_archivo, const std::string &contenido)
{
	//Por ahora solo me interesan los archivos
	if(es_archivo)
		m_contenido_soltado.push_back(contenido);
}

void Controlador_Juego::evento_soltar_inicio()
{
	m_soltando = true;
	m_contenido_soltado.clear();
}

void Controlador_Juego::evento_soltar_fin()
{
	m_soltando = false;
	if(m_contenido_soltado.size() == 0)
		return;

	for(unsigned int x=0; x<m_contenido_soltado.size(); x++)
	{
		if(this->abrir_archivo(m_contenido_soltado[x]))
			return;
	}
}

bool Controlador_Juego::abrir_archivo(const std::string &direccion)
{
	std::filesystem::path ruta(direccion);
	std::error_code error;
	if(std::filesystem::is_regular_file(ruta, error))
	{
		if(!error)
		{
			std::string extencion_archivo = Funciones::extencion_archivo(ruta);
			if(Funciones::es_midi(extencion_archivo))
			{
				std::string ruta_completa_texto = ruta.string();
				std::string ruta_archivo = "/";
				if(ruta.has_parent_path())
					ruta_archivo = ruta.parent_path().string();
				std::string carpeta_inicial = m_configuracion.base_de_datos()->ruta_carpeta_base(ruta_archivo);
				if(carpeta_inicial.size() > 0)
					m_configuracion.carpeta_inicial(carpeta_inicial);
				else
				{
					//Se agrega la nueva carpeta
					std::string nombre_carpeta = Funciones::nombre_archivo(ruta_archivo, true);
					if(nombre_carpeta.size() == 0)
						nombre_carpeta = "/";
					m_configuracion.base_de_datos()->agregar_carpeta(nombre_carpeta, ruta_archivo);
					m_configuracion.carpeta_inicial(ruta_archivo);
				}
				m_configuracion.carpeta_activa(ruta_archivo);
				//No se sabe la posicion dentro de la tabla, pero se buscara despues al cargar la tabla de archivos
				m_configuracion.base_de_datos()->guardar_ultima_seleccion(ruta_archivo, 0, ruta_completa_texto);

				m_nuevo_midi = ruta_completa_texto;
				m_accion_nueva = Accion::CambiarASeleccionPista;
				return true;//Se queda con el primer archivo midi
			}
		}
	}
	return false;
}

Configuracion *Controlador_Juego::configuracion()
{
	return &m_configuracion;
}

void Controlador_Juego::reiniciar_contador_inactividad()
{
	//Vuelve a los fps maximo
	m_contador_inactividad = 0.0f;
	if(m_fps_dinamico > 0)
	{
		m_fps_dinamico = 0;
		m_tiempo_fotograma = 1000000000 / 60;
	}
}

bool Controlador_Juego::abrir_midi_seleccionado()
{
	std::string estado = m_musica.cargar_midi(m_nuevo_midi);
	if(estado.size() > 0)
	{
		m_accion_nueva = Accion::CambiarATitulo;
		Notificacion::Error(T("Error al abrir el archivo"), 5);
		Notificacion::Error(estado, 5);
		m_nuevo_midi.clear();
		return false;
	}
	m_nuevo_midi.clear();
	return true;
}

void Controlador_Juego::iniciar_hilo_midi()
{
	if(!m_hilo_activo)
	{
		m_hilo_activo = true;
		m_hilo = new std::thread(hilo_midi, this);
	}
}

void Controlador_Juego::detener_hilo_midi()
{
	if(m_hilo_activo)
	{
		m_hilo_activo = false;
		m_hilo->join();
		delete m_hilo;
		m_hilo = nullptr;
	}
}

void Controlador_Juego::hilo_midi(Controlador_Juego *controlador)
{
	Tiempo tiempo_anterior = Reloj::now();
	Tiempo tiempo_actual = Reloj::now();
	std::int64_t nanoseconds = 0;

	while(controlador->hilo_activo())
	{
		tiempo_actual = Reloj::now();
		nanoseconds = std::chrono::duration_cast<std::chrono::nanoseconds>(tiempo_actual - tiempo_anterior).count();
		tiempo_anterior = tiempo_actual;

		controlador->actualizar_midi(static_cast<unsigned int>(nanoseconds));
		std::this_thread::sleep_for(std::chrono::milliseconds(5));
	}
}
