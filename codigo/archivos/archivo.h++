#ifndef ARCHIVO_H
#define ARCHIVO_H

#include <string>
#include <fstream>
#include <sstream>

#include "../registro.h++"

namespace Archivo
{
	class Texto
	{
	public:
		static std::string leer_archivo(const char* nombre);
	};
}
#endif
