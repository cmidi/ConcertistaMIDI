#ifndef IMAGEN_TGA
#define IMAGEN_TGA

#include <string>

#include "../registro.h++"

namespace Archivo
{
	class Tga
	{
	private:
		unsigned char * m_datos_imagen;
		unsigned short m_ancho_imagen = 0;
		unsigned short m_alto_imagen = 0;
		unsigned char m_bytes_imagen = 0;//Bits por pixel
		bool m_error = false;

	public:
		Tga(std::string direccion);
		~Tga();

		unsigned char * imagen();
		unsigned short ancho();
		unsigned short alto();
		unsigned char bytes();

		bool hay_error();

		static void Escribir(std::string direccion, float *datos, unsigned short ancho, unsigned short alto);
	};
}

#endif
