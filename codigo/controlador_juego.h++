#ifndef CONTROLADOR_JUEGO_H
#define CONTROLADOR_JUEGO_H

#include "recursos/administrador_recursos.h++"
#include "archivos/imagen_tga.h++"
#include "elementos_graficos/etiqueta.h++"
#include "elementos_graficos/notificacion.h++"
#include "dispositivos/teclas.h++"
#include "dispositivos/teclado.h++"
#include "dispositivos/raton.h++"
#include "dispositivos/pantalla.h++"
#include "fps.h++"

#include "ventanas/ventana.h++"
#include "ventanas/ventana_titulo.h++"
#include "ventanas/ventana_seleccion_musica.h++"
#include "ventanas/ventana_seleccion_pista.h++"
#include "ventanas/ventana_reproduccion.h++"
#include "ventanas/ventana_grabacion.h++"
#include "ventanas/ventana_configuracion.h++"
#include "ventanas/ventana_resultados.h++"

#include "control/configuracion.h++"
#include "control/datos_musica.h++"

#include <SDL2/SDL.h>
#include <thread>
#include <set>

class Controlador_Juego
{
private:
	Administrador_Recursos *m_recursos;
	Rectangulo *m_rectangulo;
	Etiqueta *m_texto_fps;
	Etiqueta *m_informacion;
	Notificacion *m_notificaciones;

	//Control de fps
	std::int64_t m_tiempo_fotograma;
	unsigned char m_fps_dinamico;
	float m_contador_inactividad;

	//Control
	std::thread *m_hilo;
	Accion m_accion_nueva;
	bool m_pantalla_completa;
	bool m_modo_alambre;
	bool m_terminar;
	bool m_hilo_activo;
	bool m_aviso_fps_mostrado;
	bool m_protector_pantalla;
	float m_tiempo_espera_aviso;
	std::set<unsigned char> m_teclas_pulsadas;

	//Soltar contenido
	bool m_soltando;
	std::vector<std::string> m_contenido_soltado;
	std::string m_nuevo_midi;

	//Eventos
	Raton m_raton;
	Ventana *m_ventana_actual;
	Ventana *m_ventana_lista_archivos;

	Configuracion m_configuracion;
	Datos_Musica m_musica;

	int m_fotograma;

	void reiniciar_contador_inactividad();
	bool abrir_midi_seleccionado();
	void iniciar_hilo_midi();
	void detener_hilo_midi();
	static void hilo_midi(Controlador_Juego *controlador);
public:
	Controlador_Juego();
	~Controlador_Juego();
	void asignar_administrador_recursos(Administrador_Recursos * recursos);
	void crear_ventana_inicial();
	Administrador_Recursos *obtener_administrador_recursos();
	void actualizar();
	void actualizar_midi(unsigned int diferencia_tiempo);

	std::int64_t tiempo_fotograma();
	bool es_pantalla_completa();
	bool modo_alambre_activado();
	bool terminar();
	bool hilo_activo();

	Raton *raton();

	void eventos_raton();
	void eventos_teclado(Tecla tecla, bool estado);
	void evento_ventana(float ancho, float alto);
	void evento_salir();

	void evento_soltar_contenido(bool es_archivo, const std::string &contenido);
	void evento_soltar_inicio();
	void evento_soltar_fin();

	bool abrir_archivo(const std::string &direccion);

	Configuracion *configuracion();
};

#endif
