#include "secuencia_midi.h++"

#include <cmath>
#include "../registro.h++"

Secuencia_Midi::Secuencia_Midi(unsigned short tics_por_negra)
{
	m_tics_por_negra = tics_por_negra;

	m_duracion_en_microsegundo = 0;
	m_duracion_en_tics = 0;

	m_tiempo_midi_actual = 0;
	m_tiempo_proximo_evento = 0;
	m_ultimo_evento = 0;

	m_seguidor_eventos = new Seguidor_Eventos(false);

	m_inicial_ppm = 120;
	m_inicial_compas_numerador = 4;
	m_inicial_compas_denominador = 4;
	m_inicial_armadura = Armadura_0;//Sin Armadura
	m_inicial_escala = Escala_Mayor;//Escala Mayor

	for(unsigned char g=0; g<GRUPOS_MIDI; g++)
	{
		for(unsigned char em=0; em<EspecificacionMidi::Desactivado; em++)
			m_especificacion_midi[g][em] = false;
	}
}

Secuencia_Midi::~Secuencia_Midi()
{
	delete m_seguidor_eventos;

	for(std::uint64_t x=0; x<m_eventos_secuencia.size(); x++)
		delete m_eventos_secuencia[x];

	for(unsigned short x=0; x<m_pistas.size(); x++)
		delete m_pistas[x];
}

Pista_Midi *Secuencia_Midi::crear_pista(unsigned char grupo, unsigned char canal, EspecificacionMidi especificacion, unsigned int programa_pista, unsigned char percusion_gs)
{
	unsigned char programa = static_cast<unsigned char>((programa_pista >> 24) & 0xFF);
	unsigned char banco_izquierdo = (programa_pista >> 8) & 0xFF;
	unsigned char banco_derecho = programa_pista & 0xFF;

	Pista_Midi *nueva_pista = new Pista_Midi();
	nueva_pista->grupo(grupo);
	nueva_pista->canal(canal);
	nueva_pista->programa(programa);
	nueva_pista->banco_izquierdo(banco_izquierdo);
	nueva_pista->banco_derecho(banco_derecho);

	if(especificacion == EspecificacionMidi::MPC)
	{
		if(canal == 9 || canal == 15)
			nueva_pista->percusion(Percusion::GM);
	}
	else if(especificacion == EspecificacionMidi::GS && percusion_gs < 255)
	{
		//Si es menor a 255 indica que fue establecido por un mensaje exclusivo del sistema, no debe entrar en
		//ninguna opcion más, GS puede cambiar la percusion del canal 10 a instrumento
		//0 es Instrumento normal
		//1 es Map 1
		//2 es Map 2
		if(percusion_gs == 1)
			nueva_pista->percusion(Percusion::GS_Map1);
		else if(percusion_gs == 2)
			nueva_pista->percusion(Percusion::GS_Map2);
	}
	else if(banco_izquierdo == 127 || banco_izquierdo == 126 || banco_izquierdo == 64)
	{
		//La percision se selecciona con el banco izquierdo en XG
		//127 Percusion
		//126 Percusion SFX
		//64 Banco SFX individual
		if(banco_izquierdo == 127)
			nueva_pista->percusion(Percusion::XG_127);
		else if(banco_izquierdo == 126)
			nueva_pista->percusion(Percusion::XG_126);
		else if(banco_izquierdo == 64)
			nueva_pista->percusion(Percusion::XG_64);
	}
	else if((canal == 9 && banco_izquierdo != 121 && especificacion == EspecificacionMidi::GM2) ||
			(canal == 10 && banco_izquierdo == 120))
	{
		//En GM2 el canal 10 por defecto es percusion pero puede ser cambiado a instrumento con la seleccion de banco 121
		//El canal 11 tambien puede ser percusion pero solo si se selecciona el banco 120
		nueva_pista->percusion(Percusion::GM2);
	}
	else if(banco_izquierdo != 121)
	{
		//En GM la percusion siempre esta por el canal 10
		if(canal == 9)
			nueva_pista->percusion(Percusion::GM);
	}
	return nueva_pista;
}

bool Secuencia_Midi::especificacion_midi(EspecificacionMidi *especificacion, unsigned char grupo, std::vector<unsigned char> *datos)
{
	if(datos->size() == 4 && (*datos)[0] == 0x7E && (*datos)[2] == 0x09)
	{
		if((*datos)[3] == 0x01)
		{
			//7E 7F 09 01
			especificacion[grupo] = EspecificacionMidi::GM;
			return true;
		}
		else if((*datos)[3] == 0x02)
		{
			//7E 7F 09 02
			especificacion[grupo] = EspecificacionMidi::Desactivado;
			return true;
		}
		else if((*datos)[3] == 0x03)
		{
			//7E 7F 09 03
			especificacion[grupo] = EspecificacionMidi::GM2;
			return true;
		}
	}
	else if(datos->size() == 7 &&
			(*datos)[0] == 0x43 &&
			(*datos)[2] == 0x4C &&
			(*datos)[3] == 0x00 &&
			(*datos)[4] == 0x00 &&
			(*datos)[5] == 0x7E &&
			(*datos)[6] == 0x00)
	{
		//43 10 4C 00 00 7E 00
		especificacion[grupo] = EspecificacionMidi::XG;
		return true;
	}
	else if(datos->size() == 9 &&
			(*datos)[0] == 0x41 &&
			//(*datos)[1] == 0x10 &&
			//(*datos)[2] == 0x42 &&
			//(*datos)[3] == 0x12 &&
			(*datos)[4] == 0x40 &&
			(*datos)[5] == 0x00 &&
			(*datos)[6] == 0x7F &&
			(*datos)[7] == 0x00 &&
			(*datos)[8] == 0x41)
	{
		//41 10 42 12 40 00 7F 00 41
		especificacion[grupo] = EspecificacionMidi::GS;
		return true;
	}
	return false;
}

bool Secuencia_Midi::perscusion_gs(std::vector<unsigned char> *datos, unsigned char *canal, unsigned char *tipo)
{
	//Detecta percusion en GS (pag 60 SC-8850_e3)
	if(	datos->size() == 9 &&
		(*datos)[0] == 0x41 &&	//Fabricante Roland
		//(*datos)[1] == 0x10 &&	//ID Dispositivo
		//(*datos)[2] == 0x42 &&	//ID Modelo
		//(*datos)[3] == 0x12 &&	//ID Comando
		(*datos)[4] == 0x40 &&			//Direccion
		((*datos)[5] & 0xF0) == 0x10 &&	//Direccion y canal
		(*datos)[6] == 0x15)			//Direccion
	{
		*canal = (*datos)[5] & 0x0F;
		//En GS el canal 10 se representa con 0
		//hay que restar uno a los valores del 1 al 9 para ajustarlo
		if(*canal == 0)
			*canal = 9;
		else if(*canal < 10)
			*canal = *canal - 1;

		*tipo =  (*datos)[7];

		//Suma de verificación (pag 245 SC-8850_e3)
		unsigned int verificacion = static_cast<unsigned int>((*datos)[4]);
		verificacion += static_cast<unsigned int>((*datos)[5]);
		verificacion += static_cast<unsigned int>((*datos)[6]);
		verificacion += static_cast<unsigned int>((*datos)[7]);
		verificacion = verificacion % 128;
		verificacion = 128 - verificacion;
		if((*datos)[8] != verificacion)
			return false;

		return true;
	}
	return false;
}

void Secuencia_Midi::tics_por_negra(unsigned short tics_por_negra)
{
	m_tics_por_negra = tics_por_negra;
}

void Secuencia_Midi::agregar_datos_evento(Datos_Evento* datos_evento)
{
	//Guarda el evento
	m_eventos_secuencia.push_back(datos_evento);
}

void Secuencia_Midi::crear_pistas(bool ordenar)
{
	unsigned int BANCO_BEI = 0x0000FF00;
	unsigned int BANCO_BED = 0x000000FF;

	//Se ordenan por los tics acumulados, solo si hay más de un fragmento en la secuencia
	//porque de lo contrario ya esta ordenado
	if(ordenar)
		std::sort(m_eventos_secuencia.begin(), m_eventos_secuencia.end(), Datos_Evento::comparar_puntero);

	EspecificacionMidi especificacion_sin_confirmar[GRUPOS_MIDI];
	EspecificacionMidi especificacion_confirmada[GRUPOS_MIDI];//Se confirma con un programa
	EspecificacionMidi especificacion_usada[GRUPOS_MIDI];//Al menos usada con una nota

	//Almacena la informacion del cambio de programa_pista al estilo de MIDI 2.0, excepto que el
	//programa y los bancos tiene el octavo bite reservado
	//Programa + Reservado + Banco BEI + Banco BED
	unsigned int banco_sin_confirmar[GRUPOS_MIDI][CANALES_MIDI];
	unsigned int programa_sin_confirmar[GRUPOS_MIDI][CANALES_MIDI];
	unsigned int programa_pista[GRUPOS_MIDI][CANALES_MIDI];
	unsigned char percusion_gs[GRUPOS_MIDI][CANALES_MIDI];
	unsigned int programa_id_nota[GRUPOS_MIDI][CANALES_MIDI][128];//Almacena el ultimo programa por cada canal y nota

	std::vector<unsigned char> datos_sysex[GRUPOS_MIDI];

	//Inicia todos los programas de todos los canales y notas en 0
	for(unsigned char grupo=0; grupo<GRUPOS_MIDI; grupo++)
	{
		especificacion_sin_confirmar[grupo] = EspecificacionMidi::Desactivado;
		especificacion_confirmada[grupo] = EspecificacionMidi::Desactivado;
		especificacion_usada[grupo] = EspecificacionMidi::Desactivado;

		for(unsigned char canal=0; canal<CANALES_MIDI; canal++)
		{
			banco_sin_confirmar[grupo][canal] = 0;
			programa_sin_confirmar[grupo][canal] = 0;
			programa_pista[grupo][canal] = 0;
			percusion_gs[grupo][canal] = 255;

			for(unsigned char id=0; id<128; id++)
				programa_id_nota[grupo][canal][id] = 0;
		}
	}

	bool primer_ppm = false, primer_compas = false, primer_armadura = false;

	double us_acumulado = 0;
	std::uint64_t tics_anterior = 0;
	double tiempo_por_tics = static_cast<double>(MICROSEGUNDOS_POR_NEGRA) / static_cast<double>(m_tics_por_negra);

	std::map<std::uint64_t, Pista_Midi*> pistas_midi;
	std::map<std::uint64_t, double> tempos;
	tempos[0] = MICROSEGUNDOS_POR_NEGRA / m_tics_por_negra;
	std::map<std::uint64_t, Evento_Midi*> compas;

	unsigned int ultima_armadura_tonalidad = 0;

	//Se crean las pistas
	char armadura_actual = Armadura_0;
	unsigned char escala_actual = Escala_Mayor;
	for(std::uint64_t n=0; n<m_eventos_secuencia.size(); n++)
	{
		Datos_Evento *d = m_eventos_secuencia[n];
		Evento_Midi *e = d->evento();

		//Calcula los microsegundos acumulados
		//Tengo que calcular el delta en relacion al evento anterior
		std::uint64_t delta = d->tics() - tics_anterior;
		tics_anterior = d->tics();
		us_acumulado += static_cast<double>(delta) * tiempo_por_tics;
		d->microsegundos(static_cast<std::int64_t>(us_acumulado));

		if(e->contiene_grupo())
		{
			if(datos_sysex[e->grupo()].size() > 0)
			{
				if(	e->tipo_mensaje() != TipoMensaje_ExclusivoDelSistema7bits &&
					e->tipo_mensaje() != TipoMensaje_ComunYTiempoReal)
				{
					bool respuesta = false;
					//Interrumpe sysex actual
					respuesta = this->especificacion_midi(especificacion_sin_confirmar, e->grupo(), &(datos_sysex[e->grupo()]));

					if(respuesta)
					{
						for(unsigned char c=0; c<CANALES_MIDI; c++)
						{
							banco_sin_confirmar[e->grupo()][c] = 0;
							programa_sin_confirmar[e->grupo()][c] = 0;
							programa_pista[e->grupo()][c] = 0;
						}
					}
					else
					{
						//No es una especificacion MIDI, se prueba si es una percusion GS
						unsigned char canal = 0;
						unsigned char tipo = 0;
						respuesta = this->perscusion_gs(&(datos_sysex[e->grupo()]), &canal, &tipo);
						if(respuesta)
							percusion_gs[e->grupo()][canal] = tipo;
					}
					datos_sysex[e->grupo()].clear();
				}
			}
		}

		if(e->tipo_mensaje() == TipoMensaje_DatosFlexibles)
		{
			if(e->tipo_dato_flexible() == DatosFlexibles_Tempo)
			{
				double tempo_us = e->tempo_en_microsegundos();
				tiempo_por_tics = tempo_us / static_cast<double>(m_tics_por_negra);

				tempos[d->tics()] = tiempo_por_tics;
				if(!primer_ppm || tics_anterior == 0)
				{
					m_inicial_ppm = Funciones_Midi::us_a_ppm(static_cast<unsigned int>(tempo_us));
					primer_ppm = true;
				}
				m_tempo.push_back(d);
			}
			else if(e->tipo_dato_flexible() == DatosFlexibles_Compas)
			{
				compas[d->tics()] = e;
				if(!primer_compas || tics_anterior == 0)
				{
					m_inicial_compas_numerador = e->compas_numerador();
					m_inicial_compas_denominador = e->compas_denominador();
					primer_compas = true;
				}
				m_compas.push_back(d);
			}
			else if(e->tipo_dato_flexible() == DatosFlexibles_Armadura)
			{
				unsigned int armadura = static_cast<unsigned int>(e->armadura()+7);//Quita numeros negativos de -7
				unsigned int armadura_tonalidad = armadura << 8 | e->escala();
				if(armadura_tonalidad != ultima_armadura_tonalidad)
				{
					//Crea un marcador solo cuando la armadura cambia, pueden haber muchos eventos sin cambio
					ultima_armadura_tonalidad = armadura_tonalidad;
					std::string texto = Nombre_Armadura(e->armadura(), e->escala()) + " " + Nombre_Escala(e->escala());
					m_marcadores.push_back(std::tuple<std::int64_t, unsigned char, std::string>(us_acumulado, 0, texto));

					armadura_actual = e->armadura();
					escala_actual = e->escala();
				}
				if(!primer_armadura || tics_anterior == 0)
				{
					m_inicial_armadura = armadura_actual;
					m_inicial_escala = escala_actual;
					primer_armadura = true;
				}
				m_armadura.push_back(d);
			}
			else if(e->version_midi() == VersionMidi::Version_1)
			{
				if(e->tipo_metaevento_midi1() == MetaEventoMidi_Marcador)
					m_marcadores.push_back(std::tuple<std::int64_t, unsigned char, std::string>(us_acumulado, 1, e->texto()));
				else if(e->tipo_metaevento_midi1() == MetaEventoMidi_PuntoReferencia)
					m_marcadores.push_back(std::tuple<std::int64_t, unsigned char, std::string>(us_acumulado, 2, e->texto()));
				else if(e->tipo_metaevento_midi1() == MetaEventoMidi_EspecificoDelSecuenciador)
				{
					//FF 7F 00 00 41
					if(e->largo_datos() == 5)
					{
						unsigned char *datos = e->datos();
						if(datos[1] == 0x7F &&
							datos[2] == 0x00 &&
							datos[3] == 0x00 &&
							datos[4] == 0x41)
							especificacion_sin_confirmar[e->grupo()] = EspecificacionMidi::MPC;
					}
				}
			}
		}
		else if(e->tipo_mensaje() == TipoMensaje_ExclusivoDelSistema7bits)
		{
			bool mensaje_completo = false;
			bool restablecer = false;
			bool restablecer_m2 = false;
			unsigned char canal_gs = 0;
			unsigned char tipo_gs = 0;
			if(e->version_midi() == VersionMidi::Version_1)
			{
				unsigned char *datos = e->datos();

				//Se omite el byte F0 y F7
				for(unsigned char x=1; x<e->largo_datos()-1; x++)
					datos_sysex[e->grupo()].push_back(datos[x]);

				mensaje_completo = true;
			}
			else if(e->version_midi() == VersionMidi::Version_2)
			{
				unsigned char *datos = e->datos();
				unsigned char estado = e->estado_mensaje();

				//El mensaje anterior finaliza solo porque empieza otro mensaje
				//hay que analizarlo de inmediato para continuar con el actual
				if(	datos_sysex[e->grupo()].size() > 0 &&
					(estado == EstadoMensaje_Unico || estado == EstadoMensaje_Inicio))
				{
					restablecer_m2 = this->especificacion_midi(especificacion_sin_confirmar, e->grupo(), &(datos_sysex[e->grupo()]));

					//No es una especificacion MIDI, se prueba otro tipo de sysex
					if(!restablecer_m2)
					{
						bool respuesta = this->perscusion_gs(&(datos_sysex[e->grupo()]), &canal_gs, &tipo_gs);
						if(respuesta)
							percusion_gs[e->grupo()][canal_gs] = tipo_gs;
					}
					datos_sysex[e->grupo()].clear();
				}

				//Se procesa el mensaje actual
				for(unsigned char x=0; x<e->numero_bytes_mensaje(); x++)
					datos_sysex[e->grupo()].push_back(datos[x+2]);

				if(estado == EstadoMensaje_Unico || estado == EstadoMensaje_Fin)
					mensaje_completo = true;
			}

			if(mensaje_completo)
			{
				restablecer = this->especificacion_midi(especificacion_sin_confirmar, e->grupo(), &(datos_sysex[e->grupo()]));
				//No es una especificacion MIDI, se prueba otro tipo de sysex
				if(!restablecer)
				{
					bool respuesta = this->perscusion_gs(&(datos_sysex[e->grupo()]), &canal_gs, &tipo_gs);
					if(respuesta)
						percusion_gs[e->grupo()][canal_gs] = tipo_gs;
				}
				datos_sysex[e->grupo()].clear();
			}

			//Se restablecen los programas por un evento exclusivo del sistema GM GM2 XG o GS, todos los canales
			if(restablecer_m2 || restablecer)
			{
				for(unsigned char c=0; c<CANALES_MIDI; c++)
				{
					banco_sin_confirmar[e->grupo()][c] = 0;
					programa_sin_confirmar[e->grupo()][c] = 0;
					programa_pista[e->grupo()][c] = 0;
				}
			}
		}
		else if(e->tipo_mensaje() == TipoMensaje_VozDeCanalMidi1)
		{
			if(e->tipo_voz_de_canal() == EventoMidi_Controlador)
			{
				//Se obtiene la informacion del ultimo banco seleccionado
				unsigned int valor = e->controlador_valor();
				if(e->controlador_mensaje() == MensajeControlador_SeleccionDeBanco_BEI)
					banco_sin_confirmar[e->grupo()][e->canal()] = (banco_sin_confirmar[e->grupo()][e->canal()] & ~BANCO_BEI) | valor << 8;
				else if(e->controlador_mensaje() == MensajeControlador_SeleccionDeBanco_BED)
					banco_sin_confirmar[e->grupo()][e->canal()] = (banco_sin_confirmar[e->grupo()][e->canal()] & ~BANCO_BED) | valor;
				else if(e->controlador_mensaje() == MensajeControlador_RestablecerTodosLosControles)
				{
					//Se restablece por el controlador el canal actual
					banco_sin_confirmar[e->grupo()][e->canal()] = 0;
					programa_sin_confirmar[e->grupo()][e->canal()] = 0;
					programa_pista[e->grupo()][e->canal()] = 0;
				}
			}
			else if(e->tipo_voz_de_canal() == EventoMidi_Programa)
			{
				especificacion_confirmada[e->grupo()] = especificacion_sin_confirmar[e->grupo()];

				//Se obtiene la informacion del ultimo programa seleccionado
				//Confirma la seleccion de banco
				unsigned int valor = e->programa();
				programa_sin_confirmar[e->grupo()][e->canal()] = valor << 24 | banco_sin_confirmar[e->grupo()][e->canal()];
			}
		}
		else if(e->tipo_mensaje() == TipoMensaje_VozDeCanalMidi2)
		{
			if(e->tipo_voz_de_canal() == EventoMidi_Programa)
			{
				especificacion_confirmada[e->grupo()] = especificacion_sin_confirmar[e->grupo()];
				if(e->programa_banco_valido() == 1)
				{
					//El programa contiene bancos validos
					unsigned int banco_izquierdo = e->programa_banco_izquierdo();
					unsigned int banco_derecho = e->programa_banco_derecho();

					banco_sin_confirmar[e->grupo()][e->canal()] = banco_izquierdo << 8 | banco_derecho;
				}

				//Se obtiene la informacion del ultimo programa seleccionado
				//Confirma la seleccion de banco
				unsigned int valor = e->programa();
				programa_sin_confirmar[e->grupo()][e->canal()] = valor << 24 | banco_sin_confirmar[e->grupo()][e->canal()];
			}
		}

		if(e->es_nota())
		{
			std::uint64_t fragmento = d->fragmento();
			std::uint64_t grupo = e->grupo();
			std::uint64_t canal = e->canal();

			//Se confirma la seleccion del programa y banco
			programa_pista[grupo][canal] = programa_sin_confirmar[grupo][canal];
			especificacion_usada[grupo] = especificacion_confirmada[grupo];

			//Se detecta la espesificación MIDI de forma indirecta
			if(especificacion_usada[e->grupo()] == EspecificacionMidi::Desactivado)
			{
				unsigned char banco_BEI = ((programa_pista[e->grupo()][e->canal()] & BANCO_BEI) >> 8) & 0xFF;
				//unsigned char banco_BED = ((programa_pista[grupo][canal] & BANCO_BED)) & 0xFF;
				if(banco_BEI == 120 || banco_BEI == 121)
					especificacion_usada[e->grupo()] = EspecificacionMidi::GM2;
				else if(banco_BEI == 127 || banco_BEI == 126 || banco_BEI == 64)
					especificacion_usada[e->grupo()] = EspecificacionMidi::XG;
			}

			if(especificacion_usada[grupo] != EspecificacionMidi::Desactivado)
				m_especificacion_midi[grupo][especificacion_usada[grupo]] = true;

			//Clave para la pista, 16 bit fragmento + 8 bits grupo + 8 bits canal + 32 bits programa_pista
			std::uint64_t clave_pista = fragmento << 48 | grupo << 40 | canal << 32 | programa_pista[grupo][canal];

			//En caso de haber cambiado de programa antes de que terminara la nota actual,
			//se recupera el programa que inicio la nota para insertar el evento en la pista correcta.
			//No importa el fragmento o pista porque eso depende solo de la nota actual
			//Recuerda por cada canal y cada nota, cual fue el programa que inicio la nota
			if(e->tipo_voz_de_canal() == EventoMidi_NotaEncendida)
				programa_id_nota[grupo][canal][e->id_nota()] = programa_pista[grupo][canal];
			else
			{
				//Si el programa actual es distinto al que inicio la nota, cambia la clave con el programa correcto
				if(programa_id_nota[grupo][canal][e->id_nota()] != programa_pista[grupo][canal])
					clave_pista = fragmento << 48 | grupo << 40 | canal << 32 | programa_id_nota[grupo][canal][e->id_nota()];
			}

			//Se busca la pista que concuerde con la clave
			std::map<std::uint64_t, Pista_Midi*>::iterator pista_buscada = pistas_midi.find(clave_pista);

			//Si la pista no existe se crea
			Pista_Midi* pista_actual;
			if(pista_buscada != pistas_midi.end())
				pista_actual = pista_buscada->second;//La pista ya existe
			else
			{
				//grupo & 0x0F, me resulta mas corto que un static_cast en este caso
				if(pistas_midi.size() < std::numeric_limits<unsigned short>::max() - 1)
					pista_actual = this->crear_pista(grupo & 0x0F, canal &0x0F, especificacion_usada[grupo], programa_pista[grupo][canal], percusion_gs[grupo][canal]);
				else
				{
					Registro::Depurar("Se alcanzo el numero maximo de pistas MIDI");
					pista_actual = pistas_midi.begin()->second;//Se guarda el evento en la primera pista que encuentre
				}
				pistas_midi[clave_pista] = pista_actual;
			}

			//Se agrega la nota actual
			d->pista(pista_actual);//Se guarda la pista en los datos del evento
			pista_actual->agregar_evento(d, armadura_actual, escala_actual);
		}
	}

	//Guarda la duracion del archivo MIDI
	m_duracion_en_microsegundo = static_cast<std::int64_t>(us_acumulado);
	m_duracion_en_tics = tics_anterior;

	//Se guardan las pistas, se pasan del std::map al std::vector
	m_pistas.reserve(pistas_midi.size());
	std::map<std::uint64_t, Pista_Midi*>::iterator pista_actual;
	unsigned short id_pista = 0;
	for(pista_actual = pistas_midi.begin(); pista_actual != pistas_midi.end(); pista_actual++)
	{
		Pista_Midi *p = pista_actual->second;
		p->procesar_notas(id_pista);
		m_pistas.push_back(p);
		id_pista++;
	}

	//Crea las barras del compas
	std::uint64_t tics_barra = 0;
	std::int64_t microsegundo_acumulado = 0;

	//Compas predeterminado
	unsigned char numerador = 4;//Valor predeterminado 4/4 segun estandar MIDI
	unsigned char denominador = 4;
	unsigned int duracion_redonda = m_tics_por_negra * 4;//De tics por negra a tics por redonda
	std::uint64_t duracion_compas = (duracion_redonda / denominador) * numerador;
	std::uint64_t proximo_compas = 0;
	std::map<std::uint64_t, Evento_Midi*>::iterator i_compas = compas.begin();
	if(i_compas == compas.end())
		proximo_compas = std::numeric_limits<std::uint64_t>::max();
	else
	{
		//El primer evento se usa para alinear las barras, no siempre se recibe en el tic 0, aveces esta desplazado
		proximo_compas = i_compas->first;
		tics_barra = proximo_compas;
	}

	//Tempo
	std::uint64_t tics_acumulado = 0;
	tiempo_por_tics = MICROSEGUNDOS_POR_NEGRA / m_tics_por_negra;//Predeterminado de 120 PPM
	std::uint64_t proximo_tempo = 0;
	std::map<std::uint64_t, double>::iterator i_tempo = tempos.begin();
	if(i_tempo == tempos.end())
		proximo_tempo = std::numeric_limits<std::uint64_t>::max();
	else
		proximo_tempo = i_tempo->first;


	//Se aproxima al compas final a la ultima nota mas cercana con duracion_compas-1
	unsigned short tolerancia = m_tics_por_negra / 96;
	while(tics_barra <= m_duracion_en_tics ||
		(tics_barra > m_duracion_en_tics && tics_barra >= duracion_compas && tics_barra - duracion_compas < m_duracion_en_tics && m_duracion_en_tics - (tics_barra - duracion_compas) > tolerancia)
	)
	{
		//Cambio de Compas, revisa si hay un cambio de compas antes de crear uno nuevo, revisa en el rango de la duración actual
		if(proximo_compas < (tics_barra + duracion_compas))
		{
			if(proximo_compas < tics_barra)
			{
				//Cambio adelantado
				Registro::Depurar("Cambio de compás adelantado, Objetivo:  " + std::to_string(tics_barra) + " Obtenido: " + std::to_string(proximo_compas) + " Errado por: " + std::to_string(tics_barra - proximo_compas));
			}
			if(proximo_compas > tics_barra)
			{
				//Cambio Atrasado
				Registro::Depurar("Cambio de compás atrasado, Objetivo:  " + std::to_string(tics_barra) + " Obtenido: " + std::to_string(proximo_compas) + " Errado por: " + std::to_string(proximo_compas - tics_barra));
			}

			numerador = i_compas->second->compas_numerador();
			denominador = i_compas->second->compas_denominador();
			duracion_compas = (duracion_redonda / denominador) * numerador;

			//Se cambia el compas
			i_compas++;
			if(i_compas != compas.end())
				proximo_compas = i_compas->first;
			else
				proximo_compas = std::numeric_limits<std::uint64_t>::max();
		}

		//Se calculan los microsegundos deacuerdo al tempo
		if(tics_barra < proximo_tempo)
		{
			//Este evento comienza antes del proximo cambio de tempo
			//Se calcula el tiempo delta porque no es valido al cambiar un evento de pista
			unsigned int tiempo_delta = static_cast<unsigned int>(tics_barra - tics_acumulado);
			microsegundo_acumulado += static_cast<std::int64_t>(static_cast<double>(tiempo_delta) * tiempo_por_tics);
			tics_acumulado = tics_barra;
		}
		else
		{
			//Este evento comienza despues del proximo o varios cambios de tempo
			//Suma todos los tiempos por cada cambio de tempo que ocurre
			while(tics_barra >= proximo_tempo)
			{
				//Calcula los tics que son tocados al tempo actual y los suma
				std::uint64_t faltante = proximo_tempo - tics_acumulado;
				microsegundo_acumulado += static_cast<std::int64_t>(static_cast<double>(faltante) * tiempo_por_tics);
				tics_acumulado += faltante;

				//Busca el siguiente tempo, de no haber mas se queda con el ultimo
				tiempo_por_tics = i_tempo->second;
				i_tempo++;
				if(i_tempo != tempos.end())
					proximo_tempo = i_tempo->first;
				else
					proximo_tempo = std::numeric_limits<std::uint64_t>::max();
			}
			//Calcula los tics que aun quedan con el ultimo tempo actual
			std::uint64_t faltante = tics_barra - tics_acumulado;
			microsegundo_acumulado += static_cast<std::int64_t>(static_cast<double>(faltante) * tiempo_por_tics);
			tics_acumulado += faltante;
		}
		m_barra_compas.push_back(microsegundo_acumulado);
		//Se calculan los tics
		tics_barra += duracion_compas;
	}
}

void Secuencia_Midi::calcular_duracion(bool ordenar)
{
	//Se ordenan por los tics acumulados, solo si hay más de un fragmento en la secuencia
	//porque de lo contrario ya esta ordenado
	if(ordenar)
		std::sort(m_eventos_secuencia.begin(), m_eventos_secuencia.end(), Datos_Evento::comparar_puntero);

	double us_acumulado = 0;
	std::uint64_t tics_anterior = 0;
	double tiempo_por_tics = static_cast<double>(MICROSEGUNDOS_POR_NEGRA) / static_cast<double>(m_tics_por_negra);
	for(std::uint64_t n=0; n<m_eventos_secuencia.size(); n++)
	{
		Datos_Evento *d = m_eventos_secuencia[n];
		Evento_Midi *e = d->evento();

		//Calcula los microsegundos acumulados
		//Tengo que calcular el delta en relacion al evento anterior
		std::uint64_t delta = d->tics() - tics_anterior;
		tics_anterior = d->tics();
		us_acumulado += static_cast<double>(delta) * tiempo_por_tics;

		d->microsegundos(static_cast<std::int64_t>(us_acumulado));

		if(	e->tipo_mensaje() == TipoMensaje_DatosFlexibles)
		{
			if(e->tipo_dato_flexible() == DatosFlexibles_Tempo)
				tiempo_por_tics = static_cast<double>(e->tempo()) / static_cast<double>(m_tics_por_negra);
		}
	}

	//Guarda la duracion del archivo MIDI
	m_duracion_en_microsegundo = static_cast<std::int64_t>(us_acumulado);
	m_duracion_en_tics = tics_anterior;
}

void Secuencia_Midi::crear_pista_vacia_midi1(unsigned char canal, unsigned char programa, unsigned char banco_bei, unsigned char banco_bed)
{
	unsigned int programa_int = programa;
	unsigned int banco_bei_int = banco_bei;

	programa_int = programa_int << 24;
	banco_bei_int = banco_bei_int << 8;
	unsigned int programa_pista = programa_int | banco_bei_int | banco_bed;

	if(m_pistas.size() < std::numeric_limits<unsigned short>::max() - 1)
	{
		unsigned short id_pista = static_cast<unsigned short>(m_pistas.size());

		Evento_Midi *evento_nuevo;
		Datos_Evento *evento_datos;
		unsigned char *datos;

		//Metaeventos, no se guarda el largo variable
		datos = new unsigned char[5] {0xFF,0x51,0x07,0xA1,0x20};//Tempo 120ppm
		evento_nuevo = new Evento_Midi(datos, 5, VersionMidi::Version_1);
		evento_datos = new Datos_Evento(0, id_pista, 0, 0, evento_nuevo);
		this->agregar_nuevo_evento(evento_datos);

		datos = new unsigned char[6] {0xFF,0x58,0x04,0x02,0x18,0x08};//Compas
		evento_nuevo = new Evento_Midi(datos, 6, VersionMidi::Version_1);
		evento_datos = new Datos_Evento(0, id_pista, 0, 0, evento_nuevo);
		this->agregar_nuevo_evento(evento_datos);

		datos = new unsigned char[4] {0xFF,0x59,0x00,0x00};//Armadura y escala
		evento_nuevo = new Evento_Midi(datos, 4, VersionMidi::Version_1);
		evento_datos = new Datos_Evento(0, id_pista, 0, 0, evento_nuevo);
		this->agregar_nuevo_evento(evento_datos);

		//Se crean los eventos para banco y programa
		evento_nuevo = new Evento_Midi(EventoMidi_Controlador);
		evento_nuevo->canal(canal);
		evento_nuevo->controlador_mensaje(MensajeControlador_Balance_BEI);
		evento_nuevo->controlador_valor(banco_bei);
		evento_datos = new Datos_Evento(0, id_pista, 0, 0, evento_nuevo);
		this->agregar_nuevo_evento(evento_datos);

		evento_nuevo = new Evento_Midi(EventoMidi_Controlador);
		evento_nuevo->canal(canal);
		evento_nuevo->controlador_mensaje(MensajeControlador_Balance_BED);
		evento_nuevo->controlador_valor(banco_bed);
		evento_datos = new Datos_Evento(0, id_pista, 0, 0, evento_nuevo);
		this->agregar_nuevo_evento(evento_datos);

		evento_nuevo = new Evento_Midi(EventoMidi_Programa);
		evento_nuevo->canal(canal);
		evento_nuevo->programa(programa);
		evento_datos = new Datos_Evento(0, id_pista, 0, 0, evento_nuevo);
		this->agregar_nuevo_evento(evento_datos);
		m_ultimo_evento = m_eventos_secuencia.size();

		m_barra_compas.push_back(0);//Primera barra de compas en tiempo 0
		std::string texto = Nombre_Armadura(0, 0) + " " + Nombre_Escala(0);
		m_marcadores.push_back(std::tuple<std::int64_t, unsigned char, std::string>(0, 0, texto));//Do Mayor

		Pista_Midi *nueva_pista = this->crear_pista(0, canal, EspecificacionMidi::GM, programa_pista, 0);
		nueva_pista->id_pista(id_pista);
		m_pistas.push_back(nueva_pista);
	}
}

void Secuencia_Midi::crear_pista_vacia_midi2(unsigned char grupo, unsigned char canal, unsigned char programa, unsigned char banco_bei, unsigned char banco_bed)
{
	unsigned int programa_int = programa;
	unsigned int banco_bei_int = banco_bei;

	programa_int = programa_int << 24;
	banco_bei_int = banco_bei_int << 8;
	unsigned int programa_pista = programa_int | banco_bei_int | banco_bed;

	if(m_pistas.size() < std::numeric_limits<unsigned short>::max() - 1)
	{
		unsigned short id_pista = static_cast<unsigned short>(m_pistas.size());

		Evento_Midi *evento_nuevo;
		Datos_Evento *evento_datos;
		unsigned char *datos;

		//Tiempo Delta 0
		evento_nuevo = new Evento_Midi(TipoMensaje_Utilidad, MensajeUtilidad_TiempoDelta);
		evento_datos = new Datos_Evento(0, id_pista, 0, 0, evento_nuevo);
		this->agregar_nuevo_evento(evento_datos);

		//Divición de Tiempo
		evento_nuevo = new Evento_Midi(TipoMensaje_Utilidad, MensajeUtilidad_DivicionDeTiempo);
		evento_nuevo->division_de_tiempo(480);
		evento_datos = new Datos_Evento(0, id_pista, 0, 0, evento_nuevo);
		this->agregar_nuevo_evento(evento_datos);

		//Tempo
		datos = new unsigned char[16] {0xD0,0x10,0x00,0x00, 0x02,0xFA,0xF0,0x80, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00};
		evento_nuevo = new Evento_Midi(datos, 16, VersionMidi::Version_2);
		evento_datos = new Datos_Evento(0, id_pista, 0, 0, evento_nuevo);
		this->agregar_nuevo_evento(evento_datos);

		//Compas
		datos = new unsigned char[16] {0xD0,0x10,0x00,0x01, 0x02,0x01,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00};
		evento_nuevo = new Evento_Midi(datos, 16, VersionMidi::Version_2);
		evento_datos = new Datos_Evento(0, id_pista, 0, 0, evento_nuevo);
		this->agregar_nuevo_evento(evento_datos);

		//Editor Parte 1
		datos = new unsigned char[16] {0xD0,0x40,0x01,0x08, 0x43,0x6F,0x6E,0x63, 0x65,0x72,0x74,0x69, 0x73,0x74,0x61,0x20};
		evento_nuevo = new Evento_Midi(datos, 16, VersionMidi::Version_2);
		evento_datos = new Datos_Evento(0, id_pista, 0, 0, evento_nuevo);
		this->agregar_nuevo_evento(evento_datos);

		//Editor Parte 2
		datos = new unsigned char[16] {0xD0,0xC0,0x01,0x08, 0x4D,0x49,0x44,0x49, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00};
		evento_nuevo = new Evento_Midi(datos, 16, VersionMidi::Version_2);
		evento_datos = new Datos_Evento(0, id_pista, 0, 0, evento_nuevo);
		this->agregar_nuevo_evento(evento_datos);

		//Tiempo Delta 0
		evento_nuevo = new Evento_Midi(TipoMensaje_Utilidad, MensajeUtilidad_TiempoDelta);
		evento_datos = new Datos_Evento(0, id_pista, 0, 0, evento_nuevo);
		this->agregar_nuevo_evento(evento_datos);

		//Inicio Fragmento
		datos = new unsigned char[16] {0xF0,0x20,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00};
		evento_nuevo = new Evento_Midi(datos, 16, VersionMidi::Version_2);
		evento_datos = new Datos_Evento(0, id_pista, 0, 0, evento_nuevo);
		this->agregar_nuevo_evento(evento_datos);

		//Tiempo Delta 0
		evento_nuevo = new Evento_Midi(TipoMensaje_Utilidad, MensajeUtilidad_TiempoDelta);
		evento_datos = new Datos_Evento(0, id_pista, 0, 0, evento_nuevo);
		this->agregar_nuevo_evento(evento_datos);

		//Programa
		evento_nuevo = new Evento_Midi(TipoMensaje_VozDeCanalMidi2, EventoMidi_Programa);
		evento_nuevo->grupo(grupo);
		evento_nuevo->canal(canal);
		evento_nuevo->programa_banco_valido(1);
		evento_nuevo->programa_banco_izquierdo(banco_bei);
		evento_nuevo->programa_banco_derecho(banco_bed);
		evento_nuevo->programa(programa);
		evento_datos = new Datos_Evento(0, id_pista, 0, 0, evento_nuevo);
		this->agregar_nuevo_evento(evento_datos);

		m_ultimo_evento = m_eventos_secuencia.size();

		m_barra_compas.push_back(0);//Primera barra de compas en tiempo 0
		std::string texto = Nombre_Armadura(0, 0) + " " + Nombre_Escala(0);
		m_marcadores.push_back(std::tuple<std::int64_t, unsigned char, std::string>(0, 0, texto));//Do Mayor

		Pista_Midi *nueva_pista = this->crear_pista(grupo, canal, EspecificacionMidi::GM, programa_pista, 0);
		nueva_pista->id_pista(id_pista);
		m_pistas.push_back(nueva_pista);
	}
}

void Secuencia_Midi::agregar_nuevo_evento(Datos_Evento *nuevo_evento)
{
	//Cambia la posicion al valor correcto
	unsigned int ultimo_evento = static_cast<unsigned int>(m_eventos_secuencia.size());
	nuevo_evento->posicion(ultimo_evento);
	this->agregar_datos_evento(nuevo_evento);

	if(m_pistas.size() > nuevo_evento->fragmento() && nuevo_evento->evento()->es_nota())
	{
		nuevo_evento->pista(m_pistas[nuevo_evento->fragmento()]);//Almacena la pista a la que pertenece
		m_pistas[nuevo_evento->fragmento()]->agregar_nota(nuevo_evento, Armadura_0, Escala_Mayor);
		m_ultimo_evento = m_eventos_secuencia.size();
	}
}

void Secuencia_Midi::actualizar_tiempo_actual(std::uint64_t tics, std::int64_t microsegundos)
{
	if(m_barra_compas.size() > 0)
	{
		std::int64_t ultimo_comas = m_barra_compas.back();
		if(ultimo_comas < microsegundos)
		{
			//Compas predeterminado
			unsigned char numerador = 4;//Valor predeterminado 4/4
			unsigned char denominador = 4;
			unsigned int duracion_redonda = m_tics_por_negra * 4;//De tics por negra a tics por redonda
			std::uint64_t duracion_compas = (duracion_redonda / denominador) * numerador;
			double tiempo_por_tics = static_cast<double>(MICROSEGUNDOS_POR_NEGRA) / static_cast<double>(m_tics_por_negra);
			std::int64_t microsegundo_compas = static_cast<std::int64_t>(static_cast<double>(duracion_compas) * tiempo_por_tics);
			m_barra_compas.push_back(ultimo_comas + microsegundo_compas);
		}
	}

	if(tics > m_duracion_en_tics)
		m_duracion_en_tics = tics;

	if(microsegundos > m_duracion_en_microsegundo)
		m_duracion_en_microsegundo = microsegundos;

	m_tiempo_midi_actual = microsegundos;
}

void Secuencia_Midi::actualizar(unsigned int diferencia_tiempo)
{
	m_tiempo_midi_actual += diferencia_tiempo;
}

Evento_Midi *Secuencia_Midi::siguiente_evento(unsigned short &pista)
{
	if(m_seguidor_eventos->hay_eventos())
	{
		Evento_Midi *evento = m_seguidor_eventos->siguiente_evento();
		if(evento != nullptr)
			return evento;
	}

	if(m_tiempo_proximo_evento > m_tiempo_midi_actual)
		return nullptr;

	Evento_Midi *evento = nullptr;
	if(m_ultimo_evento < m_eventos_secuencia.size())
	{
		evento = m_eventos_secuencia[m_ultimo_evento]->evento();
		if(evento->es_nota())
		{
			Pista_Midi *pista_origen = m_eventos_secuencia[m_ultimo_evento]->pista();
			if(pista_origen != nullptr)
				pista = pista_origen->id_pista();
			else
				Registro::Depurar("Una nota sin pista: " + evento->a_texto());
		}
		m_ultimo_evento++;

		if(m_ultimo_evento < m_eventos_secuencia.size())
			m_tiempo_proximo_evento = m_eventos_secuencia[m_ultimo_evento]->microsegundos();

		return evento;
	}
	return nullptr;
}

bool Secuencia_Midi::contiene_especificacion_midi(unsigned char grupo, const EspecificacionMidi especificacion) const
{
	if(especificacion < static_cast<unsigned char>(EspecificacionMidi::Desactivado))
		if(m_especificacion_midi[grupo & 0x0F][especificacion])
			return true;
	return false;
}

std::vector<EspecificacionMidi> Secuencia_Midi::lista_especificacion_midi() const
{
	bool en_secuencia[static_cast<unsigned char>(EspecificacionMidi::Desactivado)];
	for(unsigned char em=0; em<static_cast<unsigned char>(EspecificacionMidi::Desactivado); em++)
		en_secuencia[em] = false;

	for(unsigned char g=0; g<GRUPOS_MIDI; g++)
	{
		for(unsigned char em=0; em<static_cast<unsigned char>(EspecificacionMidi::Desactivado); em++)
		{
			if(m_especificacion_midi[g][em])
				en_secuencia[em] = true;
		}
	}

	std::vector<EspecificacionMidi> actual;
	for(unsigned char em=0; em<static_cast<unsigned char>(EspecificacionMidi::Desactivado); em++)
		if(en_secuencia[em])
			actual.push_back(static_cast<EspecificacionMidi>(em));

	return actual;
}

std::vector<Datos_Evento*> *Secuencia_Midi::lista_eventos()
{
	return &m_eventos_secuencia;
}

std::vector<Pista_Midi*> *Secuencia_Midi::pistas()
{
	return &m_pistas;
}

std::vector<Datos_Evento*> *Secuencia_Midi::tempo()
{
	return &m_tempo;
}

std::vector<Datos_Evento*> *Secuencia_Midi::compas()
{
	return &m_compas;
}

std::vector<Datos_Evento*> *Secuencia_Midi::armadura()
{
	return &m_armadura;
}

std::vector<std::int64_t> *Secuencia_Midi::barras_compas()
{
	return &m_barra_compas;
}

std::vector<std::tuple<std::int64_t, unsigned char, std::string>> *Secuencia_Midi::marcadores()
{
	return &m_marcadores;
}

void Secuencia_Midi::cambiar_a(std::int64_t tiempo_nuevo)
{
	if(tiempo_nuevo < m_tiempo_midi_actual)
	{
		m_ultimo_evento = 0;
		m_seguidor_eventos->borrar_registro_eventos();
	}

	if(m_seguidor_eventos->termino_enviar_eventos())
		m_seguidor_eventos->borrar_registro_eventos();

	m_tiempo_midi_actual = tiempo_nuevo;
	m_tiempo_proximo_evento = 0;

	if(tiempo_nuevo < 0)
		return;

	bool grupos_activos[GRUPOS_MIDI];
	bool canales_activos[GRUPOS_MIDI][CANALES_MIDI];

	//Inicializa todas las variables
	for(unsigned char g=0; g<GRUPOS_MIDI; g++)
	{
		grupos_activos[g] = false;
		for(unsigned char c=0; c<CANALES_MIDI; c++)
			canales_activos[g][c] = false;
	}

	//Identifica los canales activos, pueden haber varias pistas que usen los mismos canales
	for(unsigned short pista=0; pista<m_pistas.size(); pista++)
	{
		if(m_pistas[pista]->duracion_pista_microsegundos() >= m_tiempo_midi_actual)
		{
			grupos_activos[m_pistas[pista]->grupo()] = true;
			canales_activos[m_pistas[pista]->grupo()][m_pistas[pista]->canal()] = true;
		}
	}

	Evento_Midi *evento;
	for(; m_ultimo_evento<m_eventos_secuencia.size(); m_ultimo_evento++)
	{
		//Termina porque alcanzo el tiempo requerido
		if(m_eventos_secuencia[m_ultimo_evento]->microsegundos() >= m_tiempo_midi_actual)
		{
			if(m_ultimo_evento < m_eventos_secuencia.size())
				m_tiempo_proximo_evento = m_eventos_secuencia[m_ultimo_evento]->microsegundos();
			break;
		}

		evento = m_eventos_secuencia[m_ultimo_evento]->evento();
		if(evento->contiene_grupo())
		{
			if(!grupos_activos[evento->grupo()])
				continue;

			if(evento->contiene_canal())
				if(!canales_activos[evento->grupo()][evento->canal()])
					continue;
		}

		m_seguidor_eventos->registrar_evento(evento);
	}
	m_seguidor_eventos->procesar_eventos();
}

std::int64_t Secuencia_Midi::posicion_en_microsegundos()
{
	return m_tiempo_midi_actual;
}

std::int64_t Secuencia_Midi::duracion_en_microsegundos()
{
	return m_duracion_en_microsegundo;
}

bool Secuencia_Midi::fin_reproduccion()
{
	if(m_ultimo_evento == m_eventos_secuencia.size())
		return true;
	return false;
}

unsigned int Secuencia_Midi::inicial_ppm()
{
	return m_inicial_ppm;
}

unsigned char Secuencia_Midi::inicial_compas_numerador()
{
	return m_inicial_compas_numerador;
}

unsigned char Secuencia_Midi::inicial_compas_denominador()
{
	return m_inicial_compas_denominador;
}

char Secuencia_Midi::inicial_armadura()
{
	return m_inicial_armadura;
}

unsigned char Secuencia_Midi::inicial_escala()
{
	return m_inicial_escala;
}
