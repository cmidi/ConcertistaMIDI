#ifndef NOTA_MIDI_H
#define NOTA_MIDI_H

#include "datos_evento.h++"
#include "definiciones_midi.h++"

struct Nota_Midi
{
	Datos_Evento *inicio = nullptr;
	Datos_Evento *fin = nullptr;

	char armadura = Armadura_0;
	unsigned char escala = Escala_Mayor;
	Dinamica dinamica = Dinamica::SinCambio;

	unsigned char id_nota()
	{
		return inicio->evento()->id_nota();
	}

	unsigned char grupo()
	{
		return inicio->evento()->grupo();
	}

	unsigned char canal()
	{
		return inicio->evento()->canal();
	}
};

#endif
