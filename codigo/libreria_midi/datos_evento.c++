#include "datos_evento.h++"

Datos_Evento::Datos_Evento(unsigned int posicion, unsigned short fragmento, std::uint64_t tics, Evento_Midi *evento)
{
	m_posicion = posicion;
	m_fragmento = fragmento;
	m_microsegundos = 0;
	m_tics = tics;
	m_pista = nullptr;
	m_evento = evento;
}

Datos_Evento::Datos_Evento(unsigned int posicion, unsigned short fragmento, std::uint64_t tics, std::int64_t microsegundos, Evento_Midi *evento)
{
	m_posicion = posicion;
	m_fragmento = fragmento;
	m_tics = tics;
	m_microsegundos = microsegundos;
	m_pista = nullptr;
	m_evento = evento;
}

Datos_Evento::~Datos_Evento()
{
	delete m_evento;
}

void Datos_Evento::posicion(unsigned int posicion)
{
	m_posicion = posicion;
}

unsigned int Datos_Evento::posicion() const
{
	return m_posicion;
}

unsigned short Datos_Evento::fragmento() const
{
	return m_fragmento;
}

std::uint64_t Datos_Evento::tics() const
{
	return m_tics;
}

void Datos_Evento::microsegundos(std::int64_t microsegundos)
{
	m_microsegundos = microsegundos;
}

std::int64_t Datos_Evento::microsegundos() const
{
	return m_microsegundos;
}

void Datos_Evento::pista(Pista_Midi * pista)
{
	m_pista = pista;
}

Pista_Midi *Datos_Evento::pista() const
{
	return m_pista;
}

Evento_Midi *Datos_Evento::evento() const
{
	return m_evento;
}

bool Datos_Evento::operator < (const Datos_Evento &datos_evento) const
{
	if(m_tics == datos_evento.m_tics)
	{
		if(m_fragmento == datos_evento.m_fragmento)
			return m_posicion < datos_evento.m_posicion;
		return m_fragmento < datos_evento.m_fragmento;
	}
	return m_tics < datos_evento.m_tics;
}

bool Datos_Evento::comparar_puntero(Datos_Evento *d1, Datos_Evento *d2)
{
	return *d1 < *d2;
}
