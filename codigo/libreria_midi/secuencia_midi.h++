#ifndef SECUENCIA_MIDI_H
#define SECUENCIA_MIDI_H

//Una secuencia es un conjunto de pistas que son tocadas al mismo tiempo
#include "datos_evento.h++"
#include "pista_midi.h++"
#include "funciones_midi.h++"
#include "seguidor_eventos.h++"

#include <algorithm>
#include <vector>
#include <tuple>

class Secuencia_Midi
{
private:
	//Almacena todos los eventos de esta secuencia sin distinguir por pistas
	std::vector<Datos_Evento*> m_eventos_secuencia;

	std::vector<Pista_Midi*> m_pistas;
	std::vector<std::tuple<std::int64_t, unsigned char, std::string>> m_marcadores;
	std::vector<Datos_Evento*> m_tempo;
	std::vector<Datos_Evento*> m_compas;
	std::vector<std::int64_t> m_barra_compas;
	std::vector<Datos_Evento*> m_armadura;

	//Datos del archivo MIDI
	unsigned short m_tics_por_negra;
	std::int64_t m_duracion_en_microsegundo;
	std::uint64_t m_duracion_en_tics;

	//Reproducción
	std::int64_t m_tiempo_midi_actual;
	std::int64_t m_tiempo_proximo_evento;
	std::uint64_t m_ultimo_evento;
	Seguidor_Eventos *m_seguidor_eventos;

	//Valores iniciales
	unsigned int m_inicial_ppm;
	unsigned char m_inicial_compas_numerador;
	unsigned char m_inicial_compas_denominador;
	char m_inicial_armadura;
	unsigned char m_inicial_escala;

	//Especificacion MIDI
	bool m_especificacion_midi[GRUPOS_MIDI][EspecificacionMidi::Desactivado];

	Pista_Midi *crear_pista(unsigned char grupo, unsigned char canal, EspecificacionMidi especificacion, unsigned int programa_pista, unsigned char percusion_gs);
	bool especificacion_midi(EspecificacionMidi *especificacion, unsigned char grupo, std::vector<unsigned char> *datos);
	bool perscusion_gs(std::vector<unsigned char> *datos, unsigned char *canal, unsigned char *tipo);

public:
	Secuencia_Midi(unsigned short tics_por_negra);
	~Secuencia_Midi();

	//Metodos usados al abrir un archivo MIDI
	void tics_por_negra(unsigned short tics_por_negra);
	void agregar_datos_evento(Datos_Evento* datos_evento);
	void crear_pistas(bool ordenar);
	void calcular_duracion(bool ordenar);

	//Metodos para la creación de archivos MIDI
	void crear_pista_vacia_midi1(unsigned char canal, unsigned char programa, unsigned char banco_bei, unsigned char banco_bed);
	void crear_pista_vacia_midi2(unsigned char grupo, unsigned char canal, unsigned char programa, unsigned char banco_bei, unsigned char banco_bed);
	void agregar_nuevo_evento(Datos_Evento *nuevo_evento);
	void actualizar_tiempo_actual(std::uint64_t tics, std::int64_t microsegundos);

	void actualizar(unsigned int diferencia_tiempo);
	Evento_Midi *siguiente_evento(unsigned short &pista);

	bool contiene_especificacion_midi(unsigned char grupo, const EspecificacionMidi especificacion) const;
	std::vector<EspecificacionMidi> lista_especificacion_midi() const;

	std::vector<Datos_Evento*> *lista_eventos();
	std::vector<Pista_Midi*> *pistas();
	std::vector<Datos_Evento*> *tempo();
	std::vector<Datos_Evento*> *compas();
	std::vector<Datos_Evento*> *armadura();
	std::vector<std::int64_t> *barras_compas();
	std::vector<std::tuple<std::int64_t, unsigned char, std::string>> *marcadores();

	void cambiar_a(std::int64_t tiempo_nuevo);
	std::int64_t posicion_en_microsegundos();
	std::int64_t duracion_en_microsegundos();
	bool fin_reproduccion();

	//Valores Iniciales
	unsigned int inicial_ppm();
	unsigned char inicial_compas_numerador();
	unsigned char inicial_compas_denominador();
	char inicial_armadura();
	unsigned char inicial_escala();
};

#endif
