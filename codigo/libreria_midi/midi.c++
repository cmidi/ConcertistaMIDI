#include "midi.h++"

#include "midi1.h++"
#include "midi2.h++"

Midi::Midi()
{
	m_duracion_en_microsegundo = 0;
	m_secuencia_seleccionada = 0;//La primera secuencia es predeterminada
}

Midi::~Midi()
{
}

unsigned int Midi::uint32_extremo_chico(const char *datos, std::uint64_t posicion_actual)
{
	unsigned int b1 = static_cast<unsigned char>(datos[posicion_actual]);
	unsigned int b2 = static_cast<unsigned char>(datos[posicion_actual+1]);
	unsigned int b3 = static_cast<unsigned char>(datos[posicion_actual+2]);
	unsigned int b4 = static_cast<unsigned char>(datos[posicion_actual+3]);
	return (b4 << 24) | (b3 << 16) | (b2 << 8) | b1;
}

unsigned int Midi::uint32_extremo_grande(const char *datos, std::uint64_t posicion_actual)
{
	unsigned int b1 = static_cast<unsigned char>(datos[posicion_actual]);
	unsigned int b2 = static_cast<unsigned char>(datos[posicion_actual+1]);
	unsigned int b3 = static_cast<unsigned char>(datos[posicion_actual+2]);
	unsigned int b4 = static_cast<unsigned char>(datos[posicion_actual+3]);
	return (b1 << 24) + (b2 << 16) + (b3 << 8) + b4;
}

unsigned short Midi::uint16_extremo_grande(const char *datos, std::uint64_t posicion_actual)
{
	unsigned short b1 = static_cast<unsigned char>(datos[posicion_actual]);
	unsigned short b2 = static_cast<unsigned char>(datos[posicion_actual+1]);
	return static_cast<unsigned short>((b1 << 8) + b2);
}

void Midi::escribir_uint32_extremo_grande(std::fstream *archivo, unsigned int valor)
{
	char b1 = static_cast<char>((valor >> 24) & 0x000000FF);
	char b2 = static_cast<char>((valor >> 16) & 0x000000FF);
	char b3 = static_cast<char>((valor >> 8) & 0x000000FF);
	char b4 = static_cast<char>(valor & 0x000000FF);
	archivo->write(&b1, sizeof(char));
	archivo->write(&b2, sizeof(char));
	archivo->write(&b3, sizeof(char));
	archivo->write(&b4, sizeof(char));
}

void Midi::escribir_uint16_extremo_grande(std::fstream *archivo, unsigned short valor)
{
	char b1 = static_cast<char>((0xFF00 & valor) >> 8);
	char b2 = static_cast<char>((0x00FF & valor));
	archivo->write(&b1, sizeof(char));
	archivo->write(&b2, sizeof(char));
}

bool Midi::contiene_cadena(const char *cadena1, const char *cadena2, unsigned int largo, std::uint64_t posicion_actual)
{
	for(std::uint64_t x=0; x<largo; x++)
		if(cadena1[posicion_actual+x] != cadena2[x])
			return false;
	return true;
}

unsigned short Midi::formato_midi() const
{
	return m_formato_midi;
}

unsigned short Midi::tics_por_negra() const
{
	return m_tics_por_negra;
}

void Midi::actualizar(unsigned int diferencia_tiempo)
{
	return m_secuencias[m_secuencia_seleccionada]->actualizar(diferencia_tiempo);
}

Evento_Midi *Midi::siguiente_evento(unsigned short &pista)
{
	return m_secuencias[m_secuencia_seleccionada]->siguiente_evento(pista);
}


unsigned short Midi::id_secuencia_activa()
{
	return m_secuencia_seleccionada;
}

void Midi::seleccionar_secuencia(unsigned short secuencia)
{
	if(secuencia < m_secuencias.size())
		m_secuencia_seleccionada = secuencia;
}

unsigned short Midi::numero_secuencias()
{
	return static_cast<unsigned short>(m_secuencias.size());
}

Secuencia_Midi *Midi::secuencia_activa()
{
	return m_secuencias[m_secuencia_seleccionada];
}

Secuencia_Midi *Midi::secuencia(unsigned short secuencia)
{
	if(secuencia < m_secuencias.size())
		return m_secuencias[secuencia];

	return nullptr;
}

double Midi::tiempo_por_tics()
{
	//Retorna tiempo en microsegundos
	return m_tiempo_por_tics;
}

std::int64_t Midi::duracion_en_microsegundos()
{
	return m_duracion_en_microsegundo;
}

Midi* Midi::abrir_midi(const std::string &nombre_archivo)
{
	//Abre el archivo MIDI con la version correcta mirando los primeros bytes del archivo
	std::fstream archivo(nombre_archivo, std::ios::in | std::ios::binary | std::ios::ate);
	if(archivo.is_open())
	{
		std::uint64_t largo_archivo = static_cast<std::uint64_t>(archivo.tellg());
		if(largo_archivo < 8)
			throw Excepcion_Midi(CodigoErrorMidi::ArchivoMidiMuyCorto);;

		archivo.seekg(0, std::ios::beg);
		char *datos_archivo = new char[sizeof(char) * 8];
		archivo.read(datos_archivo, static_cast<std::int64_t>(sizeof(char) * 8));

		if(Midi::contiene_cadena(datos_archivo, "MThd", 4, 0))
		{
			delete[] datos_archivo;
			return new Midi1(&archivo);
		}
		else if(Midi::contiene_cadena(datos_archivo, "SMF2CLIP", 8, 0))
		{
			delete[] datos_archivo;
			return new Midi2(&archivo);
		}
		else
		{
			delete[] datos_archivo;
			throw Excepcion_Midi(CodigoErrorMidi::ArchivoDesconocido);
		}
	}
	else
		throw Excepcion_Midi(CodigoErrorMidi::FalloAbrirArchivo);
}

std::int64_t Midi::duracion_midi(const std::string &nombre_archivo)
{
	std::fstream archivo(nombre_archivo, std::ios::in | std::ios::binary | std::ios::ate);
	if(archivo.is_open())
	{
		std::uint64_t largo_archivo = static_cast<std::uint64_t>(archivo.tellg());
		if(largo_archivo < 8)
			return 0;

		archivo.seekg(0, std::ios::beg);
		char *datos_archivo = new char[sizeof(char) * 8];
		archivo.read(datos_archivo, static_cast<std::int64_t>(sizeof(char) * 8));

		if(Midi::contiene_cadena(datos_archivo, "MThd", 4, 0))
		{
			delete[] datos_archivo;
			return Midi1::duracion_midi(&archivo);
		}
		else if(Midi::contiene_cadena(datos_archivo, "SMF2CLIP", 8, 0))
		{
			delete[] datos_archivo;
			return Midi2::duracion_midi(&archivo);
		}
		else
		{
			delete[] datos_archivo;
			return 0;
		}
	}
	else
		return 0;
}
