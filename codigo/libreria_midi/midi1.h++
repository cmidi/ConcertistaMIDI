#ifndef MIDI1_H
#define MIDI1_H

#include "midi.h++"

class Midi1 : public Midi
{
private:
	unsigned int escribir_uint32_largo_variable(std::fstream *archivo, unsigned int valor);
	void cargar_archivo(std::fstream *archivo);

public:
	Midi1(std::fstream *archivo);
	Midi1(unsigned short formato_midi, unsigned short division_tiempo);
	~Midi1();

	void guardar(const std::string &nombre_archivo) override;
	unsigned char version_midi() const override;

	static std::int64_t duracion_midi(std::fstream *archivo);
};

#endif
