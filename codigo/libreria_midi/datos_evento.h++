#ifndef DATOS_EVENTO_H
#define DATOS_EVENTO_H

#include "evento_midi.h++"
#include <cstdint>//Para std::uint64_t y std::int64_t

class Pista_Midi;

class Datos_Evento
{
private:
	unsigned int m_posicion;
	unsigned short m_fragmento;

	std::uint64_t m_tics;
	std::int64_t m_microsegundos;

	Pista_Midi *m_pista;
	Evento_Midi *m_evento;

public:
	Datos_Evento(unsigned int posicion, unsigned short fragmento, std::uint64_t tics, Evento_Midi *evento);
	Datos_Evento(unsigned int posicion, unsigned short fragmento, std::uint64_t tics, std::int64_t microsegundos, Evento_Midi *evento);
	~Datos_Evento();

	void posicion(unsigned int posicion);
	unsigned int posicion() const;
	unsigned short fragmento() const;

	std::uint64_t tics() const;

	void microsegundos(std::int64_t microsegundos);
	std::int64_t microsegundos() const;

	void pista(Pista_Midi * pista);
	Pista_Midi *pista() const;
	Evento_Midi *evento() const;

	bool operator < (const Datos_Evento &datos_evento) const;
	static bool comparar_puntero(Datos_Evento *d1, Datos_Evento *d2);
};

#endif
