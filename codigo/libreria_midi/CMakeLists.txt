set(libreria_midi
	${CMAKE_CURRENT_SOURCE_DIR}/midi.c++
	${CMAKE_CURRENT_SOURCE_DIR}/midi1.c++
	${CMAKE_CURRENT_SOURCE_DIR}/midi2.c++
	${CMAKE_CURRENT_SOURCE_DIR}/secuencia_midi.c++
	${CMAKE_CURRENT_SOURCE_DIR}/pista_midi.c++
	${CMAKE_CURRENT_SOURCE_DIR}/evento_midi.c++
	${CMAKE_CURRENT_SOURCE_DIR}/datos_evento.c++
	${CMAKE_CURRENT_SOURCE_DIR}/excepcion_midi.c++
	${CMAKE_CURRENT_SOURCE_DIR}/descripciones_midi.c++
	${CMAKE_CURRENT_SOURCE_DIR}/funciones_midi.c++
	${CMAKE_CURRENT_SOURCE_DIR}/seguidor_eventos.c++

	${CMAKE_CURRENT_SOURCE_DIR}/reproductor_midi.c++
	PARENT_SCOPE
)
