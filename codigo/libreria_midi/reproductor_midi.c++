#include "reproductor_midi.h++"

Reproductor_Midi::Reproductor_Midi()
{
	m_controlador = nullptr;
	m_midi = nullptr;
	m_volumen = 1.0;
	m_reproduciondo = false;
	m_reproducir_en_bucle = false;
	m_tiempo_espera = 0;
}

Reproductor_Midi::~Reproductor_Midi()
{
}

void Reproductor_Midi::nota_activa(unsigned short pista, unsigned char id_nota, bool encendida)
{
	unsigned int pista_int = pista;

	unsigned int clave = (pista_int << 16) | (id_nota & 0x7F);
	if(encendida)//Nueva nota activada
		m_notas_activas.push_back(clave);
	else
	{
		for(std::vector<unsigned int>::iterator i = m_notas_activas.begin(); i != m_notas_activas.end(); i++)
		{
			if(*i == clave)
			{
				m_notas_activas.erase(i);
				return;
			}
		}
	}
}

void Reproductor_Midi::controlador_midi(Controlador_Midi *controlador)
{
	m_controlador = controlador;
}

void Reproductor_Midi::cargar_midi(Midi *midi)
{
	m_midi = midi;

	if(midi == nullptr)
		return;

	m_midi->secuencia_activa()->cambiar_a(0);
	m_habilitar_pistas.clear();
	m_habilitar_pistas.reserve(m_midi->numero_secuencias());
	for(unsigned short s=0; s<m_midi->numero_secuencias(); s++)
	{
		m_habilitar_pistas.push_back(std::vector<bool>());
		m_habilitar_pistas[s].reserve(m_midi->secuencia(s)->pistas()->size());
		for(unsigned short p=0; p<m_midi->secuencia(s)->pistas()->size(); p++)
			m_habilitar_pistas[s].push_back(true);
	}
}

void Reproductor_Midi::actualizar(unsigned int diferencia_tiempo)
{
	if(m_controlador == nullptr || m_midi == nullptr)
		return;

	if(!m_reproduciondo)
		return;

	if(m_tiempo_espera > 0)
	{
		if(m_tiempo_espera > diferencia_tiempo)
			m_tiempo_espera-= diferencia_tiempo;
		else
			m_tiempo_espera = 0;

		return;
	}

	m_midi->actualizar(diferencia_tiempo);

	unsigned short pista = 0;
	Evento_Midi* evento = m_midi->siguiente_evento(pista);
	while(evento != nullptr)
	{
		if(	evento->version_midi() == VersionMidi::Version_1 &&
			evento->tipo_evento_midi1() == EventoMidi_Metaevento)
		{
			evento = m_midi->siguiente_evento(pista);
			continue;
		}

		if(evento->es_nota())
		{
			//Verifica que la pista este habilitada antes de reproducir el evento
			if(m_habilitar_pistas[m_midi->id_secuencia_activa()][pista] == true)
			{
				//Se copia el evento para poder editarlo y escalar el volumen
				Evento_Midi evento_salida = *evento;
				if(evento_salida.tipo_mensaje() == TipoMensaje_VozDeCanalMidi1)
					evento_salida.velocidad_nota_7(static_cast<unsigned char>(evento_salida.velocidad_nota_7() * m_volumen));
				else
					evento_salida.velocidad_nota_16(static_cast<unsigned short>(evento_salida.velocidad_nota_16() * m_volumen));

				bool estado = false;
				if(evento->tipo_voz_de_canal() == EventoMidi_NotaEncendida)
					estado = true;

				this->nota_activa(pista, evento_salida.id_nota(), estado);
				m_controlador->escribir(evento_salida);
			}
		}
		else
		{
			//F0 43 10 4C 00 00 7E 00 F7
			if(evento->tipo_mensaje() == TipoMensaje_ExclusivoDelSistema7bits)
			{
				if(evento->version_midi() == VersionMidi::Version_1)
				{
					m_tiempo_espera = 0;
					unsigned char *datos = evento->datos();
					if(evento->largo_datos() == 6 && datos[1] == 0x7E && datos[3] == 0x09)
					{
						if(datos[4] == 0x01 || datos[4] == 0x02 || datos[4] == 0x03)//GM apagado, GM y GM2
							m_tiempo_espera = 100 * 1000;//Espera 100ms
					}
					else if(evento->largo_datos() == 9 &&
							datos[1] == 0x43 &&
							datos[3] == 0x4C &&
							datos[4] == 0x00 &&
							datos[5] == 0x00 &&
							datos[6] == 0x7E &&
							datos[7] == 0x00)
						m_tiempo_espera = 50 * 1000;//Espera 50 ms XG
					else if(evento->largo_datos() == 11 &&
							datos[1] == 0x41 &&
							datos[3] == 0x42 &&
							datos[4] == 0x12 &&
							datos[5] == 0x40 &&
							datos[6] == 0x00 &&
							datos[7] == 0x7F &&
							datos[8] == 0x00 &&
							datos[9] == 0x41)
						m_tiempo_espera = 50 * 1000;//Espera 50 ms GS
				}
			}
			m_controlador->escribir(*evento);
		}

		evento = m_midi->siguiente_evento(pista);
	}

	if(m_midi->secuencia_activa()->fin_reproduccion())
	{
		if(m_reproducir_en_bucle)
			this->reiniciar();
		else
			this->detener();
	}
}

void Reproductor_Midi::reproducir_en_bucle(bool valor)
{
	m_reproducir_en_bucle = valor;
}

void Reproductor_Midi::cambiar_a(std::int64_t microsegundos)
{
	if(m_midi == nullptr)
		return;

	m_midi->secuencia_activa()->cambiar_a(microsegundos);
}

bool Reproductor_Midi::reproduciendo()
{
	return m_reproduciondo;
}

void Reproductor_Midi::reproducir()
{
	m_reproduciondo = true;
}

void Reproductor_Midi::pausar()
{
	m_reproduciondo = false;
	m_notas_activas.clear();
	m_controlador->desactivar_notas();
}

void Reproductor_Midi::detener()
{
	if(m_reproduciondo)
	{
		m_reproduciondo = false;
		this->reiniciar();
	}
}

void Reproductor_Midi::reiniciar()
{
	if(m_controlador == nullptr || m_midi == nullptr)
		return;

	m_midi->secuencia_activa()->cambiar_a(0);
	m_notas_activas.clear();
	m_controlador->restablecer();
}

void Reproductor_Midi::habilitar_pistas(bool estado)
{
	if(m_controlador == nullptr || m_midi == nullptr)
		return;

	for(unsigned short p=0; p<m_midi->secuencia_activa()->pistas()->size(); p++)
		m_habilitar_pistas[m_midi->id_secuencia_activa()][p] = estado;

	if(!estado)
	{
		m_notas_activas.clear();
		m_controlador->restablecer();
	}
}

void Reproductor_Midi::habilitar_pista(unsigned short pista, bool estado)
{
	//Pista de la secuencia secuencia_activa
	if(m_controlador == nullptr || m_midi == nullptr)
		return;

	if(pista < m_habilitar_pistas[m_midi->id_secuencia_activa()].size())
		m_habilitar_pistas[m_midi->id_secuencia_activa()][pista] = estado;

	if(!estado)
	{
		//Solo apagar las notas de las pistas que se desactivan
		std::vector<unsigned int>::iterator i = m_notas_activas.begin();
		while(i != m_notas_activas.end())
		{
			unsigned int clave_actual = *i;
			unsigned int pista_actual = clave_actual >> 16;
			unsigned char canal_actual = (clave_actual >> 8) & 0x0000000F;
			unsigned char id_nota_actual = clave_actual & 0x0000007F;
			if(pista_actual == pista)
			{
				Evento_Midi evento(EventoMidi_NotaApagada);
				evento.canal(canal_actual);
				evento.id_nota(id_nota_actual);
				evento.cliente(DIRECCIONES_SUSCRITAS);
				evento.puerto(DIRECCIONES_SUSCRITAS);
				m_controlador->escribir(evento);
				m_notas_activas.erase(i);//Cuando elimina un elemento apunta al siguiente
			}
			else
				i++;
		}
	}
}

void Reproductor_Midi::subir_volumen()
{
	m_volumen += 0.1;
	if(m_volumen > 2)
		m_volumen = 2;
}

void Reproductor_Midi::bajar_volumen()
{
	m_volumen -= 0.1;
	if(m_volumen <= 0.1)
		m_volumen = 0.1;
}

std::int64_t Reproductor_Midi::posicion_en_microsegundos()
{
	if(m_midi == nullptr)
		return 0;

	return m_midi->secuencia_activa()->posicion_en_microsegundos();
}

std::int64_t Reproductor_Midi::duracion_en_microsegundos()
{
	if(m_midi == nullptr)
		return 0;

	return m_midi->secuencia_activa()->duracion_en_microsegundos();
}

std::vector<bool> Reproductor_Midi::pistas_reproduciendo()
{
	if(m_midi == nullptr)
		return std::vector<bool>();

	return m_habilitar_pistas[m_midi->id_secuencia_activa()];
}
