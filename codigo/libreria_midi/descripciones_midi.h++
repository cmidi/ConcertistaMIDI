#ifndef DESCRIPCIONES_MIDI_H
#define DESCRIPCIONES_MIDI_H

#include "definiciones_midi.h++"

#include <string>

std::string Nombre_TipoMensaje(unsigned char mensaje);
std::string Nombre_Utilidad(unsigned char mensaje);
std::string Nombre_TiempoReal(unsigned char mensaje);
std::string Nombre_VozDeCanalMidi1(unsigned char mensaje);
std::string Nombre_VozDeCanalMidi2(unsigned char mensaje);
std::string Nombre_DatosFlexibles(unsigned short mensaje);
std::string Nombre_FlujoPaqueteMidiUniversal(unsigned short mensaje);
std::string Nombre_MetaEvento(unsigned char metaevento);

std::string Nombre_EstadoMensaje(unsigned char estado);
std::string Nombre_DatosFlexiblesDireccion(unsigned char valor);
std::string Nombre_DatosFlexiblesBanco(unsigned char valor);

std::string Nombre_MensajeControlador(unsigned char controlador);
std::string Nombre_ParametroRegistrado(unsigned short parametro);
std::string Nombre_Percusion(Percusion percusion, unsigned char programa, unsigned char banco_izquierdo, unsigned char banco_derecho);
std::string Nombre_Instrumento(unsigned char programa, unsigned char banco_izquierdo, unsigned char banco_derecho);

std::string Nombre_Armadura(char armadura, unsigned char escala);
std::string Nombre_Escala(unsigned char escala);
std::string Nombre_Dinamica(Dinamica dinamica);
std::string Nombre_EspecificacionMidi(EspecificacionMidi s);
std::string Nombre_LargoEspecificacionMidi(EspecificacionMidi s);

std::string Nombre_Notas(SistemaNombreNotas sistema, unsigned char nota);
std::string Nombre_FiguraMusical(FiguraMusical figura);

#endif
