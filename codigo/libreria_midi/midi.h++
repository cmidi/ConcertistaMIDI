#ifndef MIDI_H
#define MIDI_H

#include "secuencia_midi.h++"
#include "pista_midi.h++"
#include "evento_midi.h++"
#include "excepcion_midi.h++"
#include "definiciones_midi.h++"

#include <string>
#include <vector>
#include <map>
#include <fstream>

class Midi
{
protected:
	//Solo hay una secuencia para archivos MIDI formato 0 y 1
	//Los archivos MIDI 1.0 formato 2 puede tener N secuencias
	std::vector<Secuencia_Midi*> m_secuencias;
	unsigned short m_secuencia_seleccionada;

	unsigned short m_formato_midi;
	unsigned short m_numero_fragmentos;
	unsigned short m_division_tiempo;
	unsigned short m_tics_por_negra;
	double m_tiempo_por_tics;
	std::int64_t m_duracion_en_microsegundo;

	static unsigned int uint32_extremo_chico(const char *datos, std::uint64_t posicion_actual);
	static unsigned int uint32_extremo_grande(const char *datos, std::uint64_t posicion_actual);
	static unsigned short uint16_extremo_grande(const char *datos, std::uint64_t posicion_actual);

	void escribir_uint32_extremo_grande(std::fstream *archivo, unsigned int valor);
	void escribir_uint16_extremo_grande(std::fstream *archivo, unsigned short valor);
	unsigned int escribir_uint32_largo_variable(std::fstream *archivo, unsigned int valor);

public:
	Midi();
	virtual ~Midi();

	static bool contiene_cadena(const char *cadena1, const char *cadena2, unsigned int largo, std::uint64_t posicion_actual);

	static Midi* abrir_midi(const std::string &nombre_archivo);
	virtual void guardar(const std::string &nombre_archivo) = 0;

	virtual unsigned char version_midi() const = 0;
	unsigned short formato_midi() const;
	unsigned short tics_por_negra() const;

	void actualizar(unsigned int diferencia_tiempo);
	Evento_Midi *siguiente_evento(unsigned short &pista);

	unsigned short id_secuencia_activa();
	void seleccionar_secuencia(unsigned short secuencia);
	unsigned short numero_secuencias();
	Secuencia_Midi *secuencia_activa();
	Secuencia_Midi *secuencia(unsigned short secuencia);

	double tiempo_por_tics();
	std::int64_t duracion_en_microsegundos();

	static std::int64_t duracion_midi(const std::string &nombre_archivo);
};

#endif
