#ifndef REPRODUCTOR_MIDI_H
#define REPRODUCTOR_MIDI_H

#include "midi.h++"
#include "../dispositivos_midi/controlador_midi.h++"

class Reproductor_Midi
{
private:
	Controlador_Midi *m_controlador;
	Midi *m_midi;

	std::vector<std::vector<bool>> m_habilitar_pistas;
	std::vector<unsigned int> m_notas_activas;

	double m_volumen;
	bool m_reproduciondo;
	bool m_reproducir_en_bucle;
	std::int64_t m_tiempo_espera;

	void nota_activa(unsigned short pista, unsigned char id_nota, bool encendida);

public:
	Reproductor_Midi();
	~Reproductor_Midi();

	void controlador_midi(Controlador_Midi *controlador);
	void cargar_midi(Midi *midi);

	void actualizar(unsigned int diferencia_tiempo);

	void reproducir_en_bucle(bool valor);
	void cambiar_a(std::int64_t microsegundos);
	bool reproduciendo();
	void reproducir();
	void pausar();
	void detener();
	void reiniciar();

	void habilitar_pistas(bool estado);
	void habilitar_pista(unsigned short pista, bool estado);

	void subir_volumen();
	void bajar_volumen();

	std::int64_t posicion_en_microsegundos();
	std::int64_t duracion_en_microsegundos();

	std::vector<bool> pistas_reproduciendo();
};

#endif
