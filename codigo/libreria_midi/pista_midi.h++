#ifndef PISTA_MIDI_H
#define PISTA_MIDI_H

#include "datos_evento.h++"
#include "nota_midi.h++"
#include <vector>
#include <map>
#include <string>
#include <cstdint>

class Pista_Midi
{
private:
	std::vector<Nota_Midi*> m_notas;

	unsigned short m_id_pista;
	unsigned char m_grupo;
	unsigned char m_canal;
	unsigned char m_programa;
	unsigned char m_banco_izquierdo;
	unsigned char m_banco_derecho;
	Percusion m_percusion;

	std::int64_t m_duracion_pista_microsegundos;
	std::uint64_t m_duracion_pista_tics;

	std::int64_t m_tiempo_primera_nota;
	std::int64_t m_tiempo_ultima_nota;

	//Para el calculo de notas
	std::map<unsigned char, Nota_Midi*> m_notas_nuevas;
	std::vector<Nota_Midi*> m_notas_preseleccionadas;
	unsigned int m_notas_eliminadas;
public:
	Pista_Midi();
	~Pista_Midi();

	//Metodos usados al abrir un archivo MIDI
	void agregar_evento(Datos_Evento *datos_evento, char armadura, unsigned char escala);
	void procesar_notas(unsigned short id_pista);

	//Metodo para la creación de archivos MIDI
	void agregar_nota(Datos_Evento *datos_evento, char armadura, unsigned char escala);

	//Acceso a datos
	std::vector<Nota_Midi*> &notas();

	void grupo(unsigned char grupo);
	unsigned char grupo() const;

	void canal(unsigned char canal);
	unsigned char canal() const;

	void programa(unsigned char programa);
	unsigned char programa() const;

	void banco_izquierdo(unsigned char banco_izquierdo);
	unsigned char banco_izquierdo() const;

	void banco_derecho(unsigned char banco_derecho);
	unsigned char banco_derecho() const;

	void percusion(Percusion percusion);
	Percusion percusion() const;

	std::string nombre_instrumento() const;

	void id_pista(unsigned short id);
	unsigned short id_pista() const;
	unsigned int numero_notas() const;

	std::int64_t tiempo_primera_nota() const;
	std::int64_t tiempo_ultima_nota() const;

	std::int64_t duracion_pista_microsegundos() const;
	std::uint64_t duracion_pista_tics() const;
};

#endif
