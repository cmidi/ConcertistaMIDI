#include "pista_midi.h++"

#include "evento_midi.h++"
#include "funciones_midi.h++"
#include "descripciones_midi.h++"
#include "../registro.h++"
#include <limits>

Pista_Midi::Pista_Midi()
{
	m_notas_eliminadas = 0;

	m_id_pista = 0;
	m_grupo = 0;
	m_canal = 0;
	m_programa = 0;
	m_banco_izquierdo = 0;
	m_banco_derecho = 0;
	m_percusion = Percusion::Desactivado;

	m_duracion_pista_microsegundos = 0;
	m_duracion_pista_tics = 0;
}

Pista_Midi::~Pista_Midi()
{
	for(unsigned int x=0; x<m_notas.size(); x++)
		delete m_notas[x];

	m_notas.clear();
}

void Pista_Midi::agregar_evento(Datos_Evento *datos_evento, char armadura, unsigned char escala)
{
	//Se crean las notas
	if(	datos_evento->evento()->es_nota())
	{
		std::map<unsigned char, Nota_Midi*>::iterator buscar = m_notas_nuevas.find(datos_evento->evento()->id_nota());
		if(datos_evento->evento()->tipo_voz_de_canal() == EventoMidi_NotaEncendida)
		{
			if(buscar == m_notas_nuevas.end())
			{
				//La nota no existe
				Nota_Midi *nueva = new Nota_Midi();
				nueva->inicio = datos_evento;
				nueva->armadura = armadura;
				nueva->escala = escala;

				m_notas_nuevas[datos_evento->evento()->id_nota()] = nueva;
				m_notas_preseleccionadas.push_back(nueva);
			}
			else
			{
				//La nota ya existe
				Nota_Midi *existente = buscar->second;
				//Si tiene una duración > 0 entonces se guarda la nota de lo contrario se ignora
				//es en el caso de resivir dos EventoMidi_NotaEncendida, solo se queda con una si no cambia el tiempo
				if(existente->inicio->microsegundos() < datos_evento->microsegundos())
				{
					existente->fin = datos_evento;

					//Se crea una nueva nota en la misma posicion donde se cierra la anterior
					Nota_Midi *nueva = new Nota_Midi();
					nueva->inicio = datos_evento;
					nueva->armadura = armadura;
					nueva->escala = escala;

					m_notas_nuevas[datos_evento->evento()->id_nota()] = nueva;
					m_notas_preseleccionadas.push_back(nueva);
				}
				else
					m_notas_eliminadas++;
			}
		}
		else if(datos_evento->evento()->tipo_voz_de_canal() == EventoMidi_NotaApagada)
		{
			if(buscar != m_notas_nuevas.end())
			{
				Nota_Midi *existente = buscar->second;
				//Se cierra la nota
				existente->fin = datos_evento;
				m_notas_nuevas.erase(buscar);
				//Si tiene una duracion menor o igual a 0, no sera guardada en la función
				//procesar_notas, solo se permiten notas de duracion 0 para la percusion
				if(existente->inicio->microsegundos() == existente->fin->microsegundos() && m_percusion == Percusion::Desactivado)
					m_notas_eliminadas++;
			}
		}
	}

	//El ultimo evento siempre tiene la duracion total
	m_duracion_pista_microsegundos = datos_evento->microsegundos();
	m_duracion_pista_tics = datos_evento->tics();
}

void Pista_Midi::procesar_notas(unsigned short id_pista)
{
	m_id_pista = id_pista;
	m_notas_nuevas.clear();

	if(m_notas_eliminadas < m_notas_preseleccionadas.size())
		m_notas.reserve(m_notas_preseleccionadas.size() - m_notas_eliminadas);

	//Traspasa las notas en el orden correcto y solo las que tienen una duración mayor o igual que 0
	//ya estan en orden, solo se quitan las notas invalidas
	unsigned char velocidad_actual = 0;
	unsigned char velocidad_anterior = 0;
	Dinamica dinamica = Dinamica::SinCambio;
	bool cambio_dinamica = true;
	for(std::uint64_t x=0; x<m_notas_preseleccionadas.size(); x++)
	{
		//Notas de instrumentos mayor a 0
		//Notas de percusion mayor o igual a 0
		if(	m_notas_preseleccionadas[x]->fin != nullptr &&
			(m_notas_preseleccionadas[x]->inicio->microsegundos() < m_notas_preseleccionadas[x]->fin->microsegundos() ||
			m_percusion != Percusion::Desactivado))
		{
			velocidad_actual = m_notas_preseleccionadas[x]->inicio->evento()->velocidad_nota_7();

			if(dinamica != Dinamica::SinCambio)
			{
				//Le doy un rango de +- 8 para interpretarla como un cambio de dinamica
				if(	(velocidad_anterior < velocidad_actual && velocidad_anterior+8 < velocidad_actual) ||
					(velocidad_anterior > velocidad_actual && velocidad_anterior-8 > velocidad_actual)
				)
					cambio_dinamica = true;
				else
					cambio_dinamica = false;
			}
			if(cambio_dinamica)
			{
				dinamica = Funciones_Midi::velocidad_a_dinamica(velocidad_actual);
				m_notas_preseleccionadas[x]->dinamica = dinamica;
				velocidad_anterior = velocidad_actual;
			}

			if(m_notas.size() < std::numeric_limits<unsigned int>::max() - 1)
				m_notas.push_back(m_notas_preseleccionadas[x]);
			else//Un archivo MIDI normal jamas deberia alcanzar este punto
				Registro::Depurar("Alcanzo el numero maximo de notas por pista");
		}
		else
			delete m_notas_preseleccionadas[x];
	}
	//Guarda el tiempo de la primera y ultima nota
	if(m_notas.size() > 0)
	{
		m_tiempo_primera_nota = m_notas[0]->inicio->microsegundos();
		m_tiempo_ultima_nota = m_notas.back()->fin->microsegundos();
	}

	m_notas_preseleccionadas.clear();
}

void Pista_Midi::agregar_nota(Datos_Evento *datos_evento, char armadura, unsigned char escala)
{
	if(	datos_evento->evento()->es_nota())
	{
		unsigned char velocidad_actual = 0;
		unsigned char velocidad_anterior = 0;
		Dinamica dinamica = Dinamica::SinCambio;
		bool cambio_dinamica = true;
		if(m_notas.size() > 0)
		{
			velocidad_anterior = m_notas.back()->inicio->evento()->velocidad_nota_7();
			dinamica = m_notas.back()->dinamica;
		}
		std::map<unsigned char, Nota_Midi*>::iterator buscar = m_notas_nuevas.find(datos_evento->evento()->id_nota());
		if(datos_evento->evento()->tipo_voz_de_canal() == EventoMidi_NotaEncendida)
		{
			if(buscar == m_notas_nuevas.end())
			{
				Nota_Midi *nueva = new Nota_Midi();
				nueva->inicio = datos_evento;
				nueva->fin = datos_evento;
				nueva->armadura = armadura;
				nueva->escala = escala;

				velocidad_actual = datos_evento->evento()->velocidad_nota_7();

				if(dinamica != Dinamica::SinCambio)
				{
					//Le doy un rango de +- 8 para interpretarla como un cambio de dinamica
					if(	(velocidad_anterior < velocidad_actual && velocidad_anterior+8 < velocidad_actual) ||
						(velocidad_anterior > velocidad_actual && velocidad_anterior-8 > velocidad_actual)
					)
						cambio_dinamica = true;
					else
						cambio_dinamica = false;
				}
				if(cambio_dinamica)
				{
					dinamica = Funciones_Midi::velocidad_a_dinamica(velocidad_actual);
					nueva->dinamica = dinamica;
				}

				m_notas_nuevas[datos_evento->evento()->id_nota()] = nueva;
				m_notas.push_back(nueva);
			}
		}
		else if(datos_evento->evento()->tipo_voz_de_canal() == EventoMidi_NotaApagada)
		{
			if(buscar != m_notas_nuevas.end())
			{
				Nota_Midi *existente = buscar->second;
				//Se cierra la nota
				existente->fin = datos_evento;
				m_notas_nuevas.erase(buscar);
			}
		}
	}

	//El ultimo evento siempre tiene la duracion total
	m_duracion_pista_microsegundos = datos_evento->microsegundos();
	m_duracion_pista_tics = datos_evento->tics();
}

std::vector<Nota_Midi*> &Pista_Midi::notas()
{
	return m_notas;
}

void Pista_Midi::grupo(unsigned char grupo)
{
	m_grupo = grupo & 0x0F;
}

unsigned char Pista_Midi::grupo() const
{
	return m_grupo;
}

void Pista_Midi::canal(unsigned char canal)
{
	m_canal = canal & 0x0F;
}

unsigned char Pista_Midi::canal() const
{
	return m_canal;
}

void Pista_Midi::programa(unsigned char programa)
{
	m_programa = programa & 0x7F;
}

unsigned char Pista_Midi::programa() const
{
	return m_programa;
}

void Pista_Midi::banco_izquierdo(unsigned char banco_izquierdo)
{
	m_banco_izquierdo = banco_izquierdo & 0x7F;
}

unsigned char Pista_Midi::banco_izquierdo() const
{
	return m_banco_izquierdo;
}

void Pista_Midi::banco_derecho(unsigned char banco_derecho)
{
	m_banco_derecho = banco_derecho & 0x7F;
}

unsigned char Pista_Midi::banco_derecho() const
{
	return m_banco_derecho;
}

void Pista_Midi::percusion(Percusion percusion)
{
	m_percusion = percusion;
}

Percusion Pista_Midi::percusion() const
{
	return m_percusion;
}

std::string Pista_Midi::nombre_instrumento() const
{
	if(this->percusion() != Percusion::Desactivado)
		return Nombre_Percusion(this->percusion(), m_programa, m_banco_izquierdo, m_banco_derecho);

	return Nombre_Instrumento(m_programa, m_banco_izquierdo, m_banco_derecho);
}

void Pista_Midi::id_pista(unsigned short id)
{
	m_id_pista = id;
}

unsigned short Pista_Midi::id_pista() const
{
	return m_id_pista;
}

unsigned int Pista_Midi::numero_notas() const
{
	return static_cast<unsigned int>(m_notas.size());
}

std::int64_t Pista_Midi::tiempo_primera_nota() const
{
	return m_tiempo_primera_nota;
}

std::int64_t Pista_Midi::tiempo_ultima_nota() const
{
	return m_tiempo_ultima_nota;
}

std::int64_t Pista_Midi::duracion_pista_microsegundos() const
{
	return m_duracion_pista_microsegundos;
}

std::uint64_t Pista_Midi::duracion_pista_tics() const
{
	return m_duracion_pista_tics;
}
