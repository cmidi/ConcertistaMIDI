#include "seguidor_eventos.h++"

Seguidor_Eventos::Seguidor_Eventos(bool copiar_eventos)
{
	m_copiar_eventos = copiar_eventos;
	this->inicializar();
}

Seguidor_Eventos::~Seguidor_Eventos()
{
	this->vaciar_memoria();
}

void Seguidor_Eventos::vaciar_memoria()
{
	if(m_copiar_eventos)
	{
		if(m_mensaje_utilidad_dctpq != nullptr)
			delete m_mensaje_utilidad_dctpq;

		for(std::uint64_t x=0; x<m_eventos_repetibles.size(); x++)
			delete m_eventos_repetibles[x];

		for(unsigned char grupo=0; grupo<GRUPOS_MIDI; grupo++)
		{
			if(!m_grupos_usados[grupo])
				continue;

			for(unsigned char canal=0; canal<CANALES_MIDI; canal++)
			{
				if(!m_canales_usados[grupo][canal])
					continue;

				if(m_hay_controlador)
				{
					for(unsigned char controlador = 0; controlador < CONTROLADORES_MIDI; controlador++)
					{
						if(m_controlador[grupo][canal][controlador] != nullptr)
							delete m_controlador[grupo][canal][controlador];

						if(controlador < 2)
							if(m_controlador_banco_no_confirmado[grupo][canal][controlador] != nullptr)
								delete m_controlador_banco_no_confirmado[grupo][canal][controlador];
					}
				}

				if(m_programa[grupo][canal] != nullptr)
					delete m_programa[grupo][canal];

				if(m_presion_canal[grupo][canal] != nullptr)
					delete m_presion_canal[grupo][canal];

				if(m_inflexion_tono[grupo][canal] != nullptr)
					delete m_inflexion_tono[grupo][canal];
			}

			for(unsigned char datos_flexibles=0; datos_flexibles < PosicionDatosFlexibles::TotalDF; datos_flexibles++)
				if(m_datos_flexibles_unicos[grupo][datos_flexibles] != nullptr)
					delete m_datos_flexibles_unicos[grupo][datos_flexibles];
		}
	}
	m_eventos_repetibles.clear();
	m_eventos_listos.clear();
}

void Seguidor_Eventos::inicializar()
{
	m_mensaje_utilidad_dctpq = nullptr;
	for(unsigned char grupo=0; grupo<GRUPOS_MIDI; grupo++)
	{
		m_grupos_usados[grupo] = false;
		for(unsigned char canal=0; canal<CANALES_MIDI; canal++)
		{
			m_canales_usados[grupo][canal] = false;
			m_programa[grupo][canal] = nullptr;
			m_presion_canal[grupo][canal] = nullptr;
			m_inflexion_tono[grupo][canal] = nullptr;

			for(unsigned char controlador = 0; controlador < CONTROLADORES_MIDI; controlador++)
			{
				m_controlador[grupo][canal][controlador] = nullptr;
				if(controlador < 2)
					m_controlador_banco_no_confirmado[grupo][canal][controlador] = nullptr;
			}
		}

		for(unsigned char datos_flexibles=0; datos_flexibles < PosicionDatosFlexibles::TotalDF; datos_flexibles++)
			m_datos_flexibles_unicos[grupo][datos_flexibles] = nullptr;
	}
	m_hay_eventos = false;
	m_termino_enviar_eventos = false;
	m_hay_controlador = false;
	m_procesamiento_requerido = false;
	m_actual_repetible = 0;
	m_actual_listos = 0;
}

void Seguidor_Eventos::registrar_evento(Evento_Midi * evento)
{
	m_hay_eventos = true;

	if(	evento->tipo_mensaje() == TipoMensaje_VozDeCanalMidi1 ||
		evento->tipo_mensaje() == TipoMensaje_VozDeCanalMidi2)
	{
		if(	evento->tipo_voz_de_canal() != EventoMidi_Controlador &&
			evento->tipo_voz_de_canal() != EventoMidi_Programa &&
			evento->tipo_voz_de_canal() != EventoMidi_PresionCanal &&
			evento->tipo_voz_de_canal() != EventoMidi_InflexionDeTono &&
			evento->tipo_voz_de_canal() != EventoMidi_ControladorRegistrado &&
			evento->tipo_voz_de_canal() != EventoMidi_ControladorNoRegistrado &&
			evento->tipo_voz_de_canal() != EventoMidi_ControladorRelativoRegistrado &&
			evento->tipo_voz_de_canal() != EventoMidi_ControladorRelativoNoRegistrado)
			return;
	}

	Evento_Midi *e;
	if(m_copiar_eventos)
		e = new Evento_Midi(*evento);
	else
		e = evento;

	if(e->tipo_mensaje() == TipoMensaje_Utilidad)
	{
		if(m_copiar_eventos && m_mensaje_utilidad_dctpq != nullptr)
			delete m_mensaje_utilidad_dctpq;

		m_mensaje_utilidad_dctpq = e;
	}
	else if(e->tipo_mensaje() == TipoMensaje_ComunYTiempoReal ||
			e->tipo_mensaje() == TipoMensaje_ExclusivoDelSistema7bits ||
			e->tipo_mensaje() == TipoMensaje_ExclusivoDelSistema8bits ||
			e->tipo_mensaje() == TipoMensaje_FlujoPaqueteMidiUniversal)
		m_eventos_repetibles.push_back(e);
	else if(e->tipo_mensaje() == TipoMensaje_VozDeCanalMidi1 ||
			e->tipo_mensaje() == TipoMensaje_VozDeCanalMidi2)
	{
		/*
		EventoMidi_NotaApagada, EventoMidi_NotaEncendida son ignorados,
		porque no se puede activar una nota fuera de tiempo, por lo tanto
		tambien se ignoran los eventos de control sobre la nota activa
		EventoMidi_PresionNota, EventoMidi_ControladorPorNotaRegistrado,
		EventoMidi_ControladorPorNotaNoRegistrado, EventoMidi_InflexionDeTonoPorNota,
		EventoMidi_GestionPorNota
		*/
		if(e->tipo_voz_de_canal() == EventoMidi_Controlador)
		{
			if(	e->controlador_mensaje() == MensajeControlador_ParametroNoRegistrado_BEI ||
				e->controlador_mensaje() == MensajeControlador_ParametroNoRegistrado_BED ||
				e->controlador_mensaje() == MensajeControlador_ParametroRegistrado_BEI ||
				e->controlador_mensaje() == MensajeControlador_ParametroRegistrado_BED ||
				e->controlador_mensaje() == MensajeControlador_EntradaDeDatos_BEI ||
				e->controlador_mensaje() == MensajeControlador_EntradaDeDatos_BED ||
				e->controlador_mensaje() == MensajeControlador_EntradaDeDatosMas1 ||
				e->controlador_mensaje() == MensajeControlador_EntradaDeDatosMenos1)
				m_eventos_repetibles.push_back(e);
			else
			{
				m_hay_controlador = true;
				m_grupos_usados[e->grupo()] = true;
				m_canales_usados[e->grupo()][e->canal()] = true;
				m_procesamiento_requerido = true;

				if(e->controlador_mensaje() == MensajeControlador_SeleccionDeBanco_BEI)
				{
					if(m_copiar_eventos && m_controlador_banco_no_confirmado[e->grupo()][e->canal()][PosicionControladorBanco::Banco_BEI] != nullptr)
						delete m_controlador_banco_no_confirmado[e->grupo()][e->canal()][PosicionControladorBanco::Banco_BEI];

					m_controlador_banco_no_confirmado[e->grupo()][e->canal()][PosicionControladorBanco::Banco_BEI] = e;
				}
				else if(e->controlador_mensaje() == MensajeControlador_SeleccionDeBanco_BED)
				{
					if(m_copiar_eventos && m_controlador_banco_no_confirmado[e->grupo()][e->canal()][PosicionControladorBanco::Banco_BED] != nullptr)
						delete m_controlador_banco_no_confirmado[e->grupo()][e->canal()][PosicionControladorBanco::Banco_BED];

					m_controlador_banco_no_confirmado[e->grupo()][e->canal()][PosicionControladorBanco::Banco_BED] = e;
				}
				else
				{
					if(m_copiar_eventos && m_controlador[e->grupo()][e->canal()][e->controlador_mensaje()] != nullptr)
						delete m_controlador[e->grupo()][e->canal()][e->controlador_mensaje()];

					m_controlador[e->grupo()][e->canal()][e->controlador_mensaje()] = e;
				}
			}
		}
		else if(e->tipo_voz_de_canal() == EventoMidi_Programa)
		{
			m_grupos_usados[e->grupo()] = true;
			m_canales_usados[e->grupo()][e->canal()] = true;
			m_procesamiento_requerido = true;

			if(m_copiar_eventos && m_programa[e->grupo()][e->canal()] != nullptr)
				delete m_programa[e->grupo()][e->canal()];

			m_programa[e->grupo()][e->canal()] = e;

			Evento_Midi *bei = m_controlador_banco_no_confirmado[e->grupo()][e->canal()][PosicionControladorBanco::Banco_BEI];
			if(bei != nullptr)
			{
				if(m_copiar_eventos && m_controlador[e->grupo()][e->canal()][bei->controlador_mensaje()] != nullptr)
					delete m_controlador[e->grupo()][e->canal()][bei->controlador_mensaje()];

				m_controlador[e->grupo()][e->canal()][bei->controlador_mensaje()] = bei;
				m_controlador_banco_no_confirmado[e->grupo()][e->canal()][PosicionControladorBanco::Banco_BEI] = nullptr;
			}

			Evento_Midi *bed = m_controlador_banco_no_confirmado[e->grupo()][e->canal()][PosicionControladorBanco::Banco_BED];
			if(bed != nullptr)
			{
				if(m_copiar_eventos && m_controlador[e->grupo()][e->canal()][bed->controlador_mensaje()] != nullptr)
					delete m_controlador[e->grupo()][e->canal()][bed->controlador_mensaje()];

				m_controlador[e->grupo()][e->canal()][bed->controlador_mensaje()] = bed;
				m_controlador_banco_no_confirmado[e->grupo()][e->canal()][PosicionControladorBanco::Banco_BED] = nullptr;
			}
		}
		else if(e->tipo_voz_de_canal() == EventoMidi_PresionCanal)
		{
			m_grupos_usados[e->grupo()] = true;
			m_canales_usados[e->grupo()][e->canal()] = true;
			m_procesamiento_requerido = true;

			if(m_copiar_eventos && m_presion_canal[e->grupo()][e->canal()] != nullptr)
				delete m_presion_canal[e->grupo()][e->canal()];

			m_presion_canal[e->grupo()][e->canal()] = e;
		}
		else if(e->tipo_voz_de_canal() == EventoMidi_InflexionDeTono)
		{
			m_grupos_usados[e->grupo()] = true;
			m_canales_usados[e->grupo()][e->canal()] = true;
			m_procesamiento_requerido = true;

			if(m_copiar_eventos && m_inflexion_tono[e->grupo()][e->canal()] != nullptr)
				delete m_inflexion_tono[e->grupo()][e->canal()];

			m_inflexion_tono[e->grupo()][e->canal()] = e;
		}
		else if(e->tipo_voz_de_canal() == EventoMidi_ControladorRegistrado ||
				e->tipo_voz_de_canal() == EventoMidi_ControladorNoRegistrado ||
				e->tipo_voz_de_canal() == EventoMidi_ControladorRelativoRegistrado ||
				e->tipo_voz_de_canal() == EventoMidi_ControladorRelativoNoRegistrado)
			m_eventos_repetibles.push_back(e);
	}
	else if(e->tipo_mensaje() == TipoMensaje_DatosFlexibles)
	{
		if(e->tipo_dato_flexible() == DatosFlexibles_Tempo)
		{
			m_grupos_usados[e->grupo()] = true;
			m_procesamiento_requerido = true;

			if(m_copiar_eventos && m_datos_flexibles_unicos[e->grupo()][PosicionDatosFlexibles::Tempo] != nullptr)
				delete m_datos_flexibles_unicos[e->grupo()][PosicionDatosFlexibles::Tempo];

			m_datos_flexibles_unicos[e->grupo()][PosicionDatosFlexibles::Tempo] = e;
		}
		else if(e->tipo_dato_flexible() == DatosFlexibles_Compas)
		{
			m_grupos_usados[e->grupo()] = true;
			m_procesamiento_requerido = true;

			if(m_copiar_eventos && m_datos_flexibles_unicos[e->grupo()][PosicionDatosFlexibles::Compas] != nullptr)
				delete m_datos_flexibles_unicos[e->grupo()][PosicionDatosFlexibles::Compas];

			m_datos_flexibles_unicos[e->grupo()][PosicionDatosFlexibles::Compas] = e;
		}
		else if(e->tipo_dato_flexible() == DatosFlexibles_Metronomo)
		{
			m_grupos_usados[e->grupo()] = true;
			m_procesamiento_requerido = true;

			if(m_copiar_eventos && m_datos_flexibles_unicos[e->grupo()][PosicionDatosFlexibles::Metronomo] != nullptr)
				delete m_datos_flexibles_unicos[e->grupo()][PosicionDatosFlexibles::Metronomo];

			m_datos_flexibles_unicos[e->grupo()][PosicionDatosFlexibles::Metronomo] = e;
		}
		else if(e->tipo_dato_flexible() == DatosFlexibles_Armadura)
		{
			m_grupos_usados[e->grupo()] = true;
			m_procesamiento_requerido = true;

			if(m_copiar_eventos && m_datos_flexibles_unicos[e->grupo()][PosicionDatosFlexibles::Armadura] != nullptr)
				delete m_datos_flexibles_unicos[e->grupo()][PosicionDatosFlexibles::Armadura];

			m_datos_flexibles_unicos[e->grupo()][PosicionDatosFlexibles::Armadura] = e;
		}
		else if(e->tipo_dato_flexible() == DatosFlexibles_Acorde)
		{
			m_grupos_usados[e->grupo()] = true;
			m_procesamiento_requerido = true;

			if(m_copiar_eventos && m_datos_flexibles_unicos[e->grupo()][PosicionDatosFlexibles::Acorde] != nullptr)
				delete m_datos_flexibles_unicos[e->grupo()][PosicionDatosFlexibles::Acorde];

			m_datos_flexibles_unicos[e->grupo()][PosicionDatosFlexibles::Acorde] = e;
		}
		else
			m_eventos_repetibles.push_back(e);
	}
	else if(e->tipo_mensaje() == TipoMensaje_Reservado0x6_32bits ||
			e->tipo_mensaje() == TipoMensaje_Reservado0x7_32bits ||
			e->tipo_mensaje() == TipoMensaje_Reservado0x8_64bits ||
			e->tipo_mensaje() == TipoMensaje_Reservado0x9_64bits ||
			e->tipo_mensaje() == TipoMensaje_Reservado0xA_64bits ||
			e->tipo_mensaje() == TipoMensaje_Reservado0xB_96bits ||
			e->tipo_mensaje() == TipoMensaje_Reservado0xC_96bits ||
			e->tipo_mensaje() == TipoMensaje_Reservado0xE_128bits)
		m_eventos_repetibles.push_back(e);
}

void Seguidor_Eventos::procesar_eventos()
{
	m_actual_repetible = 0;
	m_actual_listos = 0;
	if(m_procesamiento_requerido)
	{
		m_procesamiento_requerido = false;
		m_eventos_listos.clear();

		if(m_mensaje_utilidad_dctpq != nullptr)
			m_eventos_listos.push_back(m_mensaje_utilidad_dctpq);

		for(unsigned char grupo=0; grupo<GRUPOS_MIDI; grupo++)
		{
			if(!m_grupos_usados[grupo])
				continue;

			for(unsigned char canal=0; canal<CANALES_MIDI; canal++)
			{
				if(!m_canales_usados[grupo][canal])
					continue;

				if(m_hay_controlador)
				{
					//Se guardan ordenados los eventos (solo por gusto)
					//bit extremo izquierdo primero y luego el bit extremo derecho
					for(unsigned char mensaje = 0; mensaje < 32; mensaje++)
					{
						if(m_controlador[grupo][canal][mensaje] != nullptr)
							m_eventos_listos.push_back(m_controlador[grupo][canal][mensaje]);
						if(m_controlador[grupo][canal][mensaje+32] != nullptr)
							m_eventos_listos.push_back(m_controlador[grupo][canal][mensaje]);
					}
					//Los demas controladores
					for(unsigned char mensaje = 64; mensaje < CONTROLADORES_MIDI; mensaje++)
					{
						if(m_controlador[grupo][canal][mensaje] != nullptr)
							m_eventos_listos.push_back(m_controlador[grupo][canal][mensaje]);
					}
				}

				if(m_programa[grupo][canal] != nullptr)
					m_eventos_listos.push_back(m_programa[grupo][canal]);

				if(m_controlador_banco_no_confirmado[grupo][canal][PosicionControladorBanco::Banco_BEI] != nullptr)
					m_eventos_listos.push_back(m_controlador_banco_no_confirmado[grupo][canal][PosicionControladorBanco::Banco_BEI]);

				if(m_controlador_banco_no_confirmado[grupo][canal][PosicionControladorBanco::Banco_BED] != nullptr)
					m_eventos_listos.push_back(m_controlador_banco_no_confirmado[grupo][canal][PosicionControladorBanco::Banco_BED]);

				if(m_presion_canal[grupo][canal] != nullptr)
					m_eventos_listos.push_back(m_presion_canal[grupo][canal]);

				if(m_inflexion_tono[grupo][canal] != nullptr)
					m_eventos_listos.push_back(m_inflexion_tono[grupo][canal]);
			}

			for(unsigned char datos_flexibles=0; datos_flexibles < PosicionDatosFlexibles::TotalDF; datos_flexibles++)
			{
				if(m_datos_flexibles_unicos[grupo][datos_flexibles] != nullptr)
					m_eventos_listos.push_back(m_datos_flexibles_unicos[grupo][datos_flexibles]);
			}
		}
	}
}

Evento_Midi *Seguidor_Eventos::siguiente_evento()
{
	Evento_Midi *actual = nullptr;
	if(m_actual_repetible < m_eventos_repetibles.size())
	{
		actual = m_eventos_repetibles[m_actual_repetible];
		m_actual_repetible++;
		return actual;
	}
	else if(m_actual_listos < m_eventos_listos.size())
	{
		actual = m_eventos_listos[m_actual_listos];
		m_actual_listos++;
		return actual;
	}
	else
	{
		m_termino_enviar_eventos = true;
		return actual;
	}
}

void Seguidor_Eventos::borrar_registro_eventos()
{
	this->vaciar_memoria();
	this->inicializar();
}

bool Seguidor_Eventos::hay_eventos()
{
	return m_hay_eventos;
}

bool Seguidor_Eventos::termino_enviar_eventos()
{
	return m_termino_enviar_eventos;
}

bool Seguidor_Eventos::grupo_usado(unsigned char grupo) const
{
	if(grupo >= GRUPOS_MIDI)
		return false;

	return m_grupos_usados[grupo];
}

bool Seguidor_Eventos::canal_usado(unsigned char grupo, unsigned char canal) const
{
	if(grupo >= GRUPOS_MIDI)
		return false;

	if(canal >= GRUPOS_MIDI)
		return false;

	return m_canales_usados[grupo][canal];
}

bool Seguidor_Eventos::programa_cambiado(unsigned char grupo, unsigned char canal)
{
	if(m_programa[grupo][canal] != nullptr)
		return true;
	return false;
}
