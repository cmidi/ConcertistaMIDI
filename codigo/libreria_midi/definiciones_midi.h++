#ifndef DEFINICIONES_MIDI
#define DEFINICIONES_MIDI

#define MINUTO_EN_MICROSEGUNDO 60000000
#define PPM_PREDEFINIDO 120
#define MICROSEGUNDOS_POR_NEGRA MINUTO_EN_MICROSEGUNDO / PPM_PREDEFINIDO

#define GRUPOS_MIDI 16
#define CANALES_MIDI 16
#define CONTROLADORES_MIDI 128

#define VALOR_MEDIO_7BITS 0x40
#define VALOR_MEDIO_16BITS 0x8000

//Tipo de mensaje Midi 2.0
#define TipoMensaje_Utilidad 0x00
#define TipoMensaje_ComunYTiempoReal 0x10
#define TipoMensaje_VozDeCanalMidi1 0x20
#define TipoMensaje_ExclusivoDelSistema7bits 0x30
#define TipoMensaje_VozDeCanalMidi2 0x40
#define TipoMensaje_ExclusivoDelSistema8bits 0x50
#define TipoMensaje_Reservado0x6_32bits 0x60
#define TipoMensaje_Reservado0x7_32bits 0x70
#define TipoMensaje_Reservado0x8_64bits 0x80
#define TipoMensaje_Reservado0x9_64bits 0x90
#define TipoMensaje_Reservado0xA_64bits 0xA0
#define TipoMensaje_Reservado0xB_96bits 0xB0
#define TipoMensaje_Reservado0xC_96bits 0xC0
#define TipoMensaje_DatosFlexibles 0xD0
#define TipoMensaje_Reservado0xE_128bits 0xE0
#define TipoMensaje_FlujoPaqueteMidiUniversal 0xF0

//Mensajes de Utilidad Midi 2.0
#define MensajeUtilidad_NoOperacion 0x00
#define MensajeUtilidad_RelojJR 0x10
#define MensajeUtilidad_MarcaDeTiempoJR 0x20
#define MensajeUtilidad_DivicionDeTiempo 0x30
#define MensajeUtilidad_TiempoDelta 0x40

//Eventos en Tiempo Real Midi 1.0 y Midi 2.0
#define TiempoReal_CodigoTiempoMidi 0xF1
#define TiempoReal_PosicionCancion 0xF2
#define TiempoReal_SeleccionCancion 0xF3
#define TiempoReal_SolicitudSincronizacion 0xF6
#define TiempoReal_TiempoReloj 0xF8
#define TiempoReal_Iniciar 0xFA
#define TiempoReal_Continuar 0xFB
#define TiempoReal_Parar 0xFC
#define TiempoReal_DeteccionActiva 0xFE
#define TiempoReal_Reiniciar 0xFF

//Eventos propios, solo con MIDI 1.0
#define EventoMidi_ClienteConectado 0x00
#define EventoMidi_ClienteDesconectado 0x01
#define EventoMidi_PuertoConectado 0x02
#define EventoMidi_PuertoDesconectado 0x03
#define EventoMidi_PuertoSuscrito 0x04
#define EventoMidi_PuertoDesuscrito 0x05

//Eventos Midi 1.0
#define EventoMidi_NotaApagada 0x80
#define EventoMidi_NotaEncendida 0x90
#define EventoMidi_PresionNota 0xA0
#define EventoMidi_Controlador 0xB0
#define EventoMidi_Programa 0xC0
#define EventoMidi_PresionCanal 0xD0
#define EventoMidi_InflexionDeTono 0xE0
#define EventoMidi_ExclusivoDelSistema 0xF0
#define EventoMidi_SecuenciaDeEscape 0xF7
#define EventoMidi_Metaevento 0xFF

//Eventos Midi 2.0 (nuevos)
#define EventoMidi_ControladorPorNotaRegistrado 0x00
#define EventoMidi_ControladorPorNotaNoRegistrado 0x10
#define EventoMidi_ControladorRegistrado 0x20
#define EventoMidi_ControladorNoRegistrado 0x30
#define EventoMidi_ControladorRelativoRegistrado 0x40
#define EventoMidi_ControladorRelativoNoRegistrado 0x50
#define EventoMidi_InflexionDeTonoPorNota 0x60
//#define EventoMidi_Indefinido 0x70
#define EventoMidi_GestionPorNota 0xF0

//MetaEventos Midi 1.0
#define MetaEventoMidi_SecuenciaNumerica 0x00
#define MetaEventoMidi_Texto 0x01
#define MetaEventoMidi_DerechosDeAutor 0x02
#define MetaEventoMidi_NombreDePista 0x03
#define MetaEventoMidi_NombreDeInstrumento 0x04
#define MetaEventoMidi_Letra 0x05
#define MetaEventoMidi_Marcador 0x06
#define MetaEventoMidi_PuntoReferencia 0x07
#define MetaEventoMidi_NombreDelPrograma 0x08
#define MetaEventoMidi_NombreDelDispositivo 0x09
#define MetaEventoMidi_PrefijoCanal 0x20//Evento obsoleto (puede usarse para guardar las pista en formato 0)
#define MetaEventoMidi_PuertoMidi 0x21//Evento obsoleto
#define MetaEventoMidi_FinDePista 0x2F
#define MetaEventoMidi_Tempo 0x51
#define MetaEventoMidi_SMPTE 0x54 //FF 54 05 hora, minuto, segundo, fotograma, centecima de fotograma
#define MetaEventoMidi_Compas 0x58
#define MetaEventoMidi_Armadura 0x59
#define MetaEventoMidi_PrefijoTipoParcheXMF 0x60
#define MetaEventoMidi_EspecificoDelSecuenciador 0x7F

#define DatosFlexiblesDireccion_Canal 0x00
#define DatosFlexiblesDireccion_Grupo 0x01

#define DatosFlexiblesBanco_Configuracion 0x00
#define DatosFlexiblesBanco_Metadatos 0x01
#define DatosFlexiblesBanco_TextoDeInterpretacion 0x02

//Datos Flexibles Midi 2.0 (16 bits)
#define DatosFlexibles_Tempo 0x0000
#define DatosFlexibles_Compas 0x0001
#define DatosFlexibles_Metronomo 0x0002
#define DatosFlexibles_Armadura 0x0005
#define DatosFlexibles_Acorde 0x0006
#define DatosFlexibles_TextoDatosDesconocido 0x0100
#define DatosFlexibles_NompreProyecto 0x0101
#define DatosFlexibles_NombreCompositorCancion 0x0102
#define DatosFlexibles_NombreFragmentoMidi 0x0103
#define DatosFlexibles_DerechosDeAutor 0x0104
#define DatosFlexibles_NombreCompositor 0x0105
#define DatosFlexibles_NombreLetrista 0x0106
#define DatosFlexibles_NombreArreglista 0x0107
#define DatosFlexibles_Editor 0x0108
#define DatosFlexibles_ArtistaPrincipal 0x0109
#define DatosFlexibles_ArtistaAcompannante 0x010A
#define DatosFlexibles_Fecha 0x010B
#define DatosFlexibles_Localizacion 0x010C
#define DatosFlexibles_TextoLetraDesconocido 0x0200
#define DatosFlexibles_Letra 0x0201
#define DatosFlexibles_IdiomaLetra 0x0202
#define DatosFlexibles_Ruby 0x0203
#define DatosFlexibles_IdiomaRuby 0x0204

//Mensaje de flujo Midi 2.0 (10 bits)
#define MensajeDeFlujo_DescubrimientoDispositivos 0x000
#define MensajeDeFlujo_RespuestaInformacionDispositivo 0x001
#define MensajeDeFlujo_RespuestaIdentidadDispositivo 0x002
#define MensajeDeFlujo_RespuestaNombreDispositivo 0x003
#define MensajeDeFlujo_RespuestaIDInstanciaProducto 0x004
#define MensajeDeFlujo_SolicitudConfiguracionTransmision 0x005
#define MensajeDeFlujo_RespuestaConfiguracionTransmision 0x006
#define MensajeDeFlujo_DescubrimientoBloqueDeFuncion 0x010
#define MensajeDeFlujo_RespuestaInformacionBloqueDeFuncion 0x011
#define MensajeDeFlujo_RespuestaNombreBloqueDeFuncion 0x012
#define MensajeDeFlujo_InicioDeFragmento 0x020
#define MensajeDeFlujo_FinDeFragmento 0x021

#define EstadoMensaje_Unico 0x00
#define EstadoMensaje_Inicio 0x10
#define EstadoMensaje_Continua 0x20
#define EstadoMensaje_Fin 0x30
//#define EstadoMensaje_Indefinido 0x40
//#define EstadoMensaje_Indefinido 0x50
//#define EstadoMensaje_Indefinido 0x60
//#define EstadoMensaje_Indefinido 0x70
#define EstadoMensaje_DatosMixtosEncabezado 0x80
#define EstadoMensaje_DatosMixtosContenido 0x90

//Mensaje controlador
#define MensajeControlador_SeleccionDeBanco_BEI 0x00
#define MensajeControlador_RuedaDeModulacion_BEI 0x01
#define MensajeControlador_ControlDeRespiracion_BEI 0x02
//#define MensajeControlador_Indefinido 0x03
#define MensajeControlador_ControlDePie_BEI 0x04
#define MensajeControlador_TiempoDePortamento_BEI 0x05
#define MensajeControlador_EntradaDeDatos_BEI 0x06
#define MensajeControlador_VolumenDelCanal_BEI 0x07
#define MensajeControlador_Balance_BEI 0x08
//#define MensajeControlador_Indefinido 0x09
#define MensajeControlador_Pan_BEI 0x0A
#define MensajeControlador_ControlDeExpresion_BEI 0x0B
#define MensajeControlador_Efecto1_BEI 0x0C
#define MensajeControlador_Efecto2_BEI 0x0D
//#define MensajeControlador_Indefinido 0x0E
//#define MensajeControlador_Indefinido 0x0F
#define MensajeControlador_PropositoGeneral1_BEI 0x10
#define MensajeControlador_PropositoGeneral2_BEI 0x11
#define MensajeControlador_PropositoGeneral3_BEI 0x12
#define MensajeControlador_PropositoGeneral4_BEI 0x13
//#define MensajeControlador_Indefinido 0x14
//#define MensajeControlador_Indefinido 0x15
//#define MensajeControlador_Indefinido 0x16
//#define MensajeControlador_Indefinido 0x17
//#define MensajeControlador_Indefinido 0x18
//#define MensajeControlador_Indefinido 0x19
//#define MensajeControlador_Indefinido 0x1A
//#define MensajeControlador_Indefinido 0x1B
//#define MensajeControlador_Indefinido 0x1C
//#define MensajeControlador_Indefinido 0x1D
//#define MensajeControlador_Indefinido 0x1E
//#define MensajeControlador_Indefinido 0x1F
#define MensajeControlador_SeleccionDeBanco_BED 0x20
#define MensajeControlador_RuedaDeModulacion_BED 0x21
#define MensajeControlador_ControlDeRespiracion_BED 0x22
//#define MensajeControlador_Indefinido 0x23
#define MensajeControlador_ControlDePie_BED 0x24
#define MensajeControlador_TiempoDePortamento_BED 0x25
#define MensajeControlador_EntradaDeDatos_BED 0x26
#define MensajeControlador_VolumenDelCanal_BED 0x27
#define MensajeControlador_Balance_BED 0x28
//#define MensajeControlador_Indefinido 0x29
#define MensajeControlador_Pan_BED 0x2A
#define MensajeControlador_ControlDeExpresion_BED 0x2B
#define MensajeControlador_Efecto1_BED 0x2C
#define MensajeControlador_Efecto2_BED 0x2D
//#define MensajeControlador_Indefinido 0x2E
//#define MensajeControlador_Indefinido 0x2F
#define MensajeControlador_PropositoGeneral1_BED 0x30
#define MensajeControlador_PropositoGeneral2_BED 0x31
#define MensajeControlador_PropositoGeneral3_BED 0x32
#define MensajeControlador_PropositoGeneral4_BED 0x33
//#define MensajeControlador_Indefinido 0x34
//#define MensajeControlador_Indefinido 0x35
//#define MensajeControlador_Indefinido 0x36
//#define MensajeControlador_Indefinido 0x37
//#define MensajeControlador_Indefinido 0x38
//#define MensajeControlador_Indefinido 0x39
//#define MensajeControlador_Indefinido 0x3A
//#define MensajeControlador_Indefinido 0x3B
//#define MensajeControlador_Indefinido 0x3C
//#define MensajeControlador_Indefinido 0x3D
//#define MensajeControlador_Indefinido 0x3E
//#define MensajeControlador_Indefinido 0x3F
#define MensajeControlador_PedalDeAmortiguador 0x40
#define MensajeControlador_Portamento 0x41
#define MensajeControlador_Sostenuto 0x42
#define MensajeControlador_PedalSuave 0x43
#define MensajeControlador_PedalLegato 0x44
#define MensajeControlador_Sostenido 0x45
#define MensajeControlador_Sonido1 0x46
#define MensajeControlador_Sonido2 0x47
#define MensajeControlador_Sonido3 0x48
#define MensajeControlador_Sonido4 0x49
#define MensajeControlador_Sonido5 0x4A
#define MensajeControlador_Sonido6 0x4B
#define MensajeControlador_Sonido7 0x4C
#define MensajeControlador_Sonido8 0x4D
#define MensajeControlador_Sonido9 0x4E
#define MensajeControlador_Sonido10 0x4F
#define MensajeControlador_PropositoGeneral5 0x50
#define MensajeControlador_PropositoGeneral6 0x51
#define MensajeControlador_PropositoGeneral7 0x52
#define MensajeControlador_PropositoGeneral8 0x53
#define MensajeControlador_ControlDePortamento 0x54
//#define MensajeControlador_Indefinido 0x55
//#define MensajeControlador_Indefinido 0x56
//#define MensajeControlador_Indefinido 0x57
//#define MensajeControlador_Indefinido 0x58
//#define MensajeControlador_Indefinido 0x59
//#define MensajeControlador_Indefinido 0x5A
#define MensajeControlador_EfectoProfundida1 0x5B
#define MensajeControlador_EfectoProfundida2 0x5C
#define MensajeControlador_EfectoProfundida3 0x5D
#define MensajeControlador_EfectoProfundida4 0x5E
#define MensajeControlador_EfectoProfundida5 0x5F
#define MensajeControlador_EntradaDeDatosMas1 0x60
#define MensajeControlador_EntradaDeDatosMenos1 0x61
#define MensajeControlador_ParametroNoRegistrado_BED 0x62
#define MensajeControlador_ParametroNoRegistrado_BEI 0x63
#define MensajeControlador_ParametroRegistrado_BED 0x64
#define MensajeControlador_ParametroRegistrado_BEI 0x65
//#define MensajeControlador_Indefinido 0x66
//#define MensajeControlador_Indefinido 0x67
//#define MensajeControlador_Indefinido 0x68
//#define MensajeControlador_Indefinido 0x69
//#define MensajeControlador_Indefinido 0x6A
//#define MensajeControlador_Indefinido 0x6B
//#define MensajeControlador_Indefinido 0x6C
//#define MensajeControlador_Indefinido 0x6D
//#define MensajeControlador_Indefinido 0x6E
//#define MensajeControlador_Indefinido 0x6F
//#define MensajeControlador_Indefinido 0x70
//#define MensajeControlador_Indefinido 0x71
//#define MensajeControlador_Indefinido 0x72
//#define MensajeControlador_Indefinido 0x73
//#define MensajeControlador_Indefinido 0x74
//#define MensajeControlador_Indefinido 0x75
//#define MensajeControlador_Indefinido 0x76
//#define MensajeControlador_Indefinido 0x77
#define MensajeControlador_TodosLosSonidosApagados 0x78
#define MensajeControlador_RestablecerTodosLosControles 0x79
#define MensajeControlador_ControlLocal 0x7A
#define MensajeControlador_TodasLasNotasApagadas 0x7B
#define MensajeControlador_ModoOmniApagado 0x7C
#define MensajeControlador_ModoOmniActivado 0x7D
#define MensajeControlador_ModoMono 0x7E
#define MensajeControlador_ModoPoli 0x7F

//Parametros Registrados
#define ParametroRegistrado_RangoInflexionDeTono 0x0000			//0x00 0x00
#define ParametroRegistrado_AjusteFinoDeCanal 0x0001			//0x00 0x01
#define ParametroRegistrado_AjusteGruesoDeCanal 0x0002			//0x00 0x02
#define ParametroRegistrado_AjustePrograma 0x0003				//0x00 0x03
#define ParametroRegistrado_AjusteSeleccionBanco 0x0004			//0x00 0x04
#define ParametroRegistrado_RangoProfundidadDeModulacion 0x0005	//0x00 0x05
//Parametros Registrados Reservados para el futuro (0x3D)
#define ParametroRegistrado_AnguloDeAzimut 0x1E80				//0x3D 0x00
#define ParametroRegistrado_AnguloDeElevacion 0x1E81			//0x3D 0x01
#define ParametroRegistrado_Ganancia 0x1E82						//0x3D 0x02
#define ParametroRegistrado_RelacionDeDistancia 0x1E83			//0x3D 0x03
#define ParametroRegistrado_DistanciaMaxima 0x1E84				//0x3D 0x04
#define ParametroRegistrado_GananciaDistanciaMaxima 0x1E85		//0x3D 0x05
#define ParametroRegistrado_RelacionDistanciaDeReferencia 0x1E86//0x3D 0x06
#define ParametroRegistrado_AnguloExtensionDePan 0x1E87			//0x3D 0x07
#define ParametroRegistrado_AnguloDeBalanceo 0x1E88				//0x3D 0x08

#define ParametroRegistrado_Fin 0x3FFF							//0x7F 0x7F

//Parametros No Registrados
#define ParametroNoRegistrado_Fin 0x3FFF						//0x7F 0x7F

#define Armadura_0 0
#define Armadura_1Bemol -1
#define Armadura_2Bemol -2
#define Armadura_3Bemol -3
#define Armadura_4Bemol -4
#define Armadura_5Bemol -5
#define Armadura_6Bemol -6
#define Armadura_7Bemol -7
#define Armadura_1Sostenido 1
#define Armadura_2Sostenido 2
#define Armadura_3Sostenido 3
#define Armadura_4Sostenido 4
#define Armadura_5Sostenido 5
#define Armadura_6Sostenido 6
#define Armadura_7Sostenido 7

#define Escala_Mayor 0
#define Escala_Menor 1

//Dinamica
enum class Dinamica : unsigned char
{
	PPP = 0,
	PP,
	P,
	MP,
	MF,
	F,
	FF,
	FFF,
	SinCambio
};

enum class VersionMidi : unsigned char
{
	Version_1 = 1,
	Version_2 = 2
};

enum class Percusion : unsigned char
{
	GM,			//GM
	GM2,		//GM2
	XG_127,		//XG
	XG_126,		//XG
	XG_64,		//XG
	GS_Map1,	//GS
	GS_Map2,	//GS
	Desactivado
};

enum EspecificacionMidi : unsigned char
{
	GM = 0,	//General Midi
	GM2,	//General Midi 2
	XG,		//Extended General Midi (Yamaha)
	GS,		//General Standard (Roland)
	MPC,	//Multimedia PC (Microsoft)
	Desactivado,
};

enum class SistemaNombreNotas : unsigned char
{
	DoFijo = 0,
	DoVariable,
	Ingles,
	Numerico,
};

enum class FiguraMusical : unsigned char
{
	Maxima = 0,
	Longa,
	Cuadrada,
	Redonda,
	Blanca,
	Negra,
	Corchea,
	SemiCorchea,
	Fusa,
	SemiFusa,
	Garrapatea,
	SemiGarrapatea
};

#endif
