#ifndef EXCEPCION_MIDI_H
#define EXCEPCION_MIDI_H

#include <exception>
#include <string>

enum class CodigoErrorMidi : unsigned char
{
	FalloAbrirArchivo,
	ArchivoMidi2_No_Compatible,
	NoEsArchivoRMID,
	RMIDSinDatos,
	RMIDSinDatosMidi,
	ArchivoDesconocido,
	ArchivoMidiMuyCorto,
	EncabezadoMidiMuyCorto,
	FormatoMidiDesconocido,
	ArchivoMidiSinPistas,
	TiempoSMPTENoSoportado,
	ArchivoSinPMU,

	EventoNulo,
	NoEsEventoMidi1,
	NoEsEventoMidi2,
	EventoNoDisponible,
	EventoNoValido,
	EventoLargoIncorrecto,

	NoEsMensajeDeUtilidad,
	NoEsMensajeComunYTiempoReal,
	NoEsMensajeDeVozDeCanal,
	NoEsMensajeSistemaExclusivo8,
	NoEsMensajeDeDatosFlexibles,
	NoEsMensajeDeFlujoPMU,
	NoEsEventoDeVozDeCanalMidi2,

	EventoNoContieneBanco,

	NoEsDivicionDeTiempo,
	NoEsTiempoDelta,
	EventoNoContieneNota,
	EventoNoContieneGrupo,
	EventoNoContieneCanal,
	EventoNoContieneVelocidad,
	EventoNoContieneTexto,
	EventoNoContieneEstadoMensaje,
	EventoNoContieneNumeroBytes,
	NoEsEventoPresionNota,
	NoEsEventoPresionCanal,
	NoEsEventoPrograma,
	NoEsEventoInflexionDeTono,
	NoEsEventoControlador,

	NoEsDatoFlexibleTempo,
	NoEsDatoFlexibleCompas,
	NoEsDatoFlexibleArmadura,

	NoEsMetaEvento,
	NoEsMetaEventoSecuenciaNumerica,
	NoEsMetaEventoTempo,
	NoEsMetaEventoSMPTE,
	NoEsMetaEventoCompas,
	NoEsMetaEventoArmadura,

	IntentoAsignarGrupoAEventoV1,
	IntentoAsignarCanalAMetaEventoV1,

	FalloCrearArchivo,
};

class Excepcion_Midi : public std::exception
{
private:
	const CodigoErrorMidi m_codigo_error;
	const char *m_error;
public:
	Excepcion_Midi(CodigoErrorMidi codigo_error);
	const char *what() const noexcept;
	std::string descripcion_error() const noexcept;
};

#endif
