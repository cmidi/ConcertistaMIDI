#ifndef FUNCIONES_MIDI_H
#define FUNCIONES_MIDI_H

#include "definiciones_midi.h++"

namespace Funciones_Midi
{
	unsigned int us_a_ppm(unsigned int us);
	Dinamica velocidad_a_dinamica(unsigned char velocidad);
	unsigned char desplazamiento_notas(char armadura, unsigned char escala);

	unsigned short escalar_7a16bits(unsigned char valor);
	unsigned char escalar_16a7bits(unsigned short valor);
}

#endif
