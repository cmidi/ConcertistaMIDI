#include "descripciones_midi.h++"
#include "../util/traduccion.h++"

std::string Nombre_TipoMensaje(unsigned char mensaje)
{
	mensaje = mensaje & 0xF0;
	if(mensaje == TipoMensaje_Utilidad)
		return "Utilidad";
	else if(mensaje == TipoMensaje_ComunYTiempoReal)
		return "Comun y Tiempo Real";
	else if(mensaje == TipoMensaje_VozDeCanalMidi1)
		return "Voz de Canal MIDI 1.0";
	else if(mensaje == TipoMensaje_ExclusivoDelSistema7bits)
		return "Exclusivo del Sistema de 7 bits";
	else if(mensaje == TipoMensaje_VozDeCanalMidi2)
		return "Voz de Canal MIDI 2.0";
	else if(mensaje == TipoMensaje_ExclusivoDelSistema8bits)
		return "Exclusivo del Sistema 8 btis";
	else if(mensaje == TipoMensaje_Reservado0x6_32bits ||
			mensaje == TipoMensaje_Reservado0x7_32bits)
		return "Mensaje Reservado 32 bits";
	else if(mensaje == TipoMensaje_Reservado0x8_64bits ||
			mensaje == TipoMensaje_Reservado0x9_64bits ||
			mensaje == TipoMensaje_Reservado0xA_64bits)
		return "Mensaje Reservado 64 bits";
	else if(mensaje == TipoMensaje_Reservado0xB_96bits ||
			mensaje == TipoMensaje_Reservado0xC_96bits)
		return "Mensaje Reservado 96 bits";
	else if(mensaje == TipoMensaje_DatosFlexibles)
		return "Datos Flexibles";
	else if(mensaje == TipoMensaje_Reservado0xE_128bits)
		return "Mensaje Reservado 128 bits";
	else if(mensaje == TipoMensaje_FlujoPaqueteMidiUniversal)
		return "Paquete MIDI Universal de Flujo";
	else
		return "Tipo de Mensaje no definido";
}

std::string Nombre_Utilidad(unsigned char mensaje)
{
	mensaje = mensaje & 0xF0;
	if(mensaje == MensajeUtilidad_NoOperacion)
		return "No Operación";
	else if(mensaje == MensajeUtilidad_RelojJR)
		return "Reloj Reducción de Fluctuación (JR)";
	else if(mensaje == MensajeUtilidad_MarcaDeTiempoJR)
		return "Marca de Tiempo Reducción de Fluctuación";
	else if(mensaje == MensajeUtilidad_DivicionDeTiempo)
		return "Divición de Tiempo";
	else if(mensaje == MensajeUtilidad_TiempoDelta)
		return "Tiempo Delta";
	else
		return "Mensaje de Utilidad no definido";
}

std::string Nombre_TiempoReal(unsigned char mensaje)
{
	if(mensaje == TiempoReal_CodigoTiempoMidi)
		return "Codigo de Tiempo MIDI";
	else if(mensaje == TiempoReal_PosicionCancion)
		return "Posicion de la Canción";
	else if(mensaje == TiempoReal_SeleccionCancion)
		return "Selección de la Canción";
	else if(mensaje == TiempoReal_SolicitudSincronizacion)
		return "Solicitud de Sincronización";
	else if(mensaje == TiempoReal_TiempoReloj)
		return "Tiempo de Reloj";
	else if(mensaje == TiempoReal_Iniciar)
		return "Iniciar";
	else if(mensaje == TiempoReal_Continuar)
		return "Continuar";
	else if(mensaje == TiempoReal_Parar)
		return "Parar";
	else if(mensaje == TiempoReal_DeteccionActiva)
		return "Detección Activa";
	else if(mensaje == TiempoReal_Reiniciar)
		return "Reiniciar";
	else
		return "Tiempo Real no definido";
}

std::string Nombre_VozDeCanalMidi1(unsigned char mensaje)
{
	if((mensaje & 0xF0) == EventoMidi_NotaApagada)
		return "Nota Apagada";
	else if((mensaje & 0xF0) == EventoMidi_NotaEncendida)
		return "Nota Encendida";
	else if((mensaje & 0xF0) == EventoMidi_PresionNota)
		return "Presión Nota (Despues de Tocar)";
	else if((mensaje & 0xF0) == EventoMidi_Controlador)
		return "Controlador";
	else if((mensaje & 0xF0) == EventoMidi_Programa)
		return "Programa";
	else if((mensaje & 0xF0) == EventoMidi_PresionCanal)
		return "Presión Canal (Despues de Tocar)";
	else if((mensaje & 0xF0) == EventoMidi_InflexionDeTono)
		return "Inflexion de Tono";
	else if(mensaje == EventoMidi_ExclusivoDelSistema)
		return "Exclusivo del Sistema";
	else if(mensaje == EventoMidi_SecuenciaDeEscape)
		return "Secuencia de Escape";
	else if(mensaje == EventoMidi_Metaevento)
		return "Metaevento";
	else
		return "Voz de Canal MIDI 1.0 no definido";
}

std::string Nombre_VozDeCanalMidi2(unsigned char mensaje)
{
	mensaje = mensaje & 0xF0;
	if(mensaje == EventoMidi_ControladorPorNotaRegistrado)
		return "Controlador por Nota Registrado";
	else if(mensaje == EventoMidi_ControladorPorNotaNoRegistrado)
		return "Controlador por Nota No Registrado";
	else if(mensaje == EventoMidi_ControladorRegistrado)
		return "Controlador Registrado";
	else if(mensaje == EventoMidi_ControladorNoRegistrado)
		return "Controlador No Registrado";
	else if(mensaje == EventoMidi_ControladorRelativoRegistrado)
		return "Controlador Relativo Registrado";
	else if(mensaje == EventoMidi_ControladorRelativoNoRegistrado)
		return "Controlador Relativo No Registrado";
	else if(mensaje == EventoMidi_InflexionDeTonoPorNota)
		return "Inflexion de Tono por Nota";
	else if(mensaje == EventoMidi_NotaApagada)
		return "Nota Apagada";
	else if(mensaje == EventoMidi_NotaEncendida)
		return "Nota Encendida";
	else if(mensaje == EventoMidi_PresionNota)
		return "Presión Nota (Despues de Tocar)";
	else if(mensaje == EventoMidi_Controlador)
		return "Controlador";
	else if(mensaje == EventoMidi_Programa)
		return "Programa";
	else if(mensaje == EventoMidi_PresionCanal)
		return "Presión Canal (Despues de Tocar)";
	else if(mensaje == EventoMidi_InflexionDeTono)
		return "Inflexion de Tono";
	else if(mensaje == EventoMidi_GestionPorNota)
		return "Gestion Por Nota";
	else
		return "Voz de Canal MIDI 2.0 no definido";
}

std::string Nombre_DatosFlexibles(unsigned short mensaje)
{
	if(mensaje == DatosFlexibles_Tempo)
		return "Tempo";
	else if(mensaje == DatosFlexibles_Compas)
		return "Compas";
	else if(mensaje == DatosFlexibles_Metronomo)
		return "Metronomo";
	else if(mensaje == DatosFlexibles_Armadura)
		return "Armadura";
	else if(mensaje == DatosFlexibles_Acorde)
		return "Acorde";
	else if(mensaje == DatosFlexibles_TextoDatosDesconocido)
		return "Texto Datos Desconocidos";
	else if(mensaje == DatosFlexibles_NompreProyecto)
		return "Nombre Proyecto";
	else if(mensaje == DatosFlexibles_NombreCompositorCancion)
		return "Nombre Compositor de la Canción";
	else if(mensaje == DatosFlexibles_NombreFragmentoMidi)
		return "Nombre Fragmento Midi";
	else if(mensaje == DatosFlexibles_DerechosDeAutor)
		return "Derechos de Autor";
	else if(mensaje == DatosFlexibles_NombreCompositor)
		return "Nombre Compositor";
	else if(mensaje == DatosFlexibles_NombreLetrista)
		return "Nombre Letrista";
	else if(mensaje == DatosFlexibles_NombreArreglista)
		return "Nombre Arreglista";
	else if(mensaje == DatosFlexibles_Editor)
		return "Editor";
	else if(mensaje == DatosFlexibles_ArtistaPrincipal)
		return "Artista Principal";
	else if(mensaje == DatosFlexibles_ArtistaAcompannante)
		return "Acompañante";
	else if(mensaje == DatosFlexibles_Fecha)
		return "Fecha";
	else if(mensaje == DatosFlexibles_Localizacion)
		return "Localización";
	else if(mensaje == DatosFlexibles_TextoLetraDesconocido)
		return "Texto Letra Datos desconocido";
	else if(mensaje == DatosFlexibles_Letra)
		return "Letra";
	else if(mensaje == DatosFlexibles_IdiomaLetra)
		return "Idioma de la Letra";
	else if(mensaje == DatosFlexibles_Ruby)
		return "Ruby";
	else if(mensaje == DatosFlexibles_IdiomaRuby)
		return "Idioma del Ruby";
	else
		return "Datos Flexibles no definido";
}

std::string Nombre_FlujoPaqueteMidiUniversal(unsigned short mensaje)
{
	mensaje = mensaje & 0x3FF;
	if(mensaje == MensajeDeFlujo_DescubrimientoDispositivos)
		return "Descubrimiento de Dispositivos";
	else if(mensaje == MensajeDeFlujo_RespuestaInformacionDispositivo)
		return "Respuesta Información de Dispositivo";
	else if(mensaje == MensajeDeFlujo_RespuestaIdentidadDispositivo)
		return "Respuesta Identidad de Dispositivo";
	else if(mensaje == MensajeDeFlujo_RespuestaNombreDispositivo)
		return "Respuesta Nombre de Dispositivo";
	else if(mensaje == MensajeDeFlujo_RespuestaIDInstanciaProducto)
		return "Respuesta Id de Instancia del Producto";
	else if(mensaje == MensajeDeFlujo_SolicitudConfiguracionTransmision)
		return "Solicitud de Configuración de Transmisión";
	else if(mensaje == MensajeDeFlujo_RespuestaConfiguracionTransmision)
		return "Respuesta Configuracion de Transmisión";
	else if(mensaje == MensajeDeFlujo_DescubrimientoBloqueDeFuncion)
		return "Descubrimiento de Bloque de Función";
	else if(mensaje == MensajeDeFlujo_RespuestaInformacionBloqueDeFuncion)
		return "Respuesta Información de Bloque de Función";
	else if(mensaje == MensajeDeFlujo_RespuestaNombreBloqueDeFuncion)
		return "Respuesta Nombre de Bloque de Función";
	else if(mensaje == MensajeDeFlujo_InicioDeFragmento)
		return "Inicio de Fragmento";
	else if(mensaje == MensajeDeFlujo_FinDeFragmento)
		return "Fin de Fragmento";
	else
		return "Flujo Paquete Midi Universal no definido";
}

std::string Nombre_MetaEvento(unsigned char metaevento)
{
	if(metaevento > 0x7F)
		return "No es un Metaevento MIDI valido";

	if(metaevento == MetaEventoMidi_SecuenciaNumerica)
		return "Secuencia Numerica";
	else if(metaevento == MetaEventoMidi_Texto)
		return "Texto";
	else if(metaevento == MetaEventoMidi_DerechosDeAutor)
		return "Derechos de Autor";
	else if(metaevento == MetaEventoMidi_NombreDePista)
		return "Nombre de Pista";
	else if(metaevento == MetaEventoMidi_NombreDeInstrumento)
		return "Nombre de Instrumento";
	else if(metaevento == MetaEventoMidi_Letra)
		return "Letra";
	else if(metaevento == MetaEventoMidi_Marcador)
		return "Marcador";
	else if(metaevento == MetaEventoMidi_PuntoReferencia)
		return "Punto de Referencia";
	else if(metaevento == MetaEventoMidi_NombreDelPrograma)
		return "Nombre del Programa";
	else if(metaevento == MetaEventoMidi_NombreDelDispositivo)
		return "Nombre del Dispositivo";
	else if(metaevento == MetaEventoMidi_PrefijoCanal)//Obsoleto
		return "Prefijo Canal (Obsoleto)";
	else if(metaevento == MetaEventoMidi_PuertoMidi)//Obsoleto
		return "Puerto MIDI (Obsoleto)";
	else if(metaevento == MetaEventoMidi_FinDePista)
		return "Fin de Pista";
	else if(metaevento == MetaEventoMidi_Tempo)
		return "Tempo";
	else if(metaevento == MetaEventoMidi_SMPTE)
		return "SMPTE";
	else if(metaevento == MetaEventoMidi_Compas)
		return "Compas";
	else if(metaevento == MetaEventoMidi_Armadura)
		return "Armadura";
	else if(metaevento == MetaEventoMidi_PrefijoTipoParcheXMF)
		return "Prefijo de tipo parche XMF";
	else if(metaevento == MetaEventoMidi_EspecificoDelSecuenciador)
		return "Específico del Secuenciador";
	else
		return "Metaevento no definido";
}

std::string Nombre_EstadoMensaje(unsigned char estado)
{
	if(estado == EstadoMensaje_Unico)
		return "Unico";
	else if(estado == EstadoMensaje_Inicio)
		return "Inicio";
	else if(estado == EstadoMensaje_Continua)
		return "Continua";
	else if(estado == EstadoMensaje_Fin)
		return "Fin";
	else if(estado == EstadoMensaje_DatosMixtosEncabezado)
		return "Datos Mixtos Encabezado";
	else if(estado == EstadoMensaje_DatosMixtosContenido)
		return "Datos Mixtos Contenido";

	return "Reservado";
}

std::string Nombre_DatosFlexiblesDireccion(unsigned char valor)
{
	if(valor == DatosFlexiblesDireccion_Canal)
		return "Canal";
	else if(valor == DatosFlexiblesDireccion_Grupo)
		return "Grupo";
	else
		return "Reservado";
}

std::string Nombre_DatosFlexiblesBanco(unsigned char valor)
{
	if(valor == DatosFlexiblesBanco_Configuracion)
		return "Configuracion";
	else if(valor == DatosFlexiblesBanco_Metadatos)
		return "Metadatos";
	else if(valor == DatosFlexiblesBanco_TextoDeInterpretacion)
		return "Texto de Interpretación";
	else
		return "Reservado";
}

std::string Nombre_MensajeControlador(unsigned char controlador)
{
	if(controlador > 0x7F)
		return "No es un Mensaje de Controlador valido";

	if(controlador == MensajeControlador_SeleccionDeBanco_BEI)
		return "Selección de Banco (Bit Extremo Izquierdo)";
	else if(controlador == MensajeControlador_RuedaDeModulacion_BEI)
		return "Rueda de Modulación (Bit Extremo Izquierdo)";
	else if(controlador == MensajeControlador_ControlDeRespiracion_BEI)
		return "Control de Respiración (Bit Extremo Izquierdo)";
	else if(controlador == MensajeControlador_ControlDePie_BEI)
		return "Control de Pie (Bit Extremo Izquierdo)";
	else if(controlador == MensajeControlador_TiempoDePortamento_BEI)
		return "Tiempo de Portamento (Bit Extremo Izquierdo)";
	else if(controlador == MensajeControlador_EntradaDeDatos_BEI)
		return "Entrada de Datos (Bit Extremo Izquierdo)";
	else if(controlador == MensajeControlador_VolumenDelCanal_BEI)
		return "Volumen del Canal (Bit Extremo Izquierdo)";
	else if(controlador == MensajeControlador_Balance_BEI)
		return "Balance (Bit Extremo Izquierdo)";
	else if(controlador == MensajeControlador_Pan_BEI)
		return "Pan (Bit Extremo Izquierdo)";
	else if(controlador == MensajeControlador_ControlDeExpresion_BEI)
		return "Control de Expresión (Bit Extremo Izquierdo)";
	else if(controlador == MensajeControlador_Efecto1_BEI)
		return "Efecto 1 (Bit Extremo Izquierdo)";
	else if(controlador == MensajeControlador_Efecto2_BEI)
		return "Efecto 2 (Bit Extremo Izquierdo)";
	else if(controlador == MensajeControlador_PropositoGeneral1_BEI)
		return "Propósito General 1 (Bit Extremo Izquierdo)";
	else if(controlador == MensajeControlador_PropositoGeneral2_BEI)
		return "Propósito General 2 (Bit Extremo Izquierdo)";
	else if(controlador == MensajeControlador_PropositoGeneral3_BEI)
		return "Propósito General 3 (Bit Extremo Izquierdo)";
	else if(controlador == MensajeControlador_PropositoGeneral4_BEI)
		return "Propósito General 4 (Bit Extremo Izquierdo)";
	else if(controlador == MensajeControlador_SeleccionDeBanco_BED)
		return "Selección de Banco (Bit Extremo Derecho)";
	else if(controlador == MensajeControlador_RuedaDeModulacion_BED)
		return "Rueda de Modulación (Bit Extremo Derecho)";
	else if(controlador == MensajeControlador_ControlDeRespiracion_BED)
		return "Control de Respiración (Bit Extremo Derecho)";
	else if(controlador == MensajeControlador_ControlDePie_BED)
		return "Control de Pie (Bit Extremo Derecho)";
	else if(controlador == MensajeControlador_TiempoDePortamento_BED)
		return "Tiempo de Portamento (Bit Extremo Derecho)";
	else if(controlador == MensajeControlador_EntradaDeDatos_BED)
		return "Entrada de Datos (Bit Extremo Derecho)";
	else if(controlador == MensajeControlador_VolumenDelCanal_BED)
		return "Volumen del Canal (Bit Extremo Derecho)";
	else if(controlador == MensajeControlador_Balance_BED)
		return "Balance (Bit Extremo Derecho)";
	else if(controlador == MensajeControlador_Pan_BED)
		return "Pan (Bit Extremo Derecho)";
	else if(controlador == MensajeControlador_ControlDeExpresion_BED)
		return "Control de Expresión (Bit Extremo Derecho)";
	else if(controlador == MensajeControlador_Efecto1_BED)
		return "Efecto 1 (Bit Extremo Derecho)";
	else if(controlador == MensajeControlador_Efecto2_BED)
		return "Efecto 2 (Bit Extremo Derecho)";
	else if(controlador == MensajeControlador_PropositoGeneral1_BED)
		return "Propósito General 1 (Bit Extremo Derecho)";
	else if(controlador == MensajeControlador_PropositoGeneral2_BED)
		return "Propósito General 2 (Bit Extremo Derecho)";
	else if(controlador == MensajeControlador_PropositoGeneral3_BED)
		return "Propósito General 3 (Bit Extremo Derecho)";
	else if(controlador == MensajeControlador_PropositoGeneral4_BED)
		return "Propósito General 4 (Bit Extremo Derecho)";
	else if(controlador == MensajeControlador_PedalDeAmortiguador)
		return "Pedal de Amortiguador";
	else if(controlador == MensajeControlador_Portamento)
		return "Portamento";
	else if(controlador == MensajeControlador_Sostenuto)
		return "Sostenuto";
	else if(controlador == MensajeControlador_PedalSuave)
		return "Pedal Suave";
	else if(controlador == MensajeControlador_PedalLegato)
		return "Pedal Legato";
	else if(controlador == MensajeControlador_Sostenido)
		return "Sostenido";
	else if(controlador == MensajeControlador_Sonido1)
		return "Sonido 1";
	else if(controlador == MensajeControlador_Sonido2)
		return "Sonido 2";
	else if(controlador == MensajeControlador_Sonido3)
		return "Sonido 3";
	else if(controlador == MensajeControlador_Sonido4)
		return "Sonido 4";
	else if(controlador == MensajeControlador_Sonido5)
		return "Sonido 5";
	else if(controlador == MensajeControlador_Sonido6)
		return "Sonido 6";
	else if(controlador == MensajeControlador_Sonido7)
		return "Sonido 7";
	else if(controlador == MensajeControlador_Sonido8)
		return "Sonido 8";
	else if(controlador == MensajeControlador_Sonido9)
		return "Sonido 9";
	else if(controlador == MensajeControlador_Sonido10)
		return "Sonido 10";
	else if(controlador == MensajeControlador_PropositoGeneral5)
		return "Propósito General 5";
	else if(controlador == MensajeControlador_PropositoGeneral6)
		return "Propósito General 6";
	else if(controlador == MensajeControlador_PropositoGeneral7)
		return "Propósito General 7";
	else if(controlador == MensajeControlador_PropositoGeneral8)
		return "Propósito General 8";
	else if(controlador == MensajeControlador_ControlDePortamento)
		return "Control de Portamento";
	else if(controlador == MensajeControlador_EfectoProfundida1)
		return "Efecto de Profundidad 1";
	else if(controlador == MensajeControlador_EfectoProfundida2)
		return "Efecto de Profundidad 2";
	else if(controlador == MensajeControlador_EfectoProfundida3)
		return "Efecto de Profundidad 3";
	else if(controlador == MensajeControlador_EfectoProfundida4)
		return "Efecto de Profundidad 4";
	else if(controlador == MensajeControlador_EfectoProfundida5)
		return "Efecto de Profundidad 5";
	else if(controlador == MensajeControlador_EntradaDeDatosMas1)
		return "Entrada de datos +1";
	else if(controlador == MensajeControlador_EntradaDeDatosMenos1)
		return "Entrada de datos -1";
	else if(controlador == MensajeControlador_ParametroNoRegistrado_BED)
		return "Parámetro No Registrado (Bit Extremo Derecho)";
	else if(controlador == MensajeControlador_ParametroNoRegistrado_BEI)
		return "Parámetro No Registrado (Bit Extremo Izquierdo)";
	else if(controlador == MensajeControlador_ParametroRegistrado_BED)
		return "Parámetro Registrado (Bit Extremo Derecho)";
	else if(controlador == MensajeControlador_ParametroRegistrado_BEI)
		return "Parámetro Registrado (Bit Extremo Izquierdo)";
	else if(controlador == MensajeControlador_TodosLosSonidosApagados)
		return "Todos los Sonidos Apagados";
	else if(controlador == MensajeControlador_RestablecerTodosLosControles)
		return "Restablecer todos los Controles";
	else if(controlador == MensajeControlador_ControlLocal)
		return "Control Local (encendido/apagado)";
	else if(controlador == MensajeControlador_TodasLasNotasApagadas)
		return "Todas las Notas Apagadas";
	else if(controlador == MensajeControlador_ModoOmniApagado)
		return "Modo Omni Apagado (Todas las Notas Apagadas)";
	else if(controlador == MensajeControlador_ModoOmniActivado)
		return "Modo Omni Activado (Todas las Notas Apagadas)";
	else if(controlador == MensajeControlador_ModoMono)
		return "Modo Poli (encendido/apagado y Todas las Notas Apagadas)";
	else if(controlador == MensajeControlador_ModoPoli)
		return "Modo Poli Avtivado (Incluido mono=apagado y Todas las Notas Apagadas)";
	else
		return "Controlador no definido";
}

std::string Nombre_ParametroRegistrado(unsigned short parametro)
{
	if(parametro == ParametroRegistrado_RangoInflexionDeTono)
		return "Rango de Inflexion de Tono";
	else if(parametro == ParametroRegistrado_AjusteFinoDeCanal)
		return "Ajuste Fino de Canal";
	else if(parametro == ParametroRegistrado_AjusteGruesoDeCanal)
		return "Ajuste Grueso de Canal";
	else if(parametro == ParametroRegistrado_AjustePrograma)
		return "Ajuste de Programa";
	else if(parametro == ParametroRegistrado_AjusteSeleccionBanco)
		return "Ajuste de Selección de Banco";
	else if(parametro == ParametroRegistrado_RangoProfundidadDeModulacion)
		return "Rango de Profundida de Modulación";
	else if(parametro == ParametroRegistrado_AnguloDeAzimut)
		return "Angulo de Azimut";
	else if(parametro == ParametroRegistrado_AnguloDeElevacion)
		return "Angulo de Elevacion";
	else if(parametro == ParametroRegistrado_Ganancia)
		return "Ganancia";
	else if(parametro == ParametroRegistrado_RelacionDeDistancia)
		return "Relación de Distancia";
	else if(parametro == ParametroRegistrado_DistanciaMaxima)
		return "Distancia Maxima";
	else if(parametro == ParametroRegistrado_GananciaDistanciaMaxima)
		return "Ganancia de Distancia Maxima";
	else if(parametro == ParametroRegistrado_RelacionDistanciaDeReferencia)
		return "Relacion de Distancia de Referencia";
	else if(parametro == ParametroRegistrado_AnguloExtensionDePan)
		return "Angulo de Extension de Pan";
	else if(parametro == ParametroRegistrado_AnguloDeBalanceo)
		return "Angulo de Balanceo";
	else if(parametro == ParametroRegistrado_Fin)
		return "Fin de Parametro Registrado";
	else
		return "Parametro Registrado no definido";
}

std::string Nombre_Percusion(Percusion percusion, unsigned char programa, unsigned char /*banco_izquierdo*/, unsigned char /*banco_derecho*/)
{
	if(percusion == Percusion::GM)
		return T("Percusión");
	if(percusion == Percusion::GM2)
	{
		switch(programa)
		{
			case 0: return T("Batería Estándar");
			case 8: return T("Batería de Habitación");
			case 16: return T("Batería de Potencia");
			case 24: return T("Batería Electrónica");
			case 25: return T("Batería Analógica");
			case 32: return T("Batería de Jazz");
			case 40: return T("Batería de Escobillas");
			case 48: return T("Batería de Orquesta");
			case 56: return T("Efectos");
			default : return T("Percusión GM2");
		}
	}
	else if(percusion == Percusion::XG_127)
		return T("Percusión XG");
	else if(percusion == Percusion::XG_126)
		return T("Percusión XG Efectos");
	else if(percusion == Percusion::XG_64)
		return T("Percusión XG Efectos Individuales");
	else if(percusion == Percusion::GS_Map1)
		return T("Percusión Mapa 1");
	else if(percusion == Percusion::GS_Map2)
		return T("Percusión Mapa 2");
	return "";
}

std::string Nombre_Instrumento(unsigned char programa, unsigned char banco_izquierdo, unsigned char banco_derecho)
{
	switch(programa)
	{
		case 0:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 1, banco: 121, 1
				return T("Piano de Cola Acústico (amplio)");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 2)
				//TR General MIDI nivel 2, programa: 1, banco: 121, 2
				return T("Piano de Cola Acústico (oscuro)");
			else
				//TR General MIDI nivel 1, programa: 1, banco: 0, 0
				return T("Piano de Cola Acústico");
		}
		case 1:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 2, banco: 121, 1
				return T("Piano Brillante Acústico (amplio)");
			else
				//TR General MIDI nivel 1, programa: 2, banco: 0, 0
				return T("Piano Brillante Acústico");
		}
		case 2:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 3, banco: 121, 1
				return T("Piano de Cola Eléctrico (amplio)");
			else
				//TR General MIDI nivel 1, programa: 3, banco: 0, 0
				return T("Piano de Cola Eléctrico");
		}
		case 3:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 4, banco: 121, 1
				return T("Piano Honky tonk (amplio)");
			else
				//TR General MIDI nivel 1, programa: 4, banco: 0, 0
				return T("Piano Honky tonk");
		}
		case 4:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 5, banco: 121, 1
				return T("Piano Eléctrico Desafinado 1");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 2)
				//TR General MIDI nivel 2, programa: 5, banco: 121, 2
				return T("Piano Eléctrico 1 (velocidad)");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 3)
				//TR General MIDI nivel 2, programa: 5, banco: 121, 3
				return T("Piano Eléctrico de los 60");
			else
				//TR General MIDI nivel 1, programa: 5, banco: 0, 0
				return T("Piano Eléctrico 1 (rhodes)");
		}
		case 5:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 6, banco: 121, 1
				return T("Piano Eléctrico Desafinado 2");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 2)
				//TR General MIDI nivel 2, programa: 6, banco: 121, 2
				return T("Piano Eléctrico 2 (velocidad)");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 3)
				//TR General MIDI nivel 2, programa: 6, banco: 121, 3
				return T("Piano Eléctrico leyenda");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 4)
				//TR General MIDI nivel 2, programa: 6, banco: 121, 4
				return T("Piano Eléctrico fase");
			else
				//TR General MIDI nivel 1, programa: 6, banco: 0, 0
				return T("Piano Eléctrico 2 (coro)");
		}
		case 6:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 7, banco: 121, 1
				return T("Clavicordio (octavas)");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 2)
				//TR General MIDI nivel 2, programa: 7, banco: 121, 2
				return T("Clavicordio (amplio)");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 3)
				//TR General MIDI nivel 2, programa: 7, banco: 121, 3
				return T("Clavicordio (soltar)");//TODO falta mejor traduccion, podria ser desafinado en vez de soltar
			else
				//TR General MIDI nivel 1, programa: 7, banco: 0, 0
				return T("Clavicordio");
		}
		case 7:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 8, banco: 121, 1
				return T("Clavecín pulso");
			else
				//TR General MIDI nivel 1, programa: 8, banco: 0, 0
				return T("Clavecín");
		}
		case 8:
		{
			//TR General MIDI nivel 1, programa: 9, banco: 0, 0
			return T("Celesta");
		}
		case 9:
		{
			//TR General MIDI nivel 1, programa: 10, banco: 0, 0
			return T("Glockenspiel");
		}
		case 10:
		{
			//TR General MIDI nivel 1, programa: 11, banco: 0, 0
			return T("Caja de Música");
		}
		case 11:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 12, banco: 121, 1
				return T("Vibráfono (amplio)");
			else
				//TR General MIDI nivel 1, programa: 12, banco: 0, 0
				return T("Vibráfono");
		}
		case 12:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 13, banco: 121, 1
				return T("Marimba (amplio)");
			else
				//TR General MIDI nivel 1, programa: 13, banco: 0, 0
				return T("Marimba");
		}
		case 13:
		{
			//TR General MIDI nivel 1, programa: 14, banco: 0, 0
			return T("Xilófono");
		}
		case 14:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 15, banco: 121, 1
				return T("Campanas de Iglesia");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 2)
				//TR General MIDI nivel 2, programa: 15, banco: 121, 2
				return T("Carillón");
			else
				//TR General MIDI nivel 1, programa: 15, banco: 0, 0
				return T("Campanas Tubulares");
		}
		case 15:
		{
			//TR General MIDI nivel 1, programa: 16, banco: 0, 0
			return T("Dulcémele");
		}
		case 16:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 17, banco: 121, 1
				return T("Órgano Hammond Desafinado");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 2)
				//TR General MIDI nivel 2, programa: 17, banco: 121, 2
				return T("Órgano Italiano de los 60");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 3)
				//TR General MIDI nivel 2, programa: 17, banco: 121, 3
				return T("Órgano Hammond 2");
			else
				//TR General MIDI nivel 1, programa: 17, banco: 0, 0
				return T("Órgano Hammond");
		}
		case 17:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 18, banco: 121, 1
				return T("Órgano Percusivo Desafinado");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 2)
				//TR General MIDI nivel 2, programa: 18, banco: 121, 2
				return T("Órgano Percusivo 2");
			else
				//TR General MIDI nivel 1, programa: 18, banco: 0, 0
				return T("Órgano Percusivo");
		}
		case 18:
		{
			//TR General MIDI nivel 1, programa: 19
			return T("Órgano de Rock");
		}
		case 19:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 20, banco: 121, 1
				return T("Órgano de Iglesia (octavas)");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 2)
				//TR General MIDI nivel 2, programa: 20, banco: 121, 2
				return T("Órgano de Iglesia Desafinado");
			else
				//TR General MIDI nivel 1, programa: 20, banco: 0, 0
				return T("Órgano de Iglesia");
		}
		case 20:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 21, banco: 121, 1
				return T("Órgano Soplado");
			else
				//TR General MIDI nivel 1, programa: 21, banco: 0, 0
				return T("Armonio");
		}
		case 21:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 22, banco: 121, 1
				return T("Acordeón 2");
			else
				//TR General MIDI nivel 1, programa: 22, banco: 0, 0
				return T("Acordeón");
		}
		case 22:
		{
			//TR General MIDI nivel 1, programa: 23, banco: 0, 0
			return T("Armónica");
		}
		case 23:
		{
			//TR General MIDI nivel 1, programa: 24, banco: 0, 0
			return T("Bandoneón");
		}
		case 24:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 25, banco: 121, 1
				return T("Ukelele");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 2)
				//TR General MIDI nivel 2, programa: 25, banco: 121, 2
				return T("Guitarra Acústica (nailon + soltar)");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 3)
				//TR General MIDI nivel 2, programa: 25, banco: 121, 3
				return T("Guitarra Acústica (nailon 2)");
			else
				//TR General MIDI nivel 1, programa: 25, banco: 0, 0
				return T("Guitarra Acústica (nailon)");
		}
		case 25:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 26, banco: 121, 1
				return T("Guitarra de 12 cuerdas");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 2)
				//TR General MIDI nivel 2, programa: 26, banco: 121, 2
				return T("Mandolina");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 3)
				//TR General MIDI nivel 2, programa: 26, banco: 121, 3
				return T("Guitarra de Acero con Sonido de Caja");
			else
				//TR General MIDI nivel 1, programa: 26, banco: 0, 0
				return T("Guitarra Acústica (acero)");
		}
		case 26:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 27, banco: 121, 1
				return T("Guitarra Eléctrica (pedal de acero)");
			else
				//TR General MIDI nivel 1, programa: 27, banco: 0, 0
				return T("Guitarra Eléctrica (jazz)");
		}
		case 27:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 28, banco: 121, 1
				return T("Guitarra Eléctrica (limpia desafinada)");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 2)
				//TR General MIDI nivel 2, programa: 28, banco: 121, 2
				return T("Guitarra de Tono Medio");
			else
				//TR General MIDI nivel 1, programa: 28, banco: 0, 0
				return T("Guitarra Eléctrica (limpia)");
		}
		case 28:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 29, banco: 121, 1
				return T("Guitarra Eléctrica (corte funky)");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 2)
				//TR General MIDI nivel 2, programa: 29, banco: 121, 2
				return T("Guitarra Eléctrica (sorda con cambio de velocidad)");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 3)
				//TR General MIDI nivel 2, programa: 29, banco: 121, 3
				return T("Hombre de Jazz");
			else
				//TR General MIDI nivel 1, programa: 29, banco: 0, 0
				return T("Guitarra Eléctrica (tapada)");
		}
		case 29:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 30, banco: 121, 1
				return T("Pellizco de Guitarra");
			else
				//TR General MIDI nivel 1, programa: 30, banco: 0, 0
				return T("Guitarra Saturada");
		}
		case 30:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 31, banco: 121, 1
				return T("Guitarra Distorsionada (con retroalimentación)");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 2)
				//TR General MIDI nivel 2, programa: 31, banco: 121, 2
				return T("Guitarra Rítmica Distorcionada");
			else
				//TR General MIDI nivel 1, programa: 31, banco: 0, 0
				return T("Guitarra Distorsionada");
		}
		case 31:
		{

			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 32, banco: 121, 1
				return T("Guitarra Retroalimentación");//TODO
			else
				//TR General MIDI nivel 1, programa: 32, banco: 0, 0
				return T("Armónicos de Guitarra");
		}
		case 32:
		{
			//TR General MIDI nivel 1, programa: 33, banco: 0, 0
			return T("Bajo Acústico");
		}
		case 33:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 34, banco: 121, 1
				return T("Bajo Golpe de Dedos");
			else
				//TR General MIDI nivel 1, programa: 34, banco: 0, 0
				return T("Bajo Eléctrico (digitado)");
		}
		case 34:
		{
			//TR General MIDI nivel 1, programa: 35, banco: 0, 0
			return T("Bajo Eléctrico (púa)");
		}
		case 35:
		{
			//TR General MIDI nivel 1, programa: 36, banco: 0, 0
			return T("Bajo sin Trastes");
		}
		case 36:
		{
			//TR General MIDI nivel 1, programa: 37, banco: 0, 0
			return T("Bajo Golpe 1");
		}
		case 37:
		{
			//TR General MIDI nivel 1, programa: 38, banco: 0, 0
			return T("Bajo Golpe 2");
		}
		case 38:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 39, banco: 121, 1
				return T("Bajo Sintetizado (cálido)");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 2)
				//TR General MIDI nivel 2, programa: 39, banco: 121, 2
				return T("Bajo Sintetizado 3 (resonancia)");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 3)
				//TR General MIDI nivel 2, programa: 39, banco: 121, 3
				return T("Bajo Clavecín");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 4)
				//TR General MIDI nivel 2, programa: 39, banco: 121, 4
				return T("Bajo Percutor");
			else
				//TR General MIDI nivel 1, programa: 39, banco: 0, 0
				return T("Bajo Sintetizado 1");
		}
		case 39:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 40, banco: 121, 1
				return T("Bajo Sintetizado 4 (ataque)");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 2)
				//TR General MIDI nivel 2, programa: 40, banco: 121, 2
				return T("Bajo Sintetizado (goma)");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 3)
				//TR General MIDI nivel 2, programa: 40, banco: 121, 3
				return T("Pulso Ataque");
			else
				//TR General MIDI nivel 1, programa: 40, banco: 0, 0
				return T("Bajo Sintetizado 2");
		}
		case 40:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 41, banco: 121, 1
				return T("Violín (ataque lento)");
			else
				//TR General MIDI nivel 1, programa: 41, banco: 0, 0
				return T("Violín");
		}
		case 41:
		{
			//TR General MIDI nivel 1, programa: 42, banco: 0, 0
			return T("Viola");
		}
		case 42:
		{
			//TR General MIDI nivel 1, programa: 43, banco: 0, 0
			return T("Violonchelo");
		}
		case 43:
		{
			//TR General MIDI nivel 1, programa: 44, banco: 0, 0
			return T("Contrabajo");
		}
		case 44:
		{
			//TR General MIDI nivel 1, programa: 45, banco: 0, 0
			return T("Trémolo Cuerdas");
		}
		case 45:
		{
			//TR General MIDI nivel 1, programa: 46, banco: 0, 0
			return T("Pizzicato Cuerdas");
		}
		case 46:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 47, banco: 121, 1
				return T("Yangqin");
			else
				//TR General MIDI nivel 1, programa: 47, banco: 0, 0
				return T("Arpa Orquestal");
		}
		case 47:
		{
			//TR General MIDI nivel 1, programa: 48, banco: 0, 0
			return T("Timbales");
		}
		case 48:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 49, banco: 121, 1
				return T("Cuerdas y Metales");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 2)
				//TR General MIDI nivel 2, programa: 49, banco: 121, 2
				return T("Cuerdas de los 60");
			else
				//TR General MIDI nivel 1, programa: 49, banco: 0, 0
				return T("Conjunto de Cuerda 1");
		}
		case 49:
		{
			//TR General MIDI nivel 1, programa: 50, banco: 0, 0
			return T("Conjunto de Cuerda 2");
		}
		case 50:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 51, banco: 121, 1
				return T("Cuerdas Sintetizadas 3");
			else
				//TR General MIDI nivel 1, programa: 51, banco: 0, 0
				return T("Cuerdas Sintetizadas 1");
		}
		case 51:
		{
			//TR General MIDI nivel 1, programa: 52, banco: 0, 0
			return T("Cuerdas Sintetizadas 2");
		}
		case 52:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 53, banco: 121, 1
				return T("Coro Aahs 2");
			else
				//TR General MIDI nivel 1, programa: 53, banco: 0, 0
				return T("Coro Aahs");
		}
		case 53:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 54, banco: 121, 1
				return T("Zumbador");
			else
				//TR General MIDI nivel 1, programa: 54, banco: 0, 0
				return T("Veces Oohs");
		}
		case 54:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 55, banco: 121, 1
				return T("Voz Análoga");
			else
				//TR General MIDI nivel 1, programa: 55, banco: 0, 0
				return T("Voz Sintetizada");
		}
		case 55:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 56, banco: 121, 1
				return T("Tutti Bajo");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 2)
				//TR General MIDI nivel 2, programa: 56, banco: 121, 2
				return T("Tutti Sextas");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 3)
				//TR General MIDI nivel 2, programa: 56, banco: 121, 3
				return T("Tutti Euro");
			else
				//TR General MIDI nivel 1, programa: 56, banco: 0, 0
				return T("Tutti de Orquesta");
		}
		case 56:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 57, banco: 121, 1
				return T("Trompeta Oscura Suave");
			else
				//TR General MIDI nivel 1, programa: 57, banco: 0, 0
				return T("Trompeta");
		}
		case 57:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 58, banco: 121, 1
				return T("Trombón 2");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 2)
				//TR General MIDI nivel 2, programa: 58, banco: 121, 2
				return T("Trombón Brillante");
			else
				//TR General MIDI nivel 1, programa: 58, banco: 0, 0
				return T("Trombón");
		}
		case 58:
		{
			//TR General MIDI nivel 1, programa: 59, banco: 0, 0
			return T("Tuba");
		}
		case 59:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 60, banco: 121, 1
				return T("Trompeta con Sordina 2");
			else
				//TR General MIDI nivel 1, programa: 60, banco: 0, 0
				return T("Trompeta con Sordina");
		}
		case 60:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 61, banco: 121, 1
				return T("Corno Francés 2 (cálido)");
			else
				//TR General MIDI nivel 1, programa: 61, banco: 0, 0
				return T("Corno Francés");
		}
		case 61:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 62, banco: 121, 1
				return T("Sección de Metales 2 (octavas)");
			else
				//TR General MIDI nivel 1, programa: 62, banco: 0, 0
				return T("Sección de Metales");
		}
		case 62:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 63, banco: 121, 1
				return T("Metales Sintetizados 3");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 2)
				//TR General MIDI nivel 2, programa: 63, banco: 121, 2
				return T("Metales Sintetizados Análogo 1");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 3)
				//TR General MIDI nivel 2, programa: 63, banco: 121, 3
				return T("Metales Salto");
			else
				//TR General MIDI nivel 1, programa: 63, banco: 0, 0
				return T("Metales Sintetizados 1");
		}
		case 63:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 64, banco: 121, 1
				return T("Metales Sintetizados 4");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 2)
				//TR General MIDI nivel 2, programa: 64, banco: 121, 2
				return T("Metales Sintetizados Análogo 2");
			else
				//TR General MIDI nivel 1, programa: 64, banco: 0, 0
				return T("Metales Sintetizados 2");
		}
		case 64:
		{
			//TR General MIDI nivel 1, programa: 65, banco: 0, 0
			return T("Saxofón Soprano");
		}
		case 65:
		{
			//TR General MIDI nivel 1, programa: 66, banco: 0, 0
			return T("Saxofón Alto");
		}
		case 66:
		{
			//TR General MIDI nivel 1, programa: 67, banco: 0, 0
			return T("Saxofón Tenor");
		}
		case 67:
		{
			//TR General MIDI nivel 1, programa: 68, banco: 0, 0
			return T("Saxofón Barítono");
		}
		case 68:
		{
			//TR General MIDI nivel 1, programa: 69, banco: 0, 0
			return T("Oboe");
		}
		case 69:
		{
			//TR General MIDI nivel 1, programa: 70, banco: 0, 0
			return T("Corno Inglés");
		}
		case 70:
		{
			//TR General MIDI nivel 1, programa: 71, banco: 0, 0
			return T("Fagot");
		}
		case 71:
		{
			//TR General MIDI nivel 1, programa: 72, banco: 0, 0
			return T("Clarinete");
		}
		case 72:
		{
			//TR General MIDI nivel 1, programa: 73, banco: 0, 0
			return T("Flautín");
		}
		case 73:
		{
			//TR General MIDI nivel 1, programa: 74, banco: 0, 0
			return T("Flauta");
		}
		case 74:
		{
			//TR General MIDI nivel 1, programa: 75, banco: 0, 0
			return T("Flauta Dulce");
		}
		case 75:
		{
			//TR General MIDI nivel 1, programa: 76, banco: 0, 0
			return T("Flauta de Pan");
		}
		case 76:
		{
			//TR General MIDI nivel 1, programa: 77, banco: 0, 0
			return T("Botella Soplada");
		}
		case 77:
		{
			//TR General MIDI nivel 1, programa: 78, banco: 0, 0
			return T("Shakuhachi");
		}
		case 78:
		{
			//TR General MIDI nivel 1, programa: 79, banco: 0, 0
			return T("Silbido");
		}
		case 79:
		{
			//TR General MIDI nivel 1, programa: 80, banco: 0, 0
			return T("Ocarina");
		}
		case 80:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 81, banco: 121, 1
				return T("Solo 1a (onda cuadrada 2)");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 2)
				//TR General MIDI nivel 2, programa: 81, banco: 121, 2
				return T("Solo 1b (onda sinusoidal)");
			else
				//TR General MIDI nivel 1, programa: 81, banco: 0, 0
				return T("Solo 1 (onda cuadrada)");
		}
		case 81:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 82, banco: 121, 1
				return T("Solo 2a (diente de sierra 2)");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 2)
				//TR General MIDI nivel 2, programa: 82, banco: 121, 2
				return T("Solo 2b (diente de sierra y pulso)");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 3)
				//TR General MIDI nivel 2, programa: 82, banco: 121, 3
				return T("Solo 2c (doble diente de sierra)");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 4)
				//TR General MIDI nivel 2, programa: 82, banco: 121, 4
				return T("Solo 2d (análogo secuenciado)");
			else
				//TR General MIDI nivel 1, programa: 82, banco: 0, 0
				return T("Solo 2 (diente de sierra)");
		}
		case 82:
		{
			//TR General MIDI nivel 1, programa: 83, banco: 0, 0
			return T("Solo 3 (calíope)");
		}
		case 83:
		{
			//TR General MIDI nivel 1, programa: 84, banco: 0, 0
			return T("Solo 4 (órgano solista)");
		}
		case 84:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 85, banco: 121, 1
				return T("Solo 5a (alambre)");
			else
				//TR General MIDI nivel 1, programa: 85, banco: 0, 0
				return T("Solo 5 (charanga)");
		}
		case 85:
		{
			//TR General MIDI nivel 1, programa: 86, banco: 0, 0
			return T("Solo 6 (voz sintetizada)");
		}
		case 86:
		{
			//TR General MIDI nivel 1, programa: 87, banco: 0, 0
			return T("Solo 7 (quintas)");
		}
		case 87:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 88, banco: 121, 1
				return T("Solo 8a (retorcido suabe)");
			else
				//TR General MIDI nivel 1, programa: 88, banco: 0, 0
				return T("Solo 8 (bajo y melodía)");
		}
		case 88:
		{
			//TR General MIDI nivel 1, programa: 89, banco: 0, 0
			return T("Fondo 1");
		}
		case 89:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 90, banco: 121, 1
				return T("Fondo 2a (sinusoidal)");
			else
				//TR General MIDI nivel 1, programa: 90, banco: 0, 0
				return T("Fondo 2 (cálido)");
		}
		case 90:
		{
			//TR General MIDI nivel 1, programa: 91, banco: 0, 0
			return T("Fondo 3 (polisintetizador)");
		}
		case 91:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 92, banco: 121, 1
				return T("Fondo 4a (itopía)");
			else
				//TR General MIDI nivel 1, programa: 92, banco: 0, 0
				return T("Fondo 4 (coral)");
		}
		case 92:
		{
			//TR General MIDI nivel 1, programa: 93, banco: 0, 0
			return T("Fondo 5 (arcos)");
		}
		case 93:
		{
			//TR General MIDI nivel 1, programa: 94, banco: 0, 0
			return T("Fondo 6 (metálico)");
		}
		case 94:
		{
			//TR General MIDI nivel 1, programa: 95, banco: 0, 0
			return T("Fondo 7 (celestial)");
		}
		case 95:
		{
			//TR General MIDI nivel 1, programa: 96, banco: 0, 0
			return T("Fondo 8 (barrido armónicos)");
		}
		case 96:
		{
			//TR General MIDI nivel 1, programa: 97, banco: 0, 0
			return T("Efecto 1 (lluvia)");
		}
		case 97:
		{
			//TR General MIDI nivel 1, programa: 98, banco: 0, 0
			return T("Efecto 2 (banda sonora)");
		}
		case 98:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 99, banco: 121, 1
				return T("Efecto 3 (sintetizador mazo)");
			else
				//TR General MIDI nivel 1, programa: 99, banco: 0, 0
				return T("Efecto 3 (cristales)");
		}
		case 99:
		{
			//TR General MIDI nivel 1, programa: 100, banco: 0, 0
			return T("Efecto 4 (atmósfera)");
		}
		case 100:
		{
			//TR General MIDI nivel 1, programa: 101, banco: 0, 0
			return T("Efecto 5 (brillante)");
		}
		case 101:
		{
			//TR General MIDI nivel 1, programa: 102, banco: 0, 0
			return T("Efecto 6 (duendes)");
		}
		case 102:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 103, banco: 121, 1
				return T("Efecto 7a (eco de campana)");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 2)
				//TR General MIDI nivel 2, programa: 103, banco: 121, 2
				return T("Efecto 7b (eco panorámico)");
			else
				//TR General MIDI nivel 1, programa: 103, banco: 0, 0
				return T("Efecto 7 (ecos)");
		}
		case 103:
		{
			//TR General MIDI nivel 1, programa: 104, banco: 0, 0
			return T("Efecto 8 (ciencia ficción)");
		}
		case 104:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 105, banco: 121, 1
				return T("Sitar 2 (doblado)");
			else
				//TR General MIDI nivel 1, programa: 105, banco: 0, 0
				return T("Sitar");
		}
		case 105:
		{
			//TR General MIDI nivel 1, programa: 106, banco: 0, 0
			return T("Banjo");
		}
		case 106:
		{
			//TR General MIDI nivel 1, programa: 107, banco: 0, 0
			return T("Shamisen");
		}
		case 107:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 108, banco: 121, 1
				return T("Taishokoto");
			else
				//TR General MIDI nivel 1, programa: 108, banco: 0, 0
				return T("Koto");
		}
		case 108:
		{
			//TR General MIDI nivel 1, programa: 109, banco: 0, 0
			return T("Kalimba");
		}
		case 109:
		{
			//TR General MIDI nivel 1, programa: 110, banco: 0, 0
			return T("Gaita");
		}
		case 110:
		{
			//TR General MIDI nivel 1, programa: 111, banco: 0, 0
			return T("Violín Tradicional");
		}
		case 111:
		{
			//TR General MIDI nivel 1, programa: 112, banco: 0, 0
			return T("Shannai");
		}
		case 112:
		{
			//TR General MIDI nivel 1, programa: 113, banco: 0, 0
			return T("Campanillas");
		}
		case 113:
		{
			//TR General MIDI nivel 1, programa: 114, banco: 0, 0
			return T("Agogô");
		}
		case 114:
		{
			//TR General MIDI nivel 1, programa: 115, banco: 0, 0
			return T("Tambor Metálico");
		}
		case 115:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 116, banco: 121, 1
				return T("Castañuelas");
			else
				//TR General MIDI nivel 1, programa: 116, banco: 0, 0
				return T("Caja China");
		}
		case 116:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 117, banco: 121, 1
				return T("Bombo de Concierto");
			else
				//TR General MIDI nivel 1, programa: 117, banco: 0, 0
				return T("Taiko");
		}
		case 117:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 118, banco: 121, 1
				return T("Tom Melódico 2 (potente)");
			else
				//TR General MIDI nivel 1, programa: 118, banco: 0, 0
				return T("Tom Melódico");
		}
		case 118:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 119, banco: 121, 1
				return T("Caja de Ritmo Tom");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 2)
				//TR General MIDI nivel 2, programa: 119, banco: 121, 2
				return T("Tambór Electrico");
			else
				//TR General MIDI nivel 1, programa: 119, banco: 0, 0
				return T("Tambór Sintetizado");
		}
		case 119:
		{
			//TR General MIDI nivel 1, programa: 120, banco: 0, 0
			return T("Plato Invertido");
		}
		case 120:
		{

			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 121, banco: 121, 1
				return T("Ruido de Corte de Guitarra");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 2)
				//TR General MIDI nivel 2, programa: 121, banco: 121, 2
				return T("Golpe de Cuerda de Bajo Acústico");
			else
				//TR General MIDI nivel 1, programa: 121, banco: 0, 0
				return T("Guitarra Trastes");
		}
		case 121:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 122, banco: 121, 1
				return T("Clic de flauta");
			else
				//TR General MIDI nivel 1, programa: 122, banco: 0, 0
				return T("Ruido de Respiración");
		}
		case 122:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 123, banco: 121, 1
				return T("Lluvia");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 2)
				//TR General MIDI nivel 2, programa: 123, banco: 121, 2
				return T("Trueno");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 3)
				//TR General MIDI nivel 2, programa: 123, banco: 121, 3
				return T("Viento");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 4)
				//TR General MIDI nivel 2, programa: 123, banco: 121, 4
				return T("Río");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 5)
				//TR General MIDI nivel 2, programa: 123, banco: 121, 5
				return T("Burbuja");
			else
				//TR General MIDI nivel 1, programa: 123, banco: 0, 0
				return T("Costa de Mar");
		}
		case 123:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 124, banco: 124, 1
				return T("Perro");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 2)
				//TR General MIDI nivel 2, programa: 124, banco: 124, 2
				return T("Galopeo de Caballo");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 3)
				//TR General MIDI nivel 2, programa: 124, banco: 124, 3
				return T("Pío de Pájaro 2");
			else
				//TR General MIDI nivel 1, programa: 124, banco: 0, 0
				return T("Pío de Pájaro");
		}
		case 124:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 125, banco: 121, 1
				return T("Timbre Teléfonico 2");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 2)
				//TR General MIDI nivel 2, programa: 125, banco: 121, 2
				return T("Crujido de Puerta");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 3)
				//TR General MIDI nivel 2, programa: 125, banco: 121, 3
				return T("Puerta");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 4)
				//TR General MIDI nivel 2, programa: 125, banco: 121, 4
				return T("Movimiento de Vinilo");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 5)
				//TR General MIDI nivel 2, programa: 125, banco: 121, 5
				return T("Campanilla de Viento");
			else
				//TR General MIDI nivel 1, programa: 125, banco: 0, 0
				return T("Timbre Teléfonico");
		}
		case 125:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 126, banco: 121, 1
				return T("Motor de Autómovil");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 2)
				//TR General MIDI nivel 2, programa: 126, banco: 121, 2
				return T("Parada de Autómovil");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 3)
				//TR General MIDI nivel 2, programa: 126, banco: 121, 3
				return T("Paso de Autómovil");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 4)
				//TR General MIDI nivel 2, programa: 126, banco: 121, 4
				return T("Choque de Autómovil");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 5)
				//TR General MIDI nivel 2, programa: 126, banco: 121, 5
				return T("Sirena");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 6)
				//TR General MIDI nivel 2, programa: 126, banco: 121, 6
				return T("Tren");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 7)
				//TR General MIDI nivel 2, programa: 126, banco: 121, 7
				return T("Avión a Reacción");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 8)
				//TR General MIDI nivel 2, programa: 126, banco: 121, 8
				return T("Nave Estelar");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 9)
				//TR General MIDI nivel 2, programa: 126, banco: 121, 9
				return T("Ruido de Ráfaga");
			else
				//TR General MIDI nivel 1, programa: 126, banco: 0, 0
				return T("Helicóptero");
		}
		case 126:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 127, banco: 121, 1
				return T("Risa");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 2)
				//TR General MIDI nivel 2, programa: 127, banco: 121, 2
				return T("Grito");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 3)
				//TR General MIDI nivel 2, programa: 127, banco: 121, 3
				return T("Puñetazo");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 4)
				//TR General MIDI nivel 2, programa: 127, banco: 121, 4
				return T("Latidos del Corazón");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 5)
				//TR General MIDI nivel 2, programa: 127, banco: 121, 5
				return T("Pasos");
			else
				//TR General MIDI nivel 1, programa: 127, banco: 0, 0
				return T("Aplauso");
		}
		case 127:
		{
			if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 1)
				//TR General MIDI nivel 2, programa: 128, banco: 121, 1
				return T("Ametralladora");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 2)
				//TR General MIDI nivel 2, programa: 128, banco: 121, 2
				return T("Pistola Laser");
			else if((banco_izquierdo == 121 || banco_izquierdo == 0) && banco_derecho == 3)
				//TR General MIDI nivel 2, programa: 128, banco: 121, 3
				return T("Explosión");
			else
				//TR General MIDI nivel 1, programa: 128, banco: 0, 0
				return T("Disparo");
		}
		default:
			return T("Instrumento Desconocido");
	}
}

std::string Nombre_Armadura(char armadura, unsigned char escala)
{
	if(escala == Escala_Mayor)
	{
		switch(armadura)
		{
			case Armadura_7Bemol: return "Do♭";
			case Armadura_6Bemol: return "Sol♭";
			case Armadura_5Bemol: return "Re♭";
			case Armadura_4Bemol: return "La♭";
			case Armadura_3Bemol: return "Mi♭";
			case Armadura_2Bemol: return "Si♭";
			case Armadura_1Bemol: return "Fa";
			case Armadura_0: return "Do";
			case Armadura_1Sostenido: return "Sol";
			case Armadura_2Sostenido: return "Re";
			case Armadura_3Sostenido: return "La";
			case Armadura_4Sostenido: return "Mi";
			case Armadura_5Sostenido: return "Si";
			case Armadura_6Sostenido: return "Fa♯";
			case Armadura_7Sostenido: return "Do♯";
			default: return "";
		}
	}
	else if(escala == Escala_Menor)
	{
		switch(armadura)
		{
			case Armadura_7Bemol: return "La♭";
			case Armadura_6Bemol: return "Mi♭";
			case Armadura_5Bemol: return "Si♭";
			case Armadura_4Bemol: return "Fa";
			case Armadura_3Bemol: return "Do";
			case Armadura_2Bemol: return "Sol";
			case Armadura_1Bemol: return "Re";
			case Armadura_0: return "La";
			case Armadura_1Sostenido: return "Mi";
			case Armadura_2Sostenido: return "Si";
			case Armadura_3Sostenido: return "Fa♯";
			case Armadura_4Sostenido: return "Do♯";
			case Armadura_5Sostenido: return "Sol♯";
			case Armadura_6Sostenido: return "Re♯";
			case Armadura_7Sostenido: return "La♯";
			default: return "";
		}
	}

	return "";
}

std::string Nombre_Escala(unsigned char escala)
{
	if(escala == Escala_Mayor)
		return T("Mayor");
	else if(escala == Escala_Menor)
		return T("Menor");
	return "";
}

std::string Nombre_Dinamica(Dinamica dinamica)
{
	if(dinamica == Dinamica::PPP)
		return "𝆏𝆏𝆏";
	else if(dinamica == Dinamica::PP)
		return "𝆏𝆏";
	else if(dinamica == Dinamica::P)
		return "𝆏";
	else if(dinamica == Dinamica::MP)
		return "𝆐𝆏";
	else if(dinamica == Dinamica::MF)
		return "𝆐𝆑";
	else if(dinamica == Dinamica::F)
		return "𝆑";
	else if(dinamica == Dinamica::FF)
		return "𝆑𝆑";
	else if(dinamica == Dinamica::FFF)
		return "𝆑𝆑𝆑";
	return "";
}

std::string Nombre_EspecificacionMidi(EspecificacionMidi espesificacion)
{
	if(espesificacion == EspecificacionMidi::GM)//General Midi
		return T("GM");
	else if(espesificacion == EspecificacionMidi::GM2)//General Midi 2
		return T("GM2");
	else if(espesificacion == EspecificacionMidi::XG)//Extended General Midi (Yamaha)
		return T("XG");
	else if(espesificacion == EspecificacionMidi::GS)//General Standard (Roland)
		return T("GS");
	else if(espesificacion == EspecificacionMidi::MPC)//Multimedia PC (Microsoft)
		return T("MPC");
	else
		return "";
}

std::string Nombre_LargoEspecificacionMidi(EspecificacionMidi espesificacion)
{
	if(espesificacion == EspecificacionMidi::GM)//General Midi
		return T("General MIDI");
	else if(espesificacion == EspecificacionMidi::GM2)//General Midi 2
		return T("General MIDI Nivel 2");
	else if(espesificacion == EspecificacionMidi::XG)//Extended General Midi (Yamaha)
		return T("General MIDI Extendido");
	else if(espesificacion == EspecificacionMidi::GS)//General Standard (Roland)
		return T("Estandar General");
	else if(espesificacion == EspecificacionMidi::MPC)//Multimedia PC (Microsoft)
		return T("Multimedia PC");
	else
		return "";
}

std::string Nombre_Notas(SistemaNombreNotas sistema, unsigned char nota)
{
	if(sistema == SistemaNombreNotas::DoFijo)
	{
		switch(nota)
		{
			case 0: return "Do";
			case 1: return "Do♯";
			case 2: return "Re";
			case 3: return "Re♯";
			case 4: return "Mi";
			case 5: return "Fa";
			case 6: return "Fa♯";
			case 7: return "Sol";
			case 8: return "Sol♯";
			case 9: return "La";
			case 10: return "La♯";
			case 11: return "Si";
			default: return "";
		}
	}
	if(sistema == SistemaNombreNotas::DoVariable)
	{
		switch(nota % 12)
		{
			case 0: return "Do";
			case 1: return "Ra";
			case 2: return "Re";
			case 3: return "Me";
			case 4: return "Mi";
			case 5: return "Fa";
			case 6: return "Fi";
			case 7: return "Sol";
			case 8: return "Le";
			case 9: return "La";
			case 10: return "Te";
			case 11: return "Ti";
			default: return "";
		}
	}
	else if(sistema == SistemaNombreNotas::Ingles)
	{
		switch(nota)
		{
			case 0: return "C";
			case 1: return "C♯";
			case 2: return "D";
			case 3: return "D♯";
			case 4: return "E";
			case 5: return "F";
			case 6: return "F♯";
			case 7: return "G";
			case 8: return "G♯";
			case 9: return "A";
			case 10: return "A♯";
			case 11: return "B";
			default: return "";
		}
	}
	else if(sistema == SistemaNombreNotas::Numerico)
	{
		switch(nota % 12)
		{
			case 0: return "1";
			case 2: return "2";
			case 4: return "3";
			case 5: return "4";
			case 7: return "5";
			case 9: return "6";
			case 11: return "7";
			default: return "";
		}
	}
	return "";
}

std::string Nombre_FiguraMusical(FiguraMusical figura)
{
	if(figura == FiguraMusical::Maxima)
		return T("Maxima");
	else if(figura == FiguraMusical::Longa)
		return T("Longa");
	else if(figura == FiguraMusical::Cuadrada)
		return T("Cuadrada");
	else if(figura == FiguraMusical::Redonda)
		return T("Redonda");
	else if(figura == FiguraMusical::Blanca)
		return T("Blanca");
	else if(figura == FiguraMusical::Negra)
		return T("Negra");
	else if(figura == FiguraMusical::Corchea)
		return T("Corchea");
	else if(figura == FiguraMusical::SemiCorchea)
		return T("SemiCorchea");
	else if(figura == FiguraMusical::Fusa)
		return T("Fusa");
	else if(figura == FiguraMusical::SemiFusa)
		return T("SemiFusa");
	else if(figura == FiguraMusical::Garrapatea)
		return T("Garrapatea");
	else if(figura == FiguraMusical::SemiGarrapatea)
		return T("SemiGarrapatea");
	return "";
}
