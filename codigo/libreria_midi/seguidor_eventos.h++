#ifndef SEGUIDOR_EVENTOS
#define SEGUIDOR_EVENTOS

#include <cstdint>
#include <vector>
#include <queue>

#include "definiciones_midi.h++"
#include "evento_midi.h++"

enum PosicionControladorBanco : unsigned char
{
	Banco_BEI = 0,
	Banco_BED,
};

enum PosicionDatosFlexibles : unsigned char
{
	Tempo = 0,
	Compas,
	Metronomo,
	Armadura,
	Acorde,
	TotalDF,
};

class Seguidor_Eventos
{
private:
	bool m_grupos_usados[GRUPOS_MIDI];
	bool m_canales_usados[GRUPOS_MIDI][CANALES_MIDI];
	bool m_hay_eventos;
	bool m_termino_enviar_eventos;
	bool m_hay_controlador;

	bool m_copiar_eventos;
	Evento_Midi * m_mensaje_utilidad_dctpq;
	Evento_Midi * m_controlador[GRUPOS_MIDI][CANALES_MIDI][CONTROLADORES_MIDI];
	Evento_Midi * m_programa[GRUPOS_MIDI][CANALES_MIDI];
	Evento_Midi * m_presion_canal[GRUPOS_MIDI][CANALES_MIDI];
	Evento_Midi * m_inflexion_tono[GRUPOS_MIDI][CANALES_MIDI];
	Evento_Midi * m_controlador_banco_no_confirmado[GRUPOS_MIDI][CANALES_MIDI][2];
	Evento_Midi * m_datos_flexibles_unicos[GRUPOS_MIDI][PosicionDatosFlexibles::TotalDF];
	std::vector<Evento_Midi*> m_eventos_repetibles;
	std::vector<Evento_Midi*> m_eventos_listos;

	bool m_procesamiento_requerido;
	std::uint64_t m_actual_repetible;
	std::uint64_t m_actual_listos;

	void vaciar_memoria();
	void inicializar();

public:
	Seguidor_Eventos(bool copiar_eventos);
	~Seguidor_Eventos();

	void registrar_evento(Evento_Midi * evento);

	void procesar_eventos();
	Evento_Midi *siguiente_evento();
	void borrar_registro_eventos();

	bool hay_eventos();
	bool termino_enviar_eventos();

	bool grupo_usado(unsigned char grupo) const;
	bool canal_usado(unsigned char grupo, unsigned char canal) const;
	bool programa_cambiado(unsigned char grupo, unsigned char canal);
};

#endif
