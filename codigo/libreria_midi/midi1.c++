#include "midi1.h++"

#include <cmath>
#include "../registro.h++"
#include "../util/texto.h++"

Midi1::Midi1(std::fstream *archivo) : Midi()
{
	this->cargar_archivo(archivo);

	bool ordenar = false;
	if(m_formato_midi < 2 && m_numero_fragmentos > 1)
		ordenar = true;

	//Crea las pistas de cada secuencia
	for(unsigned short s=0; s<m_secuencias.size(); s++)
	{
		m_secuencias[s]->crear_pistas(ordenar);

		//Se queda con la maxima duracion
		if(m_secuencias[s]->duracion_en_microsegundos() > m_duracion_en_microsegundo)
			m_duracion_en_microsegundo = m_secuencias[s]->duracion_en_microsegundos();
	}
}

Midi1::Midi1(unsigned short formato_midi, unsigned short division_tiempo) : Midi()
{
	m_formato_midi = formato_midi;
	if(m_formato_midi > 2)
		throw Excepcion_Midi(CodigoErrorMidi::FormatoMidiDesconocido);

	m_division_tiempo = division_tiempo;
	if((m_division_tiempo & 0x8000) != 0)
		throw Excepcion_Midi(CodigoErrorMidi::TiempoSMPTENoSoportado);

	m_tics_por_negra = m_division_tiempo;

	m_tiempo_por_tics = MICROSEGUNDOS_POR_NEGRA / m_tics_por_negra;

	m_numero_fragmentos = 1;
	m_secuencias.push_back(new Secuencia_Midi(m_tics_por_negra));
	m_secuencia_seleccionada = 0;
}

Midi1::~Midi1()
{
	for(unsigned short x=0; x<m_secuencias.size(); x++)
		delete m_secuencias[x];

	m_secuencias.clear();
}

unsigned int Midi1::escribir_uint32_largo_variable(std::fstream *archivo, unsigned int valor)
{
	//Largo variable MIDI
	unsigned char p = 4;
	unsigned char b[5] {0, 0, 0, 0, 0};
	unsigned int largo = 0;
	while(valor > 0 && p > 0)
	{
		b[p] = valor & 0x0000007F;
		if(p < 4)
			b[p] = b[p] | 0x80;//Indica que no es el ultimo byte
		valor = valor >> 7;
		p--;
	}

	for(unsigned char x=0; x<5; x++)
	{
		if((x < 4 && b[x] > 0x80) || x == 4)
		{
			archivo->write(reinterpret_cast<char *>(&b[x]), sizeof(char));
			largo++;
		}
	}

	return largo;
}

void Midi1::cargar_archivo(std::fstream *archivo)
{
	std::uint64_t largo_archivo = 0;
	std::uint64_t b = 0;
	char *datos_archivo = nullptr;
	if(archivo->is_open())
	{
		archivo->seekg(0, std::ios::end);
		largo_archivo = static_cast<std::uint64_t>(archivo->tellg());
		archivo->seekg(0, std::ios::beg);

		datos_archivo = new char[sizeof(char) * largo_archivo];
		archivo->read(datos_archivo, static_cast<std::int64_t>(sizeof(char) * largo_archivo));
		archivo->close();
	}
	else
		throw Excepcion_Midi(CodigoErrorMidi::FalloAbrirArchivo);

	if(largo_archivo < 8)
		throw Excepcion_Midi(CodigoErrorMidi::ArchivoMidiMuyCorto);

	if(this->contiene_cadena(datos_archivo, "SMF2CLIP", 8, b))
		throw Excepcion_Midi(CodigoErrorMidi::ArchivoMidi2_No_Compatible);

	if(this->contiene_cadena(datos_archivo, "MThd", 4, b))
		b += 4;//Avanza 4 bytes
	else if(this->contiene_cadena(datos_archivo, "RIFF", 4, b))
	{
		//Agrupacion de datos SMF y DLS en un archivo RMID (RP-029), se extrae solo los datos MIDI que se encuentran
		//al comienzo del archivo, contiene datos de un solo SMF y DLS, el fragmento 'data' debe se el primer fragmento dentro
		//del fragmento RMID

		if(largo_archivo < 28)
			throw Excepcion_Midi(CodigoErrorMidi::ArchivoMidiMuyCorto);

		//unsigned int fragmento_riff = this->uint32_extremo_chico(datos_archivo, b+4);//Largo del bloque riff
		bool error_rmid = false;
		bool error_datos = false;
		//Se pretende no lanzar un error prematuro, si no encuentra RMID o data pero si MThd, no importa
		if(!this->contiene_cadena(datos_archivo, "RMID", 4, b+8))
			error_rmid =  true;
		if(!this->contiene_cadena(datos_archivo, "data", 4, b+12))
			error_datos = true;

		//unsigned int largo_rmid = this->uint32_extremo_chico(datos_archivo, b+16);
		if(!this->contiene_cadena(datos_archivo, "MThd", 4, b+20))
		{
			if(error_rmid)
				throw Excepcion_Midi(CodigoErrorMidi::NoEsArchivoRMID);
			else if(error_datos)
				throw Excepcion_Midi(CodigoErrorMidi::RMIDSinDatos);
			else
				throw Excepcion_Midi(CodigoErrorMidi::RMIDSinDatosMidi);
		}
		b += 24;
	}
	else
		throw Excepcion_Midi(CodigoErrorMidi::ArchivoDesconocido);

	//Obtiene el largo de la cabecera
	unsigned int largo_cabecera = this->uint32_extremo_grande(datos_archivo, b);
	b += 4;//Avanza 4 bytes

	//El largo del encabezado debe ser de al menos 6 bytes
	if(largo_cabecera < 6)
		throw Excepcion_Midi(CodigoErrorMidi::EncabezadoMidiMuyCorto);

	if(largo_archivo < b+largo_cabecera)
		throw Excepcion_Midi(CodigoErrorMidi::ArchivoMidiMuyCorto);

	//Se guarda el tipo de formato del archivo midi
	m_formato_midi = this->uint16_extremo_grande(datos_archivo, b);
	b += 2;

	//Se guarda el numero de fragmentos
	m_numero_fragmentos = this->uint16_extremo_grande(datos_archivo, b);
	b += 2;

	//Se guarda la divicion de tiempo
	m_division_tiempo = this->uint16_extremo_grande(datos_archivo, b);
	b += 2;

	//Formato 0: El archivo contiene solo una pista milti canal.
	//Formato 1: El archivo contiene una o mas pistas simultaneas.
	//Formato 2: El archivo contiene una o mas pistas secuencialmente independientes.
	if(m_formato_midi > 2)
		throw Excepcion_Midi(CodigoErrorMidi::FormatoMidiDesconocido);

	if(m_numero_fragmentos == 0)
		throw Excepcion_Midi(CodigoErrorMidi::ArchivoMidiSinPistas);

	//Si el bit 15 de division_tiempo es cero: los bit 14 a 0 representan el número de tics de tiempo delta que
	//componen una negra.
	//Si el bit 15 de division_tiempo es 1: los tiempos delta del archivo corresponde a subdiviciones de un segundo
	//consistente con SMPTE, bit de 14 a 8 es el formato del codigo de tiempo
	if((m_division_tiempo & 0x8000) != 0)
		throw Excepcion_Midi(CodigoErrorMidi::TiempoSMPTENoSoportado);

	m_tics_por_negra = m_division_tiempo;
	m_tiempo_por_tics = MICROSEGUNDOS_POR_NEGRA / m_tics_por_negra;

	//MTrk
	Secuencia_Midi *secuencia_actual = nullptr;
	unsigned short fragmento_actual = 0;
	bool error_mtrk = false;
	while(b+4 < largo_archivo)
	{
		//Busca la cabecera hasta el final del archivo
		if(!this->contiene_cadena(datos_archivo, "MTrk", 4, b))
		{
			//Se mantiene en el ciclo mientras no encuentre el inicio de pista o haste que termine el archivo
			if(!error_mtrk)
			{
				Registro::Depurar("No se ha encontrado MTrk en: " + std::to_string(b));
				error_mtrk = true;
			}
			b++;
			continue;
		}
		if(error_mtrk)
		{
			Registro::Depurar("MTrk encontrado en: " + std::to_string(b));
			error_mtrk = false;
		}
		b += 4;//Avanza 4 bytes, encontro el inicio de la pista

		if(largo_archivo < b+4)
		{
			Registro::Depurar("Pista sin contenido");
			break;
		}

		//Se lee el largo del fragmento
		unsigned int largo_fragmento = this->uint32_extremo_grande(datos_archivo, b);
		b += 4;//Avanza 4 bytes

		if(largo_archivo < b+largo_fragmento)
			Registro::Depurar("Pista MIDI incompleta, el archivo es más corto de lo esperado");

		//Para el formato midi tipo 2 todos los fragmentos se consideran secuencias independientes
		if(secuencia_actual == nullptr || m_formato_midi == 2)
		{
			//No puede sobrepasar el limite de secuencias
			if(m_secuencias.size() < std::numeric_limits<unsigned short>::max()-1)
			{
				secuencia_actual = new Secuencia_Midi(m_tics_por_negra);
				m_secuencias.push_back(secuencia_actual);
				fragmento_actual = 0;
			}
		}

		unsigned int posicion_evento = 0;//Posicion del evento dentro del fragmento
		unsigned char evento = 0;//Almacena el tipo de evento
		unsigned char evento_anterior = 0;
		unsigned int largo_evento = 0;
		unsigned int tiempo_delta = 0;
		unsigned int largo_variable = 0;
		std::uint64_t tics_acumulado = 0;
		bool leyendo_tiempo_delta = true;
		bool terminar_pista = false;

		for(; b<largo_archivo && !terminar_pista; b++)
		{
			std::uint64_t byte_datos = 0;
			unsigned char *datos_evento = nullptr;

			if(leyendo_tiempo_delta)
			{
				//Se calcula el tiempo delta del evento
				//El tiempo delta es una cantidad de longitud varialbe
				//Estos numeros se representan con 7 bit por byte
				//Si el bit 7 es 1 (empezando a contar desde 0), indica que aun no a terminado de leer todos los bytes que son parte del valor.
				if((datos_archivo[b] & 0x80) != 0x80)
					leyendo_tiempo_delta = false;
				tiempo_delta = (tiempo_delta << 7) | (datos_archivo[b] & 0x7F);
			}
			else
			{
				//Si no se recibe el bit de estados entonces es porque es el mismo evento que el anterior, por lo que
				//solo se tienen los bit de datos
				if((datos_archivo[b] & 0x80) != 0x80)
				{
					//Mismo evento que el anterior
					byte_datos = b;
					if(evento_anterior >= 0x80)
						evento = evento_anterior;
				}
				else
				{
					//Evento nuevo
					evento = static_cast<unsigned char>(datos_archivo[b]);

					//Solo se almacena como evento anterior, si es un evento mensaje de canal
					if(	(evento & 0xF0) == EventoMidi_NotaApagada ||
						(evento & 0xF0) == EventoMidi_NotaEncendida ||
						(evento & 0xF0) == EventoMidi_PresionNota ||
						(evento & 0xF0) == EventoMidi_Controlador ||
						(evento & 0xF0) == EventoMidi_Programa ||
						(evento & 0xF0) == EventoMidi_PresionCanal ||
						(evento & 0xF0) == EventoMidi_InflexionDeTono)
						evento_anterior = evento;
					byte_datos = b+1;

					if(largo_archivo < b+1)
					{
						Registro::Depurar("No se termino de leer el evento, el archivo esta incompleto");
						b=largo_archivo;
						break;
					}
				}

				if( (evento & 0xF0) == EventoMidi_NotaApagada ||
					(evento & 0xF0) == EventoMidi_NotaEncendida ||
					(evento & 0xF0) == EventoMidi_PresionNota ||
					(evento & 0xF0) == EventoMidi_Controlador ||
					(evento & 0xF0) == EventoMidi_InflexionDeTono)
				{
					if(largo_archivo < byte_datos+1)
					{
						Registro::Depurar("No se termino de leer el evento, el archivo esta incompleto.");
						b=largo_archivo;
						break;
					}

					largo_evento = 3;
					datos_evento = new unsigned char[largo_evento];
					datos_evento[1] = static_cast<unsigned char>(datos_archivo[byte_datos]);
					datos_evento[2] = static_cast<unsigned char>(datos_archivo[byte_datos+1]);
					//Si es un evento de NotaEncendida pero la velocidad es 0 entonces realmente es un
					//evento de NotaApagada
					if((evento & 0xF0) == EventoMidi_NotaEncendida && datos_evento[2] == 0)
						datos_evento[0] = EventoMidi_NotaApagada | (evento & 0x0F);//Conserva el canal
						else
							datos_evento[0] = evento;
					b = byte_datos + 1;
				}
				else if((evento & 0xF0) == EventoMidi_Programa ||
					(evento & 0xF0) == EventoMidi_PresionCanal)
				{
					largo_evento = 2;
					datos_evento = new unsigned char[largo_evento];
					datos_evento[0] = evento;
					datos_evento[1] = static_cast<unsigned char>(datos_archivo[byte_datos]);
					b = byte_datos;
				}
				else if(evento == EventoMidi_ExclusivoDelSistema ||	//0xF0
					evento == EventoMidi_SecuenciaDeEscape ||	//0xF7
					evento == EventoMidi_Metaevento)			//0xFF
				{
					unsigned int meta_evento = 0;
					unsigned char tipo_metaevento = 0;
					if(evento == EventoMidi_Metaevento)
					{
						tipo_metaevento = static_cast<unsigned char>(datos_archivo[byte_datos]);
						//+1 Porque tiene un byte adicional a los demas
						meta_evento = 1;
						byte_datos++;
					}

					//Se obtiene el largo del evento, que esta almacenado
					//en una cantidad de largo variable igual que el tiempo delta
					bool fin_lv = false;
					largo_variable = 0;
					for(b = byte_datos; b<largo_archivo && !fin_lv; b++)
					{
						if((datos_archivo[b] & 0x80) != 0x80)
							fin_lv = true;
						largo_variable = (largo_variable << 7) | (datos_archivo[b] & 0x7F);
					}
					largo_evento = 1 + meta_evento + largo_variable;

					if(largo_archivo < b+largo_variable)
					{
						Registro::Depurar("No se termino de leer el evento, el archivo esta incompleto.");
						b=largo_archivo;
						break;
					}

					datos_evento = new unsigned char[largo_evento];
					datos_evento[0] = evento;
					if(meta_evento > 0)
						datos_evento[1] = tipo_metaevento;

					//Se lee el evento de acuerdo al largo variable
					for(unsigned int lv = 0; lv < largo_variable; lv++)
					{
						datos_evento[1 + meta_evento + lv] = static_cast<unsigned char>(datos_archivo[b]);
						b++;
					}
					b--;//Se resta porque sumara uno nuevamente el for superior
				}
				else
				{
					//En este punto solo quedan los eventos invalidos
					//que deben ser corregido con una secuencia de escape al comienzo
					if(evento == 0xF1 || evento == 0xF3)
					{
						largo_evento = 3;
						datos_evento = new unsigned char[largo_evento];
						datos_evento[0] = 0xF7;
						datos_evento[1] = evento;
						datos_evento[2] = static_cast<unsigned char>(datos_archivo[byte_datos]);
						b = byte_datos;
					}
					else if(evento == 0xF2)
					{
						if(largo_archivo < byte_datos+1)
						{
							Registro::Depurar("No se termino de leer el evento, el archivo esta incompleto.");
							b=largo_archivo;
							break;
						}

						largo_evento = 4;
						datos_evento = new unsigned char[largo_evento];
						datos_evento[0] = 0xF7;
						datos_evento[1] = evento;
						datos_evento[2] = static_cast<unsigned char>(datos_archivo[byte_datos]);
						datos_evento[3] = static_cast<unsigned char>(datos_archivo[byte_datos+1]);
						b = byte_datos + 1;
					}
					else if(evento >= 0xF4 && evento < 0xFF)
					{
						largo_evento = 2;
						datos_evento = new unsigned char[largo_evento];
						datos_evento[0] = 0xF7;
						datos_evento[1] = evento;
						b = byte_datos-1;
					}
				}

				tics_acumulado += tiempo_delta;
				try
				{
					Evento_Midi *evento_creado = new Evento_Midi(datos_evento, largo_evento, VersionMidi::Version_1);
					Datos_Evento *evento_datos = new Datos_Evento(posicion_evento, fragmento_actual, tics_acumulado, evento_creado);
					secuencia_actual->agregar_datos_evento(evento_datos);//parametro_registrado
					posicion_evento++;

					//Termina la pista con un metaevento de fin de pista
					if(	evento_creado->tipo_evento_midi1() == EventoMidi_Metaevento &&
						evento_creado->tipo_metaevento_midi1() == MetaEventoMidi_FinDePista)
						terminar_pista = true;
				}
				catch (const Excepcion_Midi &e)
				{
					Registro::Depurar("Se omite un evento: " + e.descripcion_error());
				}
				largo_evento = 0;

				//Termino de leer un evento completo, entonces debe seguir el tiempo delta del proximo evento
				leyendo_tiempo_delta = true;
				tiempo_delta = 0;
			}
		}
		fragmento_actual++;

	}
	if(error_mtrk)
		Registro::Depurar("Hay bytes extras al final del archivo que no fueron leidos");
	//Se libera la memoria
	delete []datos_archivo;
}

void Midi1::guardar(const std::string &nombre_archivo)
{
	std::fstream archivo(nombre_archivo, std::ios::out | std::ios::binary);
	if(!archivo.is_open())
		throw Excepcion_Midi(CodigoErrorMidi::FalloCrearArchivo);

	std::map<unsigned int, std::vector<Datos_Evento*>*> fragmentos;
	for(unsigned short s=0; s<m_secuencias.size(); s++)
	{
		std::vector<Datos_Evento*> *lista_eventos = m_secuencias[s]->lista_eventos();
		unsigned int secuencia = s;
		secuencia = secuencia << 16;
		for(unsigned int e=0; e<lista_eventos->size(); e++)
		{
			unsigned clave = secuencia | (*lista_eventos)[e]->fragmento();
			std::map<unsigned int, std::vector<Datos_Evento*>*>::iterator buscado = fragmentos.find(clave);
			if(buscado != fragmentos.end())
				buscado->second->push_back((*lista_eventos)[e]);
			else
			{
				std::vector<Datos_Evento*> *nuevo = new std::vector<Datos_Evento*>();
				nuevo->push_back((*lista_eventos)[e]);
				fragmentos[clave] = nuevo;
			}
		}
	}

	char mthd[4] = {'M', 'T', 'h', 'd'};
	char mtrk[4] = {'M', 'T', 'r', 'k'};

	archivo.write(mthd, sizeof(char)*4);
	this->escribir_uint32_extremo_grande(&archivo, 6);//Largo de cabecera MIDI
	this->escribir_uint16_extremo_grande(&archivo, m_formato_midi);
	this->escribir_uint16_extremo_grande(&archivo, static_cast<unsigned short>(fragmentos.size()));
	this->escribir_uint16_extremo_grande(&archivo, m_division_tiempo);

	std::map<unsigned int, std::vector<Datos_Evento*>*>::iterator fragmento_actual = fragmentos.begin();
	while(fragmento_actual != fragmentos.end())
	{
		archivo.write(mtrk, sizeof(char)*4);
		std::int64_t posicion_largo_pista = archivo.tellp();
		this->escribir_uint32_extremo_grande(&archivo, 0);//Largo de la pista en bytes, aun no se sabe
		std::vector<Datos_Evento*>* fragmento = fragmento_actual->second;
		unsigned int largo_fragmento = 0;
		unsigned char estado_anterior = 0;
		std::uint64_t tiempo_actual = 0;
		bool fin_pista = false;
		for(unsigned int e=0; e<fragmento->size() && !fin_pista; e++)
		{
			Evento_Midi *evento = (*fragmento)[e]->evento();
			std::uint64_t delta = (*fragmento)[e]->tics() - tiempo_actual;
			//Escribe el tiempo delta
			largo_fragmento += this->escribir_uint32_largo_variable(&archivo, static_cast <unsigned int>(delta));
			largo_fragmento += evento->largo_datos();
			if(evento->es_mensaje_voz_de_canal())
			{
				//Un EventoMidi_NotaEncendida con velocidad 0 es equivalente a un evento EventoMidi_NotaApagada, asi que
				//se hace lo opuesto en este caso para reducir el tamaño del archivo midi, porque si el estado es el mismo que el anterior
				//se omite el estado y se ahorra un byte, pero solo se puede usar si la velocidad de la nota apagada es cero
				unsigned char estado_actual = 0;

				if(evento->tipo_evento_midi1() == EventoMidi_NotaApagada && evento->velocidad_nota_7() == 0)
					estado_actual = EventoMidi_NotaEncendida | evento->canal();
				else
					estado_actual = evento->tipo_evento_midi1() | evento->canal();


				if(estado_anterior != estado_actual)
				{
					archivo.write(reinterpret_cast<char*>(&estado_actual), sizeof(char));
					estado_anterior = estado_actual;
				}
				else
					largo_fragmento--;

				//Escribe los datos del evento, saltandose el byte de estado porque es el mismo que en el evento anterior
				unsigned char *datos_completos = evento->datos();
				for(unsigned int x=1; x<evento->largo_datos(); x++)
					archivo.write(reinterpret_cast<char*>(&datos_completos[x]), sizeof(char));
			}
			else if(evento->tipo_evento_midi1() == EventoMidi_ExclusivoDelSistema ||
					evento->tipo_evento_midi1() == EventoMidi_SecuenciaDeEscape ||
					evento->tipo_evento_midi1() == EventoMidi_Metaevento)
			{
				unsigned int inicio_datos = 0;
				if(evento->tipo_evento_midi1() == EventoMidi_Metaevento)
					inicio_datos = 2;
				else
					inicio_datos = 1;
				unsigned int largo_datos = evento->largo_datos()-inicio_datos;

				unsigned char *datos_completos = evento->datos();
				for(unsigned int x=0; x<inicio_datos; x++)
					archivo.write(reinterpret_cast<char*>(&datos_completos[x]), sizeof(char));

				//Escribe el largo variable del evento
				largo_fragmento += this->escribir_uint32_largo_variable(&archivo, largo_datos);
				//Escribe los datos del evento
				for(unsigned int x=inicio_datos; x<evento->largo_datos(); x++)
					archivo.write(reinterpret_cast<char*>(&datos_completos[x]), sizeof(char));
			}
			if(evento->tipo_evento_midi1() == EventoMidi_Metaevento && evento->tipo_metaevento_midi1() == MetaEventoMidi_FinDePista)
				fin_pista = true;
			tiempo_actual = (*fragmento)[e]->tics();
		}
		//Se agrega el evento de fin de pista si no existe
		if(!fin_pista)
		{
			unsigned char datos[4] {0x00, 0xFF, 0x2F, 0x00};
			archivo.write(reinterpret_cast<char*>(datos), sizeof(char)*4);
			largo_fragmento += 4;
		}

		archivo.seekp(posicion_largo_pista);//Salta a la posición donde se guarda el largo de la pista
		this->escribir_uint32_extremo_grande(&archivo, largo_fragmento);//Guarda el largo de la pista
		archivo.seekp(0, std::ios::end);//Salta al final del archivo nuevamente

		delete fragmento_actual->second;//Libera la memoria del fragmento actual

		fragmento_actual++;
	}
}

unsigned char Midi1::version_midi() const
{
	return 1;
}

std::int64_t Midi1::duracion_midi(std::fstream *archivo)
{
	//Es practicamente el mismo codigo que el metodo cargar_archivo de la clase Midi1,
	//con la unica diferencia que no se muestran los errores y solo retorna 0 si falla
	//y ademas esta simplificado para saltarse la creacion de eventos que no son necesarios para
	//calcular la duración del archivo mucho mas rapido.

	std::uint64_t largo_archivo = 0;
	std::uint64_t b = 0;
	char *datos_archivo = nullptr;
	if(archivo->is_open())
	{
		archivo->seekg(0, std::ios::end);
		largo_archivo = static_cast<std::uint64_t>(archivo->tellg());
		archivo->seekg(0, std::ios::beg);

		datos_archivo = new char[sizeof(char) * largo_archivo];
		archivo->read(datos_archivo, static_cast<std::int64_t>(sizeof(char) * largo_archivo));
		archivo->close();
	}
	else
		return 0;

	if(largo_archivo < 8)
		return 0;

	if(Midi1::contiene_cadena(datos_archivo, "MThd", 4, b))
		b += 4;
	else if(Midi1::contiene_cadena(datos_archivo, "RIFF", 4, b))
	{
		if(largo_archivo < 28)
			return 0;

		if(!Midi1::contiene_cadena(datos_archivo, "MThd", 4, b+20))
			return 0;

		b += 24;
	}
	else
		return 0;

	unsigned int largo_cabecera = Midi1::uint32_extremo_grande(datos_archivo, b);
	b += 4;

	if(largo_cabecera < 6)
		return 0;

	if(largo_archivo < b+largo_cabecera)
		return 0;

	unsigned short formato_midi = Midi1::uint16_extremo_grande(datos_archivo, b);
	b += 2;

	unsigned short numero_fragmentos = Midi1::uint16_extremo_grande(datos_archivo, b);
	b += 2;

	//Se guarda la divicion de tiempo
	unsigned short division_tiempo = Midi1::uint16_extremo_grande(datos_archivo, b);
	b += 2;

	if(formato_midi > 2)
		return 0;

	if(numero_fragmentos == 0)
		return 0;

	if((division_tiempo & 0x8000) != 0)
		return 0;

	unsigned short tics_por_negra = division_tiempo;

	std::vector<Secuencia_Midi*> secuencias;
	Secuencia_Midi *secuencia_actual = nullptr;
	unsigned short fragmento_actual = 0;
	while(b+4 < largo_archivo)
	{
		if(!Midi1::contiene_cadena(datos_archivo, "MTrk", 4, b))
		{
			b++;
			continue;
		}
		b += 4;

		if(largo_archivo < b+4)
			break;

		b += 4;//Avanza 4 byte

		if(secuencia_actual == nullptr || formato_midi == 2)
		{
			if(secuencias.size() < std::numeric_limits<unsigned short>::max()-1)
			{
				secuencia_actual = new Secuencia_Midi(tics_por_negra);
				secuencias.push_back(secuencia_actual);
				fragmento_actual = 0;
			}
		}

		unsigned int posicion_evento = 0;
		unsigned char evento = 0;
		unsigned char evento_anterior = 0;
		unsigned int largo_evento = 0;
		unsigned int tiempo_delta = 0;
		unsigned int largo_variable = 0;
		std::uint64_t tics_acumulado = 0;
		bool leyendo_tiempo_delta = true;
		bool terminar_pista = false;

		for(; b<largo_archivo && !terminar_pista; b++)
		{
			std::uint64_t byte_datos = 0;
			unsigned char *datos_evento = nullptr;

			if(leyendo_tiempo_delta)
			{
				if((datos_archivo[b] & 0x80) != 0x80)
					leyendo_tiempo_delta = false;
				tiempo_delta = (tiempo_delta << 7) | (datos_archivo[b] & 0x7F);
			}
			else
			{
				if((datos_archivo[b] & 0x80) != 0x80)
				{
					byte_datos = b;
					if(evento_anterior >= 0x80)
						evento = evento_anterior;
				}
				else
				{
					evento = static_cast<unsigned char>(datos_archivo[b]);
					if(	(evento & 0xF0) == EventoMidi_NotaApagada ||
						(evento & 0xF0) == EventoMidi_NotaEncendida ||
						(evento & 0xF0) == EventoMidi_PresionNota ||
						(evento & 0xF0) == EventoMidi_Controlador ||
						(evento & 0xF0) == EventoMidi_Programa ||
						(evento & 0xF0) == EventoMidi_PresionCanal ||
						(evento & 0xF0) == EventoMidi_InflexionDeTono)
						evento_anterior = evento;
					byte_datos = b+1;

					if(largo_archivo < b+1)
					{
						b=largo_archivo;
						break;
					}
				}

				if( (evento & 0xF0) == EventoMidi_NotaApagada ||
					(evento & 0xF0) == EventoMidi_NotaEncendida ||
					(evento & 0xF0) == EventoMidi_PresionNota ||
					(evento & 0xF0) == EventoMidi_Controlador ||
					(evento & 0xF0) == EventoMidi_InflexionDeTono)
				{
					b = byte_datos + 1;
				}
				else if((evento & 0xF0) == EventoMidi_Programa ||
					(evento & 0xF0) == EventoMidi_PresionCanal)
				{
					b = byte_datos;
				}
				else if(evento == EventoMidi_ExclusivoDelSistema ||
					evento == EventoMidi_SecuenciaDeEscape ||
					evento == EventoMidi_Metaevento)
				{
					unsigned int meta_evento = 0;
					unsigned char tipo_metaevento = 0;
					if(evento == EventoMidi_Metaevento)
					{
						tipo_metaevento = static_cast<unsigned char>(datos_archivo[byte_datos]);
						meta_evento = 1;
						byte_datos++;
					}
					bool fin_lv = false;
					largo_variable = 0;
					for(b = byte_datos; b<largo_archivo && !fin_lv; b++)
					{
						if((datos_archivo[b] & 0x80) != 0x80)
							fin_lv = true;
						largo_variable = (largo_variable << 7) | (datos_archivo[b] & 0x7F);
					}
					largo_evento = 1 + meta_evento + largo_variable;

					if(largo_archivo < b+largo_variable)
					{
						b=largo_archivo;
						break;
					}

					if(tipo_metaevento == MetaEventoMidi_Tempo || tipo_metaevento == MetaEventoMidi_FinDePista)
					{
						datos_evento = new unsigned char[largo_evento];
						datos_evento[0] = evento;
						if(meta_evento > 0)
							datos_evento[1] = tipo_metaevento;

						//Se lee el evento de acuerdo al largo variable
						for(unsigned int lv = 0; lv < largo_variable; lv++)
						{
							datos_evento[1 + meta_evento + lv] = static_cast<unsigned char>(datos_archivo[b]);
							b++;
						}
					}
					else
					{
						datos_evento = nullptr;
						b += largo_variable;
					}
					b--;
				}
				else
				{
					if(evento == 0xF1 || evento == 0xF3)
						b = byte_datos;
					else if(evento == 0xF2)
						b = byte_datos + 1;
					else if(evento >= 0xF4 && evento < 0xFF)
						b = byte_datos-1;
				}
				tics_acumulado += tiempo_delta;
				try
				{
					if(evento == EventoMidi_Metaevento && datos_evento != nullptr)
					{
						posicion_evento++;
						Evento_Midi *evento_creado = new Evento_Midi(datos_evento, largo_evento, VersionMidi::Version_1);
						Datos_Evento *evento_datos = new Datos_Evento(posicion_evento, fragmento_actual, tics_acumulado, evento_creado);
						secuencia_actual->agregar_datos_evento(evento_datos);//parametro_registrado
						//Termina la pista con un metaevento de fin de pista
						if(evento_creado->tipo_metaevento_midi1() == MetaEventoMidi_FinDePista)
							terminar_pista = true;
					}
				}
				catch (const Excepcion_Midi &e)
				{
				}
				largo_evento = 0;
				leyendo_tiempo_delta = true;
				tiempo_delta = 0;
			}
		}
		fragmento_actual++;

	}
	//Se libera la memoria
	delete []datos_archivo;

	std::int64_t duracion_en_microsegundo = 0;//numero_fragmentos
	//Solo se ordenan si hay mas de un fragmento y no es de formato tipo 2
	bool ordenar = false;
	if(formato_midi < 2 && numero_fragmentos > 1)
		ordenar = true;

	for(unsigned short s=0; s<secuencias.size(); s++)
	{
		secuencias[s]->calcular_duracion(ordenar);
		if(secuencias[s]->duracion_en_microsegundos() > duracion_en_microsegundo)
			duracion_en_microsegundo = secuencias[s]->duracion_en_microsegundos();
		delete secuencias[s];
	};
	return duracion_en_microsegundo;
}
