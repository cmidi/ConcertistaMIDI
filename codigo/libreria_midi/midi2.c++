#include "midi2.h++"

#include "../registro.h++"

Midi2::Midi2() : Midi()
{
	m_formato_midi = 0;

	m_division_tiempo = 480;
	m_tics_por_negra = m_division_tiempo;

	m_tiempo_por_tics = MICROSEGUNDOS_POR_NEGRA / m_tics_por_negra;

	m_numero_fragmentos = 1;
	m_secuencias.push_back(new Secuencia_Midi(m_tics_por_negra));
	m_secuencia_seleccionada = 0;
}

Midi2::Midi2(std::fstream *archivo) : Midi()
{
	m_formato_midi = 0;
	this->cargar_archivo(archivo);

	bool ordenar = true;
	if(m_numero_fragmentos > 1)
		ordenar = true;

	//Crea las pistas de cada secuencia
	for(unsigned short s=0; s<m_secuencias.size(); s++)
	{
		m_secuencias[s]->crear_pistas(ordenar);

		//Se queda con la maxima duracion
		if(m_secuencias[s]->duracion_en_microsegundos() > m_duracion_en_microsegundo)
			m_duracion_en_microsegundo = m_secuencias[s]->duracion_en_microsegundos();
	}
}

Midi2::~Midi2()
{

}

void Midi2::cargar_archivo(std::fstream *archivo)
{
	std::uint64_t largo_archivo = 0;
	std::uint64_t b = 0;
	char *datos_archivo = nullptr;
	if(archivo->is_open())
	{
		archivo->seekg(0, std::ios::end);
		largo_archivo = static_cast<std::uint64_t>(archivo->tellg());
		archivo->seekg(0, std::ios::beg);

		datos_archivo = new char[sizeof(char) * largo_archivo];
		archivo->read(datos_archivo, static_cast<std::int64_t>(sizeof(char) * largo_archivo));
		archivo->close();
	}
	else
		throw Excepcion_Midi(CodigoErrorMidi::FalloAbrirArchivo);

	if(largo_archivo < 8)
		throw Excepcion_Midi(CodigoErrorMidi::ArchivoMidiMuyCorto);

	if(this->contiene_cadena(datos_archivo, "SMF2CLIP", 8, b))
		b += 8;//Avanza 4 bytes
	else
		throw Excepcion_Midi(CodigoErrorMidi::ArchivoDesconocido);

	//Numero de fragmentos
	m_numero_fragmentos = 0;
	//Divicion de tiempo predeterminada
	m_division_tiempo = 480;

	m_tics_por_negra = m_division_tiempo;
	m_tiempo_por_tics = MICROSEGUNDOS_POR_NEGRA / m_tics_por_negra;

	Secuencia_Midi *secuencia_actual = new Secuencia_Midi(m_tics_por_negra);
	bool existe_divicion_de_tiempo = false;
	bool hay_eventos = false;
	unsigned short fragmento_actual = 0;
	unsigned int posicion_evento = 0;//Posicion del evento dentro del fragmento
	//unsigned int tiempo_delta = 0;
	std::uint64_t tics_acumulado = 0;
	unsigned char *datos_evento = nullptr;
	while(b+4 < largo_archivo)
	{
		unsigned char tipo_mensaje = static_cast<unsigned char>(datos_archivo[b] & 0xF0);
		unsigned char largo_mensaje = 0;

		if(	tipo_mensaje == TipoMensaje_Utilidad ||
			tipo_mensaje == TipoMensaje_ComunYTiempoReal ||
			tipo_mensaje == TipoMensaje_VozDeCanalMidi1 ||
			tipo_mensaje == TipoMensaje_Reservado0x6_32bits ||
			tipo_mensaje == TipoMensaje_Reservado0x7_32bits)
			largo_mensaje = 4;//32 bits
		else if(tipo_mensaje == TipoMensaje_ExclusivoDelSistema7bits ||
				tipo_mensaje == TipoMensaje_VozDeCanalMidi2 ||
				tipo_mensaje == TipoMensaje_Reservado0x8_64bits ||
				tipo_mensaje == TipoMensaje_Reservado0x9_64bits ||
				tipo_mensaje == TipoMensaje_Reservado0xA_64bits)
			largo_mensaje = 8;//64 bits
		else if(tipo_mensaje == TipoMensaje_Reservado0xB_96bits ||
				tipo_mensaje == TipoMensaje_Reservado0xC_96bits)
			largo_mensaje = 12;//96 bits
		else if(tipo_mensaje == TipoMensaje_ExclusivoDelSistema8bits ||
			tipo_mensaje == TipoMensaje_DatosFlexibles ||
			tipo_mensaje == TipoMensaje_Reservado0xE_128bits ||
			tipo_mensaje == TipoMensaje_Reservado0x9_64bits ||
			tipo_mensaje == TipoMensaje_FlujoPaqueteMidiUniversal)
			largo_mensaje = 16;//128 bits

		if(largo_archivo >= b+largo_mensaje)
		{
			datos_evento = new unsigned char[largo_mensaje];
			for(unsigned char x=0; x<largo_mensaje; x++)
				datos_evento[x] = static_cast<unsigned char>(datos_archivo[b+x]);

			b += largo_mensaje;

			try
			{
				Evento_Midi *evento_creado = new Evento_Midi(datos_evento, largo_mensaje, VersionMidi::Version_2);
				hay_eventos = true;

				if(evento_creado->tipo_mensaje() == TipoMensaje_Utilidad)
				{
					if(evento_creado->tipo_utilidad() == MensajeUtilidad_DivicionDeTiempo)
					{
						if(!existe_divicion_de_tiempo)
						{
							m_division_tiempo = evento_creado->division_de_tiempo();
							if(m_division_tiempo == 0)
								m_division_tiempo = 1;
							m_tics_por_negra = m_division_tiempo;
							m_tiempo_por_tics = MICROSEGUNDOS_POR_NEGRA / m_tics_por_negra;

							secuencia_actual->tics_por_negra(m_tics_por_negra);
							existe_divicion_de_tiempo = true;
						}
						else
							Registro::Error("Ya existe una Divicion de tiempo previa, evento ignorado");
					}
					else if(evento_creado->tipo_utilidad() == MensajeUtilidad_TiempoDelta)
						tics_acumulado += evento_creado->tiempo_delta();
				}
				else if(evento_creado->tipo_mensaje() == TipoMensaje_FlujoPaqueteMidiUniversal)
				{
					if(evento_creado->tipo_flujo() == MensajeDeFlujo_FinDeFragmento)
					{
						fragmento_actual = m_numero_fragmentos;
						m_numero_fragmentos++;
					}
				}

				Datos_Evento *evento_datos = new Datos_Evento(posicion_evento, fragmento_actual, tics_acumulado, evento_creado);
				secuencia_actual->agregar_datos_evento(evento_datos);//parametro_registrado
				posicion_evento++;
			}
			catch (const Excepcion_Midi &e)
			{
				Registro::Depurar("Se omite un evento: " + e.descripcion_error());
			}
		}
		else
		{
			Registro::Depurar("No se termino de leer el mensaje, el archivo esta incompleto");
			b=largo_archivo;
		}
	}
	if(!hay_eventos)
		throw Excepcion_Midi(CodigoErrorMidi::ArchivoSinPMU);

	m_secuencias.push_back(secuencia_actual);
	//Se libera la memoria
	delete []datos_archivo;
}

void Midi2::guardar(const std::string &nombre_archivo)
{
	std::fstream archivo(nombre_archivo, std::ios::out | std::ios::binary);
	if(!archivo.is_open())
		throw Excepcion_Midi(CodigoErrorMidi::FalloCrearArchivo);

	std::map<unsigned int, std::vector<Datos_Evento*>*> fragmentos;
	for(unsigned short s=0; s<m_secuencias.size(); s++)
	{
		std::vector<Datos_Evento*> *lista_eventos = m_secuencias[s]->lista_eventos();
		unsigned int secuencia = s;
		secuencia = secuencia << 16;
		for(unsigned int e=0; e<lista_eventos->size(); e++)
		{
			unsigned clave = secuencia | (*lista_eventos)[e]->fragmento();
			std::map<unsigned int, std::vector<Datos_Evento*>*>::iterator buscado = fragmentos.find(clave);
			if(buscado != fragmentos.end())
				buscado->second->push_back((*lista_eventos)[e]);
			else
			{
				std::vector<Datos_Evento*> *nuevo = new std::vector<Datos_Evento*>();
				nuevo->push_back((*lista_eventos)[e]);
				fragmentos[clave] = nuevo;
			}
		}
	}

	char encabezado[8] = {'S', 'M', 'F', '2', 'C', 'L', 'I', 'P'};
	archivo.write(encabezado, sizeof(char)*8);

	std::map<unsigned int, std::vector<Datos_Evento*>*>::iterator fragmento_actual = fragmentos.begin();
	while(fragmento_actual != fragmentos.end())
	{
		std::vector<Datos_Evento*>* fragmento = fragmento_actual->second;
		bool fin_pista = false;
		for(unsigned int e=0; e<fragmento->size() && !fin_pista; e++)
		{
			Evento_Midi *evento = (*fragmento)[e]->evento();
			if(evento->version_midi() == VersionMidi::Version_1)
				continue;

			for(unsigned int x=0; x<evento->largo_datos(); x++)
				archivo.write(reinterpret_cast<char*>(&evento->datos()[x]), sizeof(char));
		}
		//Se agrega el evento de fin de fragmento
		if(!fin_pista)
		{
			unsigned char datos[16] {0xF0,0x21,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00};
			archivo.write(reinterpret_cast<char*>(datos), sizeof(char)*16);
		}

		delete fragmento_actual->second;//Libera la memoria del fragmento actual
		fragmento_actual++;
	}
}

unsigned char Midi2::version_midi() const
{
	return 2;
}

std::int64_t Midi2::duracion_midi(std::fstream */*archivo*/)
{
	return 0;
}
