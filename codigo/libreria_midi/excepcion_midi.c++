#include "excepcion_midi.h++"
#include "../util/traduccion.h++"

Excepcion_Midi::Excepcion_Midi(CodigoErrorMidi codigo_error) : m_codigo_error(codigo_error)
{
}

const char *Excepcion_Midi::what() const noexcept
{
	switch(m_codigo_error)
	{
		case CodigoErrorMidi::FalloAbrirArchivo: 				return TC("No se puede abrir");
		case CodigoErrorMidi::ArchivoMidi2_No_Compatible:		return TC("Archivo MIDI 2.0 no compatible con esta versión");
		case CodigoErrorMidi::NoEsArchivoRMID:					return TC("No es un archivo RMID valido");
		case CodigoErrorMidi::RMIDSinDatos:						return TC("El archivo RMID no contiene datos");
		case CodigoErrorMidi::RMIDSinDatosMidi:					return TC("El archivo RMID no contiene datos MIDI");
		case CodigoErrorMidi::ArchivoDesconocido:				return TC("No es un archivo MIDI");
		case CodigoErrorMidi::ArchivoMidiMuyCorto:				return TC("El archivo MIDI es muy corto");
		case CodigoErrorMidi::EncabezadoMidiMuyCorto:			return TC("El encabezado MIDI es muy corto");
		case CodigoErrorMidi::FormatoMidiDesconocido:			return TC("El formato MIDI es desconocido");
		case CodigoErrorMidi::ArchivoMidiSinPistas:				return TC("El archivo MIDI no contiene pistas");
		case CodigoErrorMidi::TiempoSMPTENoSoportado:			return TC("El tiempo SMPTE no esta soportado");
		case CodigoErrorMidi::ArchivoSinPMU:					return TC("El archivo MIDI 2.0 no contiene ningun Paquete MIDI Universal");

		case CodigoErrorMidi::EventoNulo:						return TC("El evento es nulo");
		case CodigoErrorMidi::NoEsEventoMidi1:					return TC("No es un Eventos MIDI 1.0");
		case CodigoErrorMidi::NoEsEventoMidi2:					return TC("No es un Eventos MIDI 2.0");
		case CodigoErrorMidi::EventoNoDisponible:				return TC("Evento no disponible con este constructor");
		case CodigoErrorMidi::EventoNoValido:					return TC("El evento no es valido");
		case CodigoErrorMidi::EventoLargoIncorrecto:			return TC("El evento tiene un largo incorrecto");

		case CodigoErrorMidi::NoEsMensajeDeUtilidad:			return TC("No es un Paquete MIDI Universal de Utilidad");
		case CodigoErrorMidi::NoEsMensajeComunYTiempoReal:		return TC("No es un Paquete MIDI Universal Comun y Tiempo Real");
		case CodigoErrorMidi::NoEsMensajeDeVozDeCanal:			return TC("No es un Paquete MIDI Universal de Voz de Canal");
		case CodigoErrorMidi::NoEsMensajeSistemaExclusivo8:		return TC("No es un Paquete MIDI Universal de Sistema Exclusivo de 8 bits");
		case CodigoErrorMidi::NoEsMensajeDeDatosFlexibles:		return TC("No es un Paquete MIDI Universal de Datos Flexible");
		case CodigoErrorMidi::NoEsMensajeDeFlujoPMU:			return TC("No es un Paquete MIDI Universal de Flujo");
		case CodigoErrorMidi::NoEsEventoDeVozDeCanalMidi2:		return TC("No es un Paquete MIDI Universal de Voz de Canal MIDI 2.0");

		case CodigoErrorMidi::EventoNoContieneBanco:			return TC("El evento no contiene información de banco por bandera = 0");

		case CodigoErrorMidi::NoEsDivicionDeTiempo:				return TC("No es un evento de Divicion de Tiempo");
		case CodigoErrorMidi::NoEsTiempoDelta:					return TC("No es un evento de Tiempo Delta");
		case CodigoErrorMidi::EventoNoContieneNota:				return TC("El evento no contiene una nota");
		case CodigoErrorMidi::EventoNoContieneGrupo:			return TC("El evento no contiene informacion de grupo");
		case CodigoErrorMidi::EventoNoContieneCanal:			return TC("El evento no contiene informacion de canal");
		case CodigoErrorMidi::EventoNoContieneVelocidad:		return TC("El evento no contiene la velocidad de nota");
		case CodigoErrorMidi::EventoNoContieneTexto:			return TC("El evento no contiene texto");
		case CodigoErrorMidi::EventoNoContieneEstadoMensaje:	return TC("El evento no contiene estado de mensaje");
		case CodigoErrorMidi::EventoNoContieneNumeroBytes:		return TC("El evento no contiene el numero de bytes del mensaje");
		case CodigoErrorMidi::NoEsEventoPresionNota:			return TC("No es un evento Presión de Nota");
		case CodigoErrorMidi::NoEsEventoPresionCanal:			return TC("No es un evento Presión de Canal");
		case CodigoErrorMidi::NoEsEventoPrograma:				return TC("No es un evento Programa");
		case CodigoErrorMidi::NoEsEventoInflexionDeTono:		return TC("No es un evento Inflexion de Tono");
		case CodigoErrorMidi::NoEsEventoControlador:			return TC("No es un evento Controlador");

		case CodigoErrorMidi::NoEsDatoFlexibleTempo:			return TC("No es un Dato Flexible de Tempo");
		case CodigoErrorMidi::NoEsDatoFlexibleCompas:			return TC("No es un Dato Flexible de Compas");
		case CodigoErrorMidi::NoEsDatoFlexibleArmadura:			return TC("No es un Dato Flexible de Armadura");

		case CodigoErrorMidi::NoEsMetaEvento:					return TC("No es un metaevento");
		case CodigoErrorMidi::NoEsMetaEventoSecuenciaNumerica:	return TC("No es un metaevento de secuencia numerica");
		case CodigoErrorMidi::NoEsMetaEventoTempo:				return TC("No es un metaevento de tempo");
		case CodigoErrorMidi::NoEsMetaEventoSMPTE:				return TC("No es un metaevento de SMPTE");
		case CodigoErrorMidi::NoEsMetaEventoCompas:				return TC("No es un metaevento de compás");
		case CodigoErrorMidi::NoEsMetaEventoArmadura:			return TC("No es un metaevento de armadura");

		case CodigoErrorMidi::IntentoAsignarGrupoAEventoV1:		return TC("No se puede asignar grupo a eventos MIDI 1.0");
		case CodigoErrorMidi::IntentoAsignarCanalAMetaEventoV1:	return TC("No se puede asignar canal a MetaEventos MIDI 1.0");

		case CodigoErrorMidi::FalloCrearArchivo:				return TC("No se puede crear el archivo");

		default:												return TC("Error, error no implementado");
	}
}

std::string Excepcion_Midi::descripcion_error() const noexcept
{
	return this->what();
}
