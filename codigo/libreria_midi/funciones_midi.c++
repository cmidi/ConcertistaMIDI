#include "funciones_midi.h++"
#include <cmath>

namespace Funciones_Midi
{
	unsigned int us_a_ppm(unsigned int us)
	{
		//Convierte microsegundos a pulsos por minutom
		return static_cast<unsigned int>(std::round(60000000.0 / static_cast<double>(us)));
	}

	Dinamica velocidad_a_dinamica(unsigned char velocidad)
	{
		if(velocidad <= 15)
			return Dinamica::PPP;//1-15
		else if (velocidad >= 16 && velocidad <= 31)
			return Dinamica::PP;//16-31
		else if (velocidad >= 32 && velocidad <= 47)
			return Dinamica::P;//32-47
		else if (velocidad >= 48 && velocidad <= 63)
			return Dinamica::MP;//48-63
		else if (velocidad >= 64 && velocidad <= 79)
			return Dinamica::MF;//64-79
		else if (velocidad >= 80 && velocidad <= 95)
			return Dinamica::F;//80-95
		else if (velocidad >= 96 && velocidad <= 111)
			return Dinamica::FF;//96-111
		else if (velocidad >= 112)
			return Dinamica::FFF;//112-127

		return Dinamica::SinCambio;
	}

	unsigned char desplazamiento_notas(char armadura, unsigned char escala)
	{
		if(escala == Escala_Mayor)
		{
			switch(armadura)
			{
				case Armadura_7Bemol: return 1;//Do♭
				case Armadura_6Bemol: return 6;//Sol♭
				case Armadura_5Bemol: return 11;//Re♭
				case Armadura_4Bemol: return 4;//La♭
				case Armadura_3Bemol: return 9;//Mi♭
				case Armadura_2Bemol: return 2;//Si♭
				case Armadura_1Bemol: return 7;//Fa
				case Armadura_0: return 0;//Do
				case Armadura_1Sostenido: return 5;//Sol
				case Armadura_2Sostenido: return 10;//Re
				case Armadura_3Sostenido: return 3;//La
				case Armadura_4Sostenido: return 8;//Mi
				case Armadura_5Sostenido: return 1;//Si
				case Armadura_6Sostenido: return 6;//Fa♯
				case Armadura_7Sostenido: return 11;//Do♯
				default: return 0;
			}
		}
		else if(escala == Escala_Menor)
		{
			switch(armadura)
			{
				case Armadura_7Bemol: return 4;//La♭
				case Armadura_6Bemol: return 9;//Mi♭
				case Armadura_5Bemol: return 2;//Si♭
				case Armadura_4Bemol: return 7;//Fa
				case Armadura_3Bemol: return 0;//Do
				case Armadura_2Bemol: return 5;//Sol
				case Armadura_1Bemol: return 10;//Re
				case Armadura_0: return 3;//La
				case Armadura_1Sostenido: return 8;//Mi
				case Armadura_2Sostenido: return 1;//Si
				case Armadura_3Sostenido: return 6;//Fa♯
				case Armadura_4Sostenido: return 11;//Do♯
				case Armadura_5Sostenido: return 4;//Sol♯
				case Armadura_6Sostenido: return 9;//Re♯
				case Armadura_7Sostenido: return 2;//La♯
				default: return 0;
			}
		}
		return 0;
	}

	unsigned short escalar_7a16bits(unsigned char valor)
	{
		//Conversion de acuerdo al documento M2-104-UM pagina 95 y 96 del
		//protocolo y formato MIDI 2.0 v1.1.1
		unsigned int salida = static_cast<unsigned int>(valor) << 9;

		if(valor <= 64)
			return static_cast<unsigned short>(salida);

		unsigned int repeticion = static_cast<unsigned int>(valor) & 0x3F;
		return static_cast<unsigned short>(salida | (repeticion << 3) | (repeticion >> 3));
	}

	unsigned char escalar_16a7bits(unsigned short valor)
	{

		//Conversion de acuerdo al documento M2-104-UM pagina 96 del
		//protocolo y formato MIDI 2.0 v1.1.1
		return static_cast<unsigned char>((valor >> 9) & 0x7F);
	}
}
