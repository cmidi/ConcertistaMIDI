#include "evento_midi.h++"

#include "excepcion_midi.h++"
#include "funciones_midi.h++"
#include "../util/texto.h++"

Evento_Midi::Evento_Midi()
{
	m_version_midi = VersionMidi::Version_1;
	m_datos = nullptr;
	m_largo_evento = 0;

	m_cliente = 0;
	m_puerto = 0;
}

Evento_Midi::Evento_Midi(const Evento_Midi &evento)
{
	m_version_midi = evento.m_version_midi;
	m_largo_evento = evento.m_largo_evento;
	m_datos = new unsigned char[evento.m_largo_evento];

	for(unsigned int x=0; x<evento.m_largo_evento; x++)
		m_datos[x] = evento.m_datos[x];

	m_cliente = evento.m_cliente;
	m_puerto = evento.m_puerto;

	m_texto = evento.m_texto;
}

Evento_Midi::Evento_Midi(unsigned char tipo_evento)
{
	m_version_midi = VersionMidi::Version_1;

	if(	tipo_evento == EventoMidi_ClienteConectado ||
		tipo_evento == EventoMidi_ClienteDesconectado ||
		tipo_evento == EventoMidi_PuertoConectado ||
		tipo_evento == EventoMidi_PuertoDesconectado ||
		tipo_evento == EventoMidi_PuertoSuscrito ||
		tipo_evento == EventoMidi_PuertoDesuscrito)
	{
		m_datos = new unsigned char[1] {tipo_evento};
		m_largo_evento = 1;
	}
	else if(tipo_evento == EventoMidi_Programa ||
			tipo_evento == EventoMidi_PresionCanal)
	{
		m_datos = new unsigned char[2] {tipo_evento, 0};
		m_largo_evento = 2;
	}
	else if	(tipo_evento == EventoMidi_NotaApagada ||
			tipo_evento == EventoMidi_NotaEncendida ||
			tipo_evento == EventoMidi_PresionNota ||
			tipo_evento == EventoMidi_Controlador ||
			tipo_evento == EventoMidi_InflexionDeTono)
	{
		m_datos = new unsigned char[3] {tipo_evento, 0, 0};
		m_largo_evento = 3;

		//Velocidad minima para la Nota Encendida en MIDI 1.0 es de 1
		if(tipo_evento == EventoMidi_NotaEncendida)
			m_datos[2] = 0x01;//Se inicializa el valor en 1
	}
	else
		throw Excepcion_Midi(CodigoErrorMidi::EventoNoDisponible);

	m_cliente = 0;
	m_puerto = 0;
}

Evento_Midi::Evento_Midi(unsigned char tipo_mensaje, unsigned char tipo_evento)
{
	m_version_midi = VersionMidi::Version_2;

	m_cliente = 0;
	m_puerto = 0;

	if(	tipo_mensaje == TipoMensaje_Utilidad ||
		tipo_mensaje == TipoMensaje_VozDeCanalMidi1)
		m_largo_evento = 4;//32 bits
	else if(tipo_mensaje == TipoMensaje_VozDeCanalMidi2)
		m_largo_evento = 8;//64 bits
	else if(tipo_mensaje == TipoMensaje_DatosFlexibles ||
			tipo_mensaje == TipoMensaje_FlujoPaqueteMidiUniversal)
		m_largo_evento = 16;//128 bits

	m_datos = new unsigned char[m_largo_evento];
	for(unsigned char l=0; l<m_largo_evento; l++)
		m_datos[l] = 0x00;

	m_datos[0] = tipo_mensaje;
	m_datos[1] = tipo_evento;
	if(tipo_mensaje == TipoMensaje_Utilidad)
	{
		if(	tipo_evento != MensajeUtilidad_NoOperacion &&
			tipo_evento != MensajeUtilidad_RelojJR &&
			tipo_evento != MensajeUtilidad_MarcaDeTiempoJR &&
			tipo_evento != MensajeUtilidad_DivicionDeTiempo &&
			tipo_evento != MensajeUtilidad_TiempoDelta)
			throw Excepcion_Midi(CodigoErrorMidi::EventoNoValido);
	}
	else if(tipo_mensaje == TipoMensaje_VozDeCanalMidi1)
	{
		if(	tipo_evento != EventoMidi_NotaApagada &&
			tipo_evento != EventoMidi_NotaEncendida &&
			tipo_evento != EventoMidi_PresionNota &&
			tipo_evento != EventoMidi_Controlador &&
			tipo_evento != EventoMidi_Programa &&
			tipo_evento != EventoMidi_PresionCanal &&
			tipo_evento != EventoMidi_InflexionDeTono)
			throw Excepcion_Midi(CodigoErrorMidi::EventoNoValido);

		//Velocidad minima para la Nota Encendida en MIDI 1.0 es de 1
		if(tipo_evento == EventoMidi_NotaEncendida)
			m_datos[3] = 0x01;//Se inicializa el valor en 1
	}
	else if(tipo_mensaje == TipoMensaje_VozDeCanalMidi2)
	{
		//Unico tipo de evento no disponible en VozDeCanalMidi2
		if(tipo_evento == 0x70)
			throw Excepcion_Midi(CodigoErrorMidi::EventoNoValido);
	}
	else
		throw Excepcion_Midi(CodigoErrorMidi::EventoNoDisponible);
}

Evento_Midi::Evento_Midi(unsigned char *datos, unsigned int largo, VersionMidi version)
{
	m_version_midi = version;
	m_datos = datos;
	m_largo_evento = largo;

	if(largo < 1)
		throw Excepcion_Midi(CodigoErrorMidi::EventoLargoIncorrecto);

	if(m_version_midi == VersionMidi::Version_1)
	{
		if(	datos[0] == EventoMidi_ClienteConectado ||
			datos[0] == EventoMidi_ClienteDesconectado ||
			datos[0] == EventoMidi_PuertoConectado ||
			datos[0] == EventoMidi_PuertoDesconectado ||
			datos[0] == EventoMidi_PuertoSuscrito ||
			datos[0] == EventoMidi_PuertoDesuscrito)
		{
			if(largo != 1)
				throw Excepcion_Midi(CodigoErrorMidi::EventoLargoIncorrecto);
		}
		else if((datos[0] & 0xF0) == EventoMidi_Programa ||
				(datos[0] & 0xF0) == EventoMidi_PresionCanal)
		{
			if(largo != 2)
				throw Excepcion_Midi(CodigoErrorMidi::EventoLargoIncorrecto);
		}
		else if((datos[0] & 0xF0) == EventoMidi_NotaApagada ||
			(datos[0] & 0xF0) == EventoMidi_NotaEncendida ||
			(datos[0] & 0xF0) == EventoMidi_PresionNota ||
			(datos[0] & 0xF0) == EventoMidi_Controlador ||
			(datos[0] & 0xF0) == EventoMidi_InflexionDeTono)
		{
			if(largo != 3)
				throw Excepcion_Midi(CodigoErrorMidi::EventoLargoIncorrecto);

			//Velocidad minima para la Nota Encendida en MIDI 1.0 es de 1
			if((datos[0] & 0xF0) == EventoMidi_NotaEncendida && m_datos[2] == 0x00)
				m_datos[2] = 0x01;
		}
		else if(datos[0] == EventoMidi_ExclusivoDelSistema ||
				datos[0] == EventoMidi_SecuenciaDeEscape)
		{
			//El minimo seria F0 00, no se guardan los bites de longitud variable
			if(largo < 1)
				throw Excepcion_Midi(CodigoErrorMidi::EventoLargoIncorrecto);
		}
		else if(datos[0] == EventoMidi_Metaevento)
		{
			//El minimo es FF 2F 00 -> Fin de Pista, no se guardan los bites de longitud variable
			if(largo < 2)
				throw Excepcion_Midi(CodigoErrorMidi::EventoLargoIncorrecto);

			if(this->contiene_texto())
			{
				//Se decodifica y guarda como string
				char texto[m_largo_evento-2];
				for(unsigned int x=0; x<m_largo_evento-2; x++)
					texto[x] = static_cast<char>(m_datos[x+2]);
				m_texto = Texto::convertir_a_utf8(texto, static_cast<int>(m_largo_evento-2), "windows-1252");
			}
		}
		else
			throw Excepcion_Midi(CodigoErrorMidi::EventoNoValido);
	}
	else
	{
		//MIDI 2.0 solo hay 4 tipos de tamaños distintos
		//32 bits, 64 bits, 96 bits y 128 bits
		if(largo != 4 && largo != 8 && largo != 12 && largo != 16)
			throw Excepcion_Midi(CodigoErrorMidi::EventoLargoIncorrecto);

		//Velocidad minima para la Nota Encendida en MIDI 1.0 es de 1
		if(	(m_datos[0] & 0xF0) == TipoMensaje_VozDeCanalMidi1 &&
			(m_datos[1] & 0xF0) == EventoMidi_NotaEncendida &&
			m_datos[3] == 0x00)
			m_datos[3] = 0x01;

		if(this->contiene_texto())
		{
			char texto[m_largo_evento-4];
			for(unsigned int x=4; x<m_largo_evento; x++)
				texto[x-4] = static_cast<char>(m_datos[x]);

			m_texto = std::string(texto);
		}
	}

	m_cliente = 0;
	m_puerto = 0;
}

Evento_Midi::~Evento_Midi()
{
	if(m_datos != nullptr)
		delete[] m_datos;
}

VersionMidi Evento_Midi::version_midi() const
{
	return m_version_midi;
}

unsigned char Evento_Midi::tipo_mensaje() const
{
	if(m_datos == nullptr)
		throw Excepcion_Midi(CodigoErrorMidi::EventoNulo);

	if(m_version_midi == VersionMidi::Version_1)
	{
		//Para los Evento MIDI 1.0, se buscan los equivalentes
		unsigned char tipo_evento = m_datos[0] & 0xF0;
		if(	tipo_evento == EventoMidi_NotaApagada ||
			tipo_evento == EventoMidi_NotaEncendida ||
			tipo_evento == EventoMidi_PresionNota ||
			tipo_evento == EventoMidi_Controlador ||
			tipo_evento == EventoMidi_Programa ||
			tipo_evento == EventoMidi_PresionCanal ||
			tipo_evento == EventoMidi_InflexionDeTono)
			return TipoMensaje_VozDeCanalMidi1;
		else if(m_datos[0] == EventoMidi_Metaevento)
		{
			unsigned char metaevento = m_datos[1];
			if(metaevento == MetaEventoMidi_FinDePista)
				return TipoMensaje_FlujoPaqueteMidiUniversal;
			else
				return TipoMensaje_DatosFlexibles;
		}
		else if(m_datos[0] == EventoMidi_ExclusivoDelSistema)
			return TipoMensaje_ExclusivoDelSistema7bits;
		//Tambien puede ser la continuacion de un
		//mensaje exclusivo del sistema pero de todas forma no se usan
		//los mensajes en tiempo real, por ahora
		else //Solo queda el evento EventoMidi_SecuenciaDeEscape
			return TipoMensaje_ComunYTiempoReal;
	}
	else//MIDI 2.0 Paquete MIDI unificado
		return m_datos[0] & 0xF0;
}

unsigned char Evento_Midi::tipo_utilidad() const
{
	if(m_datos == nullptr)
		throw Excepcion_Midi(CodigoErrorMidi::EventoNulo);

	if(m_version_midi == VersionMidi::Version_1)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsMensajeDeUtilidad);
	else
	{
		if((m_datos[0] & 0xF0) == TipoMensaje_Utilidad)
			return m_datos[1] & 0xF0;
	}
	throw Excepcion_Midi(CodigoErrorMidi::NoEsMensajeDeUtilidad);
}

unsigned char Evento_Midi::tipo_tiempo_real() const
{
	if(m_datos == nullptr)
		throw Excepcion_Midi(CodigoErrorMidi::EventoNulo);

	if(m_version_midi == VersionMidi::Version_1)
	{
		if(m_datos[0] == EventoMidi_SecuenciaDeEscape)
			return m_datos[1];
	}
	else
	{
		if((m_datos[0] & 0xF0) == TipoMensaje_ComunYTiempoReal)
			return m_datos[1] & 0xF0;
	}
	throw Excepcion_Midi(CodigoErrorMidi::NoEsMensajeComunYTiempoReal);
}

unsigned char Evento_Midi::tipo_voz_de_canal() const
{
	if(m_datos == nullptr)
		throw Excepcion_Midi(CodigoErrorMidi::EventoNulo);

	if(m_version_midi == VersionMidi::Version_1)
	{
		unsigned char tipo_evento = m_datos[0] & 0xF0;
		if(	tipo_evento == EventoMidi_NotaApagada ||
			tipo_evento == EventoMidi_NotaEncendida ||
			tipo_evento == EventoMidi_PresionNota ||
			tipo_evento == EventoMidi_Controlador ||
			tipo_evento == EventoMidi_Programa ||
			tipo_evento == EventoMidi_PresionCanal ||
			tipo_evento == EventoMidi_InflexionDeTono)
			return m_datos[0] & 0xF0;
	}
	else
	{
		unsigned char tipo_mensaje = m_datos[0] & 0xF0;
		if(	tipo_mensaje == TipoMensaje_VozDeCanalMidi1 ||
			tipo_mensaje == TipoMensaje_VozDeCanalMidi2)
			return m_datos[1] & 0xF0;
	}
	throw Excepcion_Midi(CodigoErrorMidi::NoEsMensajeDeVozDeCanal);
}

unsigned short Evento_Midi::tipo_dato_flexible() const
{
	if(m_datos == nullptr)
		throw Excepcion_Midi(CodigoErrorMidi::EventoNulo);

	if(m_version_midi == VersionMidi::Version_1)
	{
		if(m_datos[0] == EventoMidi_Metaevento)
		{
			unsigned char metaevento = m_datos[1];
			if(metaevento == MetaEventoMidi_FinDePista)
				throw Excepcion_Midi(CodigoErrorMidi::NoEsMensajeDeDatosFlexibles);
			else if(metaevento == MetaEventoMidi_DerechosDeAutor)
				return DatosFlexibles_DerechosDeAutor;
			else if(metaevento == MetaEventoMidi_NombreDePista)
				return DatosFlexibles_NombreFragmentoMidi;
			else if(metaevento == MetaEventoMidi_Letra)
				return DatosFlexibles_Letra;
			else if(metaevento == MetaEventoMidi_Tempo)
				return DatosFlexibles_Tempo;
			else if(metaevento == MetaEventoMidi_Compas)
				return DatosFlexibles_Compas;
			else if(metaevento == MetaEventoMidi_Armadura)
				return DatosFlexibles_Armadura;
			else
				return DatosFlexibles_TextoDatosDesconocido;
		}
	}
	else
	{
		if((m_datos[0] & 0xF0) == TipoMensaje_DatosFlexibles)
		{
			unsigned short dato = m_datos[2];
			return static_cast<unsigned short>((dato << 8) | m_datos[3]);//16 bits
		}
	}

	throw Excepcion_Midi(CodigoErrorMidi::NoEsMensajeDeDatosFlexibles);
}

unsigned short Evento_Midi::tipo_flujo() const
{
	if(m_datos == nullptr)
		throw Excepcion_Midi(CodigoErrorMidi::EventoNulo);

	if(m_version_midi == VersionMidi::Version_1)
	{

		if(	m_datos[0] == EventoMidi_Metaevento &&
			m_datos[1] == MetaEventoMidi_FinDePista)
			return MensajeDeFlujo_FinDeFragmento;
	}
	else
	{
		if((m_datos[0] & 0xF0) == TipoMensaje_FlujoPaqueteMidiUniversal)
		{
			unsigned short dato = m_datos[0];
			return ((dato << 8) | m_datos[1]) & 0x3FF;//10 bits
		}
	}

	throw Excepcion_Midi(CodigoErrorMidi::NoEsMensajeDeFlujoPMU);
}

unsigned char Evento_Midi::tipo_evento_midi1() const
{
	//Solo para algunos metaeventos que no tienen traducción
	//que quedarian inaccesible de otro modo
	if(m_datos == nullptr)
		throw Excepcion_Midi(CodigoErrorMidi::EventoNulo);

	if(m_version_midi == VersionMidi::Version_1)
	{
		unsigned char tipo_evento = m_datos[0] & 0xF0;
		if(	tipo_evento == EventoMidi_NotaApagada ||
			tipo_evento == EventoMidi_NotaEncendida ||
			tipo_evento == EventoMidi_PresionNota ||
			tipo_evento == EventoMidi_Controlador ||
			tipo_evento == EventoMidi_Programa ||
			tipo_evento == EventoMidi_PresionCanal ||
			tipo_evento == EventoMidi_InflexionDeTono)
			//Le quita el canal a los eventos que lo tienen
			return m_datos[0] & 0xF0;
		else
			return m_datos[0];
	}

	throw Excepcion_Midi(CodigoErrorMidi::NoEsEventoMidi1);
}

unsigned char Evento_Midi::tipo_metaevento_midi1() const
{
	if(m_datos == nullptr)
		throw Excepcion_Midi(CodigoErrorMidi::EventoNulo);

	if(m_version_midi != VersionMidi::Version_1)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsMetaEvento);

	if(m_datos[0] != EventoMidi_Metaevento)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsMetaEvento);

	return m_datos[1];
}

void Evento_Midi::division_de_tiempo(unsigned short valor)
{
	if(m_datos == nullptr)
		throw Excepcion_Midi(CodigoErrorMidi::EventoNulo);

	if(m_version_midi == VersionMidi::Version_1)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsMensajeDeUtilidad);

	if((m_datos[0] & 0xF0) != TipoMensaje_Utilidad)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsMensajeDeUtilidad);

	if((m_datos[1] & 0xF0) != MensajeUtilidad_DivicionDeTiempo)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsDivicionDeTiempo);

	unsigned int dato = valor;
	m_datos[2] = (dato >> 8) & 0xFF;
	m_datos[3] = dato & 0xFF;
}

unsigned short Evento_Midi::division_de_tiempo() const
{
	if(m_datos == nullptr)
		throw Excepcion_Midi(CodigoErrorMidi::EventoNulo);

	if(m_version_midi == VersionMidi::Version_1)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsMensajeDeUtilidad);

	if((m_datos[0] & 0xF0) != TipoMensaje_Utilidad)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsMensajeDeUtilidad);

	if((m_datos[1] & 0xF0) != MensajeUtilidad_DivicionDeTiempo)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsDivicionDeTiempo);

	unsigned short b2 = m_datos[2];
	return static_cast<unsigned short>((b2 << 8) | m_datos[3]);
}

void Evento_Midi::tiempo_delta(unsigned int valor)
{
	if(m_datos == nullptr)
		throw Excepcion_Midi(CodigoErrorMidi::EventoNulo);

	if(m_version_midi == VersionMidi::Version_1)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsMensajeDeUtilidad);

	if((m_datos[0] & 0xF0) != TipoMensaje_Utilidad)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsMensajeDeUtilidad);

	if((m_datos[1] & 0xF0) != MensajeUtilidad_TiempoDelta)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsTiempoDelta);

	m_datos[1] = (m_datos[1] & 0xF0) | ((valor >> 16) & 0x0F);
	m_datos[2] = (valor >> 8) & 0xFF;
	m_datos[3] = valor & 0xFF;
}

unsigned int Evento_Midi::tiempo_delta() const
{
	if(m_datos == nullptr)
		throw Excepcion_Midi(CodigoErrorMidi::EventoNulo);

	if(m_version_midi == VersionMidi::Version_1)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsMensajeDeUtilidad);

	if((m_datos[0] & 0xF0) != TipoMensaje_Utilidad)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsMensajeDeUtilidad);

	if((m_datos[1] & 0xF0) != MensajeUtilidad_TiempoDelta)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsTiempoDelta);

	unsigned int b1 = m_datos[1] & 0x0F;
	unsigned int b2 = m_datos[2];
	return (b1 << 16) | (b2 << 8) | m_datos[3];
}

void Evento_Midi::grupo(unsigned char grupo)
{
	if(!this->contiene_grupo())
		throw Excepcion_Midi(CodigoErrorMidi::EventoNoContieneGrupo);

	//No se puede guardar el grupo en un evento MIDI version 1
	if(m_version_midi == VersionMidi::Version_1)
		throw Excepcion_Midi(CodigoErrorMidi::IntentoAsignarGrupoAEventoV1);

	if(m_version_midi == VersionMidi::Version_2)
		m_datos[0] = (m_datos[0] & 0xF0) | (grupo & 0x0F);
}

unsigned char Evento_Midi::grupo() const
{
	if(!this->contiene_grupo())
		throw Excepcion_Midi(CodigoErrorMidi::EventoNoContieneGrupo);

	if(m_version_midi == VersionMidi::Version_1)
		return 0x00;
	else
		return m_datos[0] & 0x0F;
}

void Evento_Midi::canal(unsigned char canal)
{
	if(!this->contiene_canal())
		throw Excepcion_Midi(CodigoErrorMidi::EventoNoContieneCanal);

	if(m_version_midi == VersionMidi::Version_1)
	{
		if(	m_datos[0] == EventoMidi_Metaevento)
			throw Excepcion_Midi(CodigoErrorMidi::IntentoAsignarCanalAMetaEventoV1);
		else
			m_datos[0] = (m_datos[0] & 0xF0) | (canal & 0x0F);
	}
	else
		m_datos[1] = (m_datos[1] & 0xF0) | (canal & 0x0F);
}

unsigned char Evento_Midi::canal() const
{
	if(!this->contiene_canal())
		throw Excepcion_Midi(CodigoErrorMidi::EventoNoContieneCanal);

	if(m_version_midi == VersionMidi::Version_1)
	{
		if(m_datos[0] == EventoMidi_Metaevento)
			return 0x00;
		else
			return m_datos[0] & 0x0F;
	}
	else
		return m_datos[1] & 0x0F;
}

void Evento_Midi::id_nota(unsigned char id_nota)
{
	if(!this->contiene_nota())
		throw Excepcion_Midi(CodigoErrorMidi::EventoNoContieneNota);

	if(m_version_midi == VersionMidi::Version_1)
		m_datos[1] = id_nota & 0x7F;
	else//Se mantiene el bits 8 porque esta reservado
		m_datos[2] = (m_datos[2] & 0x80) | (id_nota & 0x7F);
}

unsigned char Evento_Midi::id_nota() const
{
	if(!this->contiene_nota())
		throw Excepcion_Midi(CodigoErrorMidi::EventoNoContieneNota);

	if(m_version_midi == VersionMidi::Version_1)
		return m_datos[1];
	else//Se ignora el bits 8 porque esta reservado
		return m_datos[2] & 0x7F;
}

void Evento_Midi::velocidad_nota_7(unsigned char velocidad)
{
	if(m_datos == nullptr)
		throw Excepcion_Midi(CodigoErrorMidi::EventoNulo);

	if(!this->es_nota())
		throw Excepcion_Midi(CodigoErrorMidi::EventoNoContieneVelocidad);

	if(velocidad > 127)
		velocidad = 127;

	if(m_version_midi == VersionMidi::Version_1)
	{
		//Velocidad minima para MIDI 1.0
		if((m_datos[0] & 0xF0) == EventoMidi_NotaEncendida && velocidad == 0)
			velocidad = 1;

		m_datos[2] = velocidad;
	}
	else
	{
		if((m_datos[0] & 0xF0) == TipoMensaje_VozDeCanalMidi1)
		{
			if((m_datos[1] & 0xF0) == EventoMidi_NotaEncendida && velocidad == 0)
				velocidad = 1;

			m_datos[3] = (m_datos[3] & 0x80) | velocidad;
		}
		else
		{
			unsigned int velocidad16 = Funciones_Midi::escalar_7a16bits(velocidad);
			m_datos[4] = (velocidad16 >> 8) & 0xFF;
			m_datos[5] = velocidad16 & 0xFF;
		}
	}
}

unsigned char Evento_Midi::velocidad_nota_7() const
{
	if(m_datos == nullptr)
		throw Excepcion_Midi(CodigoErrorMidi::EventoNulo);

	if(!this->es_nota())
		throw Excepcion_Midi(CodigoErrorMidi::EventoNoContieneVelocidad);

	if(m_version_midi == VersionMidi::Version_1)
		return m_datos[2] & 0x7F;
	else
	{
		if((m_datos[0] & 0xF0) == TipoMensaje_VozDeCanalMidi1)
			return m_datos[3] & 0x7F;
		else
		{
			unsigned short velocidad = static_cast<unsigned short>(m_datos[4]);
			velocidad = velocidad << 8 | m_datos[5];

			unsigned char velocidad_salida = Funciones_Midi::escalar_16a7bits(velocidad);
			if((m_datos[1] & 0xF0) == EventoMidi_NotaEncendida && velocidad_salida == 0)
				velocidad_salida = 1;

			return velocidad_salida;
		}
	}
}

void Evento_Midi::velocidad_nota_16(unsigned short velocidad)
{
	if(m_datos == nullptr)
		throw Excepcion_Midi(CodigoErrorMidi::EventoNulo);

	if(!this->es_nota())
		throw Excepcion_Midi(CodigoErrorMidi::EventoNoContieneVelocidad);

	if(m_version_midi == VersionMidi::Version_1)
	{
		unsigned char velocidad7 = Funciones_Midi::escalar_16a7bits(velocidad);
		//Velocidad minima para MIDI 1.0
		if((m_datos[0] & 0xF0) == EventoMidi_NotaEncendida && velocidad7 == 0)
			velocidad7 = 1;

		m_datos[2] = velocidad7;
	}
	else
	{
		if((m_datos[0] & 0xF0) == TipoMensaje_VozDeCanalMidi1)
		{
			unsigned char velocidad7 = Funciones_Midi::escalar_16a7bits(velocidad);
			if((m_datos[1] & 0xF0) == EventoMidi_NotaEncendida && velocidad7 == 0)
				velocidad7 = 1;

			m_datos[3] = (m_datos[3] & 0x80) | velocidad7;
		}
		else
		{
			m_datos[4] = static_cast<unsigned char>((velocidad >> 8) & 0xFF);
			m_datos[5] = static_cast<unsigned char>(velocidad & 0xFF);
		}
	}
}

unsigned short Evento_Midi::velocidad_nota_16() const
{
	if(m_datos == nullptr)
		throw Excepcion_Midi(CodigoErrorMidi::EventoNulo);

	if(!this->es_nota())
		throw Excepcion_Midi(CodigoErrorMidi::EventoNoContieneVelocidad);

	if(m_version_midi == VersionMidi::Version_1)
		return Funciones_Midi::escalar_7a16bits(m_datos[2] & 0x7F);
	else
	{
		if((m_datos[0] & 0xF0) == TipoMensaje_VozDeCanalMidi1)
			return Funciones_Midi::escalar_7a16bits(m_datos[3] & 0x7F);
		else
		{
			unsigned short velocidad = static_cast<unsigned short>(m_datos[4]);
			velocidad = velocidad << 8 | m_datos[5];

			return velocidad;
		}
	}
}

//NOTE Hasta aqui voy con la conversion MIDI 2.0
void Evento_Midi::presion_nota(unsigned char presion)
{
	if(m_datos == nullptr)
		throw Excepcion_Midi(CodigoErrorMidi::EventoNulo);

	if(m_largo_evento < 3)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsEventoPresionNota);

	if((m_datos[0] & 0xF0) != EventoMidi_PresionNota)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsEventoPresionNota);

	if(presion > 127)
		m_datos[2] = 127;
	else
		m_datos[2] = presion;
}

unsigned char Evento_Midi::presion_nota() const
{
	if(m_datos == nullptr)
		throw Excepcion_Midi(CodigoErrorMidi::EventoNulo);

	if(m_largo_evento < 3)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsEventoPresionNota);

	if((m_datos[0] & 0xF0) != EventoMidi_PresionNota)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsEventoPresionNota);

	return m_datos[2];
}

void Evento_Midi::presion_canal(unsigned char presion)
{
	if(m_datos == nullptr)
		throw Excepcion_Midi(CodigoErrorMidi::EventoNulo);

	if(m_largo_evento < 2)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsEventoPresionCanal);

	if((m_datos[0] & 0xF0) != EventoMidi_PresionCanal)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsEventoPresionCanal);

	if(presion > 127)
		m_datos[1] = 127;
	else
		m_datos[1] = presion;
}

unsigned char Evento_Midi::presion_canal() const
{
	if(m_datos == nullptr)
		throw Excepcion_Midi(CodigoErrorMidi::EventoNulo);

	if(m_largo_evento < 2)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsEventoPresionCanal);

	if((m_datos[0] & 0xF0) != EventoMidi_PresionCanal)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsEventoPresionCanal);

	return m_datos[1];
}

void Evento_Midi::programa_banco_valido(unsigned char banco_valido)
{
	if(m_datos == nullptr)
		throw Excepcion_Midi(CodigoErrorMidi::EventoNulo);

	if(m_version_midi != VersionMidi::Version_2)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsEventoMidi2);

	if((m_datos[0] & 0xF0) != TipoMensaje_VozDeCanalMidi2)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsEventoDeVozDeCanalMidi2);

	if((m_datos[1] & 0xF0) != EventoMidi_Programa)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsEventoPrograma);

	m_datos[3] = (m_datos[3] & 0xFE) | (banco_valido & 0x01);
}

unsigned char Evento_Midi::programa_banco_valido() const
{
	if(m_datos == nullptr)
		throw Excepcion_Midi(CodigoErrorMidi::EventoNulo);

	if(m_version_midi != VersionMidi::Version_2)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsEventoMidi2);

	if((m_datos[0] & 0xF0) != TipoMensaje_VozDeCanalMidi2)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsEventoDeVozDeCanalMidi2);

	if((m_datos[1] & 0xF0) != EventoMidi_Programa)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsEventoPrograma);

	return m_datos[3] & 0x01;
}

void Evento_Midi::programa_banco_izquierdo(unsigned char valor)
{
	if(m_datos == nullptr)
		throw Excepcion_Midi(CodigoErrorMidi::EventoNulo);

	if(m_version_midi != VersionMidi::Version_2)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsEventoMidi2);

	if((m_datos[0] & 0xF0) != TipoMensaje_VozDeCanalMidi2)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsEventoDeVozDeCanalMidi2);

	if((m_datos[1] & 0xF0) != EventoMidi_Programa)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsEventoPrograma);

	if((m_datos[3] & 0x01) == 0)
		m_datos[3] = (m_datos[3] & 0xFE) | 0x01;//Activa el banco

	m_datos[6] = (m_datos[6] & 0x80) | (valor & 0x7F);
}

unsigned char Evento_Midi::programa_banco_izquierdo() const
{
	if(m_datos == nullptr)
		throw Excepcion_Midi(CodigoErrorMidi::EventoNulo);

	if(m_version_midi != VersionMidi::Version_2)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsEventoMidi2);

	if((m_datos[0] & 0xF0) != TipoMensaje_VozDeCanalMidi2)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsEventoDeVozDeCanalMidi2);

	if((m_datos[1] & 0xF0) != EventoMidi_Programa)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsEventoPrograma);

	if((m_datos[3] & 0x01) == 0)
		throw Excepcion_Midi(CodigoErrorMidi::EventoNoContieneBanco);

	return m_datos[6] & 0x7F;
}

void Evento_Midi::programa_banco_derecho(unsigned char valor)
{
	if(m_datos == nullptr)
		throw Excepcion_Midi(CodigoErrorMidi::EventoNulo);

	if(m_version_midi != VersionMidi::Version_2)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsEventoMidi2);

	if((m_datos[0] & 0xF0) != TipoMensaje_VozDeCanalMidi2)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsEventoDeVozDeCanalMidi2);

	if((m_datos[1] & 0xF0) != EventoMidi_Programa)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsEventoPrograma);

	if((m_datos[3] & 0x01) == 0)
		m_datos[3] = (m_datos[3] & 0xFE) | 0x01;//Activa el banco

	m_datos[7] = (m_datos[7] & 0x80) | (valor & 0x7F);
}

unsigned char Evento_Midi::programa_banco_derecho() const
{
	if(m_datos == nullptr)
		throw Excepcion_Midi(CodigoErrorMidi::EventoNulo);

	if(m_version_midi != VersionMidi::Version_2)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsEventoMidi2);

	if((m_datos[0] & 0xF0) != TipoMensaje_VozDeCanalMidi2)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsEventoDeVozDeCanalMidi2);

	if((m_datos[1] & 0xF0) != EventoMidi_Programa)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsEventoPrograma);

	if((m_datos[3] & 0x01) == 0)
		throw Excepcion_Midi(CodigoErrorMidi::EventoNoContieneBanco);

	return m_datos[7] & 0x7F;
}

void Evento_Midi::programa(unsigned char programa)
{
	if(m_datos == nullptr)
		throw Excepcion_Midi(CodigoErrorMidi::EventoNulo);

	if(m_version_midi == VersionMidi::Version_1)
	{
		if((m_datos[0] & 0xF0) != EventoMidi_Programa)
			throw Excepcion_Midi(CodigoErrorMidi::NoEsEventoPrograma);

		m_datos[1] = programa & 0x7F;
	}
	else
	{
		unsigned char tipo_mensaje = (m_datos[0] & 0xF0);
		if(tipo_mensaje == TipoMensaje_VozDeCanalMidi1)
		{
			if((m_datos[1] & 0xF0) != EventoMidi_Programa)
				throw Excepcion_Midi(CodigoErrorMidi::NoEsEventoPrograma);

			m_datos[2] = programa & 0x7F;
		}
		else if(tipo_mensaje == TipoMensaje_VozDeCanalMidi2)
		{
			if((m_datos[1] & 0xF0) != EventoMidi_Programa)
				throw Excepcion_Midi(CodigoErrorMidi::NoEsEventoPrograma);

			m_datos[4] = (m_datos[4] & 0x80) | (programa & 0x7F);
		}
		else
			throw Excepcion_Midi(CodigoErrorMidi::NoEsEventoPrograma);
	}
}

unsigned char Evento_Midi::programa() const
{
	if(m_datos == nullptr)
		throw Excepcion_Midi(CodigoErrorMidi::EventoNulo);

	if(m_version_midi == VersionMidi::Version_1)
	{
		if((m_datos[0] & 0xF0) != EventoMidi_Programa)
			throw Excepcion_Midi(CodigoErrorMidi::NoEsEventoPrograma);

		return m_datos[1];
	}
	else
	{
		unsigned char tipo_mensaje = m_datos[0] & 0xF0;
		if(tipo_mensaje == TipoMensaje_VozDeCanalMidi1)
		{
			if((m_datos[1] & 0xF0) != EventoMidi_Programa)
				throw Excepcion_Midi(CodigoErrorMidi::NoEsEventoPrograma);

			return m_datos[2] & 0x7F;
		}
		else if(tipo_mensaje == TipoMensaje_VozDeCanalMidi2)
		{
			if((m_datos[1] & 0xF0) != EventoMidi_Programa)
				throw Excepcion_Midi(CodigoErrorMidi::NoEsEventoPrograma);

			return m_datos[4] & 0x7F;
		}
		throw Excepcion_Midi(CodigoErrorMidi::NoEsEventoPrograma);
	}
}

void Evento_Midi::inflexion_de_tono(unsigned int valor)
{
	if(m_datos == nullptr)
		throw Excepcion_Midi(CodigoErrorMidi::EventoNulo);

	if(m_largo_evento < 3)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsEventoInflexionDeTono);

	if((m_datos[0] & 0xF0) != EventoMidi_InflexionDeTono)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsEventoInflexionDeTono);

	m_datos[1] = valor & 0x7F;
	m_datos[2] = (valor >> 7) & 0x7F;
}

unsigned int Evento_Midi::inflexion_de_tono() const
{
	if(m_datos == nullptr)
		throw Excepcion_Midi(CodigoErrorMidi::EventoNulo);

	if(m_largo_evento < 3)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsEventoInflexionDeTono);

	if((m_datos[0] & 0xF0) != EventoMidi_InflexionDeTono)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsEventoInflexionDeTono);

	unsigned int b0 = m_datos[1];//Los 7 bits menos significativos
	unsigned int b1 = m_datos[2];//Los 7 bits mas significativos
	return static_cast<unsigned int>((b1 << 7) | b0);
}

void Evento_Midi::controlador_mensaje(unsigned char mensaje)
{
	if(m_datos == nullptr)
		throw Excepcion_Midi(CodigoErrorMidi::EventoNulo);


	if(m_version_midi == VersionMidi::Version_1)
	{
		if((m_datos[0] & 0xF0) != EventoMidi_Controlador)
			throw Excepcion_Midi(CodigoErrorMidi::NoEsEventoControlador);

		m_datos[1] = mensaje & 0x7F;
	}
	else
	{
		unsigned char tipo_mensaje = m_datos[0] & 0xF0;
		if(	tipo_mensaje == TipoMensaje_VozDeCanalMidi1 ||
			tipo_mensaje == TipoMensaje_VozDeCanalMidi2)
		{
			if((m_datos[1] & 0xF0) != EventoMidi_Controlador)
				throw Excepcion_Midi(CodigoErrorMidi::NoEsEventoControlador);

			m_datos[2] = (m_datos[2] & 0x80) | (mensaje & 0x7F);
		}
		throw Excepcion_Midi(CodigoErrorMidi::NoEsEventoControlador);
	}
}

unsigned char Evento_Midi::controlador_mensaje() const
{
	if(m_datos == nullptr)
		throw Excepcion_Midi(CodigoErrorMidi::EventoNulo);

	if(m_version_midi == VersionMidi::Version_1)
	{
		if((m_datos[0] & 0xF0) != EventoMidi_Controlador)
			throw Excepcion_Midi(CodigoErrorMidi::NoEsEventoControlador);

		return m_datos[1];
	}
	else
	{
		unsigned char tipo_mensaje = m_datos[0] & 0xF0;
		if(	tipo_mensaje == TipoMensaje_VozDeCanalMidi1 ||
			tipo_mensaje == TipoMensaje_VozDeCanalMidi2)
		{
			if((m_datos[1] & 0xF0) != EventoMidi_Controlador)
				throw Excepcion_Midi(CodigoErrorMidi::NoEsEventoControlador);

			return m_datos[2] & 0x7F;
		}
		throw Excepcion_Midi(CodigoErrorMidi::NoEsEventoControlador);
	}
}

void Evento_Midi::controlador_valor(unsigned char valor)
{
	if(m_datos == nullptr)
		throw Excepcion_Midi(CodigoErrorMidi::EventoNulo);

	if(m_version_midi == VersionMidi::Version_1)
	{
		if((m_datos[0] & 0xF0) != EventoMidi_Controlador)
			throw Excepcion_Midi(CodigoErrorMidi::NoEsEventoControlador);

		m_datos[2] = valor & 0x7F;
	}
	else
	{
		unsigned char tipo_mensaje = m_datos[0] & 0xF0;
		if(tipo_mensaje == TipoMensaje_VozDeCanalMidi1)
		{
			if((m_datos[1] & 0xF0) != EventoMidi_Controlador)
				throw Excepcion_Midi(CodigoErrorMidi::NoEsEventoControlador);

			m_datos[3] = (m_datos[3] & 0x80) | (valor & 0x7F);
		}
		else if(tipo_mensaje == TipoMensaje_VozDeCanalMidi2)
		{
			if((m_datos[1] & 0xF0) != EventoMidi_Controlador)
				throw Excepcion_Midi(CodigoErrorMidi::NoEsEventoControlador);
			//TODO buscar forma correcta de escalar el valor de 7 bits a 32 bits
			//en controlador
			m_datos[4] = static_cast<unsigned char>((valor & 0x7F) << 1);
		}
		throw Excepcion_Midi(CodigoErrorMidi::NoEsEventoControlador);
	}
}

unsigned char Evento_Midi::controlador_valor() const
{
	if(m_datos == nullptr)
		throw Excepcion_Midi(CodigoErrorMidi::EventoNulo);

	if(m_version_midi == VersionMidi::Version_1)
	{
		if((m_datos[0] & 0xF0) != EventoMidi_Controlador)
			throw Excepcion_Midi(CodigoErrorMidi::NoEsEventoControlador);

		return m_datos[2];
	}
	else
	{
		unsigned char tipo_mensaje = m_datos[0] & 0xF0;
		if(tipo_mensaje == TipoMensaje_VozDeCanalMidi1)
		{
			if((m_datos[1] & 0xF0) != EventoMidi_Controlador)
				throw Excepcion_Midi(CodigoErrorMidi::NoEsEventoControlador);

			return m_datos[3] & 0x7F;
		}
		else if(tipo_mensaje == TipoMensaje_VozDeCanalMidi2)
		{
			if((m_datos[1] & 0xF0) != EventoMidi_Controlador)
				throw Excepcion_Midi(CodigoErrorMidi::NoEsEventoControlador);

			return m_datos[4] >> 1;
		}
		throw Excepcion_Midi(CodigoErrorMidi::NoEsEventoControlador);
	}
}

unsigned char Evento_Midi::estado_mensaje() const
{
	if(m_datos == nullptr)
		throw Excepcion_Midi(CodigoErrorMidi::EventoNulo);

	if(m_version_midi == VersionMidi::Version_1)
	{
		if(	m_datos[0] == EventoMidi_Metaevento &&
			m_datos[1] == MetaEventoMidi_FinDePista)
			return 0x00;
		throw Excepcion_Midi(CodigoErrorMidi::EventoNoContieneEstadoMensaje);
	}
	else
	{
		if(	(m_datos[0] & 0xF0) != TipoMensaje_ExclusivoDelSistema7bits &&
			(m_datos[0] & 0xF0) != TipoMensaje_ExclusivoDelSistema8bits &&
			(m_datos[0] & 0xF0) != TipoMensaje_DatosFlexibles &&
			(m_datos[0] & 0xF0) != TipoMensaje_FlujoPaqueteMidiUniversal)
			throw Excepcion_Midi(CodigoErrorMidi::EventoNoContieneEstadoMensaje);

		if((m_datos[0] & 0xF0) == TipoMensaje_DatosFlexibles)
			return (m_datos[1] & 0xC0) >> 2;
		else if((m_datos[0] & 0xF0) == TipoMensaje_FlujoPaqueteMidiUniversal)
			return (m_datos[0] << 4) & 0xF0;
		else
			return m_datos[1] & 0xF0;
	}
}

unsigned char Evento_Midi::numero_bytes_mensaje() const
{
	if(m_datos == nullptr)
		throw Excepcion_Midi(CodigoErrorMidi::EventoNulo);

	if(m_version_midi != VersionMidi::Version_2)
		throw Excepcion_Midi(CodigoErrorMidi::EventoNoContieneNumeroBytes);

	if(	(m_datos[0] & 0xF0) != TipoMensaje_ExclusivoDelSistema7bits &&
		(m_datos[0] & 0xF0) != TipoMensaje_ExclusivoDelSistema8bits)
		throw Excepcion_Midi(CodigoErrorMidi::EventoNoContieneNumeroBytes);

	return m_datos[1] & 0x0F;
}

unsigned char Evento_Midi::identificador_sistema_exclusivo_8() const
{
	if(m_datos == nullptr)
		throw Excepcion_Midi(CodigoErrorMidi::EventoNulo);

	if(m_version_midi != VersionMidi::Version_2)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsMensajeSistemaExclusivo8);

	if((m_datos[0] & 0xF0) != TipoMensaje_ExclusivoDelSistema8bits)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsMensajeSistemaExclusivo8);

	return m_datos[2];
}

unsigned char Evento_Midi::dato_flexible_direccion() const
{
	if(m_datos == nullptr)
		throw Excepcion_Midi(CodigoErrorMidi::EventoNulo);

	if(this->tipo_mensaje() != TipoMensaje_DatosFlexibles)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsMensajeDeDatosFlexibles);

	if(m_version_midi == VersionMidi::Version_1)
		return 0x01;//Mensaje al grupo, predeterminado
	else
		return (m_datos[1] >> 4) & 0x03;
}

unsigned char Evento_Midi::dato_flexible_banco_de_estado() const
{
	if(m_datos == nullptr)
		throw Excepcion_Midi(CodigoErrorMidi::EventoNulo);

	if(this->tipo_mensaje() != TipoMensaje_DatosFlexibles)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsMensajeDeDatosFlexibles);

	if(m_version_midi == VersionMidi::Version_1)
	{
		unsigned char metaevento = m_datos[1];
		if(	metaevento == MetaEventoMidi_Tempo ||
			metaevento == MetaEventoMidi_SMPTE ||
			metaevento == MetaEventoMidi_Compas ||
			metaevento == MetaEventoMidi_Armadura)
			return 0x00;
		else if(metaevento == MetaEventoMidi_Letra)
			return 0x02;
		else
			return 0x01;
	}
	else
		return m_datos[2];
}

std::string Evento_Midi::texto() const
{
	if(!this->contiene_texto())
		throw Excepcion_Midi(CodigoErrorMidi::EventoNoContieneTexto);

	return m_texto;
}

unsigned char *Evento_Midi::datos() const
{
	return m_datos;
}

unsigned int Evento_Midi::largo_datos() const
{
	return m_largo_evento;
}

unsigned char Evento_Midi::cliente() const
{
	return m_cliente;
}

void Evento_Midi::cliente(unsigned char cliente)
{
	m_cliente = cliente;
}

unsigned char Evento_Midi::puerto() const
{
	return m_puerto;
}

void Evento_Midi::puerto(unsigned char puerto)
{
	m_puerto = puerto;
}

unsigned short Evento_Midi::secuencia_numerica() const
{
	if(m_datos == nullptr)
		throw Excepcion_Midi(CodigoErrorMidi::EventoNulo);

	if(m_version_midi == VersionMidi::Version_1)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsEventoMidi1);

	if(m_largo_evento < 4)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsMetaEventoSecuenciaNumerica);

	if(m_datos[0] != EventoMidi_Metaevento)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsMetaEvento);

	if(m_datos[1] != MetaEventoMidi_SecuenciaNumerica)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsMetaEventoSecuenciaNumerica);

	unsigned short b0 = m_datos[2];
	unsigned short b1 = m_datos[3];

	return static_cast<unsigned short>(b0 << 8 | b1);
}

unsigned int Evento_Midi::tempo() const
{
	if(m_datos == nullptr)
		throw Excepcion_Midi(CodigoErrorMidi::EventoNulo);

	if(m_version_midi == VersionMidi::Version_1)
	{
		if(m_largo_evento < 5)
			throw Excepcion_Midi(CodigoErrorMidi::NoEsMetaEventoTempo);

		if(m_datos[0] != EventoMidi_Metaevento)
			throw Excepcion_Midi(CodigoErrorMidi::NoEsMetaEvento);

		if(m_datos[1] != MetaEventoMidi_Tempo)
			throw Excepcion_Midi(CodigoErrorMidi::NoEsMetaEventoTempo);

		unsigned int b0 = m_datos[2];
		unsigned int b1 = m_datos[3];

		return (b0 << 16) | (b1 << 8) | m_datos[4];
	}
	else
	{
		if((m_datos[0] & 0xF0) != TipoMensaje_DatosFlexibles)
			throw Excepcion_Midi(CodigoErrorMidi::NoEsMensajeDeDatosFlexibles);

		if(this->tipo_dato_flexible() != DatosFlexibles_Tempo)
			throw Excepcion_Midi(CodigoErrorMidi::NoEsDatoFlexibleTempo);

		unsigned int b0 = m_datos[4];
		unsigned int b1 = m_datos[5];
		unsigned int b2 = m_datos[6];

		return (b0 << 24) | (b1 << 16) | (b2 << 8) | m_datos[7];
	}
}

double Evento_Midi::tempo_en_microsegundos() const
{
	//La menor resolucion en MIDI 1.0 es de 1 microsegundo
	//La menor resolucion en MIDI 2.0 es de 10 nanosegundos
	double tempo = static_cast<double>(this->tempo());
	if(this->version_midi() == VersionMidi::Version_1)
		return tempo;
	else
		return tempo / 100.0;
}
/*
?? Evento_Midi:desplazamiento_SMPTE() const
{
	if(m_datos == nullptr)
		throw Excepcion_Midi(CodigoErrorMidi::EventoNulo);

	if(m_largo_evento < 7)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsMetaEventoSMPTE);

	if(m_datos[0] != EventoMidi_Metaevento)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsMetaEvento);

	if(m_datos[1] != MetaEventoMidi_SMPTE)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsMetaEventoSMPTE);
}*/

unsigned char Evento_Midi::compas_numerador() const
{
	if(m_datos == nullptr)
		throw Excepcion_Midi(CodigoErrorMidi::EventoNulo);

	if(m_version_midi == VersionMidi::Version_1)
	{
		if(m_largo_evento < 6)
			throw Excepcion_Midi(CodigoErrorMidi::NoEsMetaEventoCompas);

		if(m_datos[0] != EventoMidi_Metaevento)
			throw Excepcion_Midi(CodigoErrorMidi::NoEsMetaEvento);

		if(m_datos[1] != MetaEventoMidi_Compas)
			throw Excepcion_Midi(CodigoErrorMidi::NoEsMetaEventoCompas);

		return m_datos[2];
	}
	else
	{
		if((m_datos[0] & 0xF0) != TipoMensaje_DatosFlexibles)
			throw Excepcion_Midi(CodigoErrorMidi::NoEsMensajeDeDatosFlexibles);

		if(this->tipo_dato_flexible() != DatosFlexibles_Compas)
			throw Excepcion_Midi(CodigoErrorMidi::NoEsDatoFlexibleCompas);

		return m_datos[4];
	}
}

unsigned char Evento_Midi::compas_denominador() const
{
	if(m_datos == nullptr)
		throw Excepcion_Midi(CodigoErrorMidi::EventoNulo);

	if(m_version_midi == VersionMidi::Version_1)
	{
		if(m_largo_evento < 6)
			throw Excepcion_Midi(CodigoErrorMidi::NoEsMetaEventoCompas);

		if(m_datos[0] != EventoMidi_Metaevento)
			throw Excepcion_Midi(CodigoErrorMidi::NoEsMetaEvento);

		if(m_datos[1] != MetaEventoMidi_Compas)
			throw Excepcion_Midi(CodigoErrorMidi::NoEsMetaEventoCompas);

		return static_cast<unsigned char>(1 << m_datos[3]);//2^m_datos[3]
	}
	else
	{
		if((m_datos[0] & 0xF0) != TipoMensaje_DatosFlexibles)
			throw Excepcion_Midi(CodigoErrorMidi::NoEsMensajeDeDatosFlexibles);

		if(this->tipo_dato_flexible() != DatosFlexibles_Compas)
			throw Excepcion_Midi(CodigoErrorMidi::NoEsDatoFlexibleCompas);

		return static_cast<unsigned char>(1 << m_datos[5]);//2^m_datos[3]
	}
}

unsigned char Evento_Midi::compas_reloj() const
{
	if(m_datos == nullptr)
		throw Excepcion_Midi(CodigoErrorMidi::EventoNulo);

	if(m_largo_evento < 6)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsMetaEventoCompas);

	if(m_datos[0] != EventoMidi_Metaevento)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsMetaEvento);

	if(m_datos[1] != MetaEventoMidi_Compas)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsMetaEventoCompas);

	return m_datos[4];
}

unsigned char Evento_Midi::compas_nota() const
{
	if(m_datos == nullptr)
		throw Excepcion_Midi(CodigoErrorMidi::EventoNulo);

	if(m_largo_evento < 6)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsMetaEventoCompas);

	if(m_datos[0] != EventoMidi_Metaevento)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsMetaEvento);

	if(m_datos[1] != MetaEventoMidi_Compas)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsMetaEventoCompas);

	return m_datos[5];
}

char Evento_Midi::armadura() const
{
	if(m_datos == nullptr)
		throw Excepcion_Midi(CodigoErrorMidi::EventoNulo);

	if(m_largo_evento < 4)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsMetaEventoArmadura);

	if(m_datos[0] != EventoMidi_Metaevento)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsMetaEvento);

	if(m_datos[1] != MetaEventoMidi_Armadura)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsMetaEventoArmadura);

	return static_cast<char>(m_datos[2]);
}

unsigned char Evento_Midi::escala() const
{
	if(m_datos == nullptr)
		throw Excepcion_Midi(CodigoErrorMidi::EventoNulo);

	if(m_largo_evento < 4)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsMetaEventoArmadura);

	if(m_datos[0] != EventoMidi_Metaevento)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsMetaEvento);

	if(m_datos[1] != MetaEventoMidi_Armadura)
		throw Excepcion_Midi(CodigoErrorMidi::NoEsMetaEventoArmadura);

	return m_datos[3];
}

bool Evento_Midi::es_nulo() const
{
	if(m_datos == nullptr)
		return true;

	return false;
}

bool Evento_Midi::contiene_grupo() const
{
	if(m_datos == nullptr)
		return false;

	if(m_version_midi == VersionMidi::Version_1)
	{
		//Grupo 0 para todos los eventos MIDI 1.0, excepto el
		//metaevento fin de pista
		if(	m_datos[0] == EventoMidi_Metaevento &&
			m_datos[1] == MetaEventoMidi_FinDePista)
			return false;
	}
	else
	{
		unsigned char tipo_mensaje = m_datos[0] & 0xF0;
		if(	tipo_mensaje == TipoMensaje_Utilidad ||
			tipo_mensaje == TipoMensaje_FlujoPaqueteMidiUniversal)
			return false;
	}

	return true;
}

bool Evento_Midi::contiene_canal() const
{
	if(m_datos == nullptr)
		return false;

	if(m_version_midi == VersionMidi::Version_1)
	{
		unsigned char tipo_evento = m_datos[0] & 0xF0;
		if(	tipo_evento == EventoMidi_NotaApagada ||
			tipo_evento == EventoMidi_NotaEncendida ||
			tipo_evento == EventoMidi_PresionNota ||
			tipo_evento == EventoMidi_Controlador ||
			tipo_evento == EventoMidi_Programa ||
			tipo_evento == EventoMidi_PresionCanal ||
			tipo_evento == EventoMidi_InflexionDeTono)
			return true;
		else if(m_datos[0] == EventoMidi_Metaevento &&
				m_datos[1] != MetaEventoMidi_FinDePista)
			return true;
	}
	else
	{
		unsigned char tipo_mensaje = m_datos[0] & 0xF0;
		if(	tipo_mensaje == TipoMensaje_VozDeCanalMidi1 ||
			tipo_mensaje == TipoMensaje_VozDeCanalMidi2 ||
			tipo_mensaje == TipoMensaje_DatosFlexibles)
			return true;
	}

	return false;
}

bool Evento_Midi::es_mensaje_voz_de_canal() const
{
	if(m_datos == nullptr)
		return false;

	if(m_version_midi == VersionMidi::Version_1)
	{
		unsigned char tipo_evento = m_datos[0] & 0xF0;
		if(	tipo_evento == EventoMidi_NotaApagada ||
			tipo_evento == EventoMidi_NotaEncendida ||
			tipo_evento == EventoMidi_PresionNota ||
			tipo_evento == EventoMidi_Controlador ||
			tipo_evento == EventoMidi_Programa ||
			tipo_evento == EventoMidi_PresionCanal ||
			tipo_evento == EventoMidi_InflexionDeTono)
			return true;
	}
	else
	{
		unsigned char tipo_mensaje = m_datos[0] & 0xF0;
		if(	tipo_mensaje == TipoMensaje_VozDeCanalMidi1 ||
			tipo_mensaje == TipoMensaje_VozDeCanalMidi2)
			return true;
	}

	return false;
}

bool Evento_Midi::es_nota() const
{
	if(m_datos == nullptr)
		return false;

	if(m_version_midi == VersionMidi::Version_1)
	{
		unsigned char tipo_evento = m_datos[0] & 0xF0;
		if(	tipo_evento == EventoMidi_NotaApagada ||
			tipo_evento == EventoMidi_NotaEncendida)
			return true;
	}
	else
	{
		unsigned char tipo_mensaje = m_datos[0] & 0xF0;
		if(	tipo_mensaje == TipoMensaje_VozDeCanalMidi1 ||
			tipo_mensaje == TipoMensaje_VozDeCanalMidi2)
		{
			unsigned char tipo_evento = m_datos[1] & 0xF0;
			if(	tipo_evento == EventoMidi_NotaApagada ||
				tipo_evento == EventoMidi_NotaEncendida)
				return true;
		}
	}
	return false;
}

bool Evento_Midi::contiene_nota() const
{
	if(m_datos == nullptr)
		return false;

	if(m_version_midi == VersionMidi::Version_1)
	{
		unsigned char tipo_evento = m_datos[0] & 0xF0;
		if(	tipo_evento == EventoMidi_NotaApagada ||
			tipo_evento == EventoMidi_NotaEncendida ||
			tipo_evento == EventoMidi_PresionNota)
			return true;
	}
	else
	{
		unsigned char tipo_mensaje = m_datos[0] & 0xF0;
		if(	tipo_mensaje == TipoMensaje_VozDeCanalMidi1 ||
			tipo_mensaje == TipoMensaje_VozDeCanalMidi2)
		{
			unsigned char tipo_evento = m_datos[1] & 0xF0;
			if(	tipo_evento == EventoMidi_NotaApagada ||
				tipo_evento == EventoMidi_NotaEncendida ||
				tipo_evento == EventoMidi_PresionNota ||
				tipo_evento == EventoMidi_ControladorPorNotaRegistrado ||
				tipo_evento == EventoMidi_ControladorPorNotaNoRegistrado ||
				tipo_evento == EventoMidi_InflexionDeTonoPorNota ||
				tipo_evento == EventoMidi_GestionPorNota)
				return true;
		}
	}
	return false;
}

bool Evento_Midi::contiene_texto() const
{
	if(m_datos == nullptr)
		return false;

	if(m_version_midi == VersionMidi::Version_1)
	{
		if(m_datos[0] == EventoMidi_Metaevento)
		{
			unsigned char metaevento = m_datos[1];
		if(	metaevento == MetaEventoMidi_Texto ||
			metaevento == MetaEventoMidi_DerechosDeAutor ||
			metaevento == MetaEventoMidi_NombreDePista ||
			metaevento == MetaEventoMidi_NombreDeInstrumento ||
			metaevento == MetaEventoMidi_Letra ||
			metaevento == MetaEventoMidi_Marcador ||
			metaevento == MetaEventoMidi_PuntoReferencia ||
			metaevento == MetaEventoMidi_NombreDelPrograma ||
			metaevento == MetaEventoMidi_NombreDelDispositivo)
			return true;
		}
	}
	else
	{
		if((m_datos[0] & 0xF0) == TipoMensaje_DatosFlexibles)
		{
			unsigned char estado_banco = m_datos[2];
			if(estado_banco == 0x01 || estado_banco == 0x02)
				return true;
		}
	}
	return false;
}

std::string Evento_Midi::a_texto() const
{
	if(m_largo_evento < 1)
		return "Evento Nulo";

	//Eventos fuera del estandar MIDI
	if(m_largo_evento == 1)
	{
		if(m_datos[0] == EventoMidi_ClienteConectado)
			return "Evento Cliente Conectado";
		else if(m_datos[0] == EventoMidi_ClienteDesconectado)
			return "Evento Cliente Desconectado";
		else if(m_datos[0] == EventoMidi_PuertoConectado)
			return "Evento Puerto Conectado";
		else if(m_datos[0] == EventoMidi_PuertoDesconectado)
			return "Evento Puerto Desconectado";
		else if(m_datos[0] == EventoMidi_PuertoSuscrito)
			return "Evento Puerto Suscrito";
		else if(m_datos[0] == EventoMidi_PuertoDesuscrito)
			return "Evento Puerto Desuscrito";
	}

	std::string texto_salida = "Midi " + std::to_string(static_cast<unsigned int>(m_version_midi)) + ".0 ";

	unsigned char tipo_mensaje = this->tipo_mensaje();
	texto_salida += "\033[0;36m" + Nombre_TipoMensaje(tipo_mensaje) + " ";

	texto_salida += "\033[0;33m";
	if(tipo_mensaje == TipoMensaje_Utilidad)
	{
		texto_salida += Nombre_Utilidad(this->tipo_utilidad()) + " ";
		if(this->tipo_utilidad() == MensajeUtilidad_DivicionDeTiempo)
			texto_salida += "[" + std::to_string(static_cast<unsigned int>(this->division_de_tiempo())) + "] ";
		else if(this->tipo_utilidad() == MensajeUtilidad_TiempoDelta)
			texto_salida += "[" + std::to_string(this->tiempo_delta()) + "] ";
	}
	else if(tipo_mensaje == TipoMensaje_ComunYTiempoReal)
		texto_salida += Nombre_TiempoReal(this->tipo_tiempo_real()) + " ";
	else if(tipo_mensaje == TipoMensaje_VozDeCanalMidi1)
		texto_salida += Nombre_VozDeCanalMidi1(this->tipo_voz_de_canal()) + " ";
	else if(tipo_mensaje == TipoMensaje_VozDeCanalMidi2)
		texto_salida += Nombre_VozDeCanalMidi2(this->tipo_voz_de_canal()) + " ";
	else if(tipo_mensaje == TipoMensaje_DatosFlexibles)
	{
		if(m_version_midi == VersionMidi::Version_1 && this->tipo_evento_midi1() == EventoMidi_Metaevento)
		{
			texto_salida += Nombre_DatosFlexiblesBanco(this->dato_flexible_banco_de_estado()) + " ";
			unsigned char metaevento = this->tipo_metaevento_midi1();
			texto_salida += "[" + Nombre_MetaEvento(metaevento);
			if(this->contiene_texto())
				texto_salida += ", " + this->texto();
			else if(metaevento == MetaEventoMidi_SecuenciaNumerica)
				texto_salida += ", " + Texto::a_texto(this->secuencia_numerica());
			else if(metaevento == MetaEventoMidi_Tempo)
				texto_salida += ", " + Texto::a_texto(this->tempo());
			//else if(metaevento == MetaEventoMidi_AjusteDeSMPTE)
			//	texto_salida += ", " + Texto::a_texto(this->desplazamiento_SMPTE());
			else if(metaevento == MetaEventoMidi_Compas)
			{
				texto_salida += ", " + Texto::a_texto(this->compas_numerador());
				texto_salida += ", " + Texto::a_texto(this->compas_denominador());
				texto_salida += ", " + Texto::a_texto(this->compas_reloj());
				texto_salida += ", " + Texto::a_texto(this->compas_nota());
			}
			else if(metaevento == MetaEventoMidi_Armadura)
			{
				texto_salida += ", " + Texto::a_texto(this->armadura());
				texto_salida += ", " + Texto::a_texto(this->escala());
			}
			texto_salida += "]";
			texto_salida += "[" + Nombre_DatosFlexiblesDireccion(this->dato_flexible_direccion()) + "] ";
			texto_salida += "[" + std::to_string(static_cast<unsigned int>(this->grupo())) + ", ";
			texto_salida += std::to_string(static_cast<unsigned int>(this->canal())) + "] ";
		}
		else
		{
			texto_salida += Nombre_DatosFlexiblesBanco(this->dato_flexible_banco_de_estado()) + " ";
			texto_salida += "[" + Nombre_DatosFlexibles(this->tipo_dato_flexible()) + "]";
			texto_salida += "[" + Nombre_EstadoMensaje(this->estado_mensaje()) + "]";
			texto_salida += "[" + Nombre_DatosFlexiblesDireccion(this->dato_flexible_direccion()) + "] ";
			texto_salida += "[" + std::to_string(static_cast<unsigned int>(this->grupo())) + ", ";
			texto_salida += std::to_string(static_cast<unsigned int>(this->canal())) + "] ";
			if(this->contiene_texto())
				texto_salida += "\033[1;31m\"" + this->texto() + "\"";
		}
	}
	else if(tipo_mensaje == TipoMensaje_FlujoPaqueteMidiUniversal)
	{
		texto_salida += Nombre_FlujoPaqueteMidiUniversal(this->tipo_flujo()) + " ";
		texto_salida += "[" + Nombre_EstadoMensaje(this->estado_mensaje()) + "] ";
	}
	else if(m_version_midi == VersionMidi::Version_2 && tipo_mensaje == TipoMensaje_ExclusivoDelSistema7bits)
	{
		texto_salida += "[" + Nombre_EstadoMensaje(this->estado_mensaje()) + "] ";
		texto_salida += "bytes: " + std::to_string(static_cast<unsigned int>(this->numero_bytes_mensaje())) + " ";
	}
	else if(tipo_mensaje == TipoMensaje_ExclusivoDelSistema8bits)
	{
		texto_salida += "[" + Nombre_EstadoMensaje(this->estado_mensaje()) + "] ";
		texto_salida += "bytes: " + std::to_string(static_cast<unsigned int>(this->numero_bytes_mensaje())) + " ";
		texto_salida += "id: " + std::to_string(static_cast<unsigned int>(this->identificador_sistema_exclusivo_8())) + " ";
	}

	texto_salida += "\033[0;36m";

	if(	this->contiene_grupo() &&
		tipo_mensaje != TipoMensaje_VozDeCanalMidi1 &&
		tipo_mensaje != TipoMensaje_VozDeCanalMidi2 &&
		tipo_mensaje != TipoMensaje_DatosFlexibles)
		texto_salida += "Grupo: " + Texto::a_texto(this->grupo());

	if(tipo_mensaje == TipoMensaje_VozDeCanalMidi1 || tipo_mensaje == TipoMensaje_VozDeCanalMidi2)
	{
		texto_salida += "[" + Texto::a_texto(this->grupo()) + ", " + Texto::a_texto(this->canal());

		if(	this->contiene_nota())
			texto_salida += ", " + Texto::a_texto(this->id_nota());

		if(	this->es_nota())
		{
			if(tipo_mensaje == TipoMensaje_VozDeCanalMidi1)
				texto_salida += ", " + Texto::a_texto(this->velocidad_nota_7()) + "]";
			else
				texto_salida += ", " + Texto::a_texto(this->velocidad_nota_16()) + "]";
		}

		if(this->tipo_voz_de_canal() == EventoMidi_PresionNota)
			texto_salida += ", " + Texto::a_texto(this->presion_nota()) + "]";

		if(this->tipo_voz_de_canal() == EventoMidi_PresionCanal)
			texto_salida += ", " + Texto::a_texto(this->presion_canal()) + "]";

		if(this->tipo_voz_de_canal() == EventoMidi_Programa)
			texto_salida += ", " + Nombre_Instrumento(this->programa(), 0, 0) + "]";

		if(this->tipo_voz_de_canal() == EventoMidi_InflexionDeTono)
			texto_salida += ", " + Texto::a_texto(this->inflexion_de_tono()) + "]";

		if(this->tipo_voz_de_canal() == EventoMidi_Controlador)
			texto_salida += ", " + Nombre_MensajeControlador(this->controlador_mensaje()) + ", " + Texto::a_texto(this->controlador_valor()) + "]";
	}

	texto_salida += "\033[1;34m Datos: " + this->a_texto_en_bruto();

	texto_salida += "\033[0m Largo: " + std::to_string(m_largo_evento);

	return texto_salida;
}

std::string Evento_Midi::a_texto_en_bruto() const
{
	std::string texto_salida;
	for(unsigned int x=0; x<m_largo_evento; x++)
	{
		if(x == 0)
			texto_salida += Texto::hexadecimal_a_texto(m_datos[x], false);
		else
			texto_salida += " " + Texto::hexadecimal_a_texto(m_datos[x], false);
	}
	return texto_salida;
}
