#ifndef MIDI2_H
#define MIDI2_H

#include "midi.h++"

class Midi2 : public Midi
{
private:
	void cargar_archivo(std::fstream *archivo);

public:
	Midi2();
	Midi2(std::fstream *archivo);
	~Midi2();

	void guardar(const std::string &nombre_archivo) override;
	unsigned char version_midi() const override;

	static std::int64_t duracion_midi(std::fstream *archivo);
};

#endif
