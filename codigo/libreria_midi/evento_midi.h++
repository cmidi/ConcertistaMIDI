#ifndef EVENTO_MIDI_H
#define EVENTO_MIDI_H

#include "descripciones_midi.h++"
#include <string>

class Evento_Midi
{
private:
	VersionMidi m_version_midi;
	unsigned int m_largo_evento;
	unsigned char *m_datos;

	unsigned char m_cliente;
	unsigned char m_puerto;

	std::string m_texto;
public:
	Evento_Midi();
	Evento_Midi(const Evento_Midi &evento);
	Evento_Midi(unsigned char tipo_evento);
	Evento_Midi(unsigned char tipo_mensaje, unsigned char tipo_evento);
	Evento_Midi(unsigned char *datos, unsigned int largo, VersionMidi version);
	~Evento_Midi();

	VersionMidi version_midi() const;
	unsigned char tipo_mensaje() const;

	unsigned char tipo_utilidad() const;
	unsigned char tipo_tiempo_real() const;
	unsigned char tipo_voz_de_canal() const;
	unsigned short tipo_dato_flexible() const;
	unsigned short tipo_flujo() const;

	unsigned char tipo_evento_midi1() const;//MIDI 1.0
	unsigned char tipo_metaevento_midi1() const;//MIDI 1.0

	void division_de_tiempo(unsigned short valor);
	unsigned short division_de_tiempo() const;

	void tiempo_delta(unsigned int valor);
	unsigned int tiempo_delta() const;

	void grupo(unsigned char grupo);
	unsigned char grupo() const;

	void canal(unsigned char canal);
	unsigned char canal() const;

	void id_nota(unsigned char id_nota);
	unsigned char id_nota() const;

	void velocidad_nota_7(unsigned char velocidad);
	unsigned char velocidad_nota_7() const;

	void velocidad_nota_16(unsigned short velocidad);
	unsigned short velocidad_nota_16() const;

	void presion_nota(unsigned char presion);
	unsigned char presion_nota() const;

	void presion_canal(unsigned char presion);
	unsigned char presion_canal() const;

	void programa_banco_valido(unsigned char banco_valido);
	unsigned char programa_banco_valido() const;

	void programa_banco_izquierdo(unsigned char valor);
	unsigned char programa_banco_izquierdo() const;

	void programa_banco_derecho(unsigned char valor);
	unsigned char programa_banco_derecho() const;

	void programa(unsigned char programa);
	unsigned char programa() const;

	void inflexion_de_tono(unsigned int valor);
	unsigned int inflexion_de_tono() const;

	void controlador_mensaje(unsigned char mensaje);
	unsigned char controlador_mensaje() const;

	void controlador_valor(unsigned char valor);
	unsigned char controlador_valor() const;

	unsigned char estado_mensaje() const;
	unsigned char numero_bytes_mensaje() const;

	unsigned char identificador_sistema_exclusivo_8() const;

	unsigned char dato_flexible_direccion() const;
	unsigned char dato_flexible_banco_de_estado() const;

	std::string texto() const;

	unsigned char *datos() const;
	unsigned int largo_datos() const;

	unsigned char cliente() const;
	void cliente(unsigned char cliente);

	unsigned char puerto() const;
	void puerto(unsigned char puerto);

	//Metodos para los metaeventos
	unsigned short secuencia_numerica() const;
	unsigned int tempo() const;				//FF 51 03
	double tempo_en_microsegundos() const;
	//?? desplazamiento_SMPTE() const;			//FF 54 05 hora, minuto, segundo, fotograma, centecima de fotograma
	unsigned char compas_numerador() const;		//FF 58 04
	unsigned char compas_denominador() const;
	unsigned char compas_reloj() const;
	unsigned char compas_nota() const;
	char armadura() const;				//FF 59 02
	unsigned char escala() const;

	bool es_nulo() const;
	bool contiene_grupo() const;
	bool contiene_canal() const;
	bool es_mensaje_voz_de_canal() const;

	bool es_nota() const;
	bool contiene_nota() const;
	bool contiene_texto() const;

	std::string a_texto() const;
	std::string a_texto_en_bruto() const;
};

#endif
