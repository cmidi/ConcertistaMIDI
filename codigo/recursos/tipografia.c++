#include "tipografia.h++"
#include "../util/traduccion.h++"

Tipografia::Tipografia(Formato formato, int tamanno_letra)
{
	m_libreria = nullptr;
	m_tipografia = nullptr;
	m_tipografia_musical = nullptr;

	if(FT_Init_FreeType(&m_libreria))
		Registro::Error(T("La libreria Freetype fallo al iniciar."));

	std::string ruta_tipografia = RUTA_ARCHIVOS;
	if(formato == Formato::Normal)
		ruta_tipografia += "/tipografias/NotoSans-Regular.ttf";
	else if(formato == Formato::Negrita)
		ruta_tipografia += "/tipografias/NotoSans-Black.ttf";
	else if(formato == Formato::Cursiva)
		ruta_tipografia += "/tipografias/NotoSans-Italic.ttf";
	else if(formato == Formato::CursivaNegrita)
		ruta_tipografia += "/tipografias/NotoSans-BlackItalic.ttf";

	if(m_libreria != nullptr)
	{
		if(FT_New_Face(m_libreria, ruta_tipografia.c_str(), 0, &m_tipografia))
			Registro::Error(T_("Freetype fallo al cargar la tipografia del archivo: {0}", ruta_tipografia));

		std::string ruta_tipografia_musical = std::string(RUTA_ARCHIVOS) + "/tipografias/NotoMusic-Regular.ttf";
		if(FT_New_Face(m_libreria, ruta_tipografia_musical.c_str(), 0, &m_tipografia_musical))
			Registro::Error(T_("Freetype fallo al cargar la tipografia del archivo: {0}", ruta_tipografia_musical));

		if(tamanno_letra > 50)
			tamanno_letra = 50;
		else if(tamanno_letra < 5)
			tamanno_letra = 5;
		m_tamanno_letra = tamanno_letra;

		if(m_tipografia != nullptr)
			FT_Set_Char_Size(m_tipografia, 0, m_tamanno_letra*64, 92, 92);//91.79
		if(m_tipografia_musical != nullptr)
			FT_Set_Char_Size(m_tipografia_musical, 0, m_tamanno_letra*64, 92, 92);

		icu::UnicodeString idioma_espannol = "0123456789 "					//Numeros
											"ABCDEFGHIJKLMNÑOPQRSTUVWXYZ"	//Mayusculas
											"abcdefghijklmnñopqrstuvwxyz"	//Minusculas
											"ÁÉÍÓÚáéíóúÀÈÌÒÙàèìòùÄËÏÖÜäëïöüÂÊÎÔÛâêîôûÃẼĨÕŨãẽĩõũµ"//Vocales
											"ÆæŒœ"
											"АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдеёжзийклмнопрстуфхцчшщъыьэюя"
											",._;:'·#$€%&@\"\\()[]{}+-*/=¿?¡!<>ºªḉḈçÇ^�©®";

		icu::UnicodeString caracteres_musicales = "♭♯𝆏𝆐𝆑𝄞𝄢𝅜𝅝𝅗𝅥𝅘𝅥𝅘𝅥𝅮𝅘𝅥𝅯𝅘𝅥𝅰𝅘𝅥𝅱𝅘𝅥𝅲𝄺𝄻𝄼𝄽𝄾𝄿𝅀𝅁𝅂";

		m_ancho_atlas = 0;
		m_alto_atlas = 0;
		m_posicion_atlas = 0;

		if(m_tipografia != nullptr)
			this->calcular_tamanno_atlas(m_tipografia, idioma_espannol);
		if(m_tipografia_musical != nullptr)
			this->calcular_tamanno_atlas(m_tipografia_musical, caracteres_musicales);

		this->crear_atlas();

		if(m_tipografia != nullptr)
			this->generar_caracteres(m_tipografia, idioma_espannol);
		if(m_tipografia_musical != nullptr)
			this->generar_caracteres(m_tipografia_musical, caracteres_musicales);
	}
}

Tipografia::~Tipografia()
{
	for(std::map<unsigned int, Caracter*>::iterator i = m_caracteres.begin(); i != m_caracteres.end(); i++)
		delete i->second;
	m_caracteres.clear();

	//Destruye freetype
	if(m_tipografia != nullptr)
		FT_Done_Face(m_tipografia);
	if(m_tipografia_musical != nullptr)
		FT_Done_Face(m_tipografia_musical);
	if(m_libreria != nullptr)
		FT_Done_FreeType(m_libreria);

	glBindTexture(GL_TEXTURE_2D, 0);
	glDeleteTextures(1, &m_indice_atlas);
}

void Tipografia::calcular_tamanno_atlas(FT_Face tipografia, const icu::UnicodeString &caracteres_requeridos)
{
	bool error = false;
	unsigned int letra_unicode_32 = 0;
	FT_UInt letra_actual = 0;
	for(int n=0; n<caracteres_requeridos.length(); n++)
	{
		letra_unicode_32 = static_cast<unsigned int>(caracteres_requeridos.char32At(n));
		if(letra_unicode_32 > 0xFFFF)
			n++;//Letras UTF-32 utilizan 2 espacios UTF-16

		letra_actual = FT_Get_Char_Index(tipografia, letra_unicode_32);
		if(letra_actual == 0)
		{
			Registro::Depurar("Caracter unicode desconocido \""
			"" + std::to_string(letra_unicode_32) + "\" "
			"se requiere otra tipografia.");
			continue;
		}

		error = FT_Load_Glyph(tipografia, letra_actual, FT_LOAD_RENDER);
		if(error)
		{
			Registro::Depurar("Error al cargar el caracter: "
			"" + std::to_string(letra_unicode_32));
			continue;
		}

		FT_GlyphSlot letra = tipografia->glyph;

		m_ancho_atlas += static_cast<int>(letra->bitmap.width);
		if(static_cast<int>(letra->bitmap.rows) > m_alto_atlas)
			m_alto_atlas = static_cast<int>(letra->bitmap.rows);
	}
}

void Tipografia::crear_atlas()
{
	m_indice_atlas = 0;
	glGenTextures(1, &m_indice_atlas);
	glBindTexture(GL_TEXTURE_2D, m_indice_atlas);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	Textura2D::Ultimo_indice_seleccionado = m_indice_atlas;
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, m_ancho_atlas, m_alto_atlas, 0, GL_RED,GL_UNSIGNED_BYTE, 0);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
}

void Tipografia::generar_caracteres(FT_Face tipografia, const icu::UnicodeString &caracteres_requeridos)
{
	bool error = false;
	unsigned int letra_unicode_32 = 0;
	FT_UInt letra_actual = 0;
	for(int n=0; n<caracteres_requeridos.length(); n++)
	{
		letra_unicode_32 = static_cast<unsigned int>(caracteres_requeridos.char32At(n));
		if(letra_unicode_32 > 0xFFFF)
			n++;//Letras UTF-32 utilizan 2 espacios UTF-16

		letra_actual = FT_Get_Char_Index(tipografia, letra_unicode_32);
		if(letra_actual == 0)
			continue;

		error = FT_Load_Glyph(tipografia, letra_actual, FT_LOAD_RENDER);
		if(error)
			continue;

		FT_GlyphSlot letra = tipografia->glyph;

		if(!m_caracteres[letra_unicode_32])
		{
			Caracter *letra_nueva = new Caracter();
			letra_nueva->tipografia = tipografia;
			letra_nueva->codigo_unicode = letra_actual;
			letra_nueva->ancho = static_cast<int>(letra->bitmap.width);
			letra_nueva->alto = static_cast<int>(letra->bitmap.rows);
			letra_nueva->ajuste_izquierda = letra->bitmap_left;
			letra_nueva->ajuste_arriba = letra->bitmap_top;
			letra_nueva->avance_x = letra->advance.x;
			letra_nueva->avance_y = letra->advance.y;
			letra_nueva->textura_x = m_posicion_atlas;
			m_caracteres[letra_unicode_32] = letra_nueva;

			glTexSubImage2D(GL_TEXTURE_2D, 0, m_posicion_atlas, 0, letra_nueva->ancho, letra_nueva->alto, GL_RED, GL_UNSIGNED_BYTE, letra->bitmap.buffer);
			m_posicion_atlas += static_cast<int>(letra->bitmap.width);
		}
		else
			Registro::Depurar("El caracter codigo: " + std::to_string(letra_unicode_32) + " ya existe");
	}
}

int Tipografia::crear_texto(icu::UnicodeString texto, unsigned int *indice_objeto)
{
	FT_Pos x=0, y=0;
	FT_Pos x_inicial = x;

	unsigned int letra_unicode_32 = 0;

	//Contar el numero de espacios a omitir
	int numero_espacio = 0;
	for(int l=0; l<texto.length(); l++)
	{
		letra_unicode_32 = static_cast<unsigned int>(texto.char32At(l));
		if(letra_unicode_32 > 0xFFFF)
			l++;//Letras UTF-32 utilizan 2 espacios UTF-16

		if(letra_unicode_32 == ' ')
			numero_espacio++;
	}

	//No hay nada que hacer si es una cadena vacia
	if(texto.length() == numero_espacio)
		return 0;

	Caracter *letra;
	unsigned int letra_anterior = 0;
	float vertices[static_cast<unsigned int>(6*(texto.length()-numero_espacio))][4];
	int posicion_arreglo = 0;

	for(int n=0; n<texto.length(); n++)
	{
		letra_unicode_32 = static_cast<unsigned int>(texto.char32At(n));
		if(letra_unicode_32 > 0xFFFF)
			n++;//Letras UTF-32 utilizan 2 espacios UTF-16

		letra = m_caracteres[letra_unicode_32];

		if(!letra)
			continue;

		//Se calcula el Interletraje
		FT_Bool interletraje = FT_HAS_KERNING(letra->tipografia);
		if(interletraje && letra_anterior && letra->codigo_unicode)
		{
			FT_Vector delta;
			FT_Get_Kerning(letra->tipografia, letra_anterior, letra->codigo_unicode, FT_KERNING_DEFAULT, &delta);
			x += delta.x >> 6;
		}

		if(letra_unicode_32 != ' ')//Se omiten los espacios
		{
			float xpos = static_cast<float>(x + letra->ajuste_izquierda);
			float ypos = static_cast<float>(y - letra->ajuste_arriba);

			float an = static_cast<float>(letra->ancho);
			float al = static_cast<float>(letra->alto);
			vertices[posicion_arreglo][0] = xpos + an;
			vertices[posicion_arreglo][1] = ypos;
			vertices[posicion_arreglo][2] = (static_cast<float>(letra->textura_x) + an) / static_cast<float>(m_ancho_atlas);//mi valor / total
			vertices[posicion_arreglo][3] = 0.0f;
			posicion_arreglo++;

			vertices[posicion_arreglo][0] = xpos;
			vertices[posicion_arreglo][1] = ypos;
			vertices[posicion_arreglo][2] = static_cast<float>(letra->textura_x) / static_cast<float>(m_ancho_atlas);
			vertices[posicion_arreglo][3] = 0.0f;
			posicion_arreglo++;

			vertices[posicion_arreglo][0] = xpos + an;
			vertices[posicion_arreglo][1] = ypos + al;
			vertices[posicion_arreglo][2] = (static_cast<float>(letra->textura_x) + an) / static_cast<float>(m_ancho_atlas);
			vertices[posicion_arreglo][3] = al / static_cast<float>(m_alto_atlas);
			posicion_arreglo++;

			vertices[posicion_arreglo][0] = xpos;
			vertices[posicion_arreglo][1] = ypos + al;
			vertices[posicion_arreglo][2] = static_cast<float>(letra->textura_x) / static_cast<float>(m_ancho_atlas);
			vertices[posicion_arreglo][3] = al / static_cast<float>(m_alto_atlas);
			posicion_arreglo++;

			vertices[posicion_arreglo][0] = xpos + an;
			vertices[posicion_arreglo][1] = ypos + al;
			vertices[posicion_arreglo][2] = (static_cast<float>(letra->textura_x) + an) / static_cast<float>(m_ancho_atlas);
			vertices[posicion_arreglo][3] = al / static_cast<float>(m_alto_atlas);
			posicion_arreglo++;

			vertices[posicion_arreglo][0] = xpos;
			vertices[posicion_arreglo][1] = ypos;
			vertices[posicion_arreglo][2] = static_cast<float>(letra->textura_x) / static_cast<float>(m_ancho_atlas);
			vertices[posicion_arreglo][3] = 0.0f;
			posicion_arreglo++;
		}

		x += letra->avance_x >> 6;
		letra_anterior = letra->codigo_unicode;
	}

	//No hay nada que dibujar si es una cadena de letras desconocidas
	if(posicion_arreglo == 0)
		return 0;

	if(Textura2D::Ultimo_indice_seleccionado != m_indice_atlas)
	{
		glBindTexture(GL_TEXTURE_2D, m_indice_atlas);
		Textura2D::Ultimo_indice_seleccionado = m_indice_atlas;
	}

	glGenBuffers(1, indice_objeto);
	glBindBuffer(GL_ARRAY_BUFFER, *indice_objeto);
	glBufferData(GL_ARRAY_BUFFER, static_cast<GLsizeiptr>(sizeof(vertices)), vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(float), 0);
	glEnableVertexAttribArray(0);
	return static_cast<int>(x - x_inicial);
}

int Tipografia::alto_texto()
{
	return m_tamanno_letra;
}

void Tipografia::activar()
{
	if(Textura2D::Ultimo_indice_seleccionado != m_indice_atlas)
	{
		glBindTexture(GL_TEXTURE_2D, m_indice_atlas);
		Textura2D::Ultimo_indice_seleccionado = m_indice_atlas;
	}
}
