#include "administrador_recursos.h++"

Administrador_Recursos::Administrador_Recursos()
{
	m_matriz_proyeccion = glm::ortho(0.0f, Pantalla::Ancho, Pantalla::Alto, 0.0f, -1.0f, 1.0f);

	std::string ruta = RUTA_ARCHIVOS;
	m_archivo_texturas[Textura::Indeterminado] = ruta + "/texturas/indeterminado.tga";
	m_archivo_texturas[Textura::FondoTitulo] = ruta + "/texturas/fondo_titulo.tga";
	m_archivo_texturas[Textura::Titulo] = ruta + "/texturas/titulo.tga";
	m_archivo_texturas[Textura::Boton] = ruta + "/texturas/boton.tga";
	m_archivo_texturas[Textura::TeclaBlanca] = ruta + "/texturas/tecla_blanca.tga";
	m_archivo_texturas[Textura::TeclaNegra] = ruta + "/texturas/tecla_negra.tga";
	m_archivo_texturas[Textura::TeclaBlancaPresionada] = ruta + "/texturas/tecla_blanca_presionada.tga";
	m_archivo_texturas[Textura::TeclaBlancaPresionadaDoble] = ruta + "/texturas/tecla_blanca_presionada_doble.tga";
	m_archivo_texturas[Textura::TeclaNegraPresionada] = ruta + "/texturas/tecla_negra_presionada.tga";
	m_archivo_texturas[Textura::Circulo] = ruta + "/texturas/circulo.tga";
	m_archivo_texturas[Textura::BordeOrganoRojo] = ruta + "/texturas/borde_organo_rojo.tga";
	m_archivo_texturas[Textura::BordeOrganoNegro] = ruta + "/texturas/borde_organo_negro.tga";
	m_archivo_texturas[Textura::Nota] = ruta + "/texturas/nota.tga";
	m_archivo_texturas[Textura::NotaResaltada] = ruta + "/texturas/nota_resaltada.tga";
	m_archivo_texturas[Textura::Sombra] = ruta + "/texturas/sombra.tga";
	m_archivo_texturas[Textura::FrenteBarraProgreso] = ruta + "/texturas/frente_barra_progreso.tga";
	m_archivo_texturas[Textura::ParticulaNota] = ruta + "/texturas/particula_nota.tga";
	m_archivo_texturas[Textura::Barra] = ruta + "/texturas/barra.tga";
	m_archivo_texturas[Textura::Sonido_0] = ruta + "/texturas/sonido_0.tga";
	m_archivo_texturas[Textura::Sonido_1] = ruta + "/texturas/sonido_1.tga";
	m_archivo_texturas[Textura::Sonido_2] = ruta + "/texturas/sonido_2.tga";
	m_archivo_texturas[Textura::Sonido_3] = ruta + "/texturas/sonido_3.tga";
	m_archivo_texturas[Textura::Reproducir] = ruta + "/texturas/reproducir.tga";
	m_archivo_texturas[Textura::Pausar] = ruta + "/texturas/pausar.tga";
	m_archivo_texturas[Textura::TituloMusica] = ruta + "/texturas/titulo_musica.tga";
	m_archivo_texturas[Textura::ColorPista] = ruta + "/texturas/color_pista.tga";
	m_archivo_texturas[Textura::Tocar] = ruta + "/texturas/tocar.tga";
	m_archivo_texturas[Textura::Aprender] = ruta + "/texturas/aprender.tga";
	m_archivo_texturas[Textura::MusicaFondo] = ruta + "/texturas/musica_fondo.tga";
	m_archivo_texturas[Textura::CasillaInactiva] = ruta + "/texturas/casilla_verificacion_inactiva.tga";
	m_archivo_texturas[Textura::CasillaActiva] = ruta + "/texturas/casilla_verificacion_activa.tga";
	m_archivo_texturas[Textura::CasillaSombra] = ruta + "/texturas/casilla_verificacion_sombra.tga";
	m_archivo_texturas[Textura::FlechaIzquierda] = ruta + "/texturas/flecha_izquierda.tga";
	m_archivo_texturas[Textura::FlechaDerecha] = ruta + "/texturas/flecha_derecha.tga";
	m_archivo_texturas[Textura::FlechaAbajo] = ruta + "/texturas/flecha_abajo.tga";
	m_archivo_texturas[Textura::FlechaArriba] = ruta + "/texturas/flecha_arriba.tga";
	m_archivo_texturas[Textura::ControlDeslizante_H_Fondo] = ruta + "/texturas/control_deslizante_h_fondo.tga";
	m_archivo_texturas[Textura::ControlDeslizante_H_Relleno] = ruta + "/texturas/control_deslizante_h_relleno.tga";
	m_archivo_texturas[Textura::ControlDeslizante_V_Fondo] = ruta + "/texturas/control_deslizante_v_fondo.tga";
	m_archivo_texturas[Textura::ControlDeslizante_V_Relleno] = ruta + "/texturas/control_deslizante_v_relleno.tga";
	m_archivo_texturas[Textura::ControlDeslizante_Boton] = ruta + "/texturas/control_deslizante_boton.tga";
	m_archivo_texturas[Textura::Marcador_1] = ruta + "/texturas/marcador_1.tga";
	m_archivo_texturas[Textura::Marcador_2] = ruta + "/texturas/marcador_2.tga";
	m_archivo_texturas[Textura::Marcador_3] = ruta + "/texturas/marcador_3.tga";
	m_archivo_texturas[Textura::Marcador_4] = ruta + "/texturas/marcador_4.tga";
	m_archivo_texturas[Textura::MiniMarcador_1] = ruta + "/texturas/minimarcador_1.tga";
	m_archivo_texturas[Textura::MiniMarcador_2] = ruta + "/texturas/minimarcador_2.tga";
	m_archivo_texturas[Textura::MiniMarcador_3] = ruta + "/texturas/minimarcador_3.tga";
	m_archivo_texturas[Textura::MiniMarcador_4] = ruta + "/texturas/minimarcador_4.tga";
	m_archivo_texturas[Textura::InformacionNotaFondo] = ruta + "/texturas/informacion_nota_fondo.tga";
	m_archivo_texturas[Textura::InformacionNotaBorde] = ruta + "/texturas/informacion_nota_borde.tga";
	m_archivo_texturas[Textura::Carpeta] = ruta + "/texturas/carpeta.tga";
	m_archivo_texturas[Textura::ArchivoMIDI_1] = ruta + "/texturas/archivo_midi_1.tga";
	m_archivo_texturas[Textura::ArchivoMIDI_2] = ruta + "/texturas/archivo_midi_2.tga";
	m_archivo_texturas[Textura::ArchivoRMI] = ruta + "/texturas/archivo_rmi.tga";
	m_archivo_texturas[Textura::ArchivoKAR] = ruta + "/texturas/archivo_kar.tga";
	m_archivo_texturas[Textura::ArchivoSTY] = ruta + "/texturas/archivo_sty.tga";

	m_archivo_sombreador_vertice[SombreadorVF::Rectangulo] = ruta + "/sombreadores/rectangulo_sv.glsl";
	m_archivo_sombreador_fragmento[SombreadorVF::Rectangulo] = ruta + "/sombreadores/rectangulo_sf.glsl";

	m_archivo_sombreador_vertice[SombreadorVF::Texto] = ruta + "/sombreadores/texto_sv.glsl";
	m_archivo_sombreador_fragmento[SombreadorVF::Texto] = ruta + "/sombreadores/texto_sf.glsl";

	m_archivo_sombreador_vertice[SombreadorVF::Particula] = ruta + "/sombreadores/particula_sv.glsl";
	m_archivo_sombreador_fragmento[SombreadorVF::Particula] = ruta + "/sombreadores/particula_sf.glsl";

	m_lista_figuras[FiguraGeometrica::Rectangulo] = new Rectangulo(this->sombreador(SombreadorVF::Rectangulo));

	Sombreador *sombreador_letras = this->sombreador(SombreadorVF::Texto);
	sombreador_letras->uniforme_int("textura_texto", 0);
	sombreador_letras->uniforme_vector3f("color_texto", 0.0, 0.0, 0.0);

	m_formato_letras[ModoLetra::MuyGrandeNegrita] = new Tipografia(Formato::Negrita, 35);
	m_formato_letras[ModoLetra::MuyGrande] = new Tipografia(Formato::Normal, 35);
	m_formato_letras[ModoLetra::Grande] = new Tipografia(Formato::Normal, 20);
	m_formato_letras[ModoLetra::Mediana] = new Tipografia(Formato::Normal, 14);
	m_formato_letras[ModoLetra::Chica] = new Tipografia(Formato::Normal, 12);
	m_formato_letras[ModoLetra::MuyChica] = new Tipografia(Formato::Normal, 8);
}

Administrador_Recursos::~Administrador_Recursos()
{
	for(std::map<Textura, Textura2D*>::iterator i = m_lista_texturas.begin(); i != m_lista_texturas.end(); i++)
		delete i->second;
	m_lista_texturas.clear();

	for(std::map<SombreadorVF, Sombreador*>::iterator i = m_lista_sombreadores.begin(); i != m_lista_sombreadores.end(); i++)
		delete i->second;
	m_lista_sombreadores.clear();

	for(std::map<FiguraGeometrica, Rectangulo*>::iterator i = m_lista_figuras.begin(); i != m_lista_figuras.end(); i++)
		delete i->second;
	m_lista_figuras.clear();

	for(std::map<ModoLetra, Tipografia*>::iterator i = m_formato_letras.begin(); i != m_formato_letras.end(); i++)
		delete i->second;
	m_formato_letras.clear();
}

Textura2D *Administrador_Recursos::textura(Textura valor)
{
	Textura2D *temporal = m_lista_texturas[valor];
	if(!temporal)
	{
		//Generar la textura
		Archivo::Tga textura_nueva(m_archivo_texturas[valor]);

		temporal = new Textura2D();
		temporal->generar(static_cast<int>(textura_nueva.ancho()), static_cast<int>(textura_nueva.alto()), textura_nueva.bytes(), textura_nueva.imagen());

		m_lista_texturas[valor] = temporal;

		if(!textura_nueva.hay_error())
			Registro::Depurar("Se cargo la textura del archivo: " + std::string(m_archivo_texturas[valor]));
	}

	return temporal;
}

Sombreador *Administrador_Recursos::sombreador(SombreadorVF valor)
{
	Sombreador *temporal = m_lista_sombreadores[valor];
	if(!temporal)
	{
		std::string texto_vertice = Archivo::Texto::leer_archivo(m_archivo_sombreador_vertice[valor].c_str());
		std::string texto_fragmento = Archivo::Texto::leer_archivo(m_archivo_sombreador_fragmento[valor].c_str());

		const char* codigo_vertice = texto_vertice.c_str();
		const char* codigo_fragmento = texto_fragmento.c_str();

		temporal = new Sombreador(codigo_vertice, codigo_fragmento);
		temporal->uniforme_matriz4("proyeccion", m_matriz_proyeccion);

		m_lista_sombreadores[valor] = temporal;

		Registro::Depurar("Se cargo el sombreador de vertices del archivo: " + std::string(m_archivo_sombreador_vertice[valor]));
		Registro::Depurar("Se cargo el sombreador de fragmentos del archivo: " + std::string(m_archivo_sombreador_fragmento[valor]));
	}

	return temporal;
}

Rectangulo *Administrador_Recursos::figura(FiguraGeometrica valor)
{
	return m_lista_figuras[valor];
}

Tipografia *Administrador_Recursos::tipografia(ModoLetra tipo)
{
	return m_formato_letras[tipo];
}

void Administrador_Recursos::recortar_pantalla(float x, float y, float ancho, float alto)
{
	//Esta funcion recibe una posicion x,y arriba a la izquierda pero glScissor requiere
	//la posicion x,y abajo a la izquierda así que hay que cambiarlo
	int int_x = static_cast<int>(std::round(x));
	int int_y = static_cast<int>(std::round(Pantalla::Alto-(y+alto)));
	int int_ancho = static_cast<int>(std::round(ancho));
	int int_alto = static_cast<int>(std::round(alto));

	int anterior_x = 0;
	int anterior_y = 0;
	int anterior_ancho = static_cast<int>(Pantalla::Ancho);
	int anterior_alto = static_cast<int>(Pantalla::Alto);

	if(m_recortes.size() > 0)
	{
		std::array<int, 4> &recorte_actual = m_recortes.top();
		anterior_x = recorte_actual[0];
		anterior_y = recorte_actual[1];
		anterior_ancho = recorte_actual[2];
		anterior_alto = recorte_actual[3];
	}

	//No se permiten ancho o alto negativo
	if(int_ancho < 0)
		int_ancho = 0;
	if(int_alto < 0)
		int_alto = 0;

	//Un recorte nuevo no puede ser mayor a un recorte previo
	if(int_x < anterior_x)
		int_x = anterior_x;
	if(int_y < anterior_y)
		int_y = anterior_y;
	if(int_ancho > anterior_ancho-(anterior_x - int_x))
		int_ancho = anterior_ancho-(anterior_x - int_x);
	if(int_alto > anterior_alto-(anterior_y - int_y))
		int_alto = anterior_alto-(anterior_y - int_y);

	glScissor(int_x, int_y, int_ancho, int_alto);
	m_recortes.push({int_x, int_y, int_ancho, int_alto});
}

void Administrador_Recursos::revertir_recorte()
{
	if(m_recortes.size() > 0)
		m_recortes.pop();//Elimina el ultimo recorte

	if(m_recortes.size() > 0)
	{
		//Vuelve a un recorte superior si existe
		std::array<int, 4> &recorte = m_recortes.top();
		glScissor(recorte[0], recorte[1], recorte[2], recorte[3]);
	}
	else//Restablece el recorte al tamaño de la ventana
		glScissor(0, 0, static_cast<int>(Pantalla::Ancho), static_cast<int>(Pantalla::Alto));
}

void Administrador_Recursos::actualizar_pantalla(float nuevo_ancho, float nuevo_alto)
{
	glScissor(0, 0, static_cast<int>(nuevo_ancho), static_cast<int>(nuevo_alto));
	m_matriz_proyeccion = glm::ortho(0.0f, nuevo_ancho, nuevo_alto, 0.0f, -1.0f, 1.0f);

	for(std::map<SombreadorVF, Sombreador*>::iterator e=m_lista_sombreadores.begin(); e != m_lista_sombreadores.end(); e++)
	{
		e->second->uniforme_matriz4("proyeccion", m_matriz_proyeccion);
	}
}
