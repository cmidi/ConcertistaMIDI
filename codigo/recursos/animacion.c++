#include "animacion.h++"
#include <cmath>

Animacion::Animacion()
{
	m_tiempo_actual = 0;
	m_tiempo_total = 1;
	m_valor = 0.0;
	m_termino_reciente = false;
	m_iniciado = false;
	m_modo_animacion = Modo_Animacion::Lineal;
	m_direccion_animacion = Direccion_Animacion::Incremento;
}

Animacion::~Animacion()
{
}

void Animacion::modo(Modo_Animacion modo)
{
	m_modo_animacion = modo;
}

void Animacion::tiempo(unsigned long int tiempo_total)
{
	//Tiempo en nanosegundos
	m_tiempo_total = tiempo_total;

	//Con un valor de cero, la funcion valor retornaria siempre cero
	if(m_tiempo_total == 0)
		m_tiempo_total = 1;
}

void Animacion::direccion(Direccion_Animacion direccion)
{
	m_direccion_animacion = direccion;

	if(m_direccion_animacion == Direccion_Animacion::Incremento)
		m_valor = 0.0;
	else if(m_direccion_animacion == Direccion_Animacion::Decremento)
		m_valor = 1.0;
}

void Animacion::actualizar(unsigned int diferencia_tiempo)
{
	if(!m_iniciado)
		return;

	if(m_termino_reciente)
	{
		m_termino_reciente = false;
		m_iniciado = false;
		return;
	}

	m_tiempo_actual += diferencia_tiempo;
	if(m_tiempo_actual > m_tiempo_total)
		m_tiempo_actual = m_tiempo_total;

	//Animacion Lineal
	m_valor = static_cast<double>(m_tiempo_actual) / static_cast<double>(m_tiempo_total);

	if(m_modo_animacion == Modo_Animacion::Suavizado)
		m_valor = -(cos(M_PI * m_valor)-1)/2.0;
	else if(m_modo_animacion == Modo_Animacion::Suavizado_Entrada)
		m_valor = 1 - cos((M_PI * m_valor)/2.0);
	else if(m_modo_animacion == Modo_Animacion::Suavizado_Salida)
		m_valor = sin((M_PI * m_valor)/2.0);

	if(m_direccion_animacion == Direccion_Animacion::Incremento)
	{
		if(m_valor >= 1.0)
		{
			m_termino_reciente = true;
			m_valor = 1.0;
		}
	}
	else if(m_direccion_animacion == Direccion_Animacion::Decremento)
	{
		m_valor = 1.0 - m_valor;
		if(m_valor < 0.0)
		{
			m_termino_reciente = true;
			m_valor = 0.0;
		}
	}
}

double Animacion::valor()
{
	return m_valor;
}

float Animacion::valor_f()
{
	return static_cast<float>(m_valor);
}

bool Animacion::iniciado()
{
	return m_iniciado;
}

void Animacion::iniciar()
{
	m_iniciado = true;
}

void Animacion::detener()
{
	m_iniciado = false;
}

void Animacion::reiniciar()
{
	m_tiempo_actual = 0;

	if(m_direccion_animacion == Direccion_Animacion::Incremento)
		m_valor = 0.0;
	else if(m_direccion_animacion == Direccion_Animacion::Decremento)
		m_valor = 1.0;
}
