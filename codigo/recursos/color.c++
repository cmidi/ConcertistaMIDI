#include "color.h++"
#include "../util/funciones.h++"

Color::Color()
{
	this->establecer_valores(0.0f, 0.0f, 0.0f, 1.0f);
}

Color::Color(const Color &color)
{
	m_rojo = color.m_rojo;
	m_verde = color.m_verde;
	m_azul = color.m_azul;
	m_alfa = color.m_alfa;
}

Color::Color(int rojo, int verde, int azul)
{
	this->establecer_valores(static_cast<float>(rojo) / 254.0f, static_cast<float>(verde) / 254.0f, static_cast<float>(azul) / 254.0f, 1.0);
}

Color::Color(float rojo, float verde, float azul)
{
	this->establecer_valores(rojo, verde, azul, 1.0f);
}

Color::Color(int rojo, int verde, int azul, int alfa)
{
	this->establecer_valores(static_cast<float>(rojo) / 254.0f, static_cast<float>(verde) / 254.0f, static_cast<float>(azul) / 254.0f, static_cast<float>(alfa) / 254.0f);
}

Color::Color(float rojo, float verde, float azul, float alfa)
{
	this->establecer_valores(rojo, verde, azul, alfa);
}

Color::~Color()
{
}

float Color::valor_limitado(float valor)
{
	//Limita el valor entre 0 y 1
	if(valor < 0.0f)
		return 0.0f;
	else if(valor > 1.0f)
		return 1.0f;
	else
		return valor;
}

void Color::establecer_valores(float rojo, float verde, float azul, float alfa)
{
	m_rojo = this->valor_limitado(rojo);
	m_verde = this->valor_limitado(verde);
	m_azul = this->valor_limitado(azul);
	m_alfa = this->valor_limitado(alfa);
}

bool Color::comparar(float valor_1, float valor_2) const
{
	int resultado_1 = static_cast<int>(valor_1 * 254.0f);
	int resultado_2 = static_cast<int>(valor_2 * 254.0f);
	return resultado_1 == resultado_2;
}

void Color::color(int rojo, int verde, int azul)
{
	this->establecer_valores(static_cast<float>(rojo) / 254.0f, static_cast<float>(verde) / 254.0f, static_cast<float>(azul) / 254.0f, 1.0f);
}

void Color::color(float rojo, float verde, float azul)
{
	this->establecer_valores(rojo, verde, azul, 1.0f);
}

void Color::color(int rojo, int verde, int azul, int alfa)
{
	this->establecer_valores(static_cast<float>(rojo) / 254.0f, static_cast<float>(verde) / 254.0f, static_cast<float>(azul) / 254.0f, static_cast<float>(alfa) / 254.0f);
}

void Color::color(float rojo, float verde, float azul, float alfa)
{
	this->establecer_valores(rojo, verde, azul, alfa);
}

float Color::rojo()
{
	return m_rojo;
}

float Color::verde()
{
	return m_verde;
}

float Color::azul()
{
	return m_azul;
}

float Color::alfa()
{
	return m_alfa;
}

void Color::rojo(float valor)
{
	m_rojo = this->valor_limitado(valor);
}

void Color::verde(float valor)
{
	m_verde = this->valor_limitado(valor);
}

void Color::azul(float valor)
{
	m_azul = this->valor_limitado(valor);
}

void Color::alfa(float valor)
{
	m_alfa = this->valor_limitado(valor);
}

void Color::cambiar_luminosidad(float valor)
{
	//Combierte RVA a MSL
	float color_maximo = 0.0f;
	float color_minimo = 1.0f;
	unsigned char maximo = 0;//1->Rojo, 2->Verde, 3->Azul

	//Encuentra el color con valor mas alto
	if(color_maximo < m_rojo)
	{
		color_maximo = m_rojo;
		maximo = 1;
	}
	if(color_maximo < m_verde)
	{
		color_maximo = m_verde;
		maximo = 2;
	}
	if(color_maximo < m_azul)
	{
		color_maximo = m_azul;
		maximo = 3;
	}

	//Encuentra el color con valor mas bajo
	if(color_minimo > m_rojo)
		color_minimo = m_rojo;
	if(color_minimo > m_verde)
		color_minimo = m_verde;
	if(color_minimo > m_azul)
		color_minimo = m_azul;

	float delta = color_maximo - color_minimo;
	float m = 0.0f;
	float s = 0.0f;
	float l = 0.0f;
	if(Funciones::float_son_iguales(delta, 0, 0.001f))
		m = 0;
	else if(maximo == 1)
		m = static_cast<float>(fmod(static_cast<double>(60.0f * ((m_verde - m_azul) / delta) + 360.0f), 360.0));
	else if(maximo == 2)
		m = 60.0f * ((m_azul - m_rojo) / delta) + 120.0f;
	else if(maximo == 3)
		m = 60.0f * ((m_rojo - m_verde) / delta) + 240.0f;

	l = (color_maximo + color_minimo) / 2;

	if(Funciones::float_son_iguales(delta, 0, 0.001f))
		s = 0.0f;
	else
	{
		if(l <= 0.5f)
			s = delta / (2.0f * l);
		else
			s = delta / (2.0f - (2.0f * l));
	}
	//Ahora tenemos el color en formato msl
	//Se le resta a la luminosidad el valor


	//Ahora volver se Combierte MSL a RVA
	if(m < 0.0f)
		m = 0.0f;
	else if(m > 360)
		m = 360.0f;
	s = this->valor_limitado(s);
	l = this->valor_limitado(l+valor);

	float croma = (1.0f - std::abs(2.0f*l - 1.0f)) * s;
	float m2 = m / 60.0f;
	float x = croma * (1.0f - std::abs(static_cast<float>(fmod(static_cast<double>(m2), 2.0)) - 1.0f));

	float rojo = 0;
	float verde = 0;
	float azul = 0;

	if(m2 <= 1)
	{
		rojo = croma;
		verde = x;
		azul = 0;
	}
	else if(m2 > 1 && m2 <= 2)
	{
		rojo = x;
		verde = croma;
		azul = 0;
	}
	else if(m2 > 2 && m2 <= 3)
	{
		rojo = 0;
		verde = croma;
		azul = x;
	}
	else if(m2 > 3 && m2 <= 4)
	{
		rojo = 0;
		verde = x;
		azul = croma;
	}
	else if(m2 > 4 && m2 <= 5)
	{
		rojo = x;
		verde = 0;
		azul = croma;
	}
	else if(m2 > 5 && m2 <= 6)
	{
		rojo = croma;
		verde = 0;
		azul = x;
	}

	float y = l - (croma / 2.0f);
	m_rojo = rojo+y;
	m_verde = verde+y;
	m_azul = azul+y;
}

bool Color::operator == (const Color &c) const
{
	return comparar(m_rojo, c.m_rojo) && comparar(m_verde, c.m_verde) && comparar(m_azul, c.m_azul) && comparar(m_alfa, c.m_alfa);
}

bool Color::operator != (const Color &c) const
{
	return !comparar(m_rojo, c.m_rojo) || !comparar(m_verde, c.m_verde) || !comparar(m_azul, c.m_azul) || !comparar(m_alfa, c.m_alfa);
}

Color& Color::operator = (const Color &c)
{
	if(this != &c)
	{
		m_rojo = c.m_rojo;
		m_verde = c.m_verde;
		m_azul = c.m_azul;
		m_alfa = c.m_alfa;
	}
	return *this;
}

Color& Color::operator + (const Color &c)
{
	m_rojo = this->valor_limitado(m_rojo + c.m_rojo);
	m_verde = this->valor_limitado(m_verde + c.m_verde);
	m_azul = this->valor_limitado(m_azul + c.m_azul);
	m_alfa = this->valor_limitado(m_alfa + c.m_alfa);
	return *this;
}

Color& Color::operator - (const Color &c)
{
	m_rojo = this->valor_limitado(m_rojo - c.m_rojo);
	m_verde = this->valor_limitado(m_verde - c.m_verde);
	m_azul = this->valor_limitado(m_azul - c.m_azul);
	m_alfa = this->valor_limitado(m_alfa - c.m_alfa);
	return *this;
}

Color& Color::operator + (const float &valor)
{
	m_rojo = this->valor_limitado(m_rojo + valor);
	m_verde = this->valor_limitado(m_verde + valor);
	m_azul = this->valor_limitado(m_azul + valor);
	return *this;
}

Color& Color::operator - (const float &valor)
{
	m_rojo = this->valor_limitado(m_rojo - valor);
	m_verde = this->valor_limitado(m_verde - valor);
	m_azul = this->valor_limitado(m_azul - valor);
	return *this;
}

Color Color::MSL_a_RVA(float m, float s, float l, float alfa)
{
	if(m < 0)
		m = 0.0f;
	else if(m > 360)
		m = 360.0f;

	if(s < 0)
		s = 0.0f;
	else if(s > 1.0f)
		s = 1.0f;

	if(l < 0)
		l = 0.0f;
	else if(l > 1.0f)
		l = 1.0f;

	float croma = (1.0f - std::abs(2.0f*l - 1.0f)) * s;
	float m2 = m / 60.0f;
	float x = croma * (1.0f - std::abs(static_cast<float>(fmod(static_cast<double>(m2), 2.0)) - 1.0f));

	float rojo = 0;
	float verde = 0;
	float azul = 0;

	if(m2 <= 1)
	{
		rojo = croma;
		verde = x;
		azul = 0;
	}
	else if(m2 > 1 && m2 <= 2)
	{
		rojo = x;
		verde = croma;
		azul = 0;
	}
	else if(m2 > 2 && m2 <= 3)
	{
		rojo = 0;
		verde = croma;
		azul = x;
	}
	else if(m2 > 3 && m2 <= 4)
	{
		rojo = 0;
		verde = x;
		azul = croma;
	}
	else if(m2 > 4 && m2 <= 5)
	{
		rojo = x;
		verde = 0;
		azul = croma;
	}
	else if(m2 > 5 && m2 <= 6)
	{
		rojo = croma;
		verde = 0;
		azul = x;
	}

	float y = l - (croma / 2.0f);

	return Color(rojo+y, verde+y, azul+y, alfa);
}
