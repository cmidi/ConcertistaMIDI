#ifndef ANIMACION_H
#define ANIMACION_H

enum class Modo_Animacion : unsigned char
{
	Lineal,
	Suavizado,
	Suavizado_Entrada,
	Suavizado_Salida,
};

enum class Direccion_Animacion : unsigned char
{
	Incremento,
	Decremento,
};

class Animacion
{
private:
	Modo_Animacion m_modo_animacion;
	Direccion_Animacion m_direccion_animacion;
	unsigned long int m_tiempo_total;
	unsigned long int m_tiempo_actual;
	double m_valor;
	bool m_termino_reciente;
	bool m_iniciado;
public:
	Animacion();
	~Animacion();

	void modo(Modo_Animacion modo);
	void tiempo(unsigned long int tiempo_total);
	void direccion(Direccion_Animacion direccion);

	void actualizar(unsigned int diferencia_tiempo);

	bool iniciado();
	void iniciar();
	void detener();
	void reiniciar();

	double valor();
	float valor_f();
};

#endif
