#ifndef ADMINISTRADOR_DE_RECURSOS_H
#define ADMINISTRADOR_DE_RECURSOS_H

#include <map>
#include <array>
#include <stack>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "sombreador.h++"
#include "textura_2d.h++"
#include "tipografia.h++"
#include "color.h++"
#include "rectangulo.h++"

#include "../archivos/archivo.h++"
#include "../archivos/imagen_tga.h++"
#include "../dispositivos/pantalla.h++"

#include "../registro.h++"
#include "configuracion_cmake.h++"

enum class Textura : unsigned char
{
	Indeterminado,
	FondoTitulo,
	Titulo,
	Boton,
	TeclaBlanca,
	TeclaNegra,
	TeclaBlancaPresionada,
	TeclaBlancaPresionadaDoble,
	TeclaNegraPresionada,
	Circulo,
	BordeOrganoRojo,
	BordeOrganoNegro,
	Nota,
	NotaResaltada,
	Sombra,
	FrenteBarraProgreso,
	ParticulaNota,
	Barra,
	Sonido_0,
	Sonido_1,
	Sonido_2,
	Sonido_3,
	Reproducir,
	Pausar,
	TituloMusica,
	ColorPista,
	Tocar,
	Aprender,
	MusicaFondo,
	CasillaInactiva,
	CasillaActiva,
	CasillaSombra,
	FlechaIzquierda,
	FlechaDerecha,
	FlechaAbajo,
	FlechaArriba,
	ControlDeslizante_H_Fondo,
	ControlDeslizante_H_Relleno,
	ControlDeslizante_V_Fondo,
	ControlDeslizante_V_Relleno,
	ControlDeslizante_Boton,
	Marcador_1,
	Marcador_2,
	Marcador_3,
	Marcador_4,
	MiniMarcador_1,
	MiniMarcador_2,
	MiniMarcador_3,
	MiniMarcador_4,
	InformacionNotaFondo,
	InformacionNotaBorde,
	Carpeta,
	ArchivoMIDI_1,
	ArchivoMIDI_2,
	ArchivoRMI,
	ArchivoKAR,
	ArchivoSTY,
};

enum class SombreadorVF : unsigned char
{
	Rectangulo,
	Texto,
	Particula,
};

enum class FiguraGeometrica : unsigned char
{
	Rectangulo,
};

enum class ModoLetra : unsigned int
{
	MuyGrande = 351,
	MuyGrandeNegrita = 352,
	Grande = 20,
	Mediana = 14,
	Chica = 12,
	MuyChica = 8,
};

class Administrador_Recursos
{
private:
	std::map<Textura, Textura2D*> m_lista_texturas;
	std::map<SombreadorVF, Sombreador*> m_lista_sombreadores;
	std::map<FiguraGeometrica, Rectangulo*> m_lista_figuras;

	std::map<Textura, std::string> m_archivo_texturas;
	std::map<SombreadorVF, std::string> m_archivo_sombreador_vertice;
	std::map<SombreadorVF, std::string> m_archivo_sombreador_fragmento;

	std::map<ModoLetra, Tipografia*> m_formato_letras;

	std::stack<std::array<int, 4>> m_recortes;
	glm::mat4 m_matriz_proyeccion;
public:
	Administrador_Recursos();
	~Administrador_Recursos();

	Textura2D *textura(Textura valor);
	Sombreador *sombreador(SombreadorVF valor);
	Rectangulo *figura(FiguraGeometrica valor);
	Tipografia *tipografia(ModoLetra tipo);

	void recortar_pantalla(float x, float y, float ancho, float alto);
	void revertir_recorte();

	void actualizar_pantalla(float ancho, float alto);
};

#endif
