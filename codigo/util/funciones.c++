#include "funciones.h++"

namespace Funciones
{
	std::string microsegundo_a_texto(std::int64_t us, bool mostrar_vacio, bool mostrar_ms)
	{
		int seg  = static_cast<int>(us / 1000000);
		int horas = seg / 3600;
		int minutos = (seg - (horas * 3600)) / 60;
		int segundos =  (seg - (horas * 3600 + minutos * 60));


		std::string tminutos, tsegundos;
		if(minutos < 10)
			tminutos = "0" + std::to_string(minutos);
		else
			tminutos = std::to_string(minutos);

		if(segundos < 10)
			tsegundos = "0" + std::to_string(segundos);
		else
			tsegundos = std::to_string(segundos);

		std::string salida;
		if(!mostrar_vacio && horas == 0)
			salida = tminutos + ":" + tsegundos;
		else
			salida = std::to_string(horas) + ":" + tminutos + ":" + tsegundos;

		if(mostrar_ms)
		{
			std::string tmilisegundos;
			int milisegundos = static_cast<int>(us / 1000);
			milisegundos -= (horas * 3600000) +( minutos * 60000) + (segundos * 1000);

			if(milisegundos < 10)
				tmilisegundos = "00" + std::to_string(milisegundos);
			else if(milisegundos < 100)
				tmilisegundos = "0" + std::to_string(milisegundos);
			else
				tmilisegundos = std::to_string(milisegundos);

			salida += "." + tmilisegundos;
		}

		return salida;
	}

	std::string fecha_actual()
	{
		std::time_t tiempo = std::time(0);
		std::tm *ahora = std::localtime(&tiempo);

		std::string fecha_actual = std::to_string(ahora->tm_year + 1900);;

		if(ahora->tm_mon < 9)
			fecha_actual += "-0" + std::to_string(ahora->tm_mon+1);
		else
			fecha_actual += "-" + std::to_string(ahora->tm_mon+1);

		if(ahora->tm_mday < 10)
			fecha_actual += "-0" + std::to_string(ahora->tm_mday);
		else
			fecha_actual += "-" + std::to_string(ahora->tm_mday);

		if(ahora->tm_hour < 10)
			fecha_actual += " 0" + std::to_string(ahora->tm_hour);
		else
			fecha_actual += " " + std::to_string(ahora->tm_hour);

		if(ahora->tm_min < 10)
			fecha_actual += ":0" + std::to_string(ahora->tm_min);
		else
			fecha_actual += ":" + std::to_string(ahora->tm_min);

		if(ahora->tm_sec < 10)
			fecha_actual += ":0" + std::to_string(ahora->tm_sec);
		else
			fecha_actual += ":" + std::to_string(ahora->tm_sec);

		return fecha_actual;
	}

	bool float_son_iguales(float valor1, float valor2, float diferencia_minima)
	{
		if(valor1 < valor2 + diferencia_minima && valor1 > valor2 - diferencia_minima)
			return true;
		return false;
	}

	bool double_son_iguales(double valor1, double valor2, double diferencia_minima)
	{
		if(valor1 < valor2 + diferencia_minima && valor1 > valor2 - diferencia_minima)
			return true;
		return false;
	}

	std::string nombre_archivo(const std::string &ruta, bool carpeta)
	{
		std::uint64_t extencion = ruta.length()-1;
		for(std::uint64_t i=ruta.length(); i>0; i--)
		{
			//Encuentra el primer punto
			if(extencion == ruta.length()-1 && ruta[i-1] == '.')
				extencion = i-1;
			//Inicio del nombre del archivo
			if(ruta[i-1] == '/' && i-1 < ruta.length()-1)
			{
				if(carpeta)
					return ruta.substr(i);
				//Es archivo
				if(i-1 < extencion && extencion != ruta.length()-1)
					return ruta.substr(i, extencion-(i));
			}
		}
		return "";
	}

	std::string extencion_archivo(const std::string &nombre)
	{
		for(std::uint64_t i=nombre.length()-1; i>0; i--)
		{
			if(nombre[i] == '.' && i < nombre.length()-1)
				return nombre.substr(i+1);
		}
		return "";
	}

	TipoArchivo tipo_archivo(const std::string &extencion)
	{
		if(extencion.length() == 3 &&
			(extencion[0] == 'M' || extencion[0] == 'm') &&
			(extencion[1] == 'I' || extencion[1] == 'i') &&
			(extencion[2] == 'D' || extencion[2] == 'd'))
			return TipoArchivo::MIDI_1;
		if(extencion.length() == 3 &&
			(extencion[0] == 'R' || extencion[0] == 'r') &&
			(extencion[1] == 'M' || extencion[1] == 'm') &&
			(extencion[2] == 'I' || extencion[2] == 'i'))
			return TipoArchivo::RMID;
		if(extencion.length() == 3 &&
			(extencion[0] == 'K' || extencion[0] == 'k') &&
			(extencion[1] == 'A' || extencion[1] == 'a') &&
			(extencion[2] == 'R' || extencion[2] == 'r'))
			return TipoArchivo::KAR;
		else if(extencion.length() == 3 &&//Espesificación de archivo de estilo Yamaha
			(extencion[0] == 'S' || extencion[0] == 's') &&
			(extencion[1] == 'T' || extencion[1] == 't') &&
			(extencion[2] == 'Y' || extencion[2] == 'y'))
			return TipoArchivo::STY;
		else if(extencion.length() == 4 &&
			(extencion[0] == 'M' || extencion[0] == 'm') &&
			(extencion[1] == 'I' || extencion[1] == 'i') &&
			(extencion[2] == 'D' || extencion[2] == 'd') &&
			(extencion[3] == 'I' || extencion[3] == 'i'))
			return TipoArchivo::MIDI_1;
		else if(extencion.length() == 5 &&
			(extencion[0] == 'M' || extencion[0] == 'm') &&
			(extencion[1] == 'I' || extencion[1] == 'i') &&
			(extencion[2] == 'D' || extencion[2] == 'd') &&
			(extencion[3] == 'I' || extencion[3] == 'i') &&
			(extencion[4] == '2' || extencion[4] == '2'))
			return TipoArchivo::MIDI_2;
		return TipoArchivo::NoEsMIDI;
	}

	bool es_midi(const std::string &extencion)
	{
		if(Funciones::tipo_archivo(extencion) != TipoArchivo::NoEsMIDI)
			return true;
		return false;
	}

	unsigned int numero_de_archivos(const std::string &carpeta)
	{
		unsigned int archivos = 0;
		if(!std::ifstream(carpeta))
			return 0;
		for(const std::filesystem::directory_entry &elemento : std::filesystem::directory_iterator(carpeta))
		{
			try
			{
				std::string ruta = std::string(elemento.path());
				std::string nombre_archivo = Funciones::nombre_archivo(ruta, elemento.is_directory());
				std::string extencion = Funciones::extencion_archivo(ruta);

				bool oculto = false;
				if(nombre_archivo.length() > 0 && nombre_archivo[0] == '.')
					oculto = true;

				if((elemento.is_directory() && !oculto) || Funciones::es_midi(extencion))
					archivos++;
			}
			catch(std::filesystem::filesystem_error &e)
			{
			}
		}
		return archivos;
	}
}
