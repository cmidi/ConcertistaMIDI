#ifdef __linux__
#include <unistd.h>//getuid
#include <pwd.h>//getpwuid
#elif _WIN32
#include <shlobj.h>//SHGetKnownFolderPath
#include <atlstr.h>//CString y CT2CA
#if UNICODE
#include <codecvt>//std::wstring a std::string
#endif
#endif
#include "usuario.h++"

namespace Usuario
{
	std::string carpeta_personal()
	{
	#ifdef __linux__
		const char *carpeta = 0;
		carpeta = getenv("HOME");
		if(carpeta == 0)
		{
			struct passwd *usuario = getpwuid(getuid());
			if(usuario)
				carpeta = usuario->pw_dir;
		}
		return std::string(carpeta);
	#elif _WIN32
		PWSTR carpeta;
		HRESULT respuesta = SHGetKnownFolderPath(FOLDERID_Profile, 0, NULL, &carpeta);
		if (respuesta != S_OK)
			Registro::Error(T("No se pudo obtener la carpeta personal"));
		CString salida(carpeta);
		CT2CA convertidor(salida);
		return std::string(convertidor);
	#endif
	}

	std::string carpeta_juego()
	{
	#ifdef __linux__
		return carpeta_personal() + "/.local/share/concertistamidi/";
	#elif _WIN32
		#if UNICODE
		//Unicode usa wchar_t
		TCHAR ruta[MAX_PATH];
		SHGetFolderPath(NULL, CSIDL_APPDATA, NULL, 0, ruta);
		std::wstring ruta_w(ruta);
		std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t> convertidor;

		return convertidor.to_bytes(ruta_w) + "/concertistamidi/";
		#else
		//Usa char facil de convertir a std::string
		TCHAR ruta[MAX_PATH];
		SHGetFolderPath(NULL, CSIDL_APPDATA, NULL, 0, ruta);
		return std::string(ruta) + "/concertistamidi/";
		#endif
	#endif
	}

}
