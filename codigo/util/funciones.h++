#ifndef FUNCIONES_H
#define FUNCIONES_H

#include <string>
#include <vector>
#include <ctime>
#include <filesystem>
#include <fstream>

enum class TipoArchivo : unsigned char
{
	NoEsMIDI,
	MIDI_1,
	MIDI_2,
	RMID,
	KAR,
	STY,
};

namespace Funciones
{
	std::string microsegundo_a_texto(std::int64_t us, bool mostrar_vacio, bool mostrar_ms);
	std::string fecha_actual();

	bool float_son_iguales(float valor1, float valor2, float diferencia_minima);
	bool double_son_iguales(double valor1, double valor2, double diferencia_minima);

	std::string nombre_archivo(const std::string &ruta, bool carpeta);
	std::string extencion_archivo(const std::string &nombre);
	TipoArchivo tipo_archivo(const std::string &extencion);
	bool es_midi(const std::string &extencion);
	unsigned int numero_de_archivos(const std::string &carpeta);
}

#endif
