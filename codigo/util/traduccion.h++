#ifndef TRADUCCION_H
#define TRADUCCION_H

#include <string>
#include <format>
#include "../registro.h++"

void configurar_traduccion();

std::string N(const std::string &texto);
std::string T(const std::string &texto);
char* TC(const char* texto);

template<typename... Argumentos>
std::string T_(const std::string &texto, Argumentos&&... a)
{
	try
	{
		return std::vformat(gettext(texto.c_str()), std::make_format_args(a...));
	}
	catch (std::format_error &e)
	{
		Registro::Error(T("Error en la traduccion, el numero de parametros no coincide en la cadena:") + " " + texto);
		return texto;
	}
}


#endif
