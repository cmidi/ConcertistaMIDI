#include "traduccion.h++"
#include "configuracion_cmake.h++"

#include <libintl.h>
#include <locale.h>

void configurar_traduccion()
{
	std::string ruta = std::string(RUTA_ARCHIVOS) + "/traducciones/";
    setlocale(LC_ALL, "");
	setlocale(LC_NUMERIC, "C");
	bindtextdomain("concertistamidi", ruta.c_str());
	bind_textdomain_codeset("concertistamidi", "UTF-8");
	textdomain("concertistamidi");
}

//No hace nada, solo es para marcar la traduccion
std::string N(const std::string &texto)
{
	return texto;
}

std::string T(const std::string &texto)
{
    return gettext(texto.c_str());
}

char* TC(const char* texto)
{
	return gettext(texto);
}
