#include "texto.h++"
#include "traduccion.h++"

namespace Texto
{
	std::vector<std::string> dividir_texto(const std::string &texto, char caracter)
	{
		std::vector<std::string> dividido;
		std::string temporal;
		for(std::uint64_t x = 0; x<texto.length(); x++)
		{
			if(texto[x] == caracter)
			{
				if(temporal.length() > 0)
					dividido.push_back(temporal);
				temporal.clear();
			}
			else
			{
				temporal += texto[x];
			}
		}
		if(temporal.length() > 0)
			dividido.push_back(temporal);
		return dividido;
	}

	std::string convertir_a_utf8(const char *entrada, int largo_entrada, const char *nombre_codificacion)
	{
		UErrorCode  estado = U_ZERO_ERROR;
		UConverter *convertidor = ucnv_open(nombre_codificacion, &estado);

		//Se calcula el tamaño de la nueva cadena
		int32_t largo = ucnv_toUChars(convertidor, nullptr, 0, entrada, largo_entrada, &estado);
		//Se omite el error evidente de desbordamiento, no conozco otra forma de calcular el nuevo tamaño
		if(U_FAILURE(estado) && estado != U_BUFFER_OVERFLOW_ERROR)
		{
			Registro::Error(T_("Fallo la conversion: {0}", std::string(u_errorName(estado))));
			ucnv_close(convertidor);
			return "";
		}

		//Se construye la nueva cadena
		UChar *cadena_salida = new UChar[static_cast<std::uint64_t>(largo)+1];
		estado = U_ZERO_ERROR;
		ucnv_toUChars(convertidor, cadena_salida, largo, entrada, largo_entrada, &estado);
		if(U_FAILURE(estado))
		{
			Registro::Error(T_("Fallo la conversion: {0}", std::string(u_errorName(estado))));
			ucnv_close(convertidor);
			delete[] cadena_salida;
			return "";
		}

		//Se convierte a un string
		std::string resultado;
		icu::UnicodeString unicode = icu::UnicodeString(cadena_salida, largo);
		unicode.toUTF8String(resultado);

		ucnv_close(convertidor);
		delete[] cadena_salida;

		return resultado;
	}

	bool esta_vacio(const std::string &texto)
	{
		//Verifica si la cadena de texto esta vacia
		for(std::uint64_t x=0; x<texto.length(); x++)
		{
			if(texto[x] != ' ' && texto[x] != '\t' && texto[x] != '\r' && texto[x] != '\n')
				return false;
		}
		return true;
	}

	std::string bytes_a_texto(std::uint64_t tamanno)
	{
		if(tamanno < 1024)
			return std::to_string(tamanno) + " B";
		else if (tamanno >= 1024 && tamanno < 1024*1024)
			return std::to_string(tamanno/1024) + " KB";
		else if (tamanno >= (1024*1024) && tamanno < 1024*1024*1024)
			return std::to_string(tamanno/(1024*1024)) + " MB";
		else
			return std::to_string(tamanno/(1024*1024*1024)) + " GB";
	}

	std::string primera_letra_mayuscula(const std::string &texto)
	{
		icu::UnicodeString texto_transformado = texto.c_str();
		texto_transformado.toTitle(0).trim();

		std::string resultado = "";
		texto_transformado.toUTF8String(resultado);

		return resultado;
	}

	std::string quitar_espacios_en_extremos(std::string texto)
	{
		//Quita los espacios en blanco al principio y al final
		texto.erase(0, texto.find_first_not_of(" \t\n\r"));//Limpia el comienzo
		texto.erase(texto.find_last_not_of(" \t\n\r")+1);//Limpia el final
		return texto;
	}

	bool contiene_caracter(const std::string &texto, char caracter)
	{
		for(std::uint64_t x = 0; x<texto.length(); x++)
		{
			if(texto[x] == caracter)
				return true;
		}
		return false;
	}

	std::string a_texto(char valor)
	{
		return std::to_string(valor);
	}

	std::string a_texto(unsigned char valor)
	{
		return std::to_string(static_cast<unsigned int>(valor));
	}

	std::string a_texto(short valor)
	{
		return std::to_string(valor);
	}

	std::string a_texto(unsigned short valor)
	{
		return std::to_string(static_cast<unsigned int>(valor));
	}

	std::string a_texto(int valor)
	{
		return std::to_string(valor);
	}

	std::string a_texto(unsigned int valor)
	{
		return std::to_string(valor);
	}

	std::string bits_a_texto(unsigned char valor)
	{
		std::string salida;
		unsigned char comparador = 1 << 7;
		for(unsigned int x=0; x<8; x++)
		{
			if((valor & comparador) == 0)
				salida += "0";
			else
				salida += "1";

			if((x+1) % 8 == 0)
				salida += " ";

			comparador = comparador >> 1;
		}

		return salida;
	}

	std::string bits_a_texto(unsigned short valor)
	{
		std::string salida;
		unsigned short comparador = 1 << 15;
		for(unsigned int x=0; x<16; x++)
		{
			if((valor & comparador) == 0)
				salida += "0";
			else
				salida += "1";

			if((x+1) % 8 == 0)
				salida += " ";

			comparador = comparador >> 1;
		}

		return salida;
	}

	std::string bits_a_texto(unsigned int valor)
	{
		std::string salida;
		unsigned int comparador = static_cast<unsigned int>(1) << 31;
		for(unsigned int x=0; x<32; x++)
		{
			if((valor & comparador) == 0)
				salida += "0";
			else
				salida += "1";

			if((x+1) % 8 == 0)
				salida += " ";

			comparador = comparador >> 1;
		}

		return salida;
	}

	std::string bits_a_texto(std::uint64_t valor)
	{
		std::string salida;
		std::uint64_t comparador = static_cast<std::uint64_t>(1) << 63;
		for(unsigned int x=0; x<64; x++)
		{
			if((valor & comparador) == 0)
				salida += "0";
			else
				salida += "1";

			if((x+1) % 8 == 0)
				salida += " ";

			comparador = comparador >> 1;
		}

		return salida;
	}

	std::string hexadecimal_a_texto(std::uint64_t valor, bool prefijo)
	{
		if(valor == 0)
		{
			if(prefijo)
				return "0x00";
			return "00";
		}
		std::string salida;
		char numeros_hexadecimales[16] = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};

		std::uint64_t actual = valor;
		unsigned int contador = 0;
		while(actual > 0)
		{
			if(contador == 2)
			{
				contador = 0;
				if(prefijo)
					salida = " 0x" + salida;
				else
					salida = " " + salida;
			}
			contador++;
			salida = numeros_hexadecimales[actual % 16] + salida;
			actual = (actual / 16);
		}
		if(prefijo)
		{
			if(contador == 2)
				salida = "0x" + salida;
			else
				salida = "0x0" + salida;
		}
		else if(contador != 2)
			salida = "0" + salida;

		return salida;
	}
}
