#include "registro.h++"

#include "util/funciones.h++"
#include "util/texto.h++"
#include "util/usuario.h++"
#include "util/traduccion.h++"
#include "configuracion_cmake.h++"
#include <iostream>
#include <fstream>

std::mutex Registro::Bloqueo;

void Registro::Escribir_registro(CodigoEstado estado, std::string texto)
{
	if((estado == CodigoEstado::Aviso && NIVEL_REGISTRO < 1) ||
		(estado == CodigoEstado::Nota && NIVEL_REGISTRO < 2) ||
		(estado == CodigoEstado::Depurar && NIVEL_REGISTRO < 3))
		return;

	Registro::Bloqueo.lock();
	std::ofstream archivo;
	archivo.open(Usuario::carpeta_juego() + "registro.txt", std::ios::app);

	std::vector<std::string> lineas;
	if(Texto::contiene_caracter(texto, '\n'))
		lineas = Texto::dividir_texto(texto, '\n');
	else
		lineas.push_back(texto);


	std::string fecha = Funciones::fecha_actual();
	for(unsigned int x=0; x<lineas.size(); x++)
	{
		if(estado == CodigoEstado::Error)
		{
			archivo << "["<< fecha <<"][" << T("ERROR") << "] " << lineas[x] << "\n";
			std::cout << "\033[31m["<< fecha <<"][" << T("ERROR") << "]\033[0m " << lineas[x] << "\n" << std::flush;
		}
		else if(estado == CodigoEstado::Aviso && NIVEL_REGISTRO >= 1)
		{
			archivo << "["<< fecha <<"][" << T("AVISO") << "] " << lineas[x] << "\n";
			std::cout << "\033[33m["<< fecha <<"][" << T("AVISO") << "]\033[0m " << lineas[x] << "\n" << std::flush;
		}
		else if(estado == CodigoEstado::Nota && NIVEL_REGISTRO >= 2)
		{
			archivo << "["<< fecha <<"][" << T("NOTA") << "] " << lineas[x] << "\n";
			std::cout << "\033[32m["<< fecha <<"][" << T("NOTA") << "]\033[0m " << lineas[x] << "\n" << std::flush;
		}
		else if(estado == CodigoEstado::Depurar && NIVEL_REGISTRO >= 3)
		{
			archivo << "["<< fecha <<"][" << T("DEPURAR") << "] " << lineas[x] << "\n";
			std::cout << "\033[34m["<< fecha <<"][" << T("DEPURAR") << "]\033[0m " << lineas[x] << "\n" << std::flush;
		}
	}
	archivo.close();
	Registro::Bloqueo.unlock();
}

void Registro::Error(std::string texto)
{
	Escribir_registro(CodigoEstado::Error, texto);
}

void Registro::Aviso(std::string texto)
{
	Escribir_registro(CodigoEstado::Aviso, texto);
}

void Registro::Nota(std::string texto)
{
	Escribir_registro(CodigoEstado::Nota, texto);
}

void Registro::Depurar(std::string texto)
{
	Escribir_registro(CodigoEstado::Depurar, texto);
}
