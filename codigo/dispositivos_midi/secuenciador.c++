#include "secuenciador.h++"

Secuenciador::Secuenciador()
{
	m_midi_2_activo = false;
	m_cliente = 0;
	m_puerto_entrada = 0;
	m_puerto_salida = 0;
	m_puerto_virtual = 0;
	m_cambio_dispositivos = false;
}

Secuenciador::~Secuenciador()
{
}

unsigned char Secuenciador::cliente()
{
	return m_cliente;
}

bool Secuenciador::habilitar_midi_2()
{
	return m_midi_2_activo;
}
