#include "controlador_midi.h++"
#include "../dispositivos_midi/excepcion_dispositivo.h++"
#include "../util/traduccion.h++"

Controlador_Midi::Controlador_Midi()
{
	try
	{
		m_secuenciador = new Secuenciador_Alsa();
	}
	catch(const Excepcion_Dispositivo &e)
	{
		m_secuenciador = nullptr;
		Registro::Error(e.descripcion_error());
	}

	if(m_secuenciador != nullptr)
		m_secuenciador->crear_lista_dispositivos(m_dispositivos);

	m_necesario_restablecer = false;
	m_cambio_dispositivos = false;
	m_desconectado_pendiente = 0;

	m_seguidor_eventos = new Seguidor_Eventos(true);
}

Controlador_Midi::~Controlador_Midi()
{
	if(m_secuenciador != nullptr)
		delete m_secuenciador;

	delete m_seguidor_eventos;

	m_entrada.clear();
	m_salida.clear();

	for(unsigned int x=0; x<m_dispositivos.size(); x++)
		delete m_dispositivos[x];

	m_dispositivos.clear();
}

void Controlador_Midi::habilitar_midi_2(bool estado)
{
	if(m_secuenciador == nullptr)
		return;

	m_secuenciador->habilitar_midi_2(estado);
}

bool Controlador_Midi::habilitar_midi_2()
{
	if(m_secuenciador == nullptr)
		return false;

	return m_secuenciador->habilitar_midi_2();
}

Dispositivo_Midi *Controlador_Midi::dispositivo(unsigned char cliente, unsigned char puerto, bool conectado, bool exacto)
{
	//Encuentra un dispositivo que cumpla las condiciones establecidas
	if(m_secuenciador == nullptr)
		return nullptr;

	std::string nombre;
	if(!exacto)
		nombre = m_secuenciador->nombre_dispositivo(cliente, puerto);

	Dispositivo_Midi *dispositivo = nullptr;
	for(unsigned int x=0; x<m_dispositivos.size(); x++)
	{
		//El estado de conexion tiene que ser lo pedido
		if(m_dispositivos[x]->conectado() != conectado)
			continue;

		//Si ya lo encotro lo retorna, usado al desconectar cuando no se tiene el nombre
		if(exacto && m_dispositivos[x]->cliente() == cliente && m_dispositivos[x]->puerto() == puerto)
			return m_dispositivos[x];

		//Usado al conectar, puede haber otro dispositivo en su lugar por eso se verifica el nombre
		if(!exacto && m_dispositivos[x]->cliente() == cliente && m_dispositivos[x]->puerto() == puerto && m_dispositivos[x]->nombre() == nombre)
			return m_dispositivos[x];

		//Se encontro en otro puerto un dispositivo con el mismo nombre pero no se puede estar seguro hasta terminar
		if(!exacto && m_dispositivos[x]->puerto() == puerto && m_dispositivos[x]->nombre() == nombre)
			dispositivo = m_dispositivos[x];
	}

	if(dispositivo != nullptr)
	{
		//Actualiza el cliente porque esta en otra posición
		m_cambio_cliente[dispositivo] = dispositivo->cliente();
		dispositivo->cliente(cliente);
		return dispositivo;
	}

	return nullptr;
}

Dispositivo_Midi *Controlador_Midi::dispositivo_activo(unsigned char cliente, unsigned char puerto, unsigned char capacidad)
{
	//Encuentra un dispositivo conectado que cumpla las condiciones establecidas
	if(capacidad == ENTRADA)
	{
		for(unsigned int x=0; x<m_entrada.size(); x++)
		{
			if(m_entrada[x]->cliente() == cliente && m_entrada[x]->puerto() == puerto)
				return m_entrada[x];
		}
	}
	else if(capacidad == SALIDA)
	{
		for(unsigned int x=0; x<m_salida.size(); x++)
		{
			if(m_salida[x]->cliente() == cliente && m_salida[x]->puerto() == puerto)
				return m_salida[x];
		}
	}
	return nullptr;
}

void Controlador_Midi::reenviar_eventos(Dispositivo_Midi *dispositivo)
{
	//Envia los eventos activos al dispositivo que acaba de conectarse
	//porque es posible que se conecte despues de que empezo la musica
	if(m_secuenciador == nullptr || !m_seguidor_eventos->hay_eventos())
		return;

	m_seguidor_eventos->procesar_eventos();
	Evento_Midi *evento = m_seguidor_eventos->siguiente_evento();
	while(evento != nullptr)
	{
		this->escribir_evento_dispositivo(evento, dispositivo);
		evento = m_seguidor_eventos->siguiente_evento();
	}
}

void Controlador_Midi::escribir_evento_dispositivo(Evento_Midi *evento, Dispositivo_Midi *dispositivo)
{
	evento->cliente(dispositivo->cliente());
	evento->puerto(dispositivo->puerto());
	m_secuenciador->escribir(*evento);
}

void Controlador_Midi::nota_salida(unsigned int grupo, unsigned int canal, unsigned char id_nota, bool encendida)
{
	unsigned int clave = (grupo << 12) | (canal << 8) | (id_nota & 0x7F);
	if(encendida)//Nueva nota activada
		m_notas_activa_salida.push_back(clave);
	else
	{
		for(std::vector<unsigned int>::iterator i = m_notas_activa_salida.begin(); i != m_notas_activa_salida.end(); i++)
		{
			if(*i == clave)
			{
				m_notas_activa_salida.erase(i);
				return;
			}
		}
	}
}

void Controlador_Midi::apagar_luces(Dispositivo_Midi *dispositivo)
{
	if(m_secuenciador == nullptr)
		return;

	//Apaga las luces
	Teclas_Luminosas *teclado_actual = dispositivo->teclas_luminosas();
	if(teclado_actual != nullptr)
	{
		while(teclado_actual->quedan_luces_activas())
		{
			Evento_Midi evento = teclado_actual->apagar_siguiente();
			evento.cliente(dispositivo->cliente());
			evento.puerto(dispositivo->puerto());
			m_secuenciador->escribir(evento);
		}
	}
}

void Controlador_Midi::apagar_notas()
{
	if(m_secuenciador == nullptr)
		return;

	for(unsigned int x=0; x<m_salida.size(); x++)
		this->apagar_luces(m_salida[x]);

	//Envia un evento de nota apagada por cada nota encendida
	for(unsigned int x=0; x<m_notas_activa_salida.size(); x++)
	{
		unsigned int clave = m_notas_activa_salida[x];
		Evento_Midi evento(TipoMensaje_VozDeCanalMidi1, EventoMidi_NotaApagada);
		evento.grupo((clave >> 12) & 0x0F);
		evento.canal((clave >> 8) & 0x0F);
		evento.id_nota(clave & 0x7F);
		evento.cliente(DIRECCIONES_SUSCRITAS);
		evento.puerto(DIRECCIONES_SUSCRITAS);
		m_secuenciador->escribir(evento);
	}
	m_notas_activa_salida.clear();
}

void Controlador_Midi::conectar(Dispositivo_Midi *dispositivo)
{
	//Conecta o habilita el dispositivo
	//Habilitar el dispositivo significa que se puede usar si esta disponible y se conectara automaticamente
	//cuando lo este, de lo contrario sera ignorado.
	if(m_secuenciador == nullptr)
		return;

	if(!dispositivo->conectado() || !dispositivo->habilitado())
		return;

	if(dispositivo->entrada_activa())
	{
		if(m_secuenciador->conectar(dispositivo->cliente(), dispositivo->puerto(), TipoDispositivo::Entrada))
			m_entrada.push_back(dispositivo);
	}
	if(dispositivo->salida_activa())
	{
		if(m_secuenciador->conectar(dispositivo->cliente(), dispositivo->puerto(), TipoDispositivo::Salida))
		{
			dispositivo->reenviar_eventos(true);
			m_salida.push_back(dispositivo);
		}
	}
}

void Controlador_Midi::desconectar(Dispositivo_Midi *dispositivo)
{
	//Al perder la conexion ya no se puede desconectar del secuenciador
	if(m_secuenciador == nullptr)
		return;

	//Desconecta el dispositivo
	if(dispositivo->habilitado())
	{
		if(dispositivo->entrada_activa())
		{
			//Se desconecta el dispositivo de la entrada
			if(dispositivo->conectado())
				m_secuenciador->desconectar(dispositivo->cliente(), dispositivo->puerto(), TipoDispositivo::Entrada);

			//Se borra de los dispositivos activos
			bool borrado = false;
			for(unsigned int x=0; x<m_entrada.size() && !borrado; x++)
			{
				if(dispositivo == m_entrada[x])
				{
					m_entrada.erase(m_entrada.begin()+x);
					borrado = true;
				}
			}
		}

		if(dispositivo->salida_activa())
		{
			//Se desconecta el dispositivo de la salida
			if(dispositivo->conectado())
				m_secuenciador->desconectar(dispositivo->cliente(), dispositivo->puerto(), TipoDispositivo::Salida);

			//Se borra de los dispositivos activos
			bool borrado = false;
			for(unsigned int x=0; x<m_salida.size() && !borrado; x++)
			{
				if(dispositivo == m_salida[x])
				{
					m_salida.erase(m_salida.begin()+x);
					borrado = true;
				}
			}
		}
	}
}

void Controlador_Midi::eliminar(Dispositivo_Midi *dispositivo)
{
	if(!dispositivo->habilitado() && !dispositivo->conectado())
	{
		//Se borra de la lista porque no esta habilitado ni esta conectado
		bool borrado = false;
		for(unsigned int x=0; x<m_dispositivos.size() && !borrado; x++)
		{
			if(dispositivo == m_dispositivos[x])
			{
				m_dispositivos.erase(m_dispositivos.begin()+x);
				borrado = true;
			}
		}
	}
}

Dispositivo_Midi *Controlador_Midi::obtener_dispositivo(const Dispositivo_Midi &datos)
{
	//Retorna el dispositivo pedido para guardar la configuración
	m_bloqueo.lock();
	Dispositivo_Midi *dispositivo = nullptr;
	for(unsigned int x=0; x<m_dispositivos.size(); x++)//Salida
	{
		//Si ya fue marcado se salta
		if(m_dispositivos[x]->marcado())
			continue;

		if(m_dispositivos[x]->nombre() == datos.nombre())
		{
			//Coincide al menos en el nombre y el puerto
			if(m_dispositivos[x]->puerto() == datos.puerto())
			{
				dispositivo = m_dispositivos[x];

				//Es exactamente el mismo cliente, lo retorna
				if(	m_dispositivos[x]->cliente() == datos.cliente())
				{
					dispositivo->marcado(true);
					m_bloqueo.unlock();
					return dispositivo;
				}
			}
		}
	}
	//Retorna el dispositivo que coincide por nombre y puerto
	//pero esta en otro cliente
	if(dispositivo != nullptr && dispositivo->puerto() == datos.puerto())
	{
		dispositivo->cambio_cliente(true);
		dispositivo->marcado(true);
		m_bloqueo.unlock();
		return dispositivo;
	}
	else
	{
		//Crea un dispositivo para guardar los datos
		dispositivo = new Dispositivo_Midi(datos.cliente(), datos.puerto(), datos.version_midi(), datos.capacidad(), datos.nombre(), false);
		dispositivo->marcado(true);
		m_dispositivos.push_back(dispositivo);
		m_bloqueo.unlock();
		return dispositivo;
	}
}

Dispositivo_Midi* Controlador_Midi::obtener_dispositivo(unsigned char cliente, unsigned char puerto, const std::string &nombre)
{
	m_bloqueo.lock();
	Dispositivo_Midi *dispositivo = nullptr;
	for(unsigned int x=0; dispositivo == nullptr && x<m_dispositivos.size(); x++)
	{
		if(	m_dispositivos[x]->cliente() == cliente &&
			m_dispositivos[x]->puerto() == puerto &&
			m_dispositivos[x]->nombre() == nombre)
			dispositivo = m_dispositivos[x];
	}
	m_bloqueo.unlock();
	return dispositivo;
}

void Controlador_Midi::conectar_dispositivo(Dispositivo_Midi *dispositivo)
{
	m_bloqueo.lock();
	this->conectar(dispositivo);
	m_bloqueo.unlock();
}

void Controlador_Midi::desconectar_dispositivo(Dispositivo_Midi *dispositivo)
{
	m_bloqueo.lock();
	this->desconectar(dispositivo);
	m_bloqueo.unlock();
}

void Controlador_Midi::eliminar_dispositivo(Dispositivo_Midi *dispositivo)
{
	m_bloqueo.lock();
	this->eliminar(dispositivo);
	m_bloqueo.unlock();
}

bool Controlador_Midi::hay_eventos()
{
	//Si se desconecta un dispositivo y deja notas encendida, se crean nuevos eventos para apagarlas
	m_bloqueo.lock();
	bool hay_eventos = false;
	if(m_desconectado_pendiente > 0)
		hay_eventos = true;
	else
	{
		if(m_secuenciador != nullptr)
			hay_eventos = m_secuenciador->hay_eventos();
	}

	m_bloqueo.unlock();
	return hay_eventos;
}

Evento_Midi Controlador_Midi::leer()
{
	if(m_secuenciador == nullptr)
		return Evento_Midi();

	//Crea un evento para apagar las notas que dejo encendida al desconectarse
	m_bloqueo.lock();
	if(m_desconectado_pendiente > 0)
	{
		for(unsigned int x=0; x<m_dispositivos.size(); x++)
		{
			if(!m_dispositivos[x]->conectado() && m_dispositivos[x]->entrada_activa())
			{
				//TODO considerar notas activas en los distintos GRUPOS y CANALES
				std::vector<unsigned char> notas_aun_activas = m_dispositivos[x]->notas_entrada();
				if(notas_aun_activas.size() > 0)
				{
					//Cuando solo queda una nota, recien se puede descontar de la descionexion pendiente
					//Porque solo se puede apagar una nota a la vez
					if(notas_aun_activas.size() == 1)
						m_desconectado_pendiente--;

					Evento_Midi evento_nuevo(EventoMidi_NotaApagada);
					evento_nuevo.canal(0);
					evento_nuevo.id_nota(notas_aun_activas[0]);

					m_dispositivos[x]->nota_entrada(notas_aun_activas[0], false);
					m_bloqueo.unlock();
					return evento_nuevo;
				}
			}
		}
	}

	//Eventos reales
	Evento_Midi evento = m_secuenciador->leer();
	if(!evento.es_nulo())
	{
		if(evento.tipo_mensaje() == TipoMensaje_VozDeCanalMidi1 ||
			evento.tipo_mensaje() == TipoMensaje_VozDeCanalMidi2)
		{
			if(evento.tipo_voz_de_canal() == EventoMidi_NotaApagada)
			{
				Dispositivo_Midi *origen = dispositivo_activo(evento.cliente(), evento.puerto(), ENTRADA);
				if(origen != nullptr)
				{
					origen->nota_entrada(evento.id_nota(), false);
					if(origen->volumen_entrada() < 0.999 || origen->volumen_entrada() > 1.001)
					{
						if(evento.tipo_mensaje() == TipoMensaje_VozDeCanalMidi1)
							evento.velocidad_nota_7(static_cast<unsigned char>(evento.velocidad_nota_7() * origen->volumen_entrada()));
						else
							evento.velocidad_nota_16(static_cast<unsigned short>(evento.velocidad_nota_16() * origen->volumen_entrada()));
					}
				}
			}
			else if(evento.tipo_voz_de_canal() == EventoMidi_NotaEncendida)
			{
				Dispositivo_Midi *origen = dispositivo_activo(evento.cliente(), evento.puerto(), ENTRADA);
				if(origen != nullptr)
				{
					origen->nota_entrada(evento.id_nota(), true);
					if(origen->volumen_entrada() < 0.999 || origen->volumen_entrada() > 1.001 || !origen->sensitivo())
					{
						if(evento.tipo_mensaje() == TipoMensaje_VozDeCanalMidi1)
						{
							unsigned char velocidad = evento.velocidad_nota_7();
							if(!origen->sensitivo())
								velocidad = VALOR_MEDIO_7BITS;

							velocidad = static_cast<unsigned char>(velocidad * origen->volumen_entrada());
							evento.velocidad_nota_7(velocidad);
						}
						else
						{
							unsigned short velocidad = evento.velocidad_nota_16();
							if(!origen->sensitivo())
								velocidad = VALOR_MEDIO_16BITS;

							velocidad = static_cast<unsigned short>(velocidad * origen->volumen_entrada());
							evento.velocidad_nota_16(velocidad);
						}
					}
				}
			}
		}
		else if(evento.version_midi() == VersionMidi::Version_1)
		{
			if(evento.tipo_evento_midi1() == EventoMidi_ClienteConectado)
			{
				//Agrega el mensaje
				m_cambio_dispositivos = true;
				m_mensajes.push_back(T_("{0} - conectado", m_secuenciador->nombre_dispositivo(evento.cliente(), evento.puerto())));
			}
			else if(evento.tipo_evento_midi1() == EventoMidi_ClienteDesconectado)
			{
				//Primero se desconecta el puerto y luego el cliente, cuando llega a este punto ya esta desconectado el puerto
				m_cambio_dispositivos = true;

				for(std::map<unsigned char, std::string>::iterator actual = m_nombre_desconectado.begin(); actual != m_nombre_desconectado.end(); actual++)
					m_mensajes.push_back(T_("{0} - desconectado", actual->second));

				m_nombre_desconectado.clear();
			}
			else if(evento.tipo_evento_midi1() == EventoMidi_PuertoConectado)
			{
				Dispositivo_Midi *nuevo = this->dispositivo(evento.cliente(), evento.puerto(), false, false);
				if(nuevo != nullptr)
				{
					nuevo->conectado(true);//Conectado fisicamente
					if(nuevo->conectado())//Se conecta a un dispositivo existente
						this->conectar(nuevo);
				}
				else
				{
					//Crea un dispositivo nuevo
					Dispositivo_Midi *nuevo_dispositivo = m_secuenciador->crear_nuevo_dispositivo(evento.cliente(), evento.puerto());
					if(nuevo_dispositivo != nullptr)
						m_dispositivos.push_back(nuevo_dispositivo);
				}
			}
			else if(evento.tipo_evento_midi1() == EventoMidi_PuertoDesconectado)
			{
				//Desactiva un puerto de la lista
				Dispositivo_Midi *origen = this->dispositivo(evento.cliente(), evento.puerto(), true, true);
				if(origen != nullptr)
				{
					origen->conectado(false);//Desconectado fisicamente
					//Revisa si el dispositivo que se desconecto dejo notas encendidas que hay que apagar
					if(origen->notas_entrada().size() > 0)
						m_desconectado_pendiente++;

					//Guarda el nombre del dispositivo porque si se borra no lo tendre, (se borra solo si no esta habilitado)
					std::map<unsigned char, std::string>::iterator actual = m_nombre_desconectado.find(evento.cliente());
					if(actual == m_nombre_desconectado.end())
						m_nombre_desconectado[evento.cliente()] = origen->nombre();

					this->desconectar(origen);
					this->eliminar(origen);
				}
			}
			else if(evento.tipo_evento_midi1() == EventoMidi_PuertoSuscrito)
			{
				if(evento.cliente() != m_secuenciador->cliente())
				{
					if(m_seguidor_eventos->hay_eventos())
					{
						Dispositivo_Midi *dispositivo = dispositivo_activo(evento.cliente(), evento.puerto(), SALIDA);
						if(dispositivo != nullptr)
						{
							if(dispositivo->reenviar_eventos())
							{
								this->reenviar_eventos(dispositivo);
								dispositivo->reenviar_eventos(false);
							}
						}
					}
				}
			}
		}
	}
	m_bloqueo.unlock();
	return evento;
}

void Controlador_Midi::escribir(Evento_Midi &evento)
{
	if(m_secuenciador == nullptr)
		return;

	//Los metaevento ni "eventos especiales" se envian a los dispositivos
	if(	evento.version_midi() == VersionMidi::Version_1 &&
		(evento.tipo_evento_midi1() < EventoMidi_NotaApagada ||
		evento.tipo_evento_midi1() == EventoMidi_Metaevento))
		return;

	m_bloqueo.lock();
	m_seguidor_eventos->registrar_evento(&evento);

	//Se escala el volumen segun la configuración de cada dispositivo
	for(unsigned int x=0; x<m_salida.size(); x++)
	{
		evento.cliente(m_salida[x]->cliente());
		evento.puerto(m_salida[x]->puerto());
		if(	evento.tipo_mensaje() == TipoMensaje_VozDeCanalMidi1 ||
			evento.tipo_mensaje() == TipoMensaje_VozDeCanalMidi2)
		{
			if(m_salida[x]->volumen_salida() < 0.999 || m_salida[x]->volumen_salida() > 1.001)
			{
				if(evento.tipo_mensaje() == TipoMensaje_VozDeCanalMidi1)
					evento.velocidad_nota_7(static_cast<unsigned char>(evento.velocidad_nota_7() * m_salida[x]->volumen_salida()));
				else
					evento.velocidad_nota_16(static_cast<unsigned short>(evento.velocidad_nota_16() * m_salida[x]->volumen_salida()));
			}

			if(evento.tipo_voz_de_canal() == EventoMidi_NotaApagada)
				this->nota_salida(evento.grupo(), evento.canal(), evento.id_nota(), false);
			else if(evento.tipo_voz_de_canal() == EventoMidi_NotaEncendida)
				this->nota_salida(evento.grupo(), evento.canal(), evento.id_nota(), true);
		}
		m_secuenciador->escribir(evento);
	}

	m_necesario_restablecer = true;
	m_bloqueo.unlock();
}

void Controlador_Midi::tecla_luninosa(unsigned char id_nota, bool estado)
{
	//Este metodo se encarga de controlar las teclas luminosas del organo
	if(m_secuenciador == nullptr)
		return;

	m_bloqueo.lock();
	for(unsigned int x=0; x<m_salida.size(); x++)
	{
		Teclas_Luminosas *teclado_actual = m_salida[x]->teclas_luminosas();
		if(teclado_actual != nullptr)
		{
			if(estado)
			{
				Evento_Midi evento = teclado_actual->encender(id_nota);
				evento.cliente(m_salida[x]->cliente());
				evento.puerto(m_salida[x]->puerto());
				m_secuenciador->escribir(evento);
			}
			else
			{
				Evento_Midi evento = teclado_actual->apagar(id_nota);
				evento.cliente(m_salida[x]->cliente());
				evento.puerto(m_salida[x]->puerto());
				m_secuenciador->escribir(evento);
			}
		}
	}

	m_necesario_restablecer = true;
	m_bloqueo.unlock();
}

void Controlador_Midi::enviar_nota(unsigned char id_nota, bool estado)
{
	//Este metodo es usado para enviar eventos desde el teclado y el ratón
	m_bloqueo.lock();
	if(m_secuenciador != nullptr)
		m_secuenciador->enviar_nota(id_nota, estado);
	m_bloqueo.unlock();
}

void Controlador_Midi::actualizar(unsigned int diferencia_tiempo, bool consumir_eventos)
{
	if(m_secuenciador == nullptr)
		return;

	if(consumir_eventos)
	{
		//Esta en una ventana que no requiere leer los eventos
		//pero igualmente necesito revisarlos para detectar nuevos
		//dispositivos o perdida de conexion
		while(this->hay_eventos())
			this->leer();
	}
	else
	{
		m_bloqueo.lock();
		for(unsigned int x=0; x<m_salida.size(); x++)
		{
			Teclas_Luminosas *teclado_actual = m_salida[x]->teclas_luminosas();
			if(teclado_actual != nullptr)
			{
				Evento_Midi evento = teclado_actual->actualizar(diferencia_tiempo);
				if(!evento.es_nulo())
				{
					evento.cliente(m_salida[x]->cliente());
					evento.puerto(m_salida[x]->puerto());
					m_secuenciador->escribir(evento);
				}
			}
		}
		m_bloqueo.unlock();
	}
}

void Controlador_Midi::restablecer()
{
	if(m_secuenciador == nullptr)
		return;

	m_bloqueo.lock();
	if(m_necesario_restablecer)
	{
		this->apagar_notas();

		//Apaga todas los sonidos, notas y restablece todos los controles
		for(unsigned char g=0; g<GRUPOS_MIDI; g++)
		{
			if(!m_seguidor_eventos->grupo_usado(g))
				continue;

			for(unsigned char c=0; c<CANALES_MIDI; c++)
			{
				if(!m_seguidor_eventos->canal_usado(g, c))
					continue;

				//Restablecer programas predeterminado en todos los dispositivos
				if(m_seguidor_eventos->programa_cambiado(g, c))
				{
					Evento_Midi evento_programa(EventoMidi_Programa);
					evento_programa.cliente(DIRECCIONES_SUSCRITAS);
					evento_programa.puerto(DIRECCIONES_SUSCRITAS);
					//evento_programa.grupo(g);
					evento_programa.canal(c);
					evento_programa.programa(0);
					m_secuenciador->escribir(evento_programa);
				}

				Evento_Midi evento(EventoMidi_Controlador);
				evento.cliente(DIRECCIONES_SUSCRITAS);
				evento.puerto(DIRECCIONES_SUSCRITAS);
				//evento.grupo(g);
				evento.canal(c);

				evento.controlador_mensaje(MensajeControlador_TodosLosSonidosApagados);
				evento.controlador_valor(0x00);
				m_secuenciador->escribir(evento);

				evento.controlador_mensaje(MensajeControlador_RestablecerTodosLosControles);
				evento.controlador_valor(0x00);
				m_secuenciador->escribir(evento);

				evento.controlador_mensaje(MensajeControlador_TodasLasNotasApagadas);
				evento.controlador_valor(0x00);
				m_secuenciador->escribir(evento);
			}
		}

		//Reinicio GM
		unsigned char *datos_gm_apagado = new unsigned char[6] {0xF0, 0x7E, 0x7F, 0x09, 0x01, 0xF7};
		Evento_Midi evento_gm(datos_gm_apagado, 6, VersionMidi::Version_1);
		evento_gm.cliente(DIRECCIONES_SUSCRITAS);
		evento_gm.puerto(DIRECCIONES_SUSCRITAS);
		m_secuenciador->escribir(evento_gm);

		m_seguidor_eventos->borrar_registro_eventos();
		m_necesario_restablecer = false;
	}
	m_bloqueo.unlock();
}

void Controlador_Midi::desactivar_notas()
{
	m_bloqueo.lock();
	this->apagar_notas();
	m_bloqueo.unlock();
}

bool Controlador_Midi::hay_mensajes()
{
	m_bloqueo.lock();
	bool salida = false;

	if(m_mensajes.size() > 0)
		salida = true;

	m_bloqueo.unlock();
	return salida;
}

std::string Controlador_Midi::siguiente_mensaje()
{
	m_bloqueo.lock();
	std::string texto;
	if(m_mensajes.size() > 0)
	{
		texto = m_mensajes[m_mensajes.size()-1];
		m_mensajes.pop_back();
	}
	m_bloqueo.unlock();
	return texto;
}

std::vector<Dispositivo_Midi*> Controlador_Midi::lista_dispositivos()
{
	m_bloqueo.lock();
	std::vector<Dispositivo_Midi*> salida = m_dispositivos;
	m_bloqueo.unlock();
	return salida;
}

std::map<Dispositivo_Midi*, unsigned char> Controlador_Midi::lista_cambio_cliente()
{
	m_bloqueo.lock();
	std::map<Dispositivo_Midi*, unsigned char> lista = m_cambio_cliente;
	m_cambio_cliente.clear();
	m_bloqueo.unlock();
	return lista;
}

bool Controlador_Midi::hay_cambios_de_dispositivos()
{
	m_bloqueo.lock();
	bool cambios = m_cambio_dispositivos;
	m_cambio_dispositivos = false;
	m_bloqueo.unlock();
	return cambios;
}

bool Controlador_Midi::hay_cambio_cliente()
{
	//Indica si hay un dispositivo nuevo conectado en un puerto distinto a lo que dice la base de datos
	m_bloqueo.lock();
	bool salida = false;

	if(m_cambio_cliente.size() > 0)
		salida = true;

	m_bloqueo.unlock();
	return salida;
}

std::string Controlador_Midi::dispositivos_conectados(unsigned char capacidad)
{
	//Muestra todos los dispositivos conectados de la entrada o salida
	m_bloqueo.lock();
	std::string nombres = "";
	if(capacidad == ENTRADA)
	{
		for(unsigned int x=0; x<m_entrada.size(); x++)
		{
			if(x>0)
				nombres += ", ";
			nombres += T(m_entrada[x]->nombre());
		}
	}
	else if(capacidad == SALIDA)
	{
		for(unsigned int x=0; x<m_salida.size(); x++)
		{
			if(x>0)
				nombres += ", ";
			nombres += T(m_salida[x]->nombre());
		}
	}
	m_bloqueo.unlock();
	return nombres;
}
