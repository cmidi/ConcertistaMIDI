#ifndef EXCEPCION_DISPOSITIVO_H
#define EXCEPCION_DISPOSITIVO_H

#include <exception>
#include <string>

enum class CodigoErrorDispositivo : unsigned char
{
	FalloCrearSecuenciador,
};

class Excepcion_Dispositivo : public std::exception
{
private:
	const CodigoErrorDispositivo m_codigo_error;
	const char *m_error;
public:
	Excepcion_Dispositivo(CodigoErrorDispositivo codigo_error);
	const char *what() const noexcept;
	std::string descripcion_error() const noexcept;
};

#endif
