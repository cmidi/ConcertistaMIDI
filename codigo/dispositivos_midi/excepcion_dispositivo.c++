#include "excepcion_dispositivo.h++"
#include "../util/traduccion.h++"

Excepcion_Dispositivo::Excepcion_Dispositivo(CodigoErrorDispositivo codigo_error) : m_codigo_error(codigo_error)
{
}

const char *Excepcion_Dispositivo::what() const noexcept
{
	switch(m_codigo_error)
	{
		case CodigoErrorDispositivo::FalloCrearSecuenciador: 				return TC("No se ha podido iniciar el secuenciador");

		default:															return TC("Error en el dispositivo, error no implementado");
	}
}

std::string Excepcion_Dispositivo::descripcion_error() const noexcept
{
	return this->what();
}
