#ifndef SECUENCIADOR_ALSA_H
#define SECUENCIADOR_ALSA_H

#include "secuenciador.h++"

#include <alsa/asoundlib.h>

class Secuenciador_Alsa : public Secuenciador
{
private:
	snd_seq_t *m_secuenciador_alsa;

	static unsigned int Bandera_entrada, Bandera_salida;
	void mostrar_estado_alsa(int estado, const std::string &mensaje) const;
	void suscribir_puerto(unsigned char cliente_origen, unsigned char puerto_origen, unsigned char cliente_destino, unsigned char puerto_destino) const;

public:
	Secuenciador_Alsa();
	~Secuenciador_Alsa();

	void habilitar_midi_2(bool estado) override;
	std::string nombre_dispositivo(unsigned char cliente, unsigned char puerto) const override;

	void crear_lista_dispositivos(std::vector<Dispositivo_Midi*> &dispositivos) const override;
	Dispositivo_Midi* crear_nuevo_dispositivo(unsigned char cliente, unsigned char puerto) const override;

	bool conectar(unsigned char cliente, unsigned char puerto, TipoDispositivo tipo) const override;
	bool desconectar(unsigned char cliente, unsigned char puerto, TipoDispositivo tipo) const override;


	void escribir(const Evento_Midi &evento_salida) const override;
	Evento_Midi leer() const override;
	bool hay_eventos() const override;

	void enviar_nota(unsigned char id_nota, bool estado) const override;
};

#endif
