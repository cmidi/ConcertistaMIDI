#ifndef CONTROLADOR_MIDI
#define CONTROLADOR_MIDI

#include "secuenciador.h++"
#include "secuenciador_alsa.h++"
#include "dispositivo_midi.h++"
#include "../libreria_midi/evento_midi.h++"
#include "../libreria_midi/seguidor_eventos.h++"
#include <map>
#include <mutex>

class Controlador_Midi
{
private:
	Secuenciador *m_secuenciador;
	bool m_cambio_dispositivos;
	unsigned int m_desconectado_pendiente;
	std::mutex m_bloqueo;

	std::vector<Dispositivo_Midi*> m_dispositivos;
	std::vector<Dispositivo_Midi*> m_entrada;
	std::vector<Dispositivo_Midi*> m_salida;
	std::map<Dispositivo_Midi*, unsigned char> m_cambio_cliente;//<dispositivo, cliente anterior>

	std::map<unsigned char, std::string> m_nombre_desconectado;
	std::vector<std::string> m_mensajes;

	bool m_necesario_restablecer;

	std::vector<unsigned int> m_notas_activa_salida;//grupo << 12 | canal << 8 | id_nota

	Seguidor_Eventos * m_seguidor_eventos;

	Dispositivo_Midi *dispositivo(unsigned char cliente, unsigned char puerto, bool conectado, bool exacto);
	Dispositivo_Midi *dispositivo_activo(unsigned char cliente, unsigned char puerto, unsigned char capacidad);

	void reenviar_eventos(Dispositivo_Midi *dispositivo);
	void escribir_evento_dispositivo(Evento_Midi *evento, Dispositivo_Midi *dispositivo);

	void nota_salida(unsigned int grupo, unsigned int canal, unsigned char id_nota, bool encendida);
	void apagar_luces(Dispositivo_Midi *dispositivo);
	void apagar_notas();

	void conectar(Dispositivo_Midi *dispositivo);
	void desconectar(Dispositivo_Midi *dispositivo);
	void eliminar(Dispositivo_Midi *dispositivo);

public:
	Controlador_Midi();
	~Controlador_Midi();

	void habilitar_midi_2(bool estado);
	bool habilitar_midi_2();

	Dispositivo_Midi* obtener_dispositivo(const Dispositivo_Midi &datos);
	Dispositivo_Midi* obtener_dispositivo(unsigned char cliente, unsigned char puerto, const std::string &nombre);
	void conectar_dispositivo(Dispositivo_Midi *dispositivo);
	void desconectar_dispositivo(Dispositivo_Midi *dispositivo);
	void eliminar_dispositivo(Dispositivo_Midi *dispositivo);

	bool hay_eventos();
	Evento_Midi leer();

	void escribir(Evento_Midi &evento);
	void tecla_luninosa(unsigned char id_nota, bool estado);
	void enviar_nota(unsigned char id_nota, bool estado);

	void actualizar(unsigned int diferencia_tiempo, bool consumir_eventos);

	void restablecer();
	void desactivar_notas();

	bool hay_mensajes();
	std::string siguiente_mensaje();

	std::vector<Dispositivo_Midi*> lista_dispositivos();
	std::map<Dispositivo_Midi*, unsigned char> lista_cambio_cliente();
	bool hay_cambios_de_dispositivos();
	bool hay_cambio_cliente();
	std::string dispositivos_conectados(unsigned char capacidad);
};

#endif
