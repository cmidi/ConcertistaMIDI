#include "fps.h++"
#include <iostream>
#include <cmath>

std::int64_t Fps::Nanosegundos = 0;
std::int64_t Fps::Nanosegundos_fotogramas = 0;
bool Fps::Mostrar_fps = true;
std::int64_t Fps::Contador_fps = 0;

Tiempo Fps::Tiempo_actual = Reloj::now();
Tiempo Fps::Tiempo_anterior = Reloj::now();
Tiempo Fps::Tiempo_anterior_fotogramas = Reloj::now();

unsigned int Fps::Calcular_tiempo()
{
	Fps::Tiempo_actual = Reloj::now();
	Fps::Nanosegundos = std::chrono::duration_cast<std::chrono::nanoseconds>(Fps::Tiempo_actual - Fps::Tiempo_anterior).count();
	Fps::Nanosegundos_fotogramas = std::chrono::duration_cast<std::chrono::nanoseconds>(Fps::Tiempo_actual - Fps::Tiempo_anterior_fotogramas).count();
	Fps::Tiempo_anterior = Fps::Tiempo_actual;

	if(Fps::Nanosegundos_fotogramas >= 250000000)//Mostrar cada 250 milisegundos
	{
		std::int64_t fps_total = Fps::Contador_fps + 1;

		Fps::Contador_fps = 0;
		Fps::Tiempo_anterior_fotogramas = Fps::Tiempo_actual;
		Fps::Mostrar_fps = true;

		float resultado = 1000000000.0f/(static_cast<float>(Fps::Nanosegundos_fotogramas)/static_cast<float>(fps_total));
		return static_cast<unsigned int>(std::round(resultado));//Retorna FPS
	}
	else
	{
		Fps::Mostrar_fps = false;
		Fps::Contador_fps++;
		return 0;
	}
}

bool Fps::Actualizar_fps()
{
	return Fps::Mostrar_fps;
}

unsigned int Fps::Obtener_nanosegundos()
{
	return static_cast<unsigned int>(Fps::Nanosegundos);
}
