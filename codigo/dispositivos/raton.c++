#include "raton.h++"

Raton::Raton()
{
	m_posicion_x = 0;
	m_posicion_y = 0;
	m_desplazamiento_x = 0;
	m_desplazamiento_y = 0;
	m_boton_izquierdo = false;
	m_boton_central = false;
	m_boton_derecho = false;
	m_numero_clics = 0;
	m_tipo_evento = EventoRaton::Ninguno;
}

void Raton::actualizar_boton(BotonRaton boton, bool estado, unsigned char numero_clics)
{
	if(boton == BotonRaton::Izquierdo)
		m_boton_izquierdo = estado;
	else if(boton == BotonRaton::Central)
		m_boton_central = estado;
	else if(boton == BotonRaton::Derecho)
		m_boton_derecho = estado;

	m_numero_clics = numero_clics;

	m_desplazamiento_x = 0;
	m_desplazamiento_y = 0;
	m_tipo_evento = EventoRaton::Clic;
}

void Raton::actualizar_posicion(int x, int y)
{
	m_posicion_x = x;
	m_posicion_y = y;
	m_tipo_evento = EventoRaton::Posicion;
}

void Raton::actualizar_desplazamiento(int desplazamiento_x, int desplazamiento_y)
{
	m_desplazamiento_x = desplazamiento_x;
	m_desplazamiento_y = desplazamiento_y;
	m_tipo_evento = EventoRaton::Desplazamiento;
}

void Raton::anular_desplazamiento()
{
	m_desplazamiento_x = 0;
	m_desplazamiento_y = 0;
}

bool Raton::activado(BotonRaton boton) const
{
	if(boton == BotonRaton::Izquierdo)
		return m_boton_izquierdo;
	else if(boton == BotonRaton::Central)
		return m_boton_central;
	else if(boton == BotonRaton::Derecho)
		return m_boton_derecho;
	return false;
}

BotonRaton Raton::boton_activado() const
{
	if(m_boton_izquierdo)
		return BotonRaton::Izquierdo;
	else if(m_boton_central)
		return BotonRaton::Central;
	else if(m_boton_derecho)
		return BotonRaton::Derecho;
	return BotonRaton::Ninguno;
}

unsigned char Raton::numero_clics() const
{
	return m_numero_clics;
}

int Raton::x() const
{
	return m_posicion_x;
}

int Raton::y() const
{
	return m_posicion_y;
}

int Raton::dx() const
{
	return m_desplazamiento_x;
}

int Raton::dy() const
{
	return m_desplazamiento_y;
}

EventoRaton Raton::tipo_evento() const
{
	return m_tipo_evento;
}

bool Raton::esta_sobre(float x, float y, float ancho, float alto) const
{
	if(this->x() >= static_cast<int>(x) && this->x() < static_cast<int>(x + ancho) &&
		this->y() >= static_cast<int>(y) && this->y() < static_cast<int>(y + alto))
		return true;
	return false;
}

Raton& Raton::operator = (const Raton &r)
{
	if(this != &r)
	{
		m_posicion_x = r.m_posicion_x;
		m_posicion_y = r.m_posicion_y;
		m_desplazamiento_x = r.m_desplazamiento_x;
		m_desplazamiento_y = r.m_desplazamiento_y;
		m_boton_izquierdo = r.m_boton_izquierdo;
		m_boton_central = r.m_boton_central;
		m_boton_derecho = r.m_boton_derecho;
		m_numero_clics = r.m_numero_clics;
	}
	return *this;
}
