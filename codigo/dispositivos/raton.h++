#ifndef RATON_H
#define RATON_H

enum class BotonRaton : unsigned char
{
	Izquierdo,
	Central,
	Derecho,
	Ninguno
};

enum class EventoRaton : unsigned char
{
	Posicion,
	Clic,
	Desplazamiento,
	Ninguno
};

class Raton
{
private:
	int m_posicion_x, m_posicion_y;
	int m_desplazamiento_x, m_desplazamiento_y;
	bool m_boton_izquierdo, m_boton_central, m_boton_derecho;
	unsigned char m_numero_clics;
	EventoRaton m_tipo_evento;
public:
	Raton();
	void actualizar_boton(BotonRaton boton, bool estado, unsigned char numero_clics);
	void actualizar_posicion(int x, int y);
	void actualizar_desplazamiento(int desplazamiento_x, int desplazamiento_y);
	void anular_desplazamiento();

	bool activado(BotonRaton boton) const;
	BotonRaton boton_activado() const;
	unsigned char numero_clics() const;
	int x() const;
	int y() const;
	int dx() const;
	int dy() const;
	EventoRaton tipo_evento() const;

	bool esta_sobre(float x, float y, float ancho, float alto) const;

	Raton& operator = (const Raton &r);
};

#endif
