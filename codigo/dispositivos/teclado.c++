#include "teclado.h++"

unsigned char Teclado::Tecla_a_nota(Tecla tecla)
{
	const unsigned char octava = 4;
	const unsigned char do_primera_octava = octava * 12;
	const unsigned char do_segunda_octava = (octava+1) * 12;

	#pragma GCC diagnostic push
	#pragma GCC diagnostic ignored "-Wswitch-enum"
	switch(tecla)
	{
		case Tecla::Tabulador:			return do_primera_octava;
		case Tecla::Numero_1:			return do_primera_octava + 1;
		case Tecla::Q:					return do_primera_octava + 2;
		case Tecla::Numero_2:			return do_primera_octava + 3;
		case Tecla::W:					return do_primera_octava + 4;
		case Tecla::E:					return do_primera_octava + 5;
		case Tecla::Numero_4:			return do_primera_octava + 6;
		case Tecla::R:					return do_primera_octava + 7;
		case Tecla::Numero_5:			return do_primera_octava + 8;
		case Tecla::T:					return do_primera_octava + 9;
		case Tecla::Numero_6:			return do_primera_octava + 10;
		case Tecla::Y:					return do_primera_octava + 11;

		case Tecla::U:					return do_segunda_octava;
		case Tecla::Numero_8:			return do_segunda_octava + 1;
		case Tecla::I:					return do_segunda_octava + 2;
		case Tecla::Numero_9:			return do_segunda_octava + 3;
		case Tecla::O:					return do_segunda_octava + 4;
		case Tecla::P:					return do_segunda_octava + 5;
		case Tecla::Apostrofe:			return do_segunda_octava + 6;
		case Tecla::AcentoGrave:		return do_segunda_octava + 7;
		case Tecla::ExclamacionInicio:	return do_segunda_octava + 8;
		case Tecla::Suma:				return do_segunda_octava + 9;
		case Tecla::Borrar:				return do_segunda_octava + 10;
		case Tecla::Entrar:				return do_segunda_octava + 11;
		default:						return 255;
	}
	#pragma GCC diagnostic pop
}
