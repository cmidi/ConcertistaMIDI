#ifndef PISTA_H
#define PISTA_H

#include "../recursos/color.h++"
#include "../libreria_midi/pista_midi.h++"

#include <string>

#define NUMERO_COLORES_PISTA 24

enum class Modo : unsigned char
{
	Tocar,
	Aprender,
	Fondo,
};

class Pista
{
private:
	Pista_Midi *m_pista_midi;
	Color m_color_pista;
	Modo m_modo_pista;
	bool m_visible;
	bool m_sonido;

public:
	static Color Colores_pista[NUMERO_COLORES_PISTA+1];
	static std::string Nombre_colores[NUMERO_COLORES_PISTA+1];
	Pista(Pista_Midi *pista, Color color, Modo modo, bool visible, bool sonido);
	~Pista();

	void color(Color color);
	void modo(Modo modo);
	void visible(bool estado);
	void sonido(bool estado);

	Pista_Midi *pista_midi();
	Color color();
	Modo modo();
	bool visible();
	bool sonido();
};

#endif
