#ifndef DATOS_ARCHIVOS_H
#define DATOS_ARCHIVOS_H

#include <cstdint>
#include <string>
#include <unicode/unistr.h>
#include <unicode/stringoptions.h>

struct Datos_Archivos
{
	std::string ruta;
	std::string nombre;
	icu::UnicodeString nombre_unicode;
	std::string fecha_acceso;
	std::int64_t duracion;
	unsigned int visitas;
	bool es_carpeta;
	bool es_nuevo;
	std::uint64_t tamanno;

	Datos_Archivos();
	bool operator < (const Datos_Archivos &d) const;
};
#endif
