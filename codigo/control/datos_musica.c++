#include "datos_musica.h++"

#include "../util/funciones.h++"
#include "../util/texto.h++"
#include "../util/traduccion.h++"

Datos_Musica::Datos_Musica()
{
	m_midi = nullptr;
	m_nombre_musica = T("Sin Titulo");

	m_evaluacion.notas_totales = 0;
	m_evaluacion.notas_tocadas = 0;
	m_evaluacion.errores = 0;
}

Datos_Musica::~Datos_Musica()
{
	for(std::map<unsigned short, std::vector<Pista>*>::iterator i = m_pistas.begin(); i != m_pistas.end(); i++)
		delete i->second;
	m_pistas.clear();

	for(std::map<unsigned short, TiempoTocado*>::iterator i = m_evaluacion.tiempo_tocado.begin(); i != m_evaluacion.tiempo_tocado.end(); i++)
		delete i->second;
	m_evaluacion.tiempo_tocado.clear();

	if(m_midi != nullptr)
		delete m_midi;
}

std::string Datos_Musica::cargar_midi(std::string direccion)
{
	try
	{
		if(m_midi != nullptr)
			delete m_midi;
		m_midi = Midi::abrir_midi(direccion);

		for(std::map<unsigned short, std::vector<Pista>*>::iterator i = m_pistas.begin(); i != m_pistas.end(); i++)
			delete i->second;
		m_pistas.clear();

		for(std::map<unsigned short, TiempoTocado*>::iterator i = m_evaluacion.tiempo_tocado.begin(); i != m_evaluacion.tiempo_tocado.end(); i++)
			delete i->second;
		m_evaluacion.tiempo_tocado.clear();

		//El formato del nombre se considera: Nombre autor - Nombre musica
		std::string nombre_archivo = Funciones::nombre_archivo(direccion, false);
		std::vector<std::string> nombres = Texto::dividir_texto(Texto::quitar_espacios_en_extremos(nombre_archivo), '-');
		if(nombres.size() == 0)
		{
			//Archivo sin nombre, solo espacios
			m_nombre_musica = T("Sin Nombre");
			m_autor = "";
		}
		if(nombres.size() == 1)
		{
			//Archivo con solo el nombre de la musica
			m_nombre_musica = Texto::quitar_espacios_en_extremos(nombres[0]);
			m_autor = "";
		}
		else if(nombres.size() > 1)
		{
			//Archivo con el nombre de autor y nombre de la musica
			m_autor = Texto::quitar_espacios_en_extremos(nombres[0]);
			//Unir todos los demas textos
			std::string nombre_final;
			for(unsigned int x=1; x<nombres.size(); x++)
			{
				if(x==1)
					nombre_final = nombres[x];
				else
					nombre_final += "-"+nombres[x];
			}
			m_nombre_musica = Texto::quitar_espacios_en_extremos(nombre_final);
		}
	}
	catch(const Excepcion_Midi &e)
	{
		m_midi = nullptr;
		return e.descripcion_error();
	}
	return "";
}

void Datos_Musica::pistas(unsigned short secuencia, std::vector<Pista> *pistas)
{
	//Borra las pistas cargadas anteriormente
	if(this->secuencia_cargada(secuencia))
		delete m_pistas[secuencia];

	m_pistas[secuencia] = pistas;
}

Evaluacion *Datos_Musica::evaluacion()
{
	return &m_evaluacion;
}

TiempoTocado *Datos_Musica::tiempo_tocado()
{
	unsigned short secuencia = m_midi->id_secuencia_activa();
	if(m_evaluacion.tiempo_tocado.contains(secuencia))
		return m_evaluacion.tiempo_tocado[secuencia];
	else
	{
		TiempoTocado *nuevo = new TiempoTocado();
		m_evaluacion.tiempo_tocado[secuencia] = nuevo;
		return nuevo;
	}
}

Midi *Datos_Musica::midi()
{
	return m_midi;
}

bool Datos_Musica::secuencia_cargada(unsigned short secuencia)
{
	if(m_pistas.contains(secuencia))
		return true;
	return false;
}

std::vector<Pista> *Datos_Musica::pistas()
{
	return m_pistas[m_midi->id_secuencia_activa()];
}

std::string Datos_Musica::nombre_musica()
{
	return m_nombre_musica;
}

std::string Datos_Musica::autor()
{
	return m_autor;
}
