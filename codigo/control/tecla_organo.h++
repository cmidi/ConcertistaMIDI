#ifndef TECLA_ORGANO_H
#define TECLA_ORGANO_H

struct Tecla_Organo
{
	bool activa = false;
	bool activa_automatica = false;
	unsigned char grupo = 0;
	unsigned char canal = 0;
	unsigned short pista = 0;
	unsigned int posicion = 0;
	bool sonido = false;
	bool valida = false;
	bool correcta = false;
	bool es_requerida = false;
	bool mostrar_particula = false;
	unsigned char contador_clic = 0;
	Color color_tecla;
	Color color_punto;

	float tiempo_espera = 0;

	void desactivar_nota(bool restablecer_todo)
	{
		this->activa = false;
		this->activa_automatica = false;
		this->grupo = 0;
		this->canal = 0;
		this->pista = 0;
		this->posicion = 0;
		this->sonido = false;
		this->valida = false;
		this->correcta = false;
		this->mostrar_particula = false;
		this->contador_clic = 0;
		this->color_tecla = Color(0.0f, 0.0f, 0.0f);

		if(restablecer_todo)
		{
			this->es_requerida = false;
			this->color_punto = Color(0.0f, 0.0f, 0.0f);
			this->tiempo_espera = 0.0f;
		}
	}
};

#endif
