#include "base_de_datos.h++"
#include "../util/traduccion.h++"

Base_de_Datos::Base_de_Datos()
{
	m_base_de_datos_abierta = false;
}

Base_de_Datos::~Base_de_Datos()
{
	this->cerrar();
}

int Base_de_Datos::cambios()
{
	if(!m_base_de_datos_abierta)
		return 0;

	return sqlite3_changes(m_base_de_datos);
}

int Base_de_Datos::consulta(const std::string &consulta_entrada)
{
	if(!m_base_de_datos_abierta)
		return BASE_DE_DATOS_ERROR;

	char *error = 0;
	int respuesta = sqlite3_exec(m_base_de_datos, consulta_entrada.c_str(), nullptr, 0, &error);
	if(respuesta != SQLITE_OK)
	{
		Registro::Error(consulta_entrada);
		Registro::Error(std::string(error));
		sqlite3_free(error);
		return BASE_DE_DATOS_ERROR;
	}
	return BASE_DE_DATOS_CORRECTO;
}

int Base_de_Datos::consulta(const std::string &consulta, const std::string &parametro_1, const std::string &parametro_2)
{
	if(!m_base_de_datos_abierta)
		return false;

	sqlite3_stmt * declaracion;
	int respuesta = sqlite3_prepare_v2(m_base_de_datos, consulta.c_str(), -1, &declaracion, nullptr);
	if(respuesta == SQLITE_OK)
	{
		sqlite3_bind_text(declaracion, 1, parametro_1.c_str(), -1, SQLITE_TRANSIENT);
		sqlite3_bind_text(declaracion, 2, parametro_2.c_str(), -1, SQLITE_TRANSIENT);
		sqlite3_step(declaracion);
		sqlite3_finalize(declaracion);
		return BASE_DE_DATOS_CORRECTO;
	}
	else
	{
		sqlite3_finalize(declaracion);
		return BASE_DE_DATOS_ERROR;
	}
}

std::vector<std::vector<std::string>> Base_de_Datos::consulta_tabla(const std::string &consulta, int columnas, int *estado)
{
	*estado = BASE_DE_DATOS_ERROR;
	if(!m_base_de_datos_abierta)
		return std::vector<std::vector<std::string>>();

	sqlite3_stmt * respuesta_consulta;
	int respuesta = sqlite3_prepare_v2(m_base_de_datos, consulta.c_str(), -1, &respuesta_consulta, nullptr);
	std::vector<std::vector<std::string>> tabla;
	if(respuesta == SQLITE_OK)
	{
		sqlite3_step(respuesta_consulta);
		unsigned int x = 0;
		while(sqlite3_column_text(respuesta_consulta, 0))
		{
			tabla.push_back(std::vector<std::string>());
			for(int c=0; c<columnas; c++)
			{
				if(sqlite3_column_text(respuesta_consulta, c) != nullptr)
					tabla[x].push_back(std::string(reinterpret_cast<const char*>(sqlite3_column_text(respuesta_consulta, c))));
				else
				{
					sqlite3_finalize(respuesta_consulta);
					tabla.clear();
					return tabla;
				}
			}
			sqlite3_step(respuesta_consulta);
			x++;
		}
		*estado = BASE_DE_DATOS_CORRECTO;
	}
	else
	{
		Registro::Depurar("consulta_tabla -> " + consulta);
		Registro::Error(std::string(sqlite3_errmsg(m_base_de_datos)));
	}
	sqlite3_finalize(respuesta_consulta);
	return tabla;
}

std::vector<std::string> Base_de_Datos::consulta_columna(const std::string &consulta, int *estado)
{
	//Retorna siempre la primera columna
	*estado = BASE_DE_DATOS_ERROR;
	if(!m_base_de_datos_abierta)
		return std::vector<std::string>();

	sqlite3_stmt * respuesta_consulta;
	int respuesta = sqlite3_prepare_v2(m_base_de_datos, consulta.c_str(), -1, &respuesta_consulta, nullptr);
	std::vector<std::string> datos;
	if(respuesta == SQLITE_OK)
	{
		sqlite3_step(respuesta_consulta);
		while(sqlite3_column_text(respuesta_consulta, 0))
		{
			datos.push_back(std::string(reinterpret_cast<const char*>(sqlite3_column_text(respuesta_consulta, 0))));
			sqlite3_step(respuesta_consulta);
		}
		*estado = BASE_DE_DATOS_CORRECTO;
	}
	else
	{
		Registro::Depurar("consulta_columna -> " + consulta);
		Registro::Error(std::string(sqlite3_errmsg(m_base_de_datos)));
	}
	sqlite3_finalize(respuesta_consulta);

	return datos;
}

int Base_de_Datos::depurar_consulta(unsigned int, void*, void *d, void*)
{
	sqlite3_stmt *declaracion = static_cast<sqlite3_stmt*>(d);
	char *sql_ejecutado = sqlite3_expanded_sql(declaracion);
	Registro::Depurar(std::string(sql_ejecutado));
	sqlite3_free(sql_ejecutado);
	return 0;
}

bool Base_de_Datos::abrir(const std::string &direccion)
{
	int respuesta = sqlite3_open(direccion.c_str(), &m_base_de_datos);
	if(respuesta != SQLITE_OK)
	{
		m_base_de_datos_abierta = false;
		return false;
	}
	//Depurar consultas SQL
	sqlite3_trace_v2(m_base_de_datos, SQLITE_TRACE_STMT, Base_de_Datos::depurar_consulta, NULL);
	m_base_de_datos_abierta = true;
	return true;
}

void Base_de_Datos::cerrar()
{
	if(m_base_de_datos_abierta)
	{
		sqlite3_close(m_base_de_datos);
		m_base_de_datos_abierta = false;
	}
}

bool Base_de_Datos::comprobar()
{
	//Comprueba es la base de datos correcta
	if(!m_base_de_datos_abierta)
		return false;

	std::string consulta = "SELECT valor FROM configuracion WHERE atributo = 'version_base_de_datos' LIMIT 1";
	sqlite3_stmt *declaracion;
	int respuesta = sqlite3_prepare_v2(m_base_de_datos, consulta.c_str(), -1, &declaracion, nullptr);
	if(respuesta != SQLITE_OK)
		return false;
	sqlite3_finalize(declaracion);
	return true;
}

void Base_de_Datos::crear()
{
	if(!m_base_de_datos_abierta)
		return;

	//Crea todas las tablas de la base de datos
	this->iniciar_transaccion();
	int estado = this->consulta("CREATE TABLE configuracion (atributo TEXT NOT NULL PRIMARY KEY, valor TEXT NOT NULL)");
	estado |= this->consulta("CREATE TABLE carpetas (nombre TEXT NOT NULL, ruta TEXT NOT NULL PRIMARY KEY)");
	estado |= this->consulta("CREATE TABLE seleccion (ruta TEXT NOT NULL PRIMARY KEY, seleccion INTEGER DEFAULT 0, ruta_seleccion TEXT NOT NULL)");
	estado |= this->consulta("CREATE TABLE archivos (ruta TEXT NOT NULL PRIMARY KEY, visitas INTEGER DEFAULT 0, duracion INTEGER DEFAULT 0, ultimo_acceso DATETIME)");
	estado |= this->consulta("CREATE TABLE dispositivos (	cliente INTEGER DEFAULT 0,"
												"puerto INTEGER DEFAULT 0,"
												"nombre TEXT NOT NULL,"
												"capacidad INTEGER DEFAULT 0,"
												"capacidad_activa INTEGER DEFAULT 0,"
												"habilitado INTEGER DEFAULT 0,"
												"sensitivo INTEGER DEFAULT 1,"
												"volumen_entrada REAL DEFAULT 1,"
												"rango_teclado TEXT NOT NULL,"
												"volumen_salida REAL DEFAULT 1,"
												"teclado_luminoso INTEGER DEFAULT 0,"
												"PRIMARY KEY(cliente, puerto, nombre))");

	estado |= this->consulta("CREATE INDEX indice_configuracion_atributo ON configuracion (atributo)");
	estado |= this->consulta("CREATE INDEX indice_carpetas_ruta ON carpetas (ruta)");
	estado |= this->consulta("CREATE INDEX indice_seleccion_ruta ON seleccion (ruta)");
	estado |= this->consulta("CREATE INDEX indice_archivos_ruta ON archivos (ruta)");
	estado |= this->consulta("CREATE INDEX indice_dispositivos_clave_primaria ON dispositivos (cliente, puerto, nombre)");

	bool registro_1 = this->escribir_configuracion("version_base_de_datos", std::to_string(VERSION_BASE_DE_DATOS));
	bool registro_2 = this->escribir_configuracion("ruta_instalacion", RUTA_ARCHIVOS);
	bool registro_3 = this->agregar_carpeta(T("Canciones"), std::string(RUTA_ARCHIVOS) + "/musica");

	Dispositivo_Midi teclado_y_raton;
	teclado_y_raton.cliente(129);
	teclado_y_raton.puerto(0);
	teclado_y_raton.nombre(N("Teclado y Ratón"));
	teclado_y_raton.capacidad(ENTRADA);
	teclado_y_raton.capacidad_activa(ENTRADA);
	teclado_y_raton.habilitado(true);
	teclado_y_raton.sensitivo(false);
	teclado_y_raton.rango_teclado("48,24");
	this->agregar_dispositivo(teclado_y_raton);

	Dispositivo_Midi timidity;
	timidity.cliente(128);
	timidity.puerto(0);
	timidity.nombre("TiMidity");
	timidity.capacidad(SALIDA);
	timidity.capacidad_activa(SALIDA);
	timidity.habilitado(true);
	this->agregar_dispositivo(timidity);

	if((estado & BASE_DE_DATOS_ERROR) == BASE_DE_DATOS_ERROR || !registro_1 || !registro_2 || ! registro_3)
	{
		this->revertir_transaccion();
		Registro::Error(T("Fallo la creación de la estructura de la base de datos"));
	}
	else
		this->finalizar_transaccion();
}

void Base_de_Datos::actualizar()
{
	if(!m_base_de_datos_abierta)
		return;
	//Verificar version de la base de datos
	std::string version_actual = this->leer_configuracion("version_base_de_datos");
	std::string version_original = version_actual;
	Registro::Nota(T_("Versión de la base de datos: {0}", version_actual));
	if(version_actual != std::to_string(VERSION_BASE_DE_DATOS))
	{
		bool actualizado = false;
		int estado = 0;
		this->iniciar_transaccion();
		if(version_actual == "1.0")
		{
			actualizado = true;
			estado |= this->consulta("CREATE TABLE seleccion (ruta TEXT NOT NULL PRIMARY KEY, seleccion INT DEFAULT 0, ruta_seleccion TEXT)");
			if(estado == BASE_DE_DATOS_CORRECTO)
				version_actual = "1.1";
		}
		if(version_actual == "1.1")
		{
			actualizado = true;
			//No existia ruta de instalacion
			bool registro = this->escribir_configuracion("ruta_instalacion", "..");
			if(registro)
				version_actual = "1.2";
			else
				estado |= BASE_DE_DATOS_ERROR;
		}
		if(version_actual == "1.2")
		{
			actualizado = true;
			//No vale la pena intentar recuperar el dispositivo seleccionado
			estado |= this->consulta("DELETE FROM configuracion WHERE 	atributo = 'dispositivo_entrada'"
															"OR atributo = 'dispositivo_salida'"
															"OR atributo = 'teclado_util'"
															"OR atributo = 'teclas_luminosas'");

			//Actualiza tablas configuracion
			estado |= this->consulta("DELETE FROM configuracion WHERE atributo IS NULL OR valor IS NULL");
			estado |= this->consulta("ALTER TABLE configuracion RENAME TO configuracion_ant");
			estado |= this->consulta("CREATE TABLE configuracion (atributo TEXT NOT NULL, valor TEXT NOT NULL)");
			estado |= this->consulta("INSERT INTO configuracion (atributo, valor) SELECT atributo, valor FROM configuracion_ant");
			estado |= this->consulta("DROP TABLE configuracion_ant");

			//Actualiza tablas carpetas
			estado |= this->consulta("ALTER TABLE carpetas RENAME TO carpetas_ant");
			estado |= this->consulta("CREATE TABLE carpetas (nombre TEXT NOT NULL, ruta TEXT NOT NULL PRIMARY KEY)");
			estado |= this->consulta("INSERT INTO carpetas (nombre, ruta) SELECT nombre, ruta FROM carpetas_ant");
			estado |= this->consulta("DROP TABLE carpetas_ant");

			//Actualiza tabla selección
			estado |= this->consulta("DELETE FROM seleccion WHERE ruta_seleccion IS NULL");
			estado |= this->consulta("ALTER TABLE seleccion RENAME TO seleccion_ant");
			estado |= this->consulta("CREATE TABLE seleccion (ruta TEXT NOT NULL PRIMARY KEY, seleccion INTEGER DEFAULT 0, ruta_seleccion TEXT NOT NULL)");
			estado |= this->consulta("INSERT INTO seleccion (ruta, seleccion, ruta_seleccion) SELECT ruta, seleccion, ruta_seleccion FROM seleccion_ant");
			estado |= this->consulta("DROP TABLE seleccion_ant");

			//Actualiza tabla archivos
			estado |= this->consulta("ALTER TABLE archivos RENAME TO archivos_ant");
			estado |= this->consulta("CREATE TABLE archivos (ruta TEXT NOT NULL PRIMARY KEY, visitas INTEGER DEFAULT 0, duracion INTEGER DEFAULT 0, ultimo_acceso DATETIME)");
			estado |= this->consulta("INSERT INTO archivos (ruta, visitas, duracion, ultimo_acceso) SELECT ruta, visitas, duracion, ultimo_acceso FROM archivos_ant");
			estado |= this->consulta("DROP TABLE archivos_ant");

			//Crea la nueva tabla para almacenar la configuracion de los dispositivos
			estado |= this->consulta("CREATE TABLE dispositivos (	cliente INTEGER DEFAULT 0,"
														"puerto INTEGER DEFAULT 0,"
														"nombre TEXT NOT NULL,"
														"capacidad INTEGER DEFAULT 0,"
														"capacidad_activa INTEGER DEFAULT 0,"
														"habilitado INTEGER DEFAULT 0,"
														"sensitivo INTEGER DEFAULT 1,"
														"volumen_entrada REAL DEFAULT 1,"
														"rango_teclado TEXT NOT NULL,"
														"volumen_salida REAL DEFAULT 1,"
														"teclado_luminoso INTEGER DEFAULT 0,"
														"PRIMARY KEY(cliente, puerto, nombre))");

			if(!((estado & BASE_DE_DATOS_ERROR) == BASE_DE_DATOS_ERROR))
			{
				Dispositivo_Midi teclado_y_raton;
				teclado_y_raton.cliente(129);
				teclado_y_raton.puerto(0);
				teclado_y_raton.nombre("Teclado y Ratón");
				teclado_y_raton.capacidad(ENTRADA);
				teclado_y_raton.capacidad_activa(ENTRADA);
				teclado_y_raton.habilitado(true);
				teclado_y_raton.sensitivo(false);
				teclado_y_raton.rango_teclado("48,24");
				this->agregar_dispositivo(teclado_y_raton);

				Dispositivo_Midi timidity;
				timidity.cliente(128);
				timidity.puerto(0);
				timidity.nombre("TiMidity");
				timidity.capacidad(SALIDA);
				timidity.capacidad_activa(SALIDA);
				timidity.habilitado(true);
				this->agregar_dispositivo(timidity);

				version_actual = "1.3";
			}
		}
		else if(version_actual == "1.3")
		{
			actualizado = true;

			//Actualiza tablas configuracion
			estado |= this->consulta("ALTER TABLE configuracion RENAME TO configuracion_ant");
			estado |= this->consulta("CREATE TABLE configuracion (atributo TEXT NOT NULL PRIMARY KEY, valor TEXT NOT NULL)");
			estado |= this->consulta("INSERT INTO configuracion (atributo, valor) SELECT atributo, valor FROM configuracion_ant");
			estado |= this->consulta("DROP TABLE configuracion_ant");

			//Se crean indices
			estado |= this->consulta("CREATE INDEX indice_configuracion_atributo ON configuracion (atributo)");
			estado |= this->consulta("CREATE INDEX indice_carpetas_ruta ON carpetas (ruta)");
			estado |= this->consulta("CREATE INDEX indice_seleccion_ruta ON seleccion (ruta)");
			estado |= this->consulta("CREATE INDEX indice_archivos_ruta ON archivos (ruta)");
			estado |= this->consulta("CREATE INDEX indice_dispositivos_clave_primaria ON dispositivos (cliente, puerto, nombre)");

			if(!((estado & BASE_DE_DATOS_ERROR) == BASE_DE_DATOS_ERROR))
				version_actual = "1.4";
		}
		if(actualizado)
		{
			bool registro = this->escribir_configuracion("version_base_de_datos", std::to_string(VERSION_BASE_DE_DATOS));
			Registro::Nota(T_("Se actualizó la base de datos de la versión: {0} a la versión {1}", version_original, VERSION_BASE_DE_DATOS));

			if((estado & BASE_DE_DATOS_ERROR) == BASE_DE_DATOS_ERROR || !registro)
			{
				this->revertir_transaccion();
				Registro::Error(T("Fallo la actualización de la base de datos"));
			}
			else
				this->finalizar_transaccion();
		}
		else
			this->finalizar_transaccion();
	}

	//Cambia la ruta de instalacion de la carpeta musica
	std::string ruta_actual = this->leer_configuracion("ruta_instalacion");
	std::string ruta_nueva = RUTA_ARCHIVOS;
	if(ruta_actual != ruta_nueva)
	{
		this->iniciar_transaccion();
		int estado = this->consulta("UPDATE carpetas SET ruta=?1 || substr(ruta, length(?2)+1) WHERE ruta LIKE '"+ruta_actual+"/musica%'", ruta_nueva, ruta_actual);
		estado |= this->consulta("UPDATE archivos SET ruta=?1 || substr(ruta, length(?2)+1) WHERE ruta LIKE '"+ruta_actual+"/musica/%'", ruta_nueva, ruta_actual);
		estado |= this->consulta("UPDATE configuracion SET valor=?1 || substr(valor, length(?2)+1) WHERE valor LIKE '"+ruta_actual+"/musica/%'", ruta_nueva, ruta_actual);
		estado |= this->consulta("UPDATE seleccion SET ruta=?1 || substr(ruta, length(?2)+1) WHERE ruta LIKE '"+ruta_actual+"/musica/%'", ruta_nueva, ruta_actual);
		estado |= this->consulta("UPDATE seleccion SET ruta_seleccion=?1 || substr(ruta_seleccion, length(?2)+1) WHERE ruta_seleccion LIKE '"+ruta_actual+"/musica/%'", ruta_nueva, ruta_actual);

		//Ruta actualizada
		bool registro_correcto = this->escribir_configuracion("ruta_instalacion", ruta_nueva);

		if((estado & BASE_DE_DATOS_ERROR) == BASE_DE_DATOS_ERROR || !registro_correcto)
		{
			this->revertir_transaccion();
			Registro::Error(T("Fallo la actualización de la ruta de instalación"));
		}
		else
		{
			Registro::Nota(T("Ruta de instalación actualizada a: " + ruta_nueva));
			this->finalizar_transaccion();
		}
	}
}

void Base_de_Datos::iniciar_transaccion()
{
	if(!m_base_de_datos_abierta)
		return;

	m_bloqueo.lock();
	sqlite3_exec(m_base_de_datos, "BEGIN TRANSACTION;", nullptr, nullptr, nullptr);
	m_bloqueo.unlock();
}

void Base_de_Datos::finalizar_transaccion()
{
	if(!m_base_de_datos_abierta)
		return;

	m_bloqueo.lock();
	sqlite3_exec(m_base_de_datos, "END TRANSACTION;", nullptr, nullptr, nullptr);
	m_bloqueo.unlock();
}

void Base_de_Datos::revertir_transaccion()
{
	if(!m_base_de_datos_abierta)
		return;

	m_bloqueo.lock();
	sqlite3_exec(m_base_de_datos, "ROLLBACK;", nullptr, nullptr, nullptr);
	m_bloqueo.unlock();
}

//Tabla configuracion
bool Base_de_Datos::escribir_configuracion(const std::string &atributo, const std::string &valor)
{
	if(valor != "")
	{
		Registro::Depurar("Escribiendo el registro: " + atributo + "->"+valor);
		//No se aceptan atributos vacios
		if(this->leer_configuracion(atributo).size() == 0)
		{
			m_bloqueo.lock();
			std::string consulta = "INSERT INTO configuracion ('atributo', 'valor') VALUES (?1, ?2)";
			sqlite3_stmt *declaracion;
			int respuesta = sqlite3_prepare_v2(m_base_de_datos, consulta.c_str(), -1, &declaracion, nullptr);
			if(respuesta != SQLITE_OK)
			{
				sqlite3_finalize(declaracion);
				m_bloqueo.unlock();
				return false;
			}

			sqlite3_bind_text(declaracion, 1, atributo.c_str(), -1, SQLITE_TRANSIENT);
			sqlite3_bind_text(declaracion, 2, valor.c_str(), -1, SQLITE_TRANSIENT);

			sqlite3_step(declaracion);
			sqlite3_finalize(declaracion);
			m_bloqueo.unlock();
		}
		else
		{
			m_bloqueo.lock();
			std::string consulta = "UPDATE configuracion SET valor = ?1 WHERE atributo = ?2";
			sqlite3_stmt *declaracion;
			int respuesta = sqlite3_prepare_v2(m_base_de_datos, consulta.c_str(), -1, &declaracion, nullptr);
			if(respuesta != SQLITE_OK)
			{
				sqlite3_finalize(declaracion);
				m_bloqueo.unlock();
				return false;
			}

			sqlite3_bind_text(declaracion, 1, valor.c_str(), -1, SQLITE_TRANSIENT);
			sqlite3_bind_text(declaracion, 2, atributo.c_str(), -1, SQLITE_TRANSIENT);

			sqlite3_step(declaracion);
			sqlite3_finalize(declaracion);
			m_bloqueo.unlock();
		}
		return true;
	}
	else
	{
		Registro::Depurar("Atributo: " + atributo + " con valor vacio.");
		return false;
	}
}

std::string Base_de_Datos::leer_configuracion(const std::string &atributo)
{
	m_bloqueo.lock();
	std::string salida;
	std::string consulta = "SELECT valor FROM configuracion WHERE atributo = ?1 LIMIT 1";
	sqlite3_stmt *declaracion;
	int respuesta = sqlite3_prepare_v2(m_base_de_datos, consulta.c_str(), -1, &declaracion, nullptr);
	if(respuesta == SQLITE_OK)
	{
		sqlite3_bind_text(declaracion, 1, atributo.c_str(), -1, SQLITE_TRANSIENT);

		sqlite3_step(declaracion);
		if(sqlite3_column_text(declaracion, 0) != nullptr)
		{
			salida = std::string(reinterpret_cast<const char*>(sqlite3_column_text(declaracion, 0)));
		}
	}
	sqlite3_finalize(declaracion);
	m_bloqueo.unlock();
	return salida;
}

bool Base_de_Datos::existe_dispositivo(const Dispositivo_Midi &dispositivo)
{
	if(!m_base_de_datos_abierta)
		return false;

	m_bloqueo.lock();
	std::string consulta = "SELECT nombre FROM dispositivos WHERE cliente = ?1 AND puerto = ?2 AND nombre = ?3";
	sqlite3_stmt * declaracion;
	int respuesta = sqlite3_prepare_v2(m_base_de_datos, consulta.c_str(), -1, &declaracion, nullptr);
	if(respuesta == SQLITE_OK)
	{
		sqlite3_bind_int(declaracion, 1, dispositivo.cliente());
		sqlite3_bind_int(declaracion, 2, dispositivo.puerto());
		sqlite3_bind_text(declaracion, 3, dispositivo.nombre().c_str(), -1, SQLITE_TRANSIENT);

		sqlite3_step(declaracion);
		if(sqlite3_column_text(declaracion, 0) != nullptr)
		{
			sqlite3_finalize(declaracion);
			m_bloqueo.unlock();
			return true;
		}
	}
	sqlite3_finalize(declaracion);
	m_bloqueo.unlock();
	return false;
}

void Base_de_Datos::agregar_dispositivo(const Dispositivo_Midi &dispositivo)
{
	//Inserta dispositivo nuevo
	if(!m_base_de_datos_abierta)
		return;

	m_bloqueo.lock();
	std::string consulta = "INSERT INTO dispositivos (cliente, puerto, nombre, capacidad, capacidad_activa, habilitado,"
											" sensitivo, volumen_entrada, rango_teclado, volumen_salida, teclado_luminoso) "
								"VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11)";
	sqlite3_stmt *declaracion;
	int respuesta = sqlite3_prepare_v2(m_base_de_datos, consulta.c_str(), -1, &declaracion, nullptr);
	if(respuesta == SQLITE_OK)
	{
		sqlite3_bind_int(declaracion, 1, dispositivo.cliente());
		sqlite3_bind_int(declaracion, 2, dispositivo.puerto());
		sqlite3_bind_text(declaracion, 3, dispositivo.nombre().c_str(), -1, SQLITE_TRANSIENT);
		sqlite3_bind_int(declaracion, 4, dispositivo.capacidad());
		sqlite3_bind_int(declaracion, 5, dispositivo.capacidad_activa());
		sqlite3_bind_int(declaracion, 6, dispositivo.habilitado());
		sqlite3_bind_int(declaracion, 7, dispositivo.sensitivo());
		sqlite3_bind_double(declaracion, 8, dispositivo.volumen_entrada());
		sqlite3_bind_text(declaracion, 9, dispositivo.rango_teclado().texto().c_str(), -1, SQLITE_TRANSIENT);
		sqlite3_bind_double(declaracion, 10, dispositivo.volumen_salida());
		sqlite3_bind_int64(declaracion, 11, dispositivo.id_teclas_luminosas());

		sqlite3_step(declaracion);
	}
	else
		Registro::Nota(T("Error al agregar el dispositivo"));


	sqlite3_finalize(declaracion);
	m_bloqueo.unlock();
}

void Base_de_Datos::eliminar_dispositivo(const Dispositivo_Midi &dispositivo)
{
	if(!m_base_de_datos_abierta)
		return;

	m_bloqueo.lock();
	std::string consulta = "DELETE FROM dispositivos WHERE cliente = ?1 AND puerto = ?2 AND nombre = ?3";
	sqlite3_stmt *declaracion;
	int respuesta = sqlite3_prepare_v2(m_base_de_datos, consulta.c_str(), -1, &declaracion, nullptr);
	if(respuesta == SQLITE_OK)
	{
		sqlite3_bind_int(declaracion, 1, dispositivo.cliente());
		sqlite3_bind_int(declaracion, 2, dispositivo.puerto());
		sqlite3_bind_text(declaracion, 3, dispositivo.nombre().c_str(), -1, SQLITE_TRANSIENT);
		sqlite3_step(declaracion);
	}
	else
		Registro::Nota(T("Error al eliminar el dispositivo"));

	sqlite3_finalize(declaracion);
	m_bloqueo.unlock();
}

void Base_de_Datos::actualizar_cliente_dispositivo(unsigned char cliente_antiguo, const Dispositivo_Midi &dispositivo)
{
	//El dispositivo es encontrado en otro id cliente y hay que actualizarlo
	if(!m_base_de_datos_abierta)
		return;

	m_bloqueo.lock();
	std::string consulta = "UPDATE dispositivos SET cliente = ?1 WHERE cliente = ?2 AND puerto = ?3 AND nombre = ?4";
	sqlite3_stmt *declaracion;
	int respuesta = sqlite3_prepare_v2(m_base_de_datos, consulta.c_str(), -1, &declaracion, nullptr);
	if(respuesta == SQLITE_OK)
	{
		sqlite3_bind_int(declaracion, 1, dispositivo.cliente());
		sqlite3_bind_int(declaracion, 2, cliente_antiguo);
		sqlite3_bind_int(declaracion, 3, dispositivo.puerto());
		sqlite3_bind_text(declaracion, 4, dispositivo.nombre().c_str(), -1, SQLITE_TRANSIENT);
		sqlite3_step(declaracion);
	}
	else
		Registro::Nota(T("Error al actualizar el cliente del dispositivo"));

	sqlite3_finalize(declaracion);
	m_bloqueo.unlock();
}

std::vector<Dispositivo_Midi> Base_de_Datos::lista_dispositivos()
{
	m_bloqueo.lock();
	int estado;
	std::vector<std::vector<std::string>> datos = this->consulta_tabla("SELECT 	cliente, puerto, nombre, capacidad, capacidad_activa, "
																				"habilitado, sensitivo, volumen_entrada, rango_teclado, "
																				"volumen_salida, teclado_luminoso "
																		"FROM dispositivos", 11, &estado);
	std::vector<Dispositivo_Midi> dispositivos;

	for(unsigned int x=0; x<datos.size(); x++)
	{
		Dispositivo_Midi nuevo;

		if(datos[x][0].size() <= 3)
			nuevo.cliente(static_cast<unsigned char>(std::stoi(datos[x][0])));
		if(datos[x][1].size() <= 3)
			nuevo.puerto(static_cast<unsigned char>(std::stoi(datos[x][1])));
		nuevo.nombre(datos[x][2]);
		if(datos[x][3].size() <= 1)
			nuevo.capacidad(static_cast<unsigned char>(std::stoi(datos[x][3])));
		if(datos[x][4].size() <= 1)
			nuevo.capacidad_activa(static_cast<unsigned char>(std::stoi(datos[x][4])));
		if(datos[x][5].size() <= 1)
			nuevo.habilitado(static_cast<bool>(std::stoi(datos[x][5])));
		if(datos[x][6].size() <= 1)
			nuevo.sensitivo(static_cast<bool>(std::stoi(datos[x][6])));
		nuevo.volumen_entrada(static_cast<double>(std::stod(datos[x][7])));
		nuevo.rango_teclado(datos[x][8]);
		nuevo.volumen_salida(static_cast<double>(std::stod(datos[x][9])));
		if(datos[x][10].size() <= 3)
			nuevo.teclas_luminosas(static_cast<unsigned int>(std::stoi(datos[x][10])));

		dispositivos.push_back(nuevo);
	}
	m_bloqueo.unlock();
	return dispositivos;
}

bool Base_de_Datos::agregar_carpeta(const std::string &nombre, const std::string &ruta)
{
	if(!m_base_de_datos_abierta)
		return false;

	if(ruta.length() == 0)
		return false;

	m_bloqueo.lock();
	std::string consulta = "INSERT INTO carpetas ('nombre', 'ruta') VALUES (?1, ?2)";
	sqlite3_stmt *declaracion;
	int respuesta = sqlite3_prepare_v2(m_base_de_datos, consulta.c_str(), -1, &declaracion, nullptr);
	if(respuesta != SQLITE_OK)
	{
		sqlite3_finalize(declaracion);
		m_bloqueo.unlock();
		return false;
	}

	sqlite3_bind_text(declaracion, 1, nombre.c_str(), -1, SQLITE_TRANSIENT);
	sqlite3_bind_text(declaracion, 2, ruta.c_str(), -1, SQLITE_TRANSIENT);

	sqlite3_step(declaracion);
	sqlite3_finalize(declaracion);

	m_bloqueo.unlock();
	return true;
}

std::vector<std::vector<std::string>> Base_de_Datos::carpetas()
{
	m_bloqueo.lock();
	int estado;
	std::vector<std::vector<std::string>> salida = this->consulta_tabla("SELECT nombre, ruta FROM carpetas", 2, &estado);
	m_bloqueo.unlock();
	return salida;
}

bool Base_de_Datos::eliminar_carpeta(const std::string &ruta)
{
	if(!m_base_de_datos_abierta)
		return false;

	if(ruta.length() == 0)
		return false;

	this->iniciar_transaccion();

	m_bloqueo.lock();
	int estado = 0;
	std::string consulta = "DELETE FROM carpetas WHERE ruta = ?1";
	sqlite3_stmt *declaracion;
	int respuesta = sqlite3_prepare_v2(m_base_de_datos, consulta.c_str(), -1, &declaracion, nullptr);
	if(respuesta == SQLITE_OK)
	{
		sqlite3_bind_text(declaracion, 1, ruta.c_str(), -1, SQLITE_TRANSIENT);
		sqlite3_step(declaracion);
		estado = BASE_DE_DATOS_CORRECTO;
	}
	else
		estado = BASE_DE_DATOS_ERROR;
	sqlite3_finalize(declaracion);

	//Se eliminan los archivos de la carpeta
	estado |= this->consulta("DELETE FROM archivos "
									"WHERE archivos.ruta NOT IN "
									"(SELECT archivos.ruta FROM archivos, carpetas "
															"WHERE archivos.ruta LIKE carpetas.ruta || '%')");

	//Se eliminan las selecciones de la carpeta
	estado |= this->consulta("DELETE FROM seleccion "
									"WHERE seleccion.ruta NOT IN "
									"(SELECT seleccion.ruta FROM seleccion, carpetas "
															"WHERE seleccion.ruta LIKE carpetas.ruta || '%')");

	//Se elimina de la configuracion si esta marcada como ultima carpeta activa
	estado |= this->consulta("DELETE FROM configuracion WHERE configuracion.valor NOT IN "
														"(SELECT configuracion.valor FROM configuracion, carpetas "
																					"WHERE configuracion.valor = carpetas.ruta AND atributo = 'carpeta_inicial') "
														"AND atributo = 'carpeta_inicial'");

	//Borra la carpeta_activa si la carpeta_inicial es borrada
	int borrado = this->cambios();
	if(borrado > 0)
		estado |= this->consulta("DELETE FROM configuracion WHERE atributo = 'carpeta_activa'");
	m_bloqueo.unlock();

	if((estado & BASE_DE_DATOS_ERROR) == BASE_DE_DATOS_ERROR)
	{
		this->revertir_transaccion();
		Registro::Error(T_("Fallo la eliminación de la carpeta: {0}", ruta));
	}
	else
		this->finalizar_transaccion();

	return true;
}

std::string Base_de_Datos::ruta_carpeta_base(const std::string &ruta)
{
	std::string salida;
	if(!m_base_de_datos_abierta)
		return salida;

	if(ruta.length() == 0)
		return salida;

	m_bloqueo.lock();
	std::string consulta = "SELECT ruta FROM carpetas WHERE ?1 LIKE ruta || '%' ORDER BY length(ruta) DESC";
	sqlite3_stmt *declaracion;
	int respuesta = sqlite3_prepare_v2(m_base_de_datos, consulta.c_str(), -1, &declaracion, nullptr);
	if(respuesta == SQLITE_OK)
	{
		sqlite3_bind_text(declaracion, 1, ruta.c_str(), -1, SQLITE_TRANSIENT);
		sqlite3_step(declaracion);
		if(sqlite3_column_text(declaracion, 0) != nullptr)
			salida = std::string(reinterpret_cast<const char*>(sqlite3_column_text(declaracion, 0)));
	}

	sqlite3_finalize(declaracion);
	m_bloqueo.unlock();

	return salida;
}

void Base_de_Datos::guardar_ultima_seleccion(const std::string &ruta, unsigned int ultima_seleccion, const std::string &ruta_seleccion)
{
	if(!m_base_de_datos_abierta)
		return;

	if(ruta.length() == 0)
		return;

	m_bloqueo.lock();
	int existe = 0;
	std::string consulta = "SELECT EXISTS(SELECT seleccion FROM seleccion WHERE ruta = ?1 LIMIT 1)";
	sqlite3_stmt *declaracion;
	int respuesta = sqlite3_prepare_v2(m_base_de_datos, consulta.c_str(), -1, &declaracion, nullptr);
	if(respuesta == SQLITE_OK)
	{
		sqlite3_bind_text(declaracion, 1, ruta.c_str(), -1, SQLITE_TRANSIENT);
		sqlite3_step(declaracion);
		existe = sqlite3_column_int(declaracion, 0);
	}
	sqlite3_finalize(declaracion);

	if(existe)
	{
		std::string actualizar = "UPDATE seleccion SET seleccion = ?1, ruta_seleccion = ?2 WHERE ruta = ?3";
		sqlite3_stmt *declaracion_actualizar;
		int respuesta_2 = sqlite3_prepare_v2(m_base_de_datos, actualizar.c_str(), -1, &declaracion_actualizar, nullptr);
		if(respuesta_2 == SQLITE_OK)
		{
			sqlite3_bind_int64(declaracion_actualizar, 1, static_cast<long long>(ultima_seleccion));
			sqlite3_bind_text(declaracion_actualizar, 2, ruta_seleccion.c_str(), -1, SQLITE_TRANSIENT);
			sqlite3_bind_text(declaracion_actualizar, 3, ruta.c_str(), -1, SQLITE_TRANSIENT);

			sqlite3_step(declaracion_actualizar);
		}
		sqlite3_finalize(declaracion_actualizar);
	}
	else
	{
		std::string insertar = "INSERT INTO seleccion ('ruta', 'seleccion', 'ruta_seleccion') VALUES (?1, ?2, ?3)";
		sqlite3_stmt *declaracion_insertar;
		int respuesta_3 = sqlite3_prepare_v2(m_base_de_datos, insertar.c_str(), -1, &declaracion_insertar, nullptr);
		if(respuesta_3 == SQLITE_OK)
		{
			sqlite3_bind_text(declaracion_insertar, 1, ruta.c_str(), -1, SQLITE_TRANSIENT);
			sqlite3_bind_int64(declaracion_insertar, 2, static_cast<long long>(ultima_seleccion));
			sqlite3_bind_text(declaracion_insertar, 3, ruta_seleccion.c_str(), -1, SQLITE_TRANSIENT);

			sqlite3_step(declaracion_insertar);
		}
		sqlite3_finalize(declaracion_insertar);
	}
	m_bloqueo.unlock();
}

std::vector<std::string> Base_de_Datos::leer_ultima_seleccion(const std::string &ruta)
{
	std::vector<std::string> fila;

	if(!m_base_de_datos_abierta)
		return fila;

	if(ruta.length() == 0)
		return fila;

	m_bloqueo.lock();
	std::string consulta = "SELECT seleccion, ruta_seleccion FROM seleccion WHERE ruta = ?1 LIMIT 1";
	sqlite3_stmt *declaracion;
	int respuesta = sqlite3_prepare_v2(m_base_de_datos, consulta.c_str(), -1, &declaracion, nullptr);
	bool error = false;
	if(respuesta == SQLITE_OK)
	{
		sqlite3_bind_text(declaracion, 1, ruta.c_str(), -1, SQLITE_TRANSIENT);
		sqlite3_step(declaracion);

		if(sqlite3_column_text(declaracion, 0) != NULL)
			fila.push_back(std::string(reinterpret_cast<const char*>(sqlite3_column_text(declaracion, 0))));
		else
			error = true;

		if(sqlite3_column_text(declaracion, 1) != NULL)
			fila.push_back(std::string(reinterpret_cast<const char*>(sqlite3_column_text(declaracion, 1))));
		else
			error = true;

		if(error)
			fila.clear();
	}
	sqlite3_finalize(declaracion);

	m_bloqueo.unlock();
	return fila;
}

void Base_de_Datos::agregar_archivo(const std::string &ruta, std::int64_t duracion)
{
	if(!m_base_de_datos_abierta)
		return;

	if(ruta.length() == 0)
		return;

	m_bloqueo.lock();
	std::string consulta = "INSERT INTO archivos ('ruta', 'duracion') VALUES (?1, ?2)";
	sqlite3_stmt *declaracion;
	int respuesta = sqlite3_prepare_v2(m_base_de_datos, consulta.c_str(), -1, &declaracion, nullptr);
	if(respuesta == SQLITE_OK)
	{
		sqlite3_bind_text(declaracion, 1, ruta.c_str(), -1, SQLITE_TRANSIENT);
		sqlite3_bind_int64(declaracion, 2, duracion);

		sqlite3_step(declaracion);
	}

	sqlite3_finalize(declaracion);
	m_bloqueo.unlock();
}

void Base_de_Datos::actualizar_archivo(const std::string &ruta, std::int64_t duracion)
{
	if(!m_base_de_datos_abierta)
		return;

	if(ruta.length() == 0)
		return;

	m_bloqueo.lock();
	std::string consulta = "UPDATE archivos SET duracion = ?1 WHERE ruta = ?2";
	sqlite3_stmt *declaracion;
	int respuesta = sqlite3_prepare_v2(m_base_de_datos, consulta.c_str(), -1, &declaracion, nullptr);
	if(respuesta == SQLITE_OK)
	{
		sqlite3_bind_int64(declaracion, 1, duracion);
		sqlite3_bind_text(declaracion, 2, ruta.c_str(), -1, SQLITE_TRANSIENT);

		sqlite3_step(declaracion);
	}

	sqlite3_finalize(declaracion);
	m_bloqueo.unlock();
}

void Base_de_Datos::sumar_visita_archivo(const std::string &ruta)
{
	if(!m_base_de_datos_abierta)
		return;

	if(ruta.length() == 0)
		return;

	m_bloqueo.lock();
	std::string consulta = "UPDATE archivos SET visitas = visitas+1, ultimo_acceso = datetime('now', 'localtime') "
											"WHERE ruta = ?1";
	sqlite3_stmt *declaracion;
	int respuesta = sqlite3_prepare_v2(m_base_de_datos, consulta.c_str(), -1, &declaracion, nullptr);
	if(respuesta == SQLITE_OK)
	{
		sqlite3_bind_text(declaracion, 1, ruta.c_str(), -1, SQLITE_TRANSIENT);

		sqlite3_step(declaracion);
	}
	sqlite3_finalize(declaracion);
	m_bloqueo.unlock();
}

std::vector<std::string> Base_de_Datos::datos_archivo(const std::string &ruta)
{
	std::vector<std::string> fila;

	if(!m_base_de_datos_abierta)
		return fila;

	if(ruta.length() == 0)
		return fila;

	m_bloqueo.lock();
	std::string consulta = "SELECT visitas, duracion, ultimo_acceso FROM archivos WHERE ruta = ?1 LIMIT 1";
	sqlite3_stmt *declaracion;
	int respuesta = sqlite3_prepare_v2(m_base_de_datos, consulta.c_str(), -1, &declaracion, nullptr);
	bool error = false;
	if(respuesta == SQLITE_OK)
	{
		sqlite3_bind_text(declaracion, 1, ruta.c_str(), -1, SQLITE_TRANSIENT);
		sqlite3_step(declaracion);
		if(sqlite3_column_text(declaracion, 0) != NULL)
			fila.push_back(std::string(reinterpret_cast<const char*>(sqlite3_column_text(declaracion, 0))));
		else
			error = true;

		if(sqlite3_column_text(declaracion, 1) != NULL)
			fila.push_back(std::string(reinterpret_cast<const char*>(sqlite3_column_text(declaracion, 1))));
		else
			error = true;

		if(sqlite3_column_text(declaracion, 2) != NULL)
			fila.push_back(std::string(reinterpret_cast<const char*>(sqlite3_column_text(declaracion, 2))));
		else
			fila.push_back("");

		if(error)
			fila.clear();
	}
	sqlite3_finalize(declaracion);
	m_bloqueo.unlock();
	return fila;
}

std::vector<std::string> Base_de_Datos::lista_archivos()
{
	int estado;
	m_bloqueo.lock();
	std::vector<std::string> salida = this->consulta_columna("SELECT ruta FROM archivos", &estado);
	m_bloqueo.unlock();
	return salida;
}

std::vector<std::string> Base_de_Datos::lista_seleccion()
{
	int estado;
	m_bloqueo.lock();
	std::vector<std::string> salida = this->consulta_columna("SELECT ruta FROM seleccion", &estado);
	m_bloqueo.unlock();
	return salida;
}

void Base_de_Datos::borrar_archivo(const std::string &ruta)
{
	if(!m_base_de_datos_abierta)
		return;

	if(ruta.length() == 0)
		return;

	m_bloqueo.lock();
	std::string consulta = "DELETE FROM archivos WHERE ruta = ?1 LIMIT 1";
	sqlite3_stmt *declaracion;
	int respuesta = sqlite3_prepare_v2(m_base_de_datos, consulta.c_str(), -1, &declaracion, nullptr);
	if(respuesta == SQLITE_OK)
	{
		sqlite3_bind_text(declaracion, 1, ruta.c_str(), -1, SQLITE_TRANSIENT);

		sqlite3_step(declaracion);
	}
	sqlite3_finalize(declaracion);
	m_bloqueo.unlock();
}

void Base_de_Datos::borrar_archivos()
{
	m_bloqueo.lock();
	this->consulta("DELETE FROM archivos");
	m_bloqueo.unlock();
}

void Base_de_Datos::borrar_seleccion(const std::string &ruta)
{
	if(!m_base_de_datos_abierta)
		return;

	if(ruta.length() == 0)
		return;

	m_bloqueo.lock();
	std::string consulta = "DELETE FROM seleccion WHERE ruta = ?1 LIMIT 1";
	sqlite3_stmt *declaracion;
	int respuesta = sqlite3_prepare_v2(m_base_de_datos, consulta.c_str(), -1, &declaracion, nullptr);
	if(respuesta == SQLITE_OK)
	{
		sqlite3_bind_text(declaracion, 1, ruta.c_str(), -1, SQLITE_TRANSIENT);

		sqlite3_step(declaracion);
	}
	sqlite3_finalize(declaracion);
	m_bloqueo.unlock();
}

void Base_de_Datos::borrar_selecciones()
{
	m_bloqueo.lock();
	this->consulta("DELETE FROM seleccion");
	m_bloqueo.unlock();
}
