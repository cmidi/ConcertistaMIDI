#include "pista.h++"

#include "../util/traduccion.h++"

//El primer color es para poner la pista invisible
Color Pista::Colores_pista[NUMERO_COLORES_PISTA+1] = {
	Color(0.5f, 0.5f, 0.5f),	//MSL: 0, 0, 50
	Color(0.0f, 0.6f, 0.0f),	//MSL: 120, 1, 0.3
	Color(0.15f, 0.15f, 0.85f),	//MSL: 240, 0.7, 0.5
	Color(0.85f, 0.15f, 0.15f),	//MSL: 0, 0.7, 0.5
	Color(0.0f, 0.8f, 0.4f),	//MSL: 150, 1, 0.4
	Color(0.5f, 0.0f, 1.0f),	//MSL: 270, 1, 0.5
	Color(1.0f, 0.5f, 0.0f),	//MSL: 30, 1, 0.5
	Color(0.0f, 0.7f, 0.7f),	//MSL: 180, 1, 0.35
	Color(0.8f, 0.0f, 0.8f),	//MSL: 300, 1, 0.4
	Color(0.8f, 0.8f, 0.0f),	//MSL: 60, 1, 0.4
	Color(0.25f, 0.5f, 0.0f),	//MSL: 90, 1, 0.25
	Color(0.0f, 0.5f, 1.0f),	//MSL: 210, 1, 0.5
	Color(1.0f, 0.0f, 0.5f),	//MSL: 330, 1, 0.5
	Color(0.48f, 0.72f, 0.48f),	//MSL: 120, 0.3, 0.6
	Color(0.6f, 0.6f, 1.0f),	//MSL: 240, 1, 0.8
	Color(1.0f, 0.6f, 0.6f),	//MSL: 0, 1, 0.8
	Color(0.3f, 0.7f, 0.5f),	//MSL: 150, 0.4, 0.5
	Color(0.8f, 0.6f, 1.0f),	//MSL: 270, 1, 0.8
	Color(1.0f, 0.8f, 0.6f),	//MSL: 30, 1, 0.8
	Color(0.48f, 0.72f, 0.72f),	//MSL: 180, 0.3, 0.6
	Color(1.0f, 0.4f, 1.0f),	//MSL: 300, 1, 0.7
	Color(0.88f, 0.88f, 0.52f),	//MSL: 60, 0.6, 0.7
	Color(0.4f, 0.8f, 0.0f),	//MSL: 90, 1, 0.4
	Color(0.6f, 0.8f, 1.0f),	//MSL: 210, 1, 0.8
	Color(1.0f, 0.6f, 0.8f),	//MSL: 330, 1, 0.8
};

std::string Pista::Nombre_colores[NUMERO_COLORES_PISTA+1] = {
	//TR Sin Color, la pista no se muestra
	N("Invisible"),
	//TR Color de pista #009900
	N("Verde"),
	//TR Color de pista #2626d9
	N("Azul"),
	//TR Color de pista #d92626
	N("Rojo"),
	//TR Color de pista #00cc66
	N("Verde Cian"),
	//TR Color de pista #8000ff
	N("Violeta"),
	//TR Color de pista #ff8000
	N("Naranja"),
	//TR Color de pista #00b3b3
	N("Turquesa"),
	//TR Color de pista #cc00cc
	N("Fucsia"),
	//TR Color de pista #cccc00
	N("Amarillo verdoso"),
	//TR Color de pista #408000
	N("Verde Palta"),
	//TR Color de pista #0080ff
	N("Celeste"),
	//TR Color de pista #ff0080
	N("Rosado Mexicano"),
	//TR Color de pista #7ab87a
	N("Verde Palido"),
	//TR Color de pista #9999ff
	N("Azul Palido"),
	//TR Color de pista #ff9999
	N("Rosado Claro"),
	//TR Color de pista #4db380
	N("Verde Helecho"),
	//TR Color de pista #cc99ff
	N("Lila"),
	//TR Color de pista #ffcc99
	N("Carne Melón"),
	//TR Color de pista #7ab8b8
	N("Turquesa Pastel"),
	//TR Color de pista #ff66ff
	N("Malva"),
	//TR Color de pista #e0e085
	N("Caqui Claro"),
	//TR Color de pista #66cc00
	N("Verde Pistacho"),
	//TR Color de pista #99ccff
	N("Celeste Claro"),
	//TR Color de pista #ff99cc
	N("Rosado Amaranto"),
};

Pista::Pista(Pista_Midi *pista, Color color, Modo modo, bool visible, bool sonido)
{
	m_pista_midi = pista;
	m_color_pista = color;
	m_modo_pista = modo;
	m_visible = visible;
	m_sonido = sonido;
}

Pista::~Pista()
{
}

void Pista::color(Color color)
{
	m_color_pista = color;
}

void Pista::modo(Modo modo)
{
	m_modo_pista = modo;
}

void Pista::visible(bool estado)
{
	m_visible = estado;
}

void Pista::sonido(bool estado)
{
	m_sonido = estado;
}

Pista_Midi *Pista::pista_midi()
{
	return m_pista_midi;
}

Color Pista::color()
{
	return m_color_pista;
}

Modo Pista::modo()
{
	return m_modo_pista;
}

bool Pista::visible()
{
	return m_visible;
}

bool Pista::sonido()
{
	return m_sonido;
}
