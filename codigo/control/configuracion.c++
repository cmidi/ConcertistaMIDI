#include "configuracion.h++"

Configuracion::Configuracion()
{
	//Configuracion General
	m_pantalla_completa_original = false;
	m_habilitar_midi_2_original = true;

	//Configuracion Archivo
	m_carpeta_inicial_original = "-";
	m_carpeta_activa_original = "-";
	m_carpeta_grabacion_original = Usuario::carpeta_personal();
	m_grabacion_version_midi_original = 1;

	//Configuracion Reproduccion
	m_volumen_original = 1.0;
	m_velocidad_original = 1.0;
	m_duracion_nota_original = 6500;
	m_subtitulos_original = true;
	m_teclado_visible_original.cambiar(21, 88);
	m_etiqueta_tablero_nota_original = 0;
	m_etiqueta_organo_original = 0;
	m_mosrar_reproduccion_previa_original = false;

	std::string ruta_base_de_datos = Usuario::carpeta_juego() + "concertista.db";
	if(std::ifstream(ruta_base_de_datos))
	{
		//Se abre la base de datos existente
		Registro::Depurar("Abriendo la base de datos en: " + ruta_base_de_datos);
		if(m_datos.abrir(ruta_base_de_datos))
		{
			if(m_datos.comprobar())
			{
				m_datos.actualizar();//Actualiza la base de datos

				//Configuracion General
				m_pantalla_completa_original = this->leer(CONFIGURACION_PANTALLA_COMPLETA, m_pantalla_completa_original);
				m_habilitar_midi_2_original = this->leer(CONFIGURACION_HABILITAR_MIDI_2, m_habilitar_midi_2_original);

				//Configuracion Archivo
				m_carpeta_inicial_original = this->leer(CONFIGURACION_CARPETA_INICIAL, m_carpeta_inicial_original);
				m_carpeta_activa_original = this->leer(CONFIGURACION_CARPETA_ACTIVA, m_carpeta_activa_original);
				m_carpeta_grabacion_original = this->leer(CONFIGURACION_CARPETA_GRABACION, m_carpeta_grabacion_original);
				m_grabacion_version_midi_original = this->leer(CONFIGURACION_GRABACION_VERSION_MIDI, m_grabacion_version_midi_original);

				//Configuracion Reproduccion
				m_volumen_original = this->leer(CONFIGURACION_VOLUMEN, m_volumen_original);
				m_velocidad_original = this->leer(CONFIGURACION_VELOCIDAD, m_velocidad_original);
				m_duracion_nota_original = this->leer(CONFIGURACION_DURACION_NOTA, m_duracion_nota_original);
				m_subtitulos_original = this->leer(CONFIGURACION_SUBTITULOS, m_subtitulos_original);
				m_teclado_visible_original.cargar(this->leer(CONFIGURACION_TECLADO_VISIBLE, std::string("21,88")));
				m_etiqueta_tablero_nota_original = this->leer(CONFIGURACION_ETIQUETA_NOTAS, m_etiqueta_tablero_nota_original);
				m_etiqueta_organo_original = this->leer(CONFIGURACION_ETIQUETA_ORGANO, m_etiqueta_organo_original);
				m_mosrar_reproduccion_previa_original = this->leer(CONFIGURACION_MOSTRAR_REPRODUCCION_PREVIA, m_mosrar_reproduccion_previa_original);
			}
		}
		else
			Registro::Error(T("No se puede abrir la base de datos"));
	}
	else
	{
		//Se crea la base de datos si no existe
		Registro::Depurar("Creando la base de datos en: " + ruta_base_de_datos);
		if(m_datos.abrir(ruta_base_de_datos))
			m_datos.crear();
		else
			Registro::Error(T("No se puede crear la base de datos"));
	}

	if(!m_datos.comprobar())
	{
		//Si en este punto no hay una base de datos valida se intenta corregir el problema
		m_datos.cerrar();

		try
		{
			Registro::Error(T_("Base de datos corrupta, se guardada en: {0}concertista_corrupto.db", Usuario::carpeta_juego()));
			std::filesystem::rename(Usuario::carpeta_juego() + "concertista.db", Usuario::carpeta_juego() + "concertista_corrupto.db");
			Registro::Depurar("Creando la base de datos en: " + ruta_base_de_datos);
			if(m_datos.abrir(ruta_base_de_datos))
				m_datos.crear();
			else
				Registro::Error(T("No se puede crear la nueva base de datos"));
		}
		catch(std::filesystem::filesystem_error const &e)
		{
			Registro::Error(e.what());
		}
	}

	//Se guarda la configuracion original para poder ver cual se ha modificado
	//Configuracion General
	m_pantalla_completa = m_pantalla_completa_original;
	m_habilitar_midi_2 = m_habilitar_midi_2_original;

	//Configuracion Archivo
	m_carpeta_inicial = m_carpeta_inicial_original;
	m_carpeta_activa = m_carpeta_activa_original;
	m_carpeta_grabacion = m_carpeta_grabacion_original;
	m_grabacion_version_midi = m_grabacion_version_midi_original;

	//Configuracion Reproduccion
	this->volumen(m_volumen_original);
	this->velocidad(m_velocidad_original);
	this->duracion_nota(m_duracion_nota_original);
	m_subtitulos = m_subtitulos_original;
	m_teclado_visible = m_teclado_visible_original;
	m_etiqueta_tablero_nota = m_etiqueta_tablero_nota_original;
	m_etiqueta_organo = m_etiqueta_organo_original;
	m_mosrar_reproduccion_previa = m_mosrar_reproduccion_previa_original;

	m_controlador_midi.habilitar_midi_2(m_habilitar_midi_2);

	//Se cargan los dispositivos disponibles
	std::vector<Dispositivo_Midi> lista_dispositivos = m_datos.lista_dispositivos();
	for(Dispositivo_Midi &datos : lista_dispositivos)
	{
		if(datos.habilitado())
		{
			Dispositivo_Midi *dispositivo = m_controlador_midi.obtener_dispositivo(datos);

			//Si cambio el id del cliente se actualiza la base de datos
			if(dispositivo->cambio_cliente())
			{
				m_datos.actualizar_cliente_dispositivo(datos.cliente(), *dispositivo);
				dispositivo->cambio_cliente(false);
			}

			//Carga la configuracion de la base de datos
			dispositivo->copiar_configuracion(datos);
			m_controlador_midi.conectar_dispositivo(dispositivo);
		}
	}
	this->actualizar_rango_util_organo();
}

Configuracion::~Configuracion()
{
}

Base_de_Datos* Configuracion::base_de_datos()
{
	return &m_datos;
}
std::string Configuracion::leer(std::string atributo, std::string predeterminado)
{
	std::string respuesta = m_datos.leer_configuracion(atributo);
	if(respuesta != "")
		return respuesta;
	return predeterminado;
}

unsigned char Configuracion::leer(std::string atributo, unsigned char predeterminado)
{
	std::string respuesta = m_datos.leer_configuracion(atributo);
	if(respuesta != "")
		return static_cast<unsigned char>(std::stoi(respuesta));
	return predeterminado;
}

unsigned short Configuracion::leer(std::string atributo, unsigned short predeterminado)
{
	std::string respuesta = m_datos.leer_configuracion(atributo);
	if(respuesta != "")
		return static_cast<unsigned short>(std::stoi(respuesta));
	return predeterminado;
}

unsigned int Configuracion::leer(std::string atributo, unsigned int predeterminado)
{
	std::string respuesta = m_datos.leer_configuracion(atributo);
	if(respuesta != "")
		return static_cast<unsigned int>(std::stoi(respuesta));
	return predeterminado;
}

double Configuracion::leer(std::string atributo, double predeterminado)
{
	std::string respuesta = m_datos.leer_configuracion(atributo);
	if(respuesta != "")
		return static_cast<double>(std::stod(respuesta));
	return predeterminado;
}

bool Configuracion::leer(std::string atributo, bool predeterminado)
{
	std::string respuesta = m_datos.leer_configuracion(atributo);
	if(respuesta == VERDADERO)
		return true;
	else if (respuesta == FALSO)
		return false;
	return predeterminado;
}

void Configuracion::guardar_configuracion()
{
	//Escribe la configuracion cambiada, usar al salir
	//Configuracion MIDI
	m_datos.iniciar_transaccion();

	//Configuracion General
	if(m_pantalla_completa_original != m_pantalla_completa)
	{
		if(m_pantalla_completa)
			m_datos.escribir_configuracion(CONFIGURACION_PANTALLA_COMPLETA, VERDADERO);
		else
			m_datos.escribir_configuracion(CONFIGURACION_PANTALLA_COMPLETA, FALSO);
	}
	if(m_habilitar_midi_2_original != m_habilitar_midi_2)
	{
		if(m_habilitar_midi_2)
			m_datos.escribir_configuracion(CONFIGURACION_HABILITAR_MIDI_2, VERDADERO);
		else
			m_datos.escribir_configuracion(CONFIGURACION_HABILITAR_MIDI_2, FALSO);
	}

	//Configuracion Archivo
	if(m_carpeta_inicial_original != m_carpeta_inicial)
		m_datos.escribir_configuracion(CONFIGURACION_CARPETA_INICIAL, m_carpeta_inicial);
	if(m_carpeta_activa_original != m_carpeta_activa)
		m_datos.escribir_configuracion(CONFIGURACION_CARPETA_ACTIVA, m_carpeta_activa);
	if(m_carpeta_grabacion_original != m_carpeta_grabacion)
		m_datos.escribir_configuracion(CONFIGURACION_CARPETA_GRABACION, m_carpeta_grabacion);
	if(m_grabacion_version_midi_original != m_grabacion_version_midi)
		m_datos.escribir_configuracion(CONFIGURACION_GRABACION_VERSION_MIDI, std::to_string(static_cast<unsigned int>(m_grabacion_version_midi)));

	//Configuracion Reproduccion
	if(!Funciones::double_son_iguales(m_volumen_original, m_volumen, 0.001))
		m_datos.escribir_configuracion(CONFIGURACION_VOLUMEN, std::to_string(m_volumen));
	if(!Funciones::double_son_iguales(m_velocidad_original, m_velocidad, 0.001))
		m_datos.escribir_configuracion(CONFIGURACION_VELOCIDAD, std::to_string(m_velocidad));
	if(m_duracion_nota_original != m_duracion_nota)
		m_datos.escribir_configuracion(CONFIGURACION_DURACION_NOTA, std::to_string(static_cast<unsigned int>(m_duracion_nota)));
	if(m_subtitulos_original != m_subtitulos)
	{
		if(m_subtitulos)
			m_datos.escribir_configuracion(CONFIGURACION_SUBTITULOS, VERDADERO);
		else
			m_datos.escribir_configuracion(CONFIGURACION_SUBTITULOS, FALSO);
	}
	if(m_teclado_visible_original != m_teclado_visible)
		m_datos.escribir_configuracion(CONFIGURACION_TECLADO_VISIBLE, m_teclado_visible.texto());
	if(m_etiqueta_tablero_nota_original != m_etiqueta_tablero_nota)
		m_datos.escribir_configuracion(CONFIGURACION_ETIQUETA_NOTAS, std::to_string(static_cast<unsigned int>(m_etiqueta_tablero_nota)));
	if(m_etiqueta_organo_original != m_etiqueta_organo)
		m_datos.escribir_configuracion(CONFIGURACION_ETIQUETA_ORGANO, std::to_string(static_cast<unsigned int>(m_etiqueta_organo)));
	if(m_mosrar_reproduccion_previa_original != m_mosrar_reproduccion_previa)
	{
		if(m_mosrar_reproduccion_previa)
			m_datos.escribir_configuracion(CONFIGURACION_MOSTRAR_REPRODUCCION_PREVIA, VERDADERO);
		else
			m_datos.escribir_configuracion(CONFIGURACION_MOSTRAR_REPRODUCCION_PREVIA, FALSO);
	}
	m_datos.finalizar_transaccion();
}

void Configuracion::actualizar_rango_util_organo()
{
	std::vector<Dispositivo_Midi*> dispositivos = m_controlador_midi.lista_dispositivos();

	//Tamaño minimo para quedarme con el mayor de todos los rangos
	if(dispositivos.size() > 0)
		m_teclado_util.cambiar(0, 24);//El menor de todos

	bool nuevo_rango = false;
	for(Dispositivo_Midi *d : dispositivos)
	{
		if(d->habilitado())
		{
			//Se queda con el teclado mas grande
			if(d->conectado() && d->es_entrada() && m_teclado_util <= d->rango_teclado())
			{
				m_teclado_util.cambiar(d->rango_teclado().tecla_inicial(), d->rango_teclado().numero_teclas());
				nuevo_rango = true;
			}
		}
	}
	//Si no hay dispositivo de entrada o no estan habilitados se muestra una rango predeterminado
	if(!nuevo_rango)
		m_teclado_util.cambiar(21, 88);
}

Controlador_Midi *Configuracion::controlador_midi()
{
	return &m_controlador_midi;
}

void Configuracion::actualizar_dispositivo_en_registro()
{
	std::map<Dispositivo_Midi*, unsigned char> lista_dispositivos = m_controlador_midi.lista_cambio_cliente();
	std::map<Dispositivo_Midi*, unsigned char>::iterator actual = lista_dispositivos.begin();
	while(actual != lista_dispositivos.end())
	{
		m_datos.actualizar_cliente_dispositivo(actual->second, *actual->first);
		actual++;
	}
}

//Configuracion General
void Configuracion::pantalla_completa(bool estado)
{
	m_pantalla_completa = estado;
}

bool Configuracion::pantalla_completa()
{
	return m_pantalla_completa;
}

void Configuracion::habilitar_midi_2(bool estado)
{
	m_habilitar_midi_2 = estado;
	m_controlador_midi.habilitar_midi_2(estado);
}

bool Configuracion::habilitar_midi_2()
{
	return m_habilitar_midi_2;
}

//Configuracion Archivo
void Configuracion::carpeta_inicial(const std::string& carpeta)
{
	m_carpeta_inicial = carpeta;
}

void Configuracion::carpeta_activa(const std::string& carpeta)
{
	m_carpeta_activa = carpeta;
}

void Configuracion::carpeta_grabacion(const std::string& carpeta)
{
	m_carpeta_grabacion = carpeta;
}

std::string Configuracion::carpeta_inicial()
{
	return m_carpeta_inicial;
}

std::string Configuracion::carpeta_activa()
{
	return m_carpeta_activa;
}

std::string Configuracion::carpeta_grabacion()
{
	return m_carpeta_grabacion;
}

void Configuracion::eliminar_carpeta(const std::string& carpeta)
{
	//Antes de eliminar la carpeta primero se guardan los cambios
	if(m_carpeta_inicial_original != m_carpeta_inicial)
		m_datos.escribir_configuracion(CONFIGURACION_CARPETA_INICIAL, m_carpeta_inicial);
	if(m_carpeta_activa_original != m_carpeta_activa)
		m_datos.escribir_configuracion(CONFIGURACION_CARPETA_ACTIVA, m_carpeta_activa);

	m_datos.eliminar_carpeta(carpeta);

	//Carga nuevamente la configuracion por si ha cambiado
	m_carpeta_inicial = "-";
	m_carpeta_activa = "-";
	m_carpeta_inicial = this->leer(CONFIGURACION_CARPETA_INICIAL, m_carpeta_inicial);
	m_carpeta_activa = this->leer(CONFIGURACION_CARPETA_ACTIVA, m_carpeta_activa);
}

void Configuracion::grabacion_version_midi(unsigned char version)
{
	m_grabacion_version_midi = version;
}

unsigned char Configuracion::grabacion_version_midi()
{
	return m_grabacion_version_midi;
}

//Configuracion Reproduccion
void Configuracion::volumen(double valor)
{
	if(valor < 0)
		m_volumen = 0;
	else if(valor > 2)
		m_volumen = 2;
	else
		m_volumen = valor;
}

void Configuracion::velocidad(double valor)
{
	if(valor <= 0)
		m_velocidad = 0.01;
	else if(valor > 2)
		m_velocidad = 2;
	else
		m_velocidad = valor;
}

void Configuracion::duracion_nota(unsigned short duracion)
{
	if(duracion < 1500)
		m_duracion_nota = 1500;
	else if(duracion > 14000)
		m_duracion_nota = 14000;
	else
		m_duracion_nota = duracion;
}

void Configuracion::subtitulos(bool estado)
{
	m_subtitulos = estado;
}

void Configuracion::teclado_visible(unsigned char inicial, unsigned char largo)
{
	m_teclado_visible.cambiar(inicial, largo);
}

void Configuracion::etiqueta_tablero_nota(unsigned char opcion)
{
	m_etiqueta_tablero_nota = opcion;
}

void Configuracion::etiqueta_organo(unsigned char opcion)
{
	m_etiqueta_organo = opcion;
}

void Configuracion::mosrar_reproduccion_previa(unsigned char estado)
{
	m_mosrar_reproduccion_previa = estado;
}

double Configuracion::volumen()
{
	return m_volumen;
}

double Configuracion::velocidad()
{
	return m_velocidad;
}

unsigned short Configuracion::duracion_nota()
{
	return m_duracion_nota;
}

bool Configuracion::subtitulos()
{
	return m_subtitulos;
}

unsigned char Configuracion::etiqueta_tablero_nota()
{
	return m_etiqueta_tablero_nota;
}

unsigned char Configuracion::etiqueta_organo()
{
	return m_etiqueta_organo;
}

bool Configuracion::mosrar_reproduccion_previa()
{
	return m_mosrar_reproduccion_previa;
}

Rango_Organo Configuracion::teclado_visible()
{
	return m_teclado_visible;
}

Rango_Organo Configuracion::teclado_util()
{
	return m_teclado_util;
}

void Configuracion::posicion_ventana(int *x, int *y)
{
	std::string posicion = m_datos.leer_configuracion(CONFIGURACION_POSICION_VENTANA);
	bool correcto = false;
	if(posicion != "")
	{
		std::vector<std::string> datos = Texto::dividir_texto(posicion, ',');
		if(datos.size() > 1)
		{
			*x = std::stoi(datos[0]);
			*y = std::stoi(datos[1]);
			correcto = true;
		}
	}
	if(!correcto)
	{
		*x = 0;
		*y = 0;
	}
}

void Configuracion::dimension_ventana(int *ancho, int *alto)
{
	std::string dimension = m_datos.leer_configuracion(CONFIGURACION_DIMENSION_VENTANA);
	bool correcto = false;
	if(dimension != "")
	{
		std::vector<std::string> datos = Texto::dividir_texto(dimension, 'x');
		if(datos.size() > 1)
		{
			*ancho = std::stoi(datos[0]);
			*alto = std::stoi(datos[1]);
			correcto = true;
		}
	}
	if(!correcto)
	{
		*ancho = ANCHO;
		*alto = ALTO;
	}
}

void Configuracion::guardar_posicion_ventana(int x, int y)
{
	m_datos.escribir_configuracion(CONFIGURACION_POSICION_VENTANA, std::to_string(x) + "," + std::to_string(y));
}

void Configuracion::guardar_dimension_ventana(int ancho, int alto)
{
	m_datos.escribir_configuracion(CONFIGURACION_DIMENSION_VENTANA, std::to_string(ancho) + "x" + std::to_string(alto));
}
