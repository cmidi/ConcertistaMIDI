#ifndef CONFIGURACION_H
#define CONFIGURACION_H

#include "base_de_datos.h++"
#include "../util/usuario.h++"
#include "../util/texto.h++"
#include "../control/rango_organo.h++"
#include "../elementos_graficos/notificacion.h++"
#include "../dispositivos_midi/controlador_midi.h++"
#include "../dispositivos_midi/teclas_luminosas.h++"

#define VERDADERO "verdadero"
#define FALSO "falso"

#define ANCHO 800
#define ALTO 600

#define CONFIGURACION_PANTALLA_COMPLETA "pantalla_completa"
#define CONFIGURACION_CARPETA_INICIAL "carpeta_inicial"
#define CONFIGURACION_CARPETA_ACTIVA "carpeta_activa"
#define CONFIGURACION_CARPETA_GRABACION "carpeta_grabacion"
#define CONFIGURACION_HABILITAR_MIDI_2 "habilitar_midi_2"
#define CONFIGURACION_GRABACION_VERSION_MIDI "grabacion_version_midi"
#define CONFIGURACION_VOLUMEN "volumen"
#define CONFIGURACION_VELOCIDAD "velocidad"
#define CONFIGURACION_DURACION_NOTA "duracion_nota"
#define CONFIGURACION_SUBTITULOS "subtitulos"
#define CONFIGURACION_TECLADO_VISIBLE "teclado_visible"
#define CONFIGURACION_ETIQUETA_NOTAS "etiqueta_notas"
#define CONFIGURACION_ETIQUETA_ORGANO "etiqueta_organo"
#define CONFIGURACION_MOSTRAR_REPRODUCCION_PREVIA "mostrar_reproduccion_previa"
#define CONFIGURACION_POSICION_VENTANA "posicion_ventana"
#define CONFIGURACION_DIMENSION_VENTANA "dimension_ventana"

class Configuracion
{
private:
	//Base de datos
	Base_de_Datos m_datos;

	//Configuracion General
	bool m_pantalla_completa, m_pantalla_completa_original;
	bool m_habilitar_midi_2, m_habilitar_midi_2_original;

	//Configuracion MIDI
	Controlador_Midi m_controlador_midi;

	//Configuracion Archivo
	std::string m_carpeta_inicial, m_carpeta_inicial_original;
	std::string m_carpeta_activa, m_carpeta_activa_original;
	std::string m_carpeta_grabacion, m_carpeta_grabacion_original;
	unsigned char m_grabacion_version_midi, m_grabacion_version_midi_original;

	//Configuracion Reproduccion
	double m_volumen, m_volumen_original;
	double m_velocidad, m_velocidad_original;
	unsigned short m_duracion_nota, m_duracion_nota_original;
	bool m_subtitulos, m_subtitulos_original;
	Rango_Organo m_teclado_visible, m_teclado_visible_original;
	Rango_Organo m_teclado_util;
	unsigned char m_etiqueta_tablero_nota, m_etiqueta_tablero_nota_original;
	unsigned char m_etiqueta_organo, m_etiqueta_organo_original;
	bool m_mosrar_reproduccion_previa, m_mosrar_reproduccion_previa_original;//En tablero_notas

	std::string leer(std::string atributo, std::string predeterminado);
	unsigned char leer(std::string atributo, unsigned char predeterminado);
	unsigned short leer(std::string atributo, unsigned short predeterminado);
	unsigned int leer(std::string atributo, unsigned int predeterminado);
	double leer(std::string atributo, double predeterminado);
	bool leer(std::string atributo, bool predeterminado);

public:
	Configuracion();
	~Configuracion();

	//Base de datos
	Base_de_Datos* base_de_datos();
	void guardar_configuracion();
	void actualizar_rango_util_organo();

	//Configuracion MIDI
	Controlador_Midi *controlador_midi();
	void actualizar_dispositivo_en_registro();

	//Configuracion General
	void pantalla_completa(bool estado);
	bool pantalla_completa();
	void habilitar_midi_2(bool estado);
	bool habilitar_midi_2();

	//Configuracion Archivo
	void carpeta_inicial(const std::string& carpeta);
	void carpeta_activa(const std::string& carpeta);
	void carpeta_grabacion(const std::string& carpeta);
	std::string carpeta_inicial();
	std::string carpeta_activa();
	std::string carpeta_grabacion();
	void eliminar_carpeta(const std::string& carpeta);
	void grabacion_version_midi(unsigned char version);
	unsigned char grabacion_version_midi();

	//Configuracion Reproduccion
	void volumen(double valor);
	void velocidad(double valor);
	void duracion_nota(unsigned short duracion);
	void subtitulos(bool estado);
	void teclado_visible(unsigned char inicial, unsigned char largo);
	void etiqueta_tablero_nota(unsigned char opcion);
	void etiqueta_organo(unsigned char opcion);
	void mosrar_reproduccion_previa(unsigned char estado);

	double volumen();
	double velocidad();
	unsigned short duracion_nota();
	bool subtitulos();
	unsigned char etiqueta_tablero_nota();
	unsigned char etiqueta_organo();
	bool mosrar_reproduccion_previa();

	Rango_Organo teclado_visible();
	Rango_Organo teclado_util();

	//Configuracion de ventana
	void posicion_ventana(int *x, int *y);
	void dimension_ventana(int *ancho, int *alto);
	void guardar_posicion_ventana(int x, int y);
	void guardar_dimension_ventana(int ancho, int alto);
};

#endif
