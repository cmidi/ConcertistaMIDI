#ifndef DATOS_MUSICA_H
#define DATOS_MUSICA_H

#include "../control/pista.h++"
#include "../control/tiempos_nota.h++"
#include "../libreria_midi/midi.h++"

#include <vector>
#include <map>

typedef std::map<unsigned short, std::vector<Tiempos_Nota>> TiempoTocado;//Notas por pista
struct Evaluacion
{
	std::map<unsigned short, TiempoTocado*> tiempo_tocado;//Evaluaciones por secuencia
	unsigned int notas_totales;
	unsigned int notas_tocadas;
	unsigned int errores;
};

class Datos_Musica
{
private:
	Midi *m_midi;
	std::map<unsigned short, std::vector<Pista>*> m_pistas;//<secuencias, pistas>
	Evaluacion m_evaluacion;

	std::string m_nombre_musica;
	std::string m_autor;
public:
	Datos_Musica();
	~Datos_Musica();

	std::string cargar_midi(std::string direccion);
	void pistas(unsigned short secuencia, std::vector<Pista> *pistas);

	Midi *midi();
	bool secuencia_cargada(unsigned short secuencia);
	std::vector<Pista> *pistas();
	Evaluacion *evaluacion();
	TiempoTocado *tiempo_tocado();

	std::string nombre_musica();
	std::string autor();
};

#endif
