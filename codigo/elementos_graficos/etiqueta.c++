#include "etiqueta.h++"

Color Etiqueta::Ultimo_color;
bool Etiqueta::Color_enviado = false;

Etiqueta::Etiqueta(Administrador_Recursos *recursos) : Elemento(0, 0, 0, 0), Figura(recursos->sombreador(SombreadorVF::Texto))
{
	m_rectangulo = recursos->figura(FiguraGeometrica::Rectangulo);
}

Etiqueta::Etiqueta(float x, float y, const std::string &texto, ModoLetra tipografia, Administrador_Recursos *recursos) : Elemento(x, y, 0, 0), Figura(recursos->sombreador(SombreadorVF::Texto))
{
	m_texto_actual = texto;
	m_texto = texto.c_str();
	m_tipografia = recursos->tipografia(tipografia);
	m_rectangulo = recursos->figura(FiguraGeometrica::Rectangulo);
	this->actualizar_texto();
}

Etiqueta::Etiqueta(float x, float y, const std::string &texto, Tipografia *tipografia, Administrador_Recursos *recursos) : Elemento(x, y, 0, 0), Figura(recursos->sombreador(SombreadorVF::Texto))
{
	m_texto_actual = texto;
	m_texto = texto.c_str();
	m_tipografia = tipografia;
	m_rectangulo = recursos->figura(FiguraGeometrica::Rectangulo);
	this->actualizar_texto();
}

Etiqueta::Etiqueta(float x, float y, float ancho, float alto, const std::string &texto, ModoLetra tipografia, Administrador_Recursos *recursos) : Elemento(x, y, ancho, alto), Figura(recursos->sombreador(SombreadorVF::Texto))
{
	m_texto_actual = texto;
	m_texto = texto.c_str();
	m_tipografia = recursos->tipografia(tipografia);
	m_rectangulo = recursos->figura(FiguraGeometrica::Rectangulo);
	this->actualizar_texto();
}

Etiqueta::~Etiqueta()
{
	this->limpiar();
}

void Etiqueta::actualizar_texto()
{
	if(m_tipografia != nullptr && m_texto.length() > 0)
	{
		this->limpiar();
		this->seleccionar_figura();
		m_ancho_texto = static_cast<float>(m_tipografia->crear_texto(m_texto, &m_indice_objeto));
		m_alto_texto = static_cast<float>(m_tipografia->alto_texto());

		if(m_ancho <= 0 && m_alto <= 0)
			this->dimension(m_ancho_texto + m_margen*2, m_alto_texto + m_margen*2);
		else if(m_ancho_automatico)
			this->dimension(m_ancho_texto + m_margen*2, this->alto());
		else
			this->dimension(this->ancho(), this->alto());//Actualiza la escala
	}
	else
	{
		if(m_tipografia == nullptr)
			m_alto_texto = 0;
		else
			m_alto_texto = static_cast<float>(m_tipografia->alto_texto());
		m_ancho_texto = 0;
	}
}

void Etiqueta::limpiar()
{

	if(m_indice_objeto > 0)
	{
		this->seleccionar_figura();
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glDeleteBuffers(1, &m_indice_objeto);
		m_indice_objeto = 0;
	}
}

void Etiqueta::actualizar(unsigned int /*diferencia_tiempo*/)
{
}

void Etiqueta::dibujar()
{
	if(m_texto.length() == 0)
		return;

	if(m_tipografia == nullptr)
		m_mostrar_fondo = true;

	if(m_mostrar_fondo)
	{
		m_rectangulo->textura(false);
		m_rectangulo->dibujar(this->x(), this->y(), this->ancho(), this->alto(), m_color_fondo);
	}

	if(Pantalla::ModoDesarrollo)
	{
		m_rectangulo->textura(false);

		if(m_escala >= 1)
			m_rectangulo->dibujar(this->x(), this->y(), this->ancho(), this->alto(), Color(0.1f, 0.7f, 0.1f, 0.2f));
		else
			m_rectangulo->dibujar(this->x(), this->y(), this->ancho(), this->alto(), Color(0.7f, 0.1f, 0.1f, 0.2f));

		m_rectangulo->dibujar(this->x()+m_margen, this->y()+m_margen, this->ancho()-(m_margen*2), this->alto()-(m_margen*2), Color(0.1f, 0.1f, 0.7f, 0.2f));
	}

	if(m_tipografia == nullptr)
	{
		//TR "Etiqueta" es un objeto, no debe traducirse
		Registro::Error(T_("Etiqueta sin tipografia en texto: {0}", m_texto_actual));
		return;
	}

	//Contar el numero de espacios a omitir
	int numero_espacio = 0;
	for(int l=0; l<m_texto.length(); l++)
	{
		if(m_texto[l] == ' ')
			numero_espacio++;
	}

	if(m_texto.length() == numero_espacio)
		return;

	//Se usa int para descartar los decimales que causan una representación incorrecta porque
	//dibuja una pequeña parte de la textura del caracter anterior o siguiente del caracter actual
	int nueva_x = 0;
	int nueva_y = 0;
	if(m_alineacion_horizontal == Alineacion_H::Izquierda)
		nueva_x = static_cast<int>(std::roundf(this->x()+m_margen));
	else if(m_alineacion_horizontal == Alineacion_H::Centrado)
		nueva_x = static_cast<int>(std::roundf(this->x()+this->ancho()/2.0f - (m_ancho_texto*m_escala) / 2.0f));
	else if(m_alineacion_horizontal == Alineacion_H::Derecha)
		nueva_x = static_cast<int>(std::roundf(this->x()+this->ancho()-((m_ancho_texto*m_escala)+m_margen)));

	float alto_texto_escalado = m_alto_texto*m_escala;
	if(m_alineacion_vertical == Alineacion_V::Arriba)
		nueva_y = static_cast<int>(std::roundf(this->y()+alto_texto_escalado+m_margen));
	else if(m_alineacion_vertical == Alineacion_V::Centrado)
		nueva_y = static_cast<int>(std::roundf(this->y()+(this->alto()/2.0f)+alto_texto_escalado-alto_texto_escalado/2.0f));
	else if(m_alineacion_vertical == Alineacion_V::Abajo)
		nueva_y = static_cast<int>(std::roundf(this->y()+this->alto()-m_margen));

	//TODO ¿Esto deberia estar a cargo de la tipografia?
	glm::mat4 modelo = glm::mat4(1.0f);
	modelo = glm::translate(modelo, glm::vec3(nueva_x, nueva_y, 0.0f));
	modelo = glm::scale(modelo, glm::vec3(m_escala, m_escala, 1.0f));

	m_sombreador->activar();
	m_sombreador->uniforme_matriz4("modelo", modelo);
	if(Etiqueta::Ultimo_color != m_color || !Etiqueta::Color_enviado)
	{
		m_sombreador->uniforme_vector4f("color_texto", m_color.rojo(), m_color.verde(), m_color.azul(), m_color.alfa());
		Etiqueta::Ultimo_color = m_color;
		Etiqueta::Color_enviado = true;
	}

	this->seleccionar_figura();
	m_tipografia->activar();

	glDrawArrays(GL_TRIANGLES, 0, (m_texto.length()-numero_espacio)*6);
}

void Etiqueta::evento_raton(Raton */*raton*/)
{
}

void Etiqueta::dimension(float ancho, float alto)
{
	if(m_ancho_maximo > 0 && m_ancho_maximo < ancho)
		ancho = m_ancho_maximo;
	this->_dimension(ancho, alto);
	if(m_ancho_texto > 0 && (this->ancho() - m_margen*2) < m_ancho_texto)
		m_escala = (this->ancho() - m_margen*2) / m_ancho_texto;
	else
		m_escala = 1.0f;
	float escala = 1.0f;
	if(m_alto_texto > 0 && (this->alto() - m_margen*2) < m_alto_texto)
		escala = (this->alto() - m_margen*2) / m_alto_texto;
	if(escala < m_escala)
		m_escala = escala;
}

void Etiqueta::alineacion_horizontal(Alineacion_H horizontal)
{
	m_alineacion_horizontal = horizontal;
}

void Etiqueta::alineacion_vertical(Alineacion_V vertical)
{
	m_alineacion_vertical = vertical;
}

void Etiqueta::ancho_maximo(float ancho_maximo)
{
	m_ancho_maximo = ancho_maximo;
	m_ancho = m_ancho_texto;
	this->dimension(m_ancho, m_alto);
}

void Etiqueta::margen(float margen)
{
	m_margen = margen;
	this->dimension(m_ancho, m_alto);//Recalcula la escala
}

void Etiqueta::ancho_automatico(bool valor)
{
	m_ancho_automatico = valor;
	this->dimension(m_ancho_texto + m_margen*2,  this->alto());
}

void Etiqueta::texto(const std::string &texto)
{
	//No se vuelve a crear el mismo texto dos veces
	if(m_texto_actual != texto)
		m_texto_actual = texto;
	else
		return;
	m_texto = texto.c_str();
	this->actualizar_texto();
}

void Etiqueta::color(const Color &color)
{
	m_color = color;
}

void Etiqueta::color_fondo(const Color &color)
{
	m_color_fondo = color;
}

void Etiqueta::tipografia(Tipografia *tipografia)
{
	m_tipografia = tipografia;
	m_alto_texto = static_cast<float>(m_tipografia->alto_texto());
	this->actualizar_texto();
}

Color *Etiqueta::color()
{
	return &m_color;
}

Color *Etiqueta::color_fondo()
{
	return &m_color_fondo;
}

float Etiqueta::largo_texto()
{
	return m_ancho_texto;
}

float Etiqueta::alto_texto()
{
	return m_alto_texto;
}

float Etiqueta::alto_texto_real()
{
	return m_alto_texto*m_escala;
}

std::string& Etiqueta::texto()
{
	return m_texto_actual;
}

void Etiqueta::mostrar_fondo(bool estado)
{
	m_mostrar_fondo = estado;
}
