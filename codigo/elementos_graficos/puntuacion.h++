#ifndef PUNTUACION_H
#define PUNTUACION_H

#define COMBO_MINIMO_MOSTRAR 4

#include "elemento.h++"
#include "etiqueta.h++"
#include "../control/rango_organo.h++"

#include <vector>

struct Puntos
{
	unsigned char id_nota = 0;
	std::int64_t tiempo = 0;
};

class Puntuacion : public Elemento
{
private:
	Rectangulo *m_rectangulo;

	Etiqueta m_texto_aciertos, m_texto_maxcombo, m_texto_errores;

	//Datos
	std::vector<Puntos> m_puntuacion;
	Rango_Organo m_rango_organo;
	std::int64_t m_ultimo_tiempo;

	unsigned int m_notas_totales;
	unsigned int m_notas_tocadas;
	unsigned int m_errores;
	unsigned int m_maximo_combo;
	unsigned int m_combos;

	//Control
	bool m_visible;
	bool m_actualizar_texto_aciertos;
	bool m_actualizar_texto_maxcombo;
	bool m_actualizar_texto_errores;

public:
	Puntuacion(float x, float y, float ancho, float alto, const Rango_Organo &rango, Administrador_Recursos *recursos);
	~Puntuacion();

	void actualizar(unsigned int diferencia_tiempo) override;
	void dibujar() override;
	void evento_raton(Raton *raton) override;

	void nota_correcta(unsigned char id_nota, std::int64_t tiempo_inicio);
	void notas_totales(unsigned int total_nota);
	void cambiar_a(std::int64_t tiempo_nuevo);
	void sumar_error();
	void cambiar_rango(const Rango_Organo &rango);

	void sumar_combo(unsigned int combo);
	unsigned int notas_totales();
	unsigned int notas_tocadas();
	unsigned int errores();
	unsigned int combo();
	void reiniciar_combo();
	void limpiar_contador();
	void visible(bool estado);
};

#endif
