#include "reproductor.h++"

Reproductor::Reproductor(float x, float y, float ancho, float alto, Administrador_Recursos *recursos) : Elemento(x, y, ancho, alto)
{
	m_rectangulo = recursos->figura(FiguraGeometrica::Rectangulo);
	m_textura_reproducir = recursos->textura(Textura::FlechaDerecha);
	m_textura_pausa = recursos->textura(Textura::Pausar);

	m_archivo_midi = nullptr;

	m_boton = new Boton(x, y, alto, alto, "", recursos);
	m_boton->textura(m_textura_reproducir);

	m_texto_tiempo_actual = new Etiqueta(x + 40.0f, y, 70.0f, alto, "00:00:00", ModoLetra::Chica, recursos);
	m_texto_tiempo_actual->alineacion_horizontal(Alineacion_H::Izquierda);
	m_texto_tiempo_actual->alineacion_vertical(Alineacion_V::Centrado);

	m_indicador_tiempo = new Control_Deslizante(x + 130.0f, y, ancho - 220.0f, alto, Orientacion::Horizontal, false, recursos);
	m_indicador_tiempo->valor_minimo(0.0);
	m_indicador_tiempo->valor_maximo(1.0);
	m_indicador_tiempo->valor_paso(0.00001);
	m_indicador_tiempo->valor(0.0);

	m_texto_tiempo_duracion = new Etiqueta(x + (ancho - 70.0f), y, 70.0f, alto, "00:01:30", ModoLetra::Chica, recursos);
	m_texto_tiempo_duracion->alineacion_horizontal(Alineacion_H::Derecha);
	m_texto_tiempo_duracion->alineacion_vertical(Alineacion_V::Centrado);

	m_texto_error = new Etiqueta(recursos);
	m_texto_error->posicion(x, y);
	m_texto_error->dimension(ancho, alto);
	m_texto_error->tipografia(recursos->tipografia(ModoLetra::Chica));
	m_texto_error->alineacion_horizontal(Alineacion_H::Centrado);
	m_texto_error->alineacion_vertical(Alineacion_V::Centrado);
	m_texto_error->color(Color(1.0f, 0.0f, 0.0f));

	m_reproduciendo = false;
	m_cambiando_tiempo = false;
	m_hay_error = false;
}

Reproductor::~Reproductor()
{
	m_reproductor_midi.detener();
	if(m_archivo_midi != nullptr)
		delete m_archivo_midi;

	delete m_boton;
	delete m_texto_tiempo_actual;
	delete m_indicador_tiempo;
	delete m_texto_tiempo_duracion;
	delete m_texto_error;
}

void Reproductor::controlador_midi(Controlador_Midi *controlador)
{
	m_bloqueo.lock();
	m_reproductor_midi.controlador_midi(controlador);
	m_bloqueo.unlock();
}

void Reproductor::abrir_archivo(const std::string &ruta)
{
	this->reproducir(false);
	m_bloqueo.lock();
	try
	{
		if(m_archivo_midi != nullptr)
			delete m_archivo_midi;

		Registro::Nota("Abriendo archivo: "+ ruta);
		m_archivo_midi = Midi::abrir_midi(ruta);
		m_reproductor_midi.cargar_midi(m_archivo_midi);
		m_hay_error = false;
	}
	catch(const Excepcion_Midi &e)
	{
		m_archivo_midi = nullptr;
		m_reproductor_midi.cargar_midi(nullptr);
		m_texto_error->texto(e.descripcion_error());
		m_hay_error = true;
	}
	m_bloqueo.unlock();
}

void Reproductor::cerrar_archivo()
{
	this->reproducir(false);
	m_bloqueo.lock();
	m_reproductor_midi.detener();

	if(m_archivo_midi != nullptr)
	{
		delete m_archivo_midi;
		m_archivo_midi = nullptr;
	}
	m_hay_error = false;
	m_reproductor_midi.cargar_midi(nullptr);
	m_bloqueo.unlock();
}

bool Reproductor::archivo_abierto()
{
	if(m_archivo_midi != nullptr)
		return true;

	return false;
}

bool Reproductor::hay_error()
{
	if(m_hay_error)
		return true;

	return false;
}

void Reproductor::reproducir(bool estado)
{
	m_bloqueo.lock();
	m_reproduciendo = estado;
	if(m_reproduciendo)
	{
		m_boton->textura(m_textura_pausa);
		m_reproductor_midi.reproducir();
	}
	else
	{
		m_boton->textura(m_textura_reproducir);
		m_reproductor_midi.pausar();
	}
	m_bloqueo.unlock();
}

void Reproductor::actualizar(unsigned int diferencia_tiempo)
{
	m_boton->actualizar(diferencia_tiempo);

	m_bloqueo.lock();
	m_texto_tiempo_actual->texto(Funciones::microsegundo_a_texto(m_reproductor_midi.posicion_en_microsegundos(), true, false));
	m_texto_tiempo_duracion->texto(Funciones::microsegundo_a_texto(m_reproductor_midi.duracion_en_microsegundos(), true, false));

	double actual = static_cast<double>(m_reproductor_midi.posicion_en_microsegundos());
	double total = static_cast<double>(m_reproductor_midi.duracion_en_microsegundos());

	if(m_reproduciendo && !m_reproductor_midi.reproduciendo())
	{
		m_reproduciendo = false;
		m_boton->textura(m_textura_reproducir);

	}
	m_bloqueo.unlock();

	if(!m_indicador_tiempo->cambiando_valor())
		m_indicador_tiempo->valor(actual / total);
	m_indicador_tiempo->actualizar(diferencia_tiempo);
}

void Reproductor::actualizar_midi(unsigned int diferencia_tiempo)
{
	m_bloqueo.lock();
	m_reproductor_midi.actualizar(static_cast<unsigned int>((static_cast<double>(diferencia_tiempo) / 1000.0)));
	m_bloqueo.unlock();
}

void Reproductor::dibujar()
{
	if(!m_hay_error)
	{
		m_boton->dibujar();
		m_texto_tiempo_actual->dibujar();
		m_indicador_tiempo->dibujar();
		m_texto_tiempo_duracion->dibujar();
	}
	else
		m_texto_error->dibujar();
}

void Reproductor::evento_raton(Raton *raton)
{
	m_boton->evento_raton(raton);

	if(m_boton->esta_activado())
		this->reproducir(!m_reproduciendo);

	m_bloqueo.lock();
	m_indicador_tiempo->evento_raton(raton);
	if(m_indicador_tiempo->cambiando_valor())
	{
		if(!m_reproduciendo)
		{
			m_boton->textura(m_textura_pausa);
			m_reproduciendo = true;
		}
		m_reproductor_midi.pausar();
		double total = static_cast<double>(m_reproductor_midi.duracion_en_microsegundos());
		double us = m_indicador_tiempo->valor() * total;
		m_reproductor_midi.cambiar_a(static_cast<std::int64_t>(us));
		m_cambiando_tiempo = true;
	}
	else if(m_indicador_tiempo->cambio_valor())
	{
		double total = static_cast<double>(m_reproductor_midi.duracion_en_microsegundos());
		double us = m_indicador_tiempo->valor() * total;
		m_reproductor_midi.cambiar_a(static_cast<std::int64_t>(us));
		m_reproductor_midi.reproducir();
	}
	else if(m_cambiando_tiempo)
	{
		m_cambiando_tiempo = false;
		if(m_reproduciendo)
			m_reproductor_midi.reproducir();
	}
	m_bloqueo.unlock();
}

void Reproductor::posicion(float x, float y)
{
	this->_posicion(x, y);
	m_boton->posicion(x, y);
	m_texto_tiempo_actual->posicion(x + 40.0f, y);
	m_indicador_tiempo->posicion(x + 130.0f, y);
	m_texto_tiempo_duracion->posicion(x + (this->ancho() - 70.0f), y);
	m_texto_error->posicion(x, y);
}

void Reproductor::dimension(float ancho, float alto)
{
	this->_dimension(ancho, alto);
	m_boton->dimension(alto, alto);
	m_texto_tiempo_actual->dimension(70.0f, alto);
	m_indicador_tiempo->dimension(ancho - 220.0f, alto);
	m_texto_tiempo_duracion->posicion(this->x() + (ancho - 70.0f), this->y());
	m_texto_tiempo_duracion->dimension(70.0f, alto);
	m_texto_error->dimension(ancho, alto);
}
