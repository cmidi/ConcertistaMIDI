#include "notificacion.h++"

std::vector<Mensaje *> Notificacion::Notificaciones;
int Notificacion::Tiempo_Minimo = 10;
std::mutex Notificacion::Bloqueo;

Notificacion::Notificacion(Administrador_Recursos *recursos) : Elemento(0, 0, 400, 40)
{
	m_recursos = recursos;
	m_textura_fondo = recursos->textura(Textura::Nota);
	m_rectangulo = recursos->figura(FiguraGeometrica::Rectangulo);
}

Notificacion::~Notificacion()
{
	for(Mensaje *m : Notificacion::Notificaciones)
		delete m;
	Notificacion::Notificaciones.clear();
}

void Notificacion::actualizar(unsigned int diferencia_tiempo)
{
	if(Notificacion::Tiempo_Minimo > 0)
	{
		Notificacion::Tiempo_Minimo--;
		return;
	}

	//Si no hay notificaciones se omite
	if(Notificacion::Notificaciones.size() == 0)
		return;

	unsigned int contador = 0;
	float mover = 0;
	Mensaje *actual = nullptr;
	for(unsigned int x=0; x<Notificacion::Notificaciones.size(); x++)
	{
		actual = Notificacion::Notificaciones.at(x);
		if(actual->etiqueta == nullptr)
		{
			actual->etiqueta = new Etiqueta(0, 0, 400, 40, Notificacion::Notificaciones[x]->texto, ModoLetra::Chica, m_recursos);
			actual->etiqueta->alineacion_horizontal(Alineacion_H::Centrado);
			actual->etiqueta->alineacion_vertical(Alineacion_V::Centrado);
			actual->etiqueta->color(Color(1.0f, 1.0f, 1.0f));
		}
		if(actual->tiempo > 0)
		{
			if(contador < MAXIMAS_NOTIFICACIONES)
			{
				actual->tiempo -= (static_cast<float>(diferencia_tiempo) / 1000000000.0f);
				//0.25 Segundos para aparecer

				if(actual->tiempo > 1 && actual->opacidad < 1)
					actual->opacidad += (static_cast<float>(diferencia_tiempo) / 1000000000.0f)*4;//0.25 Segundos para aparecer
				else if(actual->tiempo <= 0.25f && actual->opacidad > 0)
					actual->opacidad -= (static_cast<float>(diferencia_tiempo) / 1000000000.0f)*4;//0.25 Segundos para desaparecer
			}

			if(mover > 0)
				actual->mover += mover;

			//Animacion moviendo hacia arriba
			if(actual->mover > 0)
			{
				//Se mueve 45 pixeles en 0.25 segundos
				actual->posicion_y -= (static_cast<float>(diferencia_tiempo) / 1000000000.0f)*180;
				actual->mover -= (static_cast<float>(diferencia_tiempo) / 1000000000.0f)*180;
				if(actual->mover < 0)
				{
					actual->posicion_y += actual->mover*-1;
					actual->mover = 0;
				}
			}

			if(actual->tiempo <= 0)
			{
				//Eliminar esta notificacion
				delete actual;
				actual = nullptr;
				Notificacion::Notificaciones.erase(Notificacion::Notificaciones.begin()+x);
				x--;
				mover += 45;
			}
			else
				contador++;
		}
	}
}

void Notificacion::dibujar()
{
	//Si no hay notificaciones se omite
	if(Notificacion::Notificaciones.size() == 0)
		return;

	m_rectangulo->textura(true);
	m_rectangulo->extremos_fijos(true, true);
	for(unsigned int x=0; x<Notificacion::Notificaciones.size() && x < MAXIMAS_NOTIFICACIONES; x++)
	{
		Mensaje *actual = Notificacion::Notificaciones[x];
		if(actual->etiqueta == nullptr)
			continue;

		//Selecciona el color de fonde deacuero al tipo de mensaje
		if(actual->estado == CodigoEstado::Error)
			m_rectangulo->color(Color(0.7f, 0.0f, 0.0f, actual->opacidad));
		else if(actual->estado == CodigoEstado::Aviso)
			m_rectangulo->color(Color(0.9f, 0.5f, 0.0f, actual->opacidad));
		else if(actual->estado == CodigoEstado::Nota)
			m_rectangulo->color(Color(0.0f, 0.598f, 0.0f, actual->opacidad));
		else if(actual->estado == CodigoEstado::Depurar)
			m_rectangulo->color(Color(0.145f, 0.707f, 1.0f, actual->opacidad));

		//Dibuja el fondo
		m_textura_fondo->activar();
		m_rectangulo->dibujar_estirable(this->x(), this->y() + actual->posicion_y, this->ancho(), this->alto(), 15.0f, 12.0f);

		actual->etiqueta->color(Color(1.0f, 1.0f, 1.0f, actual->opacidad));
		actual->etiqueta->posicion(this->x(), this->y() + actual->posicion_y);
		actual->etiqueta->dibujar();
	}
	m_rectangulo->extremos_fijos(false, false);
}

void Notificacion::evento_raton(Raton */*raton*/)
{

}

bool Notificacion::mostrando_notificaciones()
{
	if(Notificacion::Notificaciones.size() > 0)
		return true;
	return false;
}

void Notificacion::Registrar(std::string texto, int tiempo, CodigoEstado estado)
{
	Notificacion::Bloqueo.lock();
	if(Notificacion::Tiempo_Minimo > 0)//Ignora las notificaciones los primeros 10 fps
	{
		Notificacion::Bloqueo.unlock();
		return;
	}

	if(tiempo < 3)
		tiempo = 3;

	Notificacion::Notificaciones.push_back(new Mensaje(nullptr, texto, static_cast<float>(tiempo), estado));

	//Se actualiza la posicion del ultimo elemento agregado
	if(Notificacion::Notificaciones.size() > 1)
	{
		unsigned int posicion = static_cast<unsigned int>(Notificacion::Notificaciones.size()-1);

		Notificacion::Notificaciones[posicion]->posicion_y = Notificacion::Notificaciones[posicion-1]->posicion_y + 45;
		Notificacion::Notificaciones[posicion]->mover = Notificacion::Notificaciones[posicion-1]->mover;
	}
	Notificacion::Bloqueo.unlock();
}

void Notificacion::Error(std::string texto, int tiempo)
{
	Notificacion::Registrar(texto, tiempo, CodigoEstado::Error);
	Registro::Error(texto);
}

void Notificacion::Aviso(std::string texto, int tiempo)
{
	Notificacion::Registrar(texto, tiempo, CodigoEstado::Aviso);
	Registro::Aviso(texto);
}

void Notificacion::Nota(std::string texto, int tiempo)
{
	Notificacion::Registrar(texto, tiempo, CodigoEstado::Nota);
	Registro::Nota(texto);
}

void Notificacion::Depurar(std::string texto, int tiempo)
{
	Notificacion::Registrar(texto, tiempo, CodigoEstado::Depurar);
	Registro::Depurar(texto);
}
