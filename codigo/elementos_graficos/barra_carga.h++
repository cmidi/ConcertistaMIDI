#ifndef BARRA_CARGA_H
#define BARRA_CARGA_H

#include "elemento.h++"
#include "etiqueta.h++"
#include "../recursos/animacion.h++"

class Barra_Carga : public Elemento
{
private:
	Rectangulo *m_rectangulo;
	Etiqueta *m_etiqueta;
	Textura2D *m_textura_fondo;
	Textura2D *m_textura_progreso;

	Animacion m_animacion;

	std::string m_texto;
	unsigned int m_valor_actual;
	unsigned int m_valor_maximo;
	bool m_cambio_valor;

public:
	Barra_Carga(float x, float y, float ancho, float alto, Administrador_Recursos *recurso);
	~Barra_Carga();

	void actualizar(unsigned int diferencia_tiempo) override;
	void dibujar() override;
	void evento_raton(Raton *raton) override;

	void posicion(float x, float y) override;
	void dimension(float ancho, float alto) override;

	void texto(const std::string &texto);
	void valor_actual(unsigned int valor);
	void valor_maximo(unsigned int valor);

	bool visible();
};

#endif
