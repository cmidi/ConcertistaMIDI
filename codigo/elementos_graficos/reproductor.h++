#ifndef REPRODUCTOR_H
#define REPRODUCTOR_H

#include "elemento.h++"
#include "../dispositivos_midi/controlador_midi.h++"
#include "../libreria_midi/reproductor_midi.h++"
#include "../elementos_graficos/boton.h++"
#include "../elementos_graficos/control_deslizante.h++"
#include "../elementos_graficos/etiqueta.h++"

#include <mutex>

class Reproductor : public Elemento
{
private:
	Rectangulo *m_rectangulo;
	Textura2D *m_textura_reproducir;
	Textura2D *m_textura_pausa;

	Boton *m_boton;
	Etiqueta *m_texto_tiempo_actual;
	Etiqueta *m_texto_tiempo_duracion;
	Etiqueta *m_texto_error;
	Control_Deslizante *m_indicador_tiempo;

	bool m_reproduciendo;
	bool m_cambiando_tiempo;
	bool m_hay_error;

	std::mutex m_bloqueo;
	Midi *m_archivo_midi;
	Reproductor_Midi m_reproductor_midi;

public:
	Reproductor(float x, float y, float ancho, float alto, Administrador_Recursos *recursos);
	~Reproductor();

	void controlador_midi(Controlador_Midi *controlador);
	void abrir_archivo(const std::string &ruta);
	void cerrar_archivo();
	bool archivo_abierto();
	bool hay_error();

	void reproducir(bool estado);

	void actualizar(unsigned int diferencia_tiempo) override;
	void actualizar_midi(unsigned int diferencia_tiempo);
	void dibujar() override;
	void evento_raton(Raton *raton) override;

	void posicion(float x, float y) override;
	void dimension(float ancho, float alto) override;
};

#endif
