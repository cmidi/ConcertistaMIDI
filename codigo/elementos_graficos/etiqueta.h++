#ifndef ETIQUETA_H
#define ETIQUETA_H

#include <string>
#include <unicode/unistr.h>
#include <cmath>

#include "elemento.h++"
#include "../recursos/figura.h++"
#include "../recursos/tipografia.h++"
#include "../registro.h++"

enum class Alineacion_H : char
{
	Izquierda,
	Centrado,
	Derecha,
};

enum class Alineacion_V : char
{
	Arriba,
	Centrado,
	Abajo,
};

class Etiqueta : public Elemento, public Figura
{
private:
	static Color Ultimo_color;
	static bool Color_enviado;

	Tipografia *m_tipografia = nullptr;
	Color m_color;
	Color m_color_fondo = Color(0.1f, 0.7f, 0.1f, 0.5f);
	icu::UnicodeString m_texto;
	std::string m_texto_actual;
	Alineacion_H m_alineacion_horizontal = Alineacion_H::Izquierda;
	Alineacion_V m_alineacion_vertical = Alineacion_V::Arriba;
	bool m_ancho_automatico = false;
	float m_escala = 1.0f;
	float m_ancho_maximo = 0.0f;
	float m_ancho_texto = 0.0f;
	float m_alto_texto = 0.0f;
	float m_margen = 0.0f;

	Rectangulo *m_rectangulo;
	bool m_mostrar_fondo = false;

	unsigned int m_indice_objeto = 0;

	void actualizar_texto();
	void limpiar();

public:
	Etiqueta(Administrador_Recursos *recursos);
	Etiqueta(float x, float y, const std::string &texto, ModoLetra tipografia, Administrador_Recursos *recursos);
	Etiqueta(float x, float y, const std::string &texto, Tipografia *tipografia, Administrador_Recursos *recursos);
	Etiqueta(float x, float y, float ancho, float alto, const std::string &texto, ModoLetra tipografia, Administrador_Recursos *recursos);
	~Etiqueta();

	void actualizar(unsigned int diferencia_tiempo) override;
	void dibujar() override;
	void evento_raton(Raton *raton) override;

	void dimension(float ancho, float alto) override;
	void alineacion_horizontal(Alineacion_H horizontal);
	void alineacion_vertical(Alineacion_V vertical);

	void ancho_maximo(float ancho_maximo);
	void margen(float margen);
	void ancho_automatico(bool valor);

	void texto(const std::string &texto);
	void color(const Color &color);
	void color_fondo(const Color &color);
	void tipografia(Tipografia *tipografia);

	Color *color();
	Color *color_fondo();

	float largo_texto();
	float alto_texto();
	float alto_texto_real();
	std::string& texto();

	void mostrar_fondo(bool estado);
};

#endif
