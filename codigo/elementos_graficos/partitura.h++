#ifndef PARTITURA_H
#define PARTITURA_H

#include "elemento.h++"
#include "../elementos_graficos/etiqueta.h++"
#include "../libreria_midi/pista_midi.h++"

class Partitura : public Elemento
{
private:
	Rectangulo *m_rectangulo;
	Etiqueta *m_clave;
	Etiqueta *m_numerador;
	Etiqueta *m_denumerador;

	Pista_Midi *m_pista;
	std::vector<Datos_Evento*> *m_tempos;
	std::vector<Datos_Evento*> *m_compas;
	std::vector<Datos_Evento*> *m_armadura;
	std::vector<std::int64_t> *m_barras_compas;

	std::int64_t m_tiempo_total;
	std::int64_t m_tiempo_actual;

public:
	Partitura(float x, float y, float ancho, float alto, Administrador_Recursos *recursos);
	~Partitura();

	void actualizar(unsigned int diferencia_tiempo) override;
	void dibujar() override;
	void evento_raton(Raton *raton) override;

	void pista(Pista_Midi *pista);
	void tempos(std::vector<Datos_Evento*> *tempos);
	void compas(std::vector<Datos_Evento*> *compas);
	void armadura(std::vector<Datos_Evento*> *armadura);
	void barras_compas(std::vector<std::int64_t> *barras_compas);

	void tiempo_total(std::int64_t tiempo_total);
	void tiempo(std::int64_t tiempo_actual);
};

#endif
