#ifndef ELEMENTO_H
#define ELEMENTO_H

#include "../recursos/administrador_recursos.h++"
#include "../dispositivos/raton.h++"
#include "../util/traduccion.h++"

class Elemento
{
protected:
	float m_x = 0, m_y = 0;
	float m_ancho = 0, m_alto = 0;

	void _posicion(float x, float y);
	void _dimension(float ancho, float alto);
public:
	Elemento(float x, float y, float ancho, float alto);
	virtual ~Elemento();

	virtual void actualizar(unsigned int diferencia_tiempo) = 0;
	virtual void dibujar() = 0;
	virtual void evento_raton(Raton *raton) = 0;

	virtual void posicion(float x, float y);
	virtual void dimension(float ancho, float alto);

	float x();
	float y();
	float ancho();
	float alto();
};

#endif
