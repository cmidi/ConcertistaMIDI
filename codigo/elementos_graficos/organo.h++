#ifndef ORGANO_H
#define ORGANO_H

#define PROPORCION_BLANCA 6.52941f
#define PROPORCION_NEGRA 0.657f
#define SIN_NOTA 130

#define ORGANO_OCULTO 0
#define ORGANO_DOFIJO 1
#define ORGANO_DOFIJO_SOLO_BLANCAS 2
#define ORGANO_DOVARIABLE 3
#define ORGANO_INGLES 4
#define ORGANO_INGLES_SOLO_BLANCAS 5
#define ORGANO_NUMERICO 6
#define ORGANO_NUMERO_ETIQUETAS 7

#include "elemento.h++"
#include "../recursos/generador_particulas.h++"
#include "../recursos/rectangulo.h++"
#include "../util/octava.h++"
#include "../control/pista.h++"
#include "../control/rango_organo.h++"
#include "../control/tecla_organo.h++"
#include "../elementos_graficos/etiqueta.h++"
#include "../libreria_midi/descripciones_midi.h++"
#include "../libreria_midi/funciones_midi.h++"

#include <array>
#include <vector>

class Organo : public Elemento
{
private:
	Administrador_Recursos *m_recursos;
	Rectangulo *m_rectangulo;

	Textura2D *m_tecla_blanca;
	Textura2D *m_tecla_blanca_presionada;
	Textura2D *m_tecla_blanca_presionada_doble;
	Textura2D *m_tecla_negra;
	Textura2D *m_tecla_negra_presionada;
	Textura2D *m_borde_negro;
	Textura2D *m_borde_rojo;
	Textura2D *m_circulo;

	Generador_Particulas *m_generador_particulas;
	float m_tiempo;
	unsigned int m_numero_particulas;

	Rango_Organo *m_teclado_visible, *m_teclado_util;
	float m_ancho_tecla_blanca, m_ancho_tecla_negra;
	float m_alto_tecla_blanca, m_alto_tecla_negra;
	float m_alto_sobre_blanca_normal, m_alto_sobre_blanca_presionada;
	float m_alto_sobre_negra_normal, m_alto_sobre_negra_presionada;

	std::array<Tecla_Organo, NUMERO_TECLAS_ORGANO> *m_teclas;
	unsigned char m_nota_enviada_anterior;
	std::vector<std::pair<unsigned char, bool>> m_eventos;

	//Nombre Notas
	std::vector<Etiqueta*> m_nombre_notas;
	unsigned char m_opcion_etiqueta;
	char m_armadura;
	unsigned char m_escala;

	//Metodos
	void dibujar_blancas(float x, float y, unsigned char tecla_inicial, unsigned char numero_teclas);
	void dibujar_negras(float x, float y, unsigned char tecla_inicial, unsigned char numero_teclas);

public:
	Organo(float x, float y, float ancho, Rango_Organo *teclado_visible, Rango_Organo *teclado_util, char armadura, unsigned char escala, Administrador_Recursos *recursos);
	~Organo();

	void actualizar(unsigned int diferencia_tiempo) override;
	void dibujar() override;
	void evento_raton(Raton *raton) override;

	void dimension(float ancho, float alto) override;

	void teclas(std::array<Tecla_Organo, NUMERO_TECLAS_ORGANO> *notas);
	void cambiar_armadura(char armadura, unsigned char escala);

	void opcion_etiqueta(unsigned char opcion);
	unsigned char numero_etiquetas();

	void calcular_tamannos();
	bool hay_eventos();
	std::pair<unsigned char, bool> obtener_evento();
};

#endif
