#ifndef TABLA_H
#define TABLA_H

#include "elemento.h++"
#include "fila.h++"
#include "panel_desplazamiento.h++"

#include <vector>

enum class TipoCelda : unsigned char
{
	Etiqueta,
	Imagen,
};

struct CeldaTitulo
{
	//NOTE posiblemente deberia agregar un campo para saber si es ocultable cuando la tabla es muy chica
	TipoCelda tipo;
	Etiqueta *texto;
	bool centrado;
	unsigned int numero_espacio;
};

struct CeldaContenido
{
	std::string texto = "";
	Textura imagen = Textura::Indeterminado;
};

class Tabla : public Elemento
{
private:
	Administrador_Recursos *m_recursos;
	std::vector<CeldaTitulo> m_fila_titulo;//Fila titulo
	std::vector<Fila*> m_filas;
	Etiqueta *m_texto_tabla_vacia;

	float m_alto_fila;
	unsigned int m_espacio_total_columnas;
	unsigned int m_fila_seleccionada;
	bool m_seleccion, m_seleccion_activada;

	Rectangulo *m_rectangulo;
	Color m_color_fondo;
	Panel_Desplazamiento *m_panel_desplazamiento;

	void actualizar_ancho_columnas();

public:
	Tabla(float x, float y, float ancho, float alto, float alto_fila, Administrador_Recursos *recursos);
	~Tabla();

	void actualizar(unsigned int diferencia_tiempo) override;
	void dibujar() override;
	void evento_raton(Raton *raton) override;

	void posicion(float x, float y) override;
	void dimension(float ancho, float alto) override;

	void agregar_columna(TipoCelda celda, std::string texto, bool centrado, unsigned int numero_espacio);
	void insertar_fila(const std::vector<CeldaContenido> &celdas, Color color);
	void insertar_fila(const std::vector<CeldaContenido> &celdas);
	void vaciar();
	void texto_tabla_vacia(const std::string &texto);

	void cambiar_seleccion(int cambio);
	void seleccionar(unsigned int seleccion);
	unsigned int obtener_seleccion();
	bool seleccion_activada();
	bool seleccion();
	bool seleccion_visible();
};

#endif
