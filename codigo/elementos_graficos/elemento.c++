#include "elemento.h++"

Elemento::Elemento(float x, float y, float ancho, float alto)
{
	m_x = x;
	m_y = y;
	m_ancho = ancho;
	m_alto = alto;
}

Elemento::~Elemento()
{
}

void Elemento::_posicion(float x, float y)
{
	m_x = x;
	m_y = y;
}

void Elemento::_dimension(float ancho, float alto)
{
	m_ancho = ancho;
	m_alto = alto;
}

void Elemento::posicion(float x, float y)
{
	this->_posicion(x, y);
}

void Elemento::dimension(float ancho, float alto)
{
	this->_dimension(ancho, alto);
}

float Elemento::x()
{
	return m_x;
}

float Elemento::y()
{
	return m_y;
}

float Elemento::ancho()
{
	return m_ancho;
}

float Elemento::alto()
{
	return m_alto;
}
