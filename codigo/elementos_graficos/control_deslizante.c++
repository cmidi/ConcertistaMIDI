#include "control_deslizante.h++"

Control_Deslizante::Control_Deslizante(float x, float y, float ancho, float alto, Orientacion orientacion, bool mostrar_valor, Administrador_Recursos *recursos) : Elemento(x, y, ancho, alto)
{
	m_largo_maximo_texto = 40;
	m_orientacion = orientacion;

	m_recursos = recursos;
	m_rectangulo = recursos->figura(FiguraGeometrica::Rectangulo);

	if(m_orientacion == Orientacion::Horizontal)
	{
		m_textura_fondo = recursos->textura(Textura::ControlDeslizante_H_Fondo);
		m_textura_relleno = recursos->textura(Textura::ControlDeslizante_H_Relleno);
	}
	else if(m_orientacion == Orientacion::Vertical)
	{
		m_textura_fondo = recursos->textura(Textura::ControlDeslizante_V_Fondo);
		m_textura_relleno = recursos->textura(Textura::ControlDeslizante_V_Relleno);
	}

	m_textura_boton = recursos->textura(Textura::ControlDeslizante_Boton);

	m_texto_valor = nullptr;
	m_mostrar_valor = mostrar_valor;
	if(m_mostrar_valor)
	{
		m_texto_valor = new Etiqueta(recursos);
		m_texto_valor->alineacion_vertical(Alineacion_V::Centrado);
		m_texto_valor->tipografia(recursos->tipografia(ModoLetra::Chica));

		if(m_orientacion == Orientacion::Horizontal)
		{
			m_texto_valor->dimension(m_largo_maximo_texto, alto);
			m_texto_valor->posicion(x+ancho - m_largo_maximo_texto, y);
		}
		else if(m_orientacion == Orientacion::Vertical)
		{
			m_texto_valor->alineacion_horizontal(Alineacion_H::Centrado);
			m_texto_valor->dimension(ancho, m_texto_valor->alto_texto_real());
			m_texto_valor->posicion(x, y+alto - m_texto_valor->alto_texto_real());
		}
	}

	if(m_orientacion == Orientacion::Horizontal)
	{
		m_centro = y + alto / 2.0f;

		if(m_mostrar_valor)
			m_largo_barra = ancho - (m_largo_maximo_texto + 20);
		else
			m_largo_barra = ancho;
	}
	else if(m_orientacion == Orientacion::Vertical)
	{
		m_centro = x + ancho / 2.0f;

		if(m_mostrar_valor)
			m_largo_barra = alto - (m_texto_valor->alto_texto_real() + 20);
		else
			m_largo_barra = alto;
	}

	m_valor_minimo = 0.0;
	m_valor_maximo = 1.5;
	m_valor_paso = 0.05;
	m_valor_actual = 1.0;
	m_cambio_valor = false;
	m_moviendo_pre_activado = false;
	m_moviendo_barra = false;
	m_posicion_agarre = 0.0f;

	this->calcular_valores_nuevos();
}

Control_Deslizante::~Control_Deslizante()
{
}

void Control_Deslizante::calcular_valores_nuevos()
{
	double largo_total = m_valor_maximo - m_valor_minimo;
	double posicion_actual = m_valor_actual - m_valor_minimo;
	double porcentaje = posicion_actual / largo_total;

	m_largo_barra_color = m_largo_barra * static_cast<float>(porcentaje);

	//Actualiza el texto
	if(m_texto_valor != nullptr)
	{
		std::string valor = std::to_string(m_valor_actual*100);
		m_texto_valor->texto(valor.substr(0, valor.find(".")) + "%");

		if(m_orientacion == Orientacion::Horizontal)
		{
			float largo = m_texto_valor->largo_texto();
			if(largo > m_texto_valor->ancho())
				largo = m_texto_valor->ancho();
			m_texto_valor->posicion(this->x()+this->ancho() - largo, this->y());
		}
	}
}

void Control_Deslizante::cambiar_valor(double valor, bool registrar_cambio)
{
	bool cambio_valor = false;
	if(valor > m_valor_maximo)
	{
		if(!Funciones::double_son_iguales(m_valor_actual, m_valor_maximo, m_valor_paso))
		{
			cambio_valor = true;
			m_valor_actual = m_valor_maximo;
		}
	}
	else if(valor < m_valor_minimo)
	{
		if(!Funciones::double_son_iguales(m_valor_actual, m_valor_minimo, m_valor_paso))
		{
			cambio_valor = true;
			m_valor_actual = m_valor_minimo;
		}
	}
	else
	{
		int multiplo = static_cast<int>(valor / m_valor_paso);
		double valor_paso = m_valor_paso * multiplo;
		if(!Funciones::double_son_iguales(m_valor_actual, valor_paso, m_valor_paso))
		{
			cambio_valor = true;
			m_valor_actual = valor_paso;
		}
	}

	if(cambio_valor)
		this->calcular_valores_nuevos();

	if(registrar_cambio)
		m_cambio_valor = cambio_valor;
}

void Control_Deslizante::actualizar(unsigned int /*diferencia_tiempo*/)
{

}

void Control_Deslizante::dibujar()
{
	//10px es el alto del control deslizante
	//40x42 es el tamaño del boton incluyendo la sombra escalado
	m_rectangulo->color(Color(1.0f, 1.0f, 1.0f));
	m_rectangulo->textura(true);

	if(m_orientacion == Orientacion::Horizontal)
	{
		m_rectangulo->extremos_fijos(true, false);

		m_textura_fondo->activar();
		m_rectangulo->dibujar_estirable(this->x(), m_centro-5.0f, m_largo_barra, 10.0f, 20.0f, 0.0f);

		m_textura_relleno->activar();
		m_rectangulo->dibujar_estirable(this->x(), m_centro-5.0f, m_largo_barra_color, 10.0f, 20.0f, 0.0f);

		m_rectangulo->extremos_fijos(false, false);

		m_textura_boton->activar();
		m_rectangulo->dibujar(this->x()+m_largo_barra_color-20.0f, m_centro-20.0f, 40.0f, 42.0f);
	}
	else if(m_orientacion == Orientacion::Vertical)
	{
		m_rectangulo->extremos_fijos(true, false);

		m_textura_fondo->activar();
		m_rectangulo->dibujar_estirable(m_centro-5.0f, this->y(), 10.0f, m_largo_barra, 0.0f, 20.0f);

		m_textura_relleno->activar();
		m_rectangulo->dibujar_estirable(m_centro-5.0f, this->y(), 10.0f, m_largo_barra_color, 0.0f, 20.0f);

		m_rectangulo->extremos_fijos(false, false);

		m_textura_boton->activar();
		m_rectangulo->dibujar(m_centro-20.0f, this->y()+m_largo_barra_color-20.0f, 40.0f, 42.0f);
	}

	if(m_texto_valor != nullptr)
		m_texto_valor->dibujar();
}

void Control_Deslizante::evento_raton(Raton *raton)
{
	//-3 y -4 porque se descarta la sompra por ese motivo el area solo es de 34x34
	bool sobre_boton = false;raton->esta_sobre(this->x()+m_largo_barra_color-(20-3), m_centro-(20-4), 34, 34);
	bool sobre_barra = false;raton->esta_sobre(this->x(), m_centro-5, m_largo_barra, 10);

	if(m_orientacion == Orientacion::Horizontal)
	{
		sobre_boton = raton->esta_sobre(this->x()+m_largo_barra_color-(20-3), m_centro-(20-4), 34, 34);
		sobre_barra = raton->esta_sobre(this->x(), m_centro-5, m_largo_barra, 10);
	}
	else if(m_orientacion == Orientacion::Vertical)
	{
		sobre_boton = raton->esta_sobre(m_centro-(20-4), this->y()+m_largo_barra_color-(20-3), 34, 34);
		sobre_barra = raton->esta_sobre(m_centro-5, this->y(), 10, m_largo_barra);
	}

	if(sobre_boton || sobre_barra)
	{
		if(raton->tipo_evento() == EventoRaton::Clic && raton->activado(BotonRaton::Izquierdo) && m_moviendo_pre_activado)
		{
			if(sobre_boton && !m_moviendo_barra)
			{
				if(m_orientacion == Orientacion::Horizontal)
					m_posicion_agarre = static_cast<float>(raton->x()) - (this->x() + m_largo_barra_color);
				else if(m_orientacion == Orientacion::Vertical)
					m_posicion_agarre = static_cast<float>(raton->y()) - (this->y() + m_largo_barra_color);
			}
			m_moviendo_barra = true;
		}
		else if(!raton->activado(BotonRaton::Izquierdo))
			m_moviendo_pre_activado = true;

		if(!m_moviendo_barra)
		{
			if(	(m_orientacion == Orientacion::Horizontal && raton->dy() > 0) ||
				(m_orientacion == Orientacion::Vertical && raton->dy() < 0))
			{
				this->cambiar_valor(m_valor_actual + m_valor_paso, true);
				raton->anular_desplazamiento();//Anula el evento de desplazamiento una vez utilizado
			}
			if(	(m_orientacion == Orientacion::Horizontal && raton->dy() < 0) ||
				(m_orientacion == Orientacion::Vertical && raton->dy() > 0))
			{
				this->cambiar_valor(m_valor_actual - m_valor_paso, true);
				raton->anular_desplazamiento();//Anula el evento de desplazamiento una vez utilizado
			}
		}
	}
	else
		m_moviendo_pre_activado = false;

	if(raton->tipo_evento() == EventoRaton::Clic && !raton->activado(BotonRaton::Izquierdo))
	{
		m_moviendo_barra = false;
		m_posicion_agarre = 0.0f;
	}

	if(m_moviendo_barra)
	{
		if(m_orientacion == Orientacion::Horizontal)
		{
			float posicion_x = static_cast<float>(raton->x()) - (this->x() + m_posicion_agarre);
			if(posicion_x > m_largo_barra)
				this->cambiar_valor(m_valor_maximo, true);
			else if(posicion_x < 0)
				this->cambiar_valor(m_valor_minimo, true);
			else
			{
				double rango_total = m_valor_maximo - m_valor_minimo;
				double porcentaje = static_cast<double>(posicion_x) / static_cast<double>(m_largo_barra);
				this->cambiar_valor(m_valor_minimo + (rango_total * porcentaje), true);
			}
		}
		else if(m_orientacion == Orientacion::Vertical)
		{
			float posicion_y = static_cast<float>(raton->y()) - (this->y() + m_posicion_agarre);
			if(posicion_y > m_largo_barra)
				this->cambiar_valor(m_valor_maximo, true);
			else if(posicion_y < 0)
				this->cambiar_valor(m_valor_minimo, true);
			else
			{
				double rango_total = m_valor_maximo - m_valor_minimo;
				double porcentaje = static_cast<double>(posicion_y) / static_cast<double>(m_largo_barra);
				this->cambiar_valor(m_valor_minimo + (rango_total * porcentaje), true);
			}
		}
	}
}

void Control_Deslizante::posicion(float x, float y)
{
	this->_posicion(x, y);

	if(m_orientacion == Orientacion::Horizontal)
		m_centro = y + this->alto() / 2.0f;
	else if(m_orientacion == Orientacion::Vertical)
		m_centro = x + this->ancho() / 2.0f;

	if(m_texto_valor != nullptr)
		m_texto_valor->posicion(x+this->ancho() - m_largo_maximo_texto, y);
}

void Control_Deslizante::dimension(float ancho, float alto)
{
	this->_dimension(ancho, alto);

	if(m_orientacion == Orientacion::Horizontal)
	{
		m_centro = this->y() + alto / 2.0f;

		if(m_mostrar_valor)
			m_largo_barra = ancho - (m_largo_maximo_texto + 20.0f);
		else
			m_largo_barra = ancho;
	}
	else if(m_orientacion == Orientacion::Vertical)
	{
		m_centro = this->x() + ancho / 2.0f;

		if(m_mostrar_valor)
			m_largo_barra = alto - (m_texto_valor->alto_texto_real() + 20.0f);
		else
			m_largo_barra = alto;
	}

	this->calcular_valores_nuevos();
}

void Control_Deslizante::valor_minimo(double valor)
{
	m_valor_minimo = valor;
	if(m_valor_actual < m_valor_minimo)
		m_valor_actual = m_valor_minimo;

	this->calcular_valores_nuevos();
}

void Control_Deslizante::valor_maximo(double valor)
{
	m_valor_maximo = valor;
	if(m_valor_actual > m_valor_maximo)
		m_valor_actual = m_valor_maximo;

	this->calcular_valores_nuevos();
}

void Control_Deslizante::valor_paso(double valor)
{
	m_valor_paso = valor;
}

void Control_Deslizante::valor(double valor)
{
	this->cambiar_valor(valor, false);
}

double Control_Deslizante::valor()
{
	return m_valor_actual;
}

bool Control_Deslizante::cambiando_valor()
{
	return m_moviendo_barra;
}

bool Control_Deslizante::cambio_valor()
{
	bool valor = m_cambio_valor;
	m_cambio_valor = false;
	return valor;
}
