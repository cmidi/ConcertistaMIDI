#include "organo.h++"

Organo::Organo(float x, float y, float ancho, Rango_Organo *teclado_visible, Rango_Organo *teclado_util, char armadura, unsigned char escala, Administrador_Recursos *recursos) : Elemento(x, y, ancho, 0)
{
	//El origen del organo esta abajo a la izquierda
	m_teclado_visible = teclado_visible;
	m_teclado_util = teclado_util;

	m_tecla_blanca = recursos->textura(Textura::TeclaBlanca);
	m_tecla_negra = recursos->textura(Textura::TeclaNegra);

	m_tecla_blanca_presionada = recursos->textura(Textura::TeclaBlancaPresionada);
	m_tecla_blanca_presionada_doble = recursos->textura(Textura::TeclaBlancaPresionadaDoble);
	m_tecla_negra_presionada = recursos->textura(Textura::TeclaNegraPresionada);
	m_circulo = recursos->textura(Textura::Circulo);

	m_borde_negro = recursos->textura(Textura::BordeOrganoNegro);
	m_borde_rojo = recursos->textura(Textura::BordeOrganoRojo);

	m_recursos = recursos;
	m_rectangulo = recursos->figura(FiguraGeometrica::Rectangulo);

	Sombreador *particulas = recursos->sombreador(SombreadorVF::Particula);
	Textura2D *t_particula = recursos->textura(Textura::ParticulaNota);
	m_generador_particulas = new Generador_Particulas(particulas, t_particula, 1500, 0);//La escala se cambia al calcular los tamaños
	m_tiempo = 0;
	m_numero_particulas = 0;

	m_nota_enviada_anterior = SIN_NOTA;

	m_teclas = nullptr;

	m_opcion_etiqueta = ORGANO_OCULTO;
	m_armadura = armadura;
	m_escala = escala;

	this->calcular_tamannos();
}

Organo::~Organo()
{
	delete m_generador_particulas;

	if(m_nombre_notas.size() > 0)
	{
		for(unsigned int x=0; x<m_nombre_notas.size(); x++)
			delete m_nombre_notas[x];
		m_nombre_notas.clear();
	}
}

void Organo::dibujar_blancas(float x, float y, unsigned char tecla_inicial, unsigned char numero_teclas)
{
	float desplazamiento = x;
	bool tecla_presionada_anterior = false;
	Etiqueta *texto_actual = nullptr;
	m_tecla_blanca->activar();

	unsigned char dn = 0;
	if(	m_opcion_etiqueta == ORGANO_DOVARIABLE ||
		m_opcion_etiqueta == ORGANO_NUMERICO)
		dn = Funciones_Midi::desplazamiento_notas(m_armadura, m_escala);

	for(unsigned char n=tecla_inicial; n<tecla_inicial+numero_teclas; n++)
	{
		m_rectangulo->textura(true);
		//Se salta las negras
		if(!Octava::es_blanca(n))
			continue;

		Tecla_Organo &tecla = m_teclas->at(n);
		if(tecla.activa || tecla.activa_automatica)
		{
			//La tecla es tocada y la tecla se cambia de color
			m_rectangulo->color(tecla.color_tecla);

			//Se agregan las particulas del mismo color que la nota
			if(tecla.mostrar_particula)
				m_generador_particulas->agregar_particulas(desplazamiento + m_ancho_tecla_blanca/2.0f - m_ancho_tecla_blanca/2.0f, y, m_numero_particulas, tecla.color_tecla);

			//Cambia el efectos de sombra
			if(tecla_presionada_anterior)
				m_tecla_blanca_presionada_doble->activar();
			else
				m_tecla_blanca_presionada->activar();
			tecla_presionada_anterior = true;
		}
		else
		{
			//La tecla no es tocada, color normal
			m_tecla_blanca->activar();
			//Fuera del m_teclado_util tiene un color mas oscuro
			if(n < m_teclado_util->tecla_inicial() || n > m_teclado_util->tecla_final())
				m_rectangulo->color(Color(0.7f, 0.7f, 0.7f));
			else
				m_rectangulo->color(Color(1.0f, 1.0f, 1.0f));
			tecla_presionada_anterior = false;
		}

		m_rectangulo->dibujar(desplazamiento, y, m_ancho_tecla_blanca - 1, m_alto_tecla_blanca);

		//Dibuja un punto indicando que debe tocar la nota
		if(tecla.es_requerida)
		{
			m_rectangulo->color(tecla.color_punto);
			m_circulo->activar();
			float circulo = (m_ancho_tecla_blanca/2.0f)-1;
			m_rectangulo->dibujar(desplazamiento+m_ancho_tecla_blanca/4.0f, y+m_alto_sobre_blanca_normal-circulo, circulo, circulo);
		}

		if(m_opcion_etiqueta != ORGANO_OCULTO)
		{
			texto_actual = m_nombre_notas[static_cast<unsigned char>((n+dn)%12)];
			texto_actual->color(Color(0.0f, 0.0f, 0.0f));
			texto_actual->dimension(m_ancho_tecla_blanca-1, texto_actual->alto_texto());

			if(tecla_presionada_anterior)
				texto_actual->posicion(desplazamiento, y+m_alto_sobre_blanca_presionada-texto_actual->alto_texto_real());
			else
				texto_actual->posicion(desplazamiento, y+m_alto_sobre_blanca_normal-texto_actual->alto_texto_real());
			texto_actual->dibujar();
		}

		desplazamiento += m_ancho_tecla_blanca;
	}
}

void Organo::dibujar_negras(float x, float y, unsigned char tecla_inicial, unsigned char numero_teclas)
{
	float desplazamiento = 0;
	unsigned char numero_blancas = 0;
	bool tecla_presionada = false;
	Etiqueta *texto_actual = nullptr;
	m_tecla_negra->activar();

	unsigned char dn = 0;
	if(	m_opcion_etiqueta == ORGANO_DOVARIABLE ||
		m_opcion_etiqueta == ORGANO_NUMERICO)
		dn = Funciones_Midi::desplazamiento_notas(m_armadura, m_escala);

	for(unsigned char n=tecla_inicial; n<tecla_inicial+numero_teclas; n++)
	{
		m_rectangulo->textura(true);
		//Se salta las blancas
		if(Octava::es_blanca(n))
		{
			numero_blancas++;
			continue;
		}

		//Numero de la tecla dentro de la octava
		desplazamiento = x + static_cast<float>(numero_blancas) * m_ancho_tecla_blanca + m_ancho_tecla_negra * Octava::desplazamiento_negra(n);

		Tecla_Organo &tecla = m_teclas->at(n);
		if(tecla.activa || tecla.activa_automatica)
		{
			//La tecla es tocada, se cambia de color
			m_rectangulo->color(tecla.color_tecla);
			m_tecla_negra_presionada->activar();
			if(tecla.mostrar_particula)//No se generan particulas para notas erroneas
				m_generador_particulas->agregar_particulas(desplazamiento + m_ancho_tecla_negra/2.0f - m_ancho_tecla_blanca/2.0f, y, m_numero_particulas, tecla.color_tecla);

			tecla_presionada = true;
		}
		else
		{
			//La tecla no es tocada, color normal
			//Fuera del m_teclado_util tiene un color mas oscuro
			if(n < m_teclado_util->tecla_inicial() || n > m_teclado_util->tecla_final())
				m_rectangulo->color(Color(0.7f, 0.7f, 0.7f));
			else
				m_rectangulo->color(Color(1.0f, 1.0f, 1.0f));
			m_tecla_negra->activar();
			tecla_presionada = false;
		}

		//El ancho de la tecla mas el ancho de la sombra
		m_rectangulo->dibujar(desplazamiento, y, m_ancho_tecla_negra + m_ancho_tecla_negra * 0.22f, m_alto_tecla_negra);

		//Dibuja un punto indicando que debe tocar la nota
		if(tecla.es_requerida)
		{
			Color color_nota(tecla.color_punto);
			color_nota.cambiar_luminosidad(-0.1f);
			m_rectangulo->color(color_nota);
			m_circulo->activar();
			float circulo = (m_ancho_tecla_negra/2.0f)-1;
			m_rectangulo->dibujar(desplazamiento+m_ancho_tecla_negra/4.0f, y+m_alto_sobre_negra_normal-circulo, circulo, circulo);
		}

		if(m_opcion_etiqueta != ORGANO_OCULTO &&
			m_opcion_etiqueta != ORGANO_DOFIJO_SOLO_BLANCAS &&
			m_opcion_etiqueta != ORGANO_INGLES_SOLO_BLANCAS)
		{
			texto_actual = m_nombre_notas[static_cast<unsigned char>((n+dn)%12)];
			texto_actual->color(Color(1.0f, 1.0f, 1.0f));
			texto_actual->dimension(m_ancho_tecla_negra-1, texto_actual->alto_texto());
			if(tecla_presionada)
				texto_actual->posicion(desplazamiento, y+m_alto_sobre_negra_presionada-texto_actual->alto_texto_real());
			else
				texto_actual->posicion(desplazamiento, y+m_alto_sobre_negra_normal-texto_actual->alto_texto_real());
			texto_actual->dibujar();
		}
	}
}

void Organo::actualizar(unsigned int diferencia_tiempo)
{
	m_tiempo += static_cast<float>(diferencia_tiempo);

	//Generar particulas
	unsigned int particulas = static_cast<unsigned int>(m_tiempo / 16666666.0f);//16ms
	if(particulas > 0)
	{
		if(static_cast<float>(particulas) * 16666666 < m_tiempo)
			m_tiempo -= static_cast<float>(particulas * 16666666);
		m_numero_particulas = particulas*2;
	}
	else
		m_numero_particulas = 0;
	m_generador_particulas->actualizar((static_cast<float>(diferencia_tiempo)/1000000000.0f)*3.0f);
}

void Organo::dibujar()
{
	//Dibuja un fondo negro
	m_rectangulo->textura(false);
	m_rectangulo->dibujar(this->x(), this->y() - this->alto(), this->ancho(), this->alto(), Color(0.0f, 0.0f, 0.0f));

	//Dibuja las notas blancas
	this->dibujar_blancas(this->x(), (this->y() - this->alto()) + 5, m_teclado_visible->tecla_inicial(), m_teclado_visible->numero_teclas());

	//Dibuja un borde rojo
	m_borde_rojo->activar();
	m_rectangulo->textura(true);
	m_rectangulo->color(Color(1.0f, 1.0f, 1.0f));
	m_rectangulo->dibujar(this->x(), (this->y() - this->alto()) + 5, this->ancho(), 5);

	//Dibuja las notas negras
	this->dibujar_negras(this->x(), (this->y() - this->alto()) + 5, m_teclado_visible->tecla_inicial(), m_teclado_visible->numero_teclas());

	//Dibuja un borde negro
	m_borde_negro->activar();
	m_rectangulo->color(Color(1.0f, 1.0f, 1.0f));
	m_rectangulo->dibujar(this->x(), (this->y() - this->alto()), this->ancho(), 5);

	//Dibuja las particulas
	m_generador_particulas->dibujar();
}

void Organo::evento_raton(Raton *raton)
{
	if(raton->activado(BotonRaton::Izquierdo) && raton->esta_sobre(this->x(), this->y() - this->alto() + 10, this->ancho(), this->alto()))
	{
		unsigned char ajuste_teclas = 0;
		//Cuenta el numero de teclas desde el inicio hasta la primera tecla visible sin contar la visible
		if(m_teclado_visible->tecla_inicial() > 0)
			ajuste_teclas = Octava::blancas_desde_inicio(m_teclado_visible->tecla_inicial()-1);

		//Cuenta las teclas blancas que aparecen en pantalla
		unsigned char tecla_presionada = static_cast<unsigned char>((static_cast<float>(raton->x()) - this->x()) / m_ancho_tecla_blanca);

		//Agrega las teclas blancas que faltan con el ajuste_teclas
		unsigned char nota_enviar = Octava::nota_id_desde_blanca(tecla_presionada+ajuste_teclas);

		//Verifica si es posible que se haya tocado una tecla negra
		if(raton->esta_sobre(this->x(), this->y() - this->alto() + 10, this->ancho(), m_alto_tecla_negra))
		{
			float desplazamiento = 0;
			//Revisa si existe una tecla negra a la izquierda
			int negra_encontrada = false;
			if(nota_enviar > 0 && !Octava::es_blanca(nota_enviar-1))
			{
				//Numero de la tecla dentro de la octava
				desplazamiento = this->x() + static_cast<float>(tecla_presionada) * m_ancho_tecla_blanca + m_ancho_tecla_negra * Octava::desplazamiento_negra(nota_enviar-1);
				//Se toco una tecla negra a la izquierda de la blanca
				if(raton->esta_sobre(desplazamiento, this->y() - this->alto() + 10, m_ancho_tecla_negra, m_alto_tecla_negra))
				{
					//Verifica si la tecla negra es visible
					if((nota_enviar-1) >= m_teclado_visible->tecla_inicial())
					{
						nota_enviar--;
						negra_encontrada = true;
					}
				}
			}
			//Revisa si existe una tecla negra a la derecha
			if(!negra_encontrada && nota_enviar < 127 && !Octava::es_blanca(nota_enviar+1))
			{
				desplazamiento = this->x() + static_cast<float>(tecla_presionada+1) * m_ancho_tecla_blanca + m_ancho_tecla_negra * Octava::desplazamiento_negra(nota_enviar+1);
				//Se toco una tecla negra a la derecha de la blanca
				if(raton->esta_sobre(desplazamiento, this->y() - this->alto() + 10, m_ancho_tecla_negra, m_alto_tecla_negra))
				{
					//Verifica si la tecla negra es visible
					if((nota_enviar+1) <= m_teclado_visible->tecla_final())
						nota_enviar++;
				}
			}
		}

		//Revisa que sea una nota diferenta a la enviada anteriormente
		if(m_nota_enviada_anterior != nota_enviar)
		{
			//Evita enviar una nota fuera del teclado visible
			if(nota_enviar >= m_teclado_visible->tecla_inicial() && nota_enviar <= m_teclado_visible->tecla_final())
			{
				//Apaga la nota anterior si se habia enviado una
				if(m_nota_enviada_anterior != SIN_NOTA)
					m_eventos.push_back(std::pair<unsigned char, bool>(m_nota_enviada_anterior, false));
				//Envia la nueva nota
				m_eventos.push_back(std::pair<unsigned char, bool>(nota_enviar, true));
				m_nota_enviada_anterior = nota_enviar;
			}
		}
	}
	else if(m_nota_enviada_anterior != SIN_NOTA)
	{
		m_eventos.push_back(std::pair<unsigned char, bool>(m_nota_enviada_anterior, false));
		m_nota_enviada_anterior = SIN_NOTA;
	}
}

void Organo::dimension(float ancho, float alto)
{
	this->_dimension(ancho, alto);
	this->calcular_tamannos();
}

void Organo::teclas(std::array<Tecla_Organo, NUMERO_TECLAS_ORGANO> *notas)
{
	m_teclas = notas;
}

void Organo::cambiar_armadura(char armadura, unsigned char escala)
{
	m_armadura = armadura;
	m_escala = escala;
}

void Organo::calcular_tamannos()
{
	unsigned int numero_blancas = Octava::numero_blancas(m_teclado_visible->tecla_inicial(), m_teclado_visible->numero_teclas());
	m_ancho_tecla_blanca = (this->ancho() / static_cast<float>(numero_blancas));
	m_alto_tecla_blanca = m_ancho_tecla_blanca * PROPORCION_BLANCA;
	if(m_alto_tecla_blanca > 250)
		m_alto_tecla_blanca = 250;

	m_ancho_tecla_negra = m_ancho_tecla_blanca * PROPORCION_ANCHO_NEGRA;
	m_alto_tecla_negra = m_alto_tecla_blanca * PROPORCION_NEGRA;

	m_alto_sobre_blanca_normal = m_alto_tecla_blanca * 0.964f;
	m_alto_sobre_blanca_presionada = m_alto_tecla_blanca * 0.94f;
	m_alto_sobre_negra_normal = m_alto_tecla_negra * 0.866f;
	m_alto_sobre_negra_presionada = m_alto_tecla_negra * 0.902f;

	m_generador_particulas->escala(m_ancho_tecla_blanca);
	this->_dimension(this->ancho(), m_alto_tecla_blanca + 6);
}

bool Organo::hay_eventos()
{
	if(m_eventos.size() > 0)
		return true;
	return false;
}

std::pair<unsigned char, bool> Organo::obtener_evento()
{
	if(m_eventos.size() > 0)
	{
		std::pair<unsigned char, bool> primero = m_eventos[0];
		m_eventos.erase(m_eventos.begin());
		return primero;
	}
	return std::pair<unsigned char, bool>(0, false);
}

void Organo::opcion_etiqueta(unsigned char opcion)
{
	if(opcion < this->numero_etiquetas())
	{
		//Opcion sin cambio
		if(m_opcion_etiqueta == opcion)
			return;

		//Etiquetas que no requieren cambio de texto al pasar de una a la otra
		if(	(m_opcion_etiqueta == ORGANO_DOFIJO || m_opcion_etiqueta == ORGANO_DOFIJO_SOLO_BLANCAS) &&
			(opcion == ORGANO_DOFIJO || opcion == ORGANO_DOFIJO_SOLO_BLANCAS))
		{
			m_opcion_etiqueta = opcion;
			return;
		}

		if(	(m_opcion_etiqueta == ORGANO_INGLES || m_opcion_etiqueta == ORGANO_INGLES_SOLO_BLANCAS) &&
			(opcion == ORGANO_INGLES || opcion == ORGANO_INGLES_SOLO_BLANCAS))
		{
			m_opcion_etiqueta = opcion;
			return;
		}

		m_opcion_etiqueta = opcion;
		if(opcion != ORGANO_OCULTO)
		{
			if(m_nombre_notas.size() == 0)
			{
				m_nombre_notas.reserve(12);
				//Crea las etiquetas para los nombres de las notas
				Tipografia *letra_notas = m_recursos->tipografia(ModoLetra::Chica);
				for(unsigned char n=0; n<12; n++)
				{
					Etiqueta *actual = new Etiqueta(0, 0, "", letra_notas, m_recursos);
					actual->alineacion_horizontal(Alineacion_H::Centrado);
					m_nombre_notas.push_back(actual);
				}
			}

			SistemaNombreNotas sistema_actual;

			if(m_opcion_etiqueta == ORGANO_DOFIJO || m_opcion_etiqueta == ORGANO_DOFIJO_SOLO_BLANCAS)
				sistema_actual = SistemaNombreNotas::DoFijo;
			else if(m_opcion_etiqueta == ORGANO_DOVARIABLE)
				sistema_actual = SistemaNombreNotas::DoVariable;
			else if(m_opcion_etiqueta == ORGANO_INGLES || m_opcion_etiqueta == ORGANO_INGLES_SOLO_BLANCAS)
				sistema_actual = SistemaNombreNotas::Ingles;
			else if(m_opcion_etiqueta == ORGANO_NUMERICO)
				sistema_actual = SistemaNombreNotas::Numerico;

			for(unsigned char n=0; n<m_nombre_notas.size(); n++)
				m_nombre_notas[n]->texto(Nombre_Notas(sistema_actual, n));

			this->calcular_tamannos();
		}
	}
}

unsigned char Organo::numero_etiquetas()
{
	return ORGANO_NUMERO_ETIQUETAS;
}
