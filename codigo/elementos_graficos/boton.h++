#ifndef BOTON_H
#define BOTON_H

#include "elemento.h++"
#include "etiqueta.h++"
#include "../recursos/animacion.h++"

class Boton : public Elemento
{
private:
	Administrador_Recursos *m_recursos;
	Textura2D *m_textura_boton;
	Rectangulo *m_rectangulo;
	Tipografia *m_tipografia_actual;

	float m_margen;
	Alineacion_H m_alineacion_horizontal;
	Alineacion_V m_alineacion_vertical;

	Etiqueta *m_texto;
	Color m_color_origen;
	Color m_color_objetivo;
	Color m_color_boton_actual;
	Color m_color_boton_normal;
	Color m_color_boton_sobre;
	Color m_color_boton_activado;
	Color m_color_texto;
	Animacion m_animacion;

	unsigned char m_estado;
	float m_color_rojo;
	float m_color_verde;
	float m_color_azul;

	bool m_sobre_boton;
	bool m_boton_pre_activado;
	bool m_boton_activado;

	bool m_habilitado;

	void inicializar(const std::string &texto, Tipografia *tipografia);

public:
	Boton(float x, float y, float ancho, float alto, std::string texto, Administrador_Recursos *recursos);
	Boton(float x, float y, float ancho, float alto, std::string texto, ModoLetra tipografia, Administrador_Recursos *recursos);
	Boton(float x, float y, float ancho, float alto, std::string texto, Tipografia *tipografia, Administrador_Recursos *recursos);
	~Boton();

	void actualizar(unsigned int diferencia_tiempo) override;
	void dibujar() override;
	void evento_raton(Raton *raton) override;

	void posicion(float x, float y) override;
	void dimension(float ancho, float alto) override;

	void textura(Textura2D *textura);
	void color_boton(Color color);
	void color_texto(Color color);
	void tipografia(Tipografia *tipografia);
	void texto(const std::string &texto);
	void margen(float margen);
	void alineacion_horizontal(Alineacion_H horizontal);
	void alineacion_vertical(Alineacion_V vertical);

	bool esta_activado();
	void habilitado(bool estado);
};

#endif
