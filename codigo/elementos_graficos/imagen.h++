#ifndef IMAGEN_H
#define IMAGEN_H

#include "elemento.h++"
#include "etiqueta.h++"

class Imagen : public Elemento
{
private:
	Rectangulo *m_rectangulo;
	Textura2D *m_textura;

	bool m_centrado;
	float m_ancho_imagen;
	float m_alto_imagen;
	float m_margen;

public:
	Imagen(float x, float y, bool centrado, Textura2D *textura, Administrador_Recursos *recursos);
	~Imagen();

	void margen(float margen);
	void textura(Textura2D *textura);

	void actualizar(unsigned int diferencia_tiempo) override;
	void dibujar() override;
	void evento_raton(Raton *raton) override;

	void posicion(float x, float y) override;
	void dimension(float ancho, float alto) override;
};

#endif
