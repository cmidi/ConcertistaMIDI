#include "imagen.h++"

Imagen::Imagen(float x, float y, bool centrado, Textura2D *textura, Administrador_Recursos *recursos) : Elemento(x, y, 0, 0)
{
	m_centrado = centrado;
	m_rectangulo = recursos->figura(FiguraGeometrica::Rectangulo);
	m_textura = textura;
	m_margen = 0.0f;
}

Imagen::~Imagen()
{
}

void Imagen::margen(float margen)
{
	m_margen = margen;
}

void Imagen::textura(Textura2D *textura)
{
	m_textura = textura;
}

void Imagen::actualizar(unsigned int /*diferencia_tiempo*/)
{
}

void Imagen::dibujar()
{
	m_rectangulo->textura(true);
	m_textura->activar();
	float posicion_x = (this->ancho() / 2.0f) - (m_ancho_imagen / 2.0f);
	float posicion_y = (this->alto() / 2.0f) - (m_alto_imagen / 2.0f);
	m_rectangulo->dibujar(this->x() + posicion_x, this->y() + posicion_y, m_ancho_imagen, m_alto_imagen, Color(1.0f, 1.0f, 1.0f));
}

void Imagen::evento_raton(Raton */*raton*/)
{
}

void Imagen::posicion(float x, float y)
{
	this->_posicion(x, y);
}

void Imagen::dimension(float ancho, float alto)
{
	this->_dimension(ancho, alto);
	float proporcion_imagen = static_cast<float>(m_textura->ancho()) / static_cast<float>(m_textura->alto());
	if(this->ancho() > this->alto())
	{
		m_ancho_imagen = (this->alto() - (m_margen * 2)) * proporcion_imagen;
		m_alto_imagen = this->alto() - (m_margen * 2);
	}
	else
	{
		m_ancho_imagen = this->ancho() - (m_margen * 2);
		m_alto_imagen = (this->ancho() - (m_margen * 2)) * proporcion_imagen;
	}
}
