#include "barra_progreso.h++"
#include "../util/funciones.h++"

Barra_Progreso::Barra_Progreso(float x, float y, float ancho, float alto, std::int64_t tiempo_total, Administrador_Recursos *recursos)
: Elemento(x, y, ancho, alto), m_texto_inicial(recursos), m_texto_final(recursos), m_texto_marcador(recursos), m_color_fondo(0.8f, 0.8f, 0.8f), m_color_progreso(0.0f, 0.8f, 0.4f)
{
	m_tiempo_total = tiempo_total;
	m_tiempo_actual = 0;
	m_tiempo_anterior = 0;
	m_sobre_barra = false;
	m_cambiando_tiempo = false;
	m_mostrar_marcador = false;

	m_frente = recursos->textura(Textura::FrenteBarraProgreso);
	m_textura_sombra = recursos->textura(Textura::Sombra);
	m_minimarcador_1 = recursos->textura(Textura::MiniMarcador_1);
	m_minimarcador_2 = recursos->textura(Textura::MiniMarcador_2);
	m_minimarcador_3 = recursos->textura(Textura::MiniMarcador_3);
	m_minimarcador_4 = recursos->textura(Textura::MiniMarcador_4);

	m_rectangulo = recursos->figura(FiguraGeometrica::Rectangulo);

	m_texto_inicial.texto(Funciones::microsegundo_a_texto(m_tiempo_actual, true, true));
	m_texto_inicial.tipografia(recursos->tipografia(ModoLetra::MuyChica));
	m_texto_inicial.posicion(this->x()+4, this->y() + this->alto() - 12);
	m_texto_inicial.color(Color(0.0f, 0.0f, 0.0f));

	m_texto_final.texto(Funciones::microsegundo_a_texto(tiempo_total, true, true));
	m_texto_final.tipografia(recursos->tipografia(ModoLetra::MuyChica));
	m_texto_final.posicion(this->x()+this->ancho() - (4 + m_texto_final.largo_texto()), this->y() + this->alto() - 12);
	m_texto_final.color(Color(0.0f, 0.0f, 0.0f));

	m_texto_marcador.tipografia(recursos->tipografia(ModoLetra::MuyChica));
	m_texto_marcador.color(Color(0.0f, 0.0f, 0.0f));

	m_direccion_cambio = 0;
}

Barra_Progreso::~Barra_Progreso()
{
}

void Barra_Progreso::actualizar(unsigned int /*diferencia_tiempo*/)
{
	if(m_tiempo_actual >= 0 && m_tiempo_total > 0)
		m_progreso = (static_cast<float>(m_tiempo_actual) / static_cast<float>(m_tiempo_total)) * this->ancho();
	else
		m_progreso = 0;

	if(m_progreso > this->ancho())
		m_progreso = this->ancho();
	else if(m_progreso < 0)
		m_progreso = 0;

	if(m_tiempo_actual >= 0)
		m_texto_inicial.texto(Funciones::microsegundo_a_texto(m_tiempo_actual, true, true));
}

void Barra_Progreso::dibujar()
{
	//Dibuja el fondo (progreso/fondo)
	m_rectangulo->textura(false);
	m_rectangulo->dibujar(this->x(), this->y(), m_progreso, this->alto(), m_color_progreso);
	m_rectangulo->dibujar(this->x()+m_progreso, this->y(), this->ancho() - m_progreso, this->alto(), m_color_fondo);

	//Dibuja las lineas verticales
	m_rectangulo->color(Color(0.5f, 0.5f, 0.5f));
	float linea_actual = 0;
	float linea_anterior = -10;
	for(unsigned int i=0; i<m_indicadores_compas->size(); i++)
	{
		float porcentaje = static_cast<float>((*m_indicadores_compas)[i]) / static_cast<float>(m_tiempo_total);
		linea_actual = porcentaje * this->ancho();
		if(linea_actual - linea_anterior > 2.0f)
		{
			m_rectangulo->dibujar(porcentaje * this->ancho(), this->y(), 1, this->alto());
			linea_anterior = linea_actual;
		}
	}

	//Dibuja los bordes de arriba y abajo
	m_rectangulo->color(Color(0.15f, 0.15f, 0.15f));
	m_rectangulo->dibujar(this->x(), this->y(), this->ancho(), 1);
	m_rectangulo->dibujar(this->x(), this->y()+this->alto()-1, this->ancho(), 1);

	//Dibuja el brillo
	m_frente->activar();
	m_rectangulo->textura(true);
	m_rectangulo->dibujar(this->x(), this->y(), this->ancho(), this->alto(), Color(1.0f, 1.0f, 1.0f));

	if(m_marcadores->size() > 0)
		this->dibujar_marcadores();

	//Dibuja la sombra
	m_textura_sombra->activar();
	m_rectangulo->color(Color(0.7f, 0.7f, 0.7f));
	m_rectangulo->dibujar(this->x(), this->y()+this->alto(), this->ancho(), 20);

	//Dibuja el tiempo inicial y el tiempo restante
	m_texto_inicial.dibujar();
	m_texto_final.dibujar();
	if(m_mostrar_marcador)
		m_texto_marcador.dibujar();
}

void Barra_Progreso::dibujar_marcadores()
{
	m_rectangulo->color(Color(1.0f, 1.0f, 1.0f));
	m_rectangulo->textura(true);
	for(unsigned int i=0; i<m_marcadores->size(); i++)
	{
		std::int64_t tiempo = std::get<0>((*m_marcadores)[i]);
		unsigned char tipo = std::get<1>((*m_marcadores)[i]);
		float posicion_x = static_cast<float>(tiempo) / static_cast<float>(m_tiempo_total) * this->ancho();

		if(tipo == 0)
			m_minimarcador_1->activar();
		else if(tipo == 1)
			m_minimarcador_2->activar();
		else if(tipo == 2)
			m_minimarcador_3->activar();
		else
			m_minimarcador_4->activar();

		float textura = 20;
		if(tipo == 0 || tipo == 2)
			//Marcadores de arriba
			m_rectangulo->dibujar(posicion_x - (textura/2), this->y()+1, textura, textura);
		else
			//Marcadores de abajo
			m_rectangulo->dibujar(posicion_x - (textura/2), this->y()+this->alto()-(textura+1), textura, textura);
	}
}

void Barra_Progreso::evento_raton(Raton *raton)
{
	if(raton->esta_sobre(this->x(), this->y(), this->ancho(), this->alto()))
	{
		bool mostrar_marcador = false;
		for(unsigned int i=0; i<m_marcadores->size(); i++)
		{
			std::int64_t tiempo = std::get<0>((*m_marcadores)[i]);
			unsigned char tipo = std::get<1>((*m_marcadores)[i]);
			float posicion_x = static_cast<float>(tiempo) / static_cast<float>(m_tiempo_total) * this->ancho();

			if(tipo == 0 || tipo == 2)
			{
				//Marcadores de arriba
				if(raton->esta_sobre(posicion_x - 7, this->y()+1, 14, 17))
				{
					if(!m_mostrar_marcador || m_marcador_mostrado != i)
					{
						std::string texto = std::get<2>((*m_marcadores)[i]);
						m_texto_marcador.posicion(static_cast<float>(raton->x()), static_cast<float>(raton->y()+20));
						m_texto_marcador.texto(texto);

						m_mostrar_marcador = true;
						m_marcador_mostrado = i;
					}
					else
						m_texto_marcador.posicion(static_cast<float>(raton->x()), static_cast<float>(raton->y()+20));

					mostrar_marcador = true;
				}
			}
			else
			{
				//Marcadores de abajo
				if(raton->esta_sobre(posicion_x - 7, this->y()+this->alto()-(17+1), 14, 17))
				{
					if(!m_mostrar_marcador || m_marcador_mostrado != i)
					{
						std::string texto = std::get<2>((*m_marcadores)[i]);
						m_texto_marcador.posicion(static_cast<float>(raton->x()), static_cast<float>(raton->y()+20));
						m_texto_marcador.texto(texto);

						m_mostrar_marcador = true;
						m_marcador_mostrado = i;
					}
					else
						m_texto_marcador.posicion(static_cast<float>(raton->x()), static_cast<float>(raton->y()+20));

					mostrar_marcador = true;
				}
			}
		}
		if(!mostrar_marcador)
			m_mostrar_marcador = false;

		if(raton->tipo_evento() == EventoRaton::Clic && raton->activado(BotonRaton::Izquierdo) && m_sobre_barra)
		{
			m_cambiando_tiempo = true;
			m_tiempo_anterior = m_tiempo_actual;
			m_direccion_cambio = 0;
		}
		else if(!raton->activado(BotonRaton::Izquierdo))
		{
			m_sobre_barra = true;

			//Cambio de tiempo con la ruedita del raton
			if(raton->dy() < 0)
			{
				if(m_tiempo_actual > 1000000)
					m_tiempo_actual = m_tiempo_actual-1000000;
				else
					m_tiempo_actual = 0;
				m_direccion_cambio = -1;
			}
			else if(raton->dy() > 0)
			{
				if(m_tiempo_actual+1000000 > m_tiempo_total)
					m_tiempo_actual = m_tiempo_total;
				else
					m_tiempo_actual = m_tiempo_actual+1000000;
				m_direccion_cambio = 1;
			}
		}
	}
	else
	{
		m_sobre_barra = false;
		m_mostrar_marcador = false;
	}

	if(m_cambiando_tiempo)
	{
		if(m_mostrar_marcador)
			m_tiempo_actual = std::get<0>((*m_marcadores)[m_marcador_mostrado]);
		else
		{
			m_tiempo_actual = static_cast<std::int64_t>((static_cast<float>(raton->x()) / static_cast<float>(this->ancho())) * static_cast<float>(m_tiempo_total));

			if(m_tiempo_actual < 0)
				m_tiempo_actual = 0;
			else if(m_tiempo_actual > m_tiempo_total)
				m_tiempo_actual = m_tiempo_total;
		}

		if(m_tiempo_actual > m_tiempo_anterior)
			m_direccion_cambio = 1;
		else if(m_tiempo_actual < m_tiempo_anterior)
			m_direccion_cambio = -1;
		else
			m_direccion_cambio = 0;

		if(!raton->activado(BotonRaton::Izquierdo))
				m_cambiando_tiempo = false;
	}
}

void Barra_Progreso::dimension(float ancho, float alto)
{
	this->_dimension(ancho, alto);
	m_texto_final.posicion(ancho - (4 + m_texto_final.largo_texto()), this->y() + this->alto() - 12);
}

void Barra_Progreso::tiempo_total(std::int64_t tiempo_total)
{
	m_tiempo_total = tiempo_total;
	std::string texto_nuevo = Funciones::microsegundo_a_texto(tiempo_total, true, true);
	if(texto_nuevo != m_texto_final.texto())
		m_texto_final.texto(texto_nuevo);
}

void Barra_Progreso::tiempo(std::int64_t tiempo_actual)
{
	m_tiempo_actual = tiempo_actual;
}

std::int64_t Barra_Progreso::tiempo()
{
	return m_tiempo_actual;
}

bool Barra_Progreso::cambiado_tiempo()
{
	return m_cambiando_tiempo;
}

int Barra_Progreso::direccion_cambio()
{
	int actual = m_direccion_cambio;
	m_direccion_cambio = 0;
	return actual;
}

void Barra_Progreso::indicadores_compas(std::vector<std::int64_t> *compas)
{
	m_indicadores_compas = compas;
}

void Barra_Progreso::marcadores(std::vector<std::tuple<std::int64_t, unsigned char, std::string>> *marcadores)
{
	m_marcadores = marcadores;
}
