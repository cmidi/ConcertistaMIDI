#include "informacion_nota.h++"
#include "../util/octava.h++"

Informacion_Nota::Informacion_Nota(Administrador_Recursos *recursos) : Elemento(0, 0, 0, 0)
{
	m_contruir_texto = false;
	m_modo_desarrollo_actual = false;
	m_existe_evento_final = false;

	m_nota = nullptr;

	m_recursos = recursos;
	m_rectangulo = recursos->figura(FiguraGeometrica::Rectangulo);
	m_textura_fondo = recursos->textura(Textura::InformacionNotaFondo);
	m_textura_borde = recursos->textura(Textura::InformacionNotaBorde);

	m_animacion.tiempo(100000000);

	m_limite_x = 0;
	m_limite_y = 0;
	m_limite_ancho = 0;
	m_limite_alto = 0;
	m_mostrar = false;
	m_visible = false;
}

Informacion_Nota::~Informacion_Nota()
{
	for(unsigned int x=0; x<m_textos.size(); x++)
		delete m_textos[x];
}

void Informacion_Nota::limites(float x, float y, float ancho, float alto)
{
	m_limite_x = x;
	m_limite_y = y;
	m_limite_ancho = ancho;
	m_limite_alto = alto;
}

void Informacion_Nota::actualizar_nota(Nota_Midi *nota, Color color, std::string instrumento, unsigned short pista, unsigned int posicion)
{
	if(m_nota != nota || m_pista != pista || m_posicion != posicion)
	{
		m_contruir_texto = true;
		m_nota = nota;
		m_pista = pista;
		m_posicion = posicion;
		m_instrumento = instrumento;
		m_color = color;

		m_modo_desarrollo_actual = Pantalla::ModoDesarrollo;
		if(nota->inicio != nota->fin)
			m_existe_evento_final = true;
		else
			m_existe_evento_final = false;
	}
	if(m_modo_desarrollo_actual != Pantalla::ModoDesarrollo)
	{
		m_contruir_texto = true;
		m_modo_desarrollo_actual = Pantalla::ModoDesarrollo;
	}
	if(!m_existe_evento_final && nota->inicio != nota->fin)
	{
		m_contruir_texto = true;
		m_existe_evento_final = true;
	}
	if(m_contruir_texto)
	{
		if(m_textos.size() > 0)
		{
			for(unsigned int x=0; x<m_textos.size(); x++)
				delete m_textos[x];
			m_textos.clear();
		}
	}
}

void Informacion_Nota::construir()
{
	if(m_contruir_texto)
	{
		m_textos.push_back(new Etiqueta(0, 0, T_("Grupo: {0} Canal: {1} - {2}{3} (id {4})",
														m_nota->grupo(),
														m_nota->canal(),
														Nombre_Notas(SistemaNombreNotas::DoFijo, m_nota->id_nota() % 12),
														Octava::indice_acustico(m_nota->id_nota()),
														m_nota->id_nota()),
										ModoLetra::Chica, m_recursos));
		m_textos.push_back(new Etiqueta(0, 0, T_("{0} (pista {1})", m_instrumento, m_pista), ModoLetra::Chica, m_recursos));
		m_textos.push_back(new Etiqueta(0, 0, T_("Tiempo Inicial: {0}", Funciones::microsegundo_a_texto(m_nota->inicio->microsegundos(), true, true)), ModoLetra::Chica, m_recursos));
		m_textos.push_back(new Etiqueta(0, 0, T_("Tiempo Final:   {0}", Funciones::microsegundo_a_texto(m_nota->fin->microsegundos(), true, true)), ModoLetra::Chica, m_recursos));

		unsigned int velocidad_nota_inicio = 0;
		if(m_nota->inicio->evento()->tipo_mensaje() == TipoMensaje_VozDeCanalMidi1)
			velocidad_nota_inicio = m_nota->inicio->evento()->velocidad_nota_7();
		else
			velocidad_nota_inicio = m_nota->inicio->evento()->velocidad_nota_16();

		m_textos.push_back(new Etiqueta(0, 0, T_("Velocidad Encendido: {0}", velocidad_nota_inicio), ModoLetra::Chica, m_recursos));
		if(m_nota->inicio != m_nota->fin)
		{
			unsigned int velocidad_nota_fin = 0;
			if(m_nota->fin->evento()->tipo_mensaje() == TipoMensaje_VozDeCanalMidi1)
				velocidad_nota_fin = m_nota->fin->evento()->velocidad_nota_7();
			else
				velocidad_nota_fin = m_nota->fin->evento()->velocidad_nota_16();
			m_textos.push_back(new Etiqueta(0, 0, T_("Velocidad Apagado: {0}", velocidad_nota_fin), ModoLetra::Chica, m_recursos));
		}
		else
			m_textos.push_back(new Etiqueta(0, 0, T_("Velocidad Apagado: {0}", "indefinido"), ModoLetra::Chica, m_recursos));

		if(Pantalla::ModoDesarrollo)
		{
			m_textos.push_back(new Etiqueta(0, 0, T_("Posicion Nota: {0}", m_posicion), ModoLetra::Chica, m_recursos));
			m_textos.push_back(new Etiqueta(0, 0, T_("Tiempo Inicial µS: {0}", m_nota->inicio->microsegundos()), ModoLetra::Chica, m_recursos));
			m_textos.push_back(new Etiqueta(0, 0, T_("Tiempo Final µS:   {0}", m_nota->fin->microsegundos()), ModoLetra::Chica, m_recursos));
		}
		m_contruir_texto = false;

		float alto_letra = static_cast<float>(m_recursos->tipografia(ModoLetra::Chica)->alto_texto());
		float alto_texto = (alto_letra+5.0f) * static_cast<float>(m_textos.size());
		float largo_texto = 0.0;
		for(unsigned int n=0; n<m_textos.size(); n++)
		{
			m_textos[n]->color(Color(0.0f, 0.0f, 0.0f, 0.0f));
			float largo_actual = m_textos[n]->largo_texto();
			if(largo_texto < largo_actual)
				largo_texto = largo_actual;
		}
		this->dimension(largo_texto + 30, alto_texto + 30);
	}
}

void Informacion_Nota::mostrar(bool mostrar)
{
	m_mostrar = mostrar;
	if(m_mostrar && !m_visible)
	{
		m_visible = true;
		m_animacion.reiniciar();
		m_animacion.direccion(Direccion_Animacion::Incremento);
		m_animacion.iniciar();
	}
}

void Informacion_Nota::actualizar(unsigned int diferencia_tiempo)
{
	m_animacion.actualizar(diferencia_tiempo);

	//Se inicia el ocultamiento
	if(!m_mostrar && m_animacion.valor() >= 1.0)
	{
		m_animacion.reiniciar();
		m_animacion.direccion(Direccion_Animacion::Decremento);
		m_animacion.iniciar();
	}

	//Cuando el alfa llega a cero, deja de ser visible
	if(m_visible && m_animacion.valor() <= 0)
		m_visible = false;
}

void Informacion_Nota::dibujar()
{
	if(!m_visible)
		return;

	if(m_textos.size() == 0)
		return;

	m_rectangulo->extremos_fijos(true, true);
	m_rectangulo->textura(true);

	m_textura_fondo->activar();
	m_rectangulo->color(Color(1.0f, 1.0f, 1.0f, 0.8f*m_animacion.valor_f()));
	m_rectangulo->dibujar_estirable(this->x()-14, this->y()-14, this->ancho()+28, this->alto()+28, 34, 34);

	m_textura_borde->activar();
	m_rectangulo->color(Color(m_color.rojo(), m_color.verde(), m_color.azul(), m_animacion.valor_f()));
	m_rectangulo->dibujar_estirable(this->x(), this->y(), this->ancho(), this->alto(), 20, 20);

	float desplazamiento = 15;//15px margen superior
	for(unsigned int n=0; n<m_textos.size(); n++)
	{
		m_textos[n]->color()->alfa(m_animacion.valor_f());
		m_textos[n]->posicion(this->x() + 15, this->y() + desplazamiento);
		m_textos[n]->dibujar();
		desplazamiento += 17;
	}

	m_rectangulo->extremos_fijos(false, false);
}

void Informacion_Nota::evento_raton(Raton *raton)
{
	float x = static_cast<float>(raton->x());
	float y = static_cast<float>(raton->y());
	if(x < m_limite_x)
		x = m_limite_x;
	else if(x + this->ancho() > m_limite_x + m_limite_ancho)
		x = (m_limite_x + m_limite_ancho) - this->ancho();

	if(y < m_limite_y)
		y = m_limite_y;
	else if(y + this->alto() > m_limite_y + m_limite_alto)
		y = (m_limite_y + m_limite_alto) - this->alto();

	this->posicion(x, y);
}
