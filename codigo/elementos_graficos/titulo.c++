#include "titulo.h++"

Titulo::Titulo(float x, float y, float ancho, float alto, Administrador_Recursos *recursos) : Elemento(x, y, ancho, alto), m_titulo(recursos), m_autor(recursos)
{
	m_rectangulo = recursos->figura(FiguraGeometrica::Rectangulo);
	m_fondo = recursos->textura(Textura::TituloMusica);

	m_titulo.tipografia(recursos->tipografia(ModoLetra::MuyGrande));
	m_titulo.dimension(ancho, m_titulo.alto_texto());
	m_titulo.color(Color(1.0f, 1.0f, 1.0f));
	m_titulo.alineacion_horizontal(Alineacion_H::Centrado);
	m_titulo.alineacion_vertical(Alineacion_V::Centrado);

	m_autor.tipografia(recursos->tipografia(ModoLetra::Grande));
	m_autor.dimension(ancho, m_autor.alto_texto());
	m_autor.color(Color(1.0f, 1.0f, 1.0f));
	m_autor.alineacion_horizontal(Alineacion_H::Centrado);
	m_autor.alineacion_vertical(Alineacion_V::Centrado);

	m_estado = 0;
	m_tiempo = 0;
	m_posicion_texto = 0;
	m_texto_largo = 0;
	m_alfa = 0;
	//1920px/s debe avanzar 32px cada 16.6ms
	m_velocidad_texto = (m_ancho / 3.6f) / (1000.0f/60.0f);

}

Titulo::~Titulo()
{
}

void Titulo::actualizar(unsigned int diferencia_tiempo)
{
	if(m_estado == 5)//Invisible y no se actualiza mas
		return;

	float ms = static_cast<float>(diferencia_tiempo) / 1000000.0f;
	float valocidad_alfa = ms * 0.003f;//0.18a/s  = 60 fps * 0.003a
	float velocidad_pixeles = (m_velocidad_texto / 16.0f) * ms;//Calcula el tiempo que debe avanzar de acuerdo a la diferencia de tiempo

	if(m_estado == 0)//Aparecer
	{
		m_alfa +=valocidad_alfa;
		if(m_alfa >= 1.0f)
		{
			m_alfa = 1.0;
			m_estado = 1;
		}
	}
	else if(m_estado == 1)//Entrada
	{
		m_posicion_texto -= velocidad_pixeles;
		if(m_posicion_texto <= 0)
		{
			m_posicion_texto = 0;
			m_estado = 2;
		}
	}
	else if(m_estado == 2)//Espera 3 segundo
	{
		m_tiempo += diferencia_tiempo;
		if(m_tiempo >= 2000000000)
			m_estado = 3;
	}
	else if(m_estado == 3)//Salida
	{
		m_posicion_texto -= velocidad_pixeles;
		if(m_posicion_texto < -m_texto_largo)
		{
			m_posicion_texto = -m_texto_largo;
			m_estado = 4;
		}
	}
	else if(m_estado == 4)//Desaparecer
	{
		m_alfa -=valocidad_alfa;
		if(m_alfa < 0.0f)
		{
			m_alfa = 0.0;
			m_estado = 5;
		}
	}

	if(m_autor.texto().length() > 0)
	{
		m_titulo.posicion(this->x()+m_posicion_texto, this->y()+this->alto()/2-m_titulo.alto_texto_real());
		m_autor.posicion(this->x()-m_posicion_texto, this->y()+this->alto()/2+20);
	}
	else
		m_titulo.posicion(this->x()+m_posicion_texto, (this->y()+this->alto()/2.0f-m_titulo.alto_texto_real()/2.0f));
}

void Titulo::dibujar()
{
	if(m_estado == 5)//Invisible
		return;

	m_fondo->activar();
	m_rectangulo->textura(true);
	m_rectangulo->dibujar(this->x(), this->y()+this->alto()/2-100, this->ancho(), 200, Color(1.0f, 1.0f, 1.0f, m_alfa));

	m_titulo.dibujar();
	if(m_autor.texto().length() > 0)
		m_autor.dibujar();
}

void Titulo::evento_raton(Raton */*raton*/)
{
}

void Titulo::dimension(float ancho, float alto)
{
	if(m_estado == 5)//Invisible y no se actualiza mas
		return;

	this->_dimension(ancho, alto);

	m_titulo.dimension(ancho, m_titulo.alto_texto_real());
	m_autor.dimension(ancho, m_autor.alto_texto_real());

	float porcentaje_avance = (m_posicion_texto + m_texto_largo) / (m_texto_largo*2);

	if(m_titulo.largo_texto() > m_autor.largo_texto())
		m_texto_largo = this->ancho() / 2.0f + m_titulo.largo_texto() / 2.0f;
	else
		m_texto_largo = this->ancho() / 2.0f + m_autor.largo_texto() / 2.0f;
	m_posicion_texto = m_texto_largo;

	m_posicion_texto = ((m_posicion_texto + m_texto_largo) * porcentaje_avance) - m_texto_largo;

	if(m_autor.texto().length() > 0)
	{
		m_titulo.posicion(this->x()+m_posicion_texto, this->y()+this->alto()/2-m_titulo.alto_texto_real());
		m_autor.posicion(this->x()-m_posicion_texto, this->y()+this->alto()/2+20);
	}
	else
		m_titulo.posicion(this->x()+m_posicion_texto, (this->y()+this->alto()/2.0f-m_titulo.alto_texto_real()/2.0f));
}

float Titulo::posicion_y_dibujo()
{
	return this->y()+this->alto()/2-100;
}

void Titulo::datos(Datos_Musica *datos_musica)
{
	m_datos = datos_musica;
	m_titulo.texto(m_datos->nombre_musica());
	m_autor.texto(m_datos->autor());
	if(m_titulo.largo_texto() > m_autor.largo_texto())
		m_texto_largo = this->ancho() / 2.0f + m_titulo.largo_texto() / 2.0f;
	else
		m_texto_largo = this->ancho() / 2.0f + m_autor.largo_texto() / 2.0f;

	m_posicion_texto = m_texto_largo;
}
