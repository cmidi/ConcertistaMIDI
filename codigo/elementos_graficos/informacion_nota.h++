#ifndef INFORMACION_NOTA
#define INFORMACION_NOTA

#include "elemento.h++"
#include "etiqueta.h++"
#include "../libreria_midi/nota_midi.h++"
#include "../recursos/animacion.h++"

#include <vector>

class Informacion_Nota : public Elemento
{
private:
	Administrador_Recursos *m_recursos;
	Rectangulo *m_rectangulo;
	Textura2D *m_textura_fondo, *m_textura_borde;
	std::vector<Etiqueta*> m_textos;
	bool m_mostrar;
	bool m_visible;

	Color m_color;
	Animacion m_animacion;
	std::string m_instrumento;
	float m_limite_x;
	float m_limite_y;
	float m_limite_ancho;
	float m_limite_alto;

	Nota_Midi *m_nota;
	unsigned short m_pista;
	unsigned int m_posicion;
	bool m_contruir_texto;
	bool m_modo_desarrollo_actual;
	bool m_existe_evento_final;

public:
	Informacion_Nota(Administrador_Recursos *recursos);
	~Informacion_Nota();

	void limites(float x, float y, float ancho, float alto);
	void actualizar_nota(Nota_Midi *nota, Color color, std::string instrumento, unsigned short pista, unsigned int posicion);
	void construir();
	void mostrar(bool mostrar);

	void actualizar(unsigned int diferencia_tiempo) override;
	void dibujar() override;
	void evento_raton(Raton *raton) override;
};

#endif
