#include "boton.h++"

Boton::Boton(float x, float y, float ancho, float alto, std::string texto, Administrador_Recursos *recursos) : Elemento(x, y, ancho, alto)
{
	m_recursos = recursos;
	m_rectangulo = recursos->figura(FiguraGeometrica::Rectangulo);
	m_textura_boton = recursos->textura(Textura::Boton);

	this->inicializar(texto, recursos->tipografia(ModoLetra::Mediana));
}

Boton::Boton(float x, float y, float ancho, float alto, std::string texto, ModoLetra tipografia, Administrador_Recursos *recursos) : Elemento(x, y, ancho, alto)
{
	m_recursos = recursos;
	m_rectangulo = recursos->figura(FiguraGeometrica::Rectangulo);
	m_textura_boton = recursos->textura(Textura::Boton);

	this->inicializar(texto, recursos->tipografia(tipografia));
}

Boton::Boton(float x, float y, float ancho, float alto, std::string texto, Tipografia *tipografia, Administrador_Recursos *recursos) : Elemento(x, y, ancho, alto)
{
	m_recursos = recursos;
	m_rectangulo = recursos->figura(FiguraGeometrica::Rectangulo);
	m_textura_boton = recursos->textura(Textura::Boton);

	this->inicializar(texto, tipografia);
}

Boton::~Boton()
{
	if(m_texto != nullptr)
		delete m_texto;
}

void Boton::inicializar(const std::string &texto, Tipografia *tipografia)
{
	m_sobre_boton = false;
	m_boton_pre_activado = false;
	m_boton_activado = false;

	Color color(1.0f, 1.0f, 1.0f);
	m_color_boton_actual = color;
	m_color_boton_normal = color;
	m_color_boton_sobre = color - 0.1f;
	m_color_boton_activado = color - 0.15f;
	m_color_texto = Color(0.0f, 0.0f, 0.0f);

	m_habilitado = true;

	m_animacion.tiempo(100000000);
	m_estado = 0;
	m_color_rojo = 0;
	m_color_verde = 0;
	m_color_azul = 0;
	m_margen = 10.0f;
	m_alineacion_horizontal = Alineacion_H::Centrado;
	m_alineacion_vertical = Alineacion_V::Centrado;

	m_tipografia_actual = tipografia;

	if(texto.length() > 0)
	{
		m_texto = new Etiqueta(m_recursos);
		m_texto->texto(texto);
		m_texto->tipografia(m_tipografia_actual);
		m_texto->margen(m_margen);
		m_texto->alineacion_horizontal(m_alineacion_horizontal);
		m_texto->alineacion_vertical(m_alineacion_vertical);
		m_texto->posicion(this->x(), this->y());
		if(m_ancho > 0)
			m_texto->dimension(m_ancho, m_alto);
		else
		{
			m_texto->dimension(0, m_alto);
			m_texto->ancho_automatico(true);
			m_ancho = m_texto->ancho();
		}
		m_texto->color(m_color_texto);
	}
	else
		m_texto = nullptr;
}

void Boton::actualizar(unsigned int diferencia_tiempo)
{
	m_animacion.actualizar(diferencia_tiempo);

	if(m_animacion.iniciado())
	{
		m_color_boton_actual.rojo(m_color_origen.rojo() + (m_color_rojo * m_animacion.valor_f()));
		m_color_boton_actual.verde(m_color_origen.verde() + (m_color_verde * m_animacion.valor_f()));
		m_color_boton_actual.azul(m_color_origen.azul() + (m_color_azul * m_animacion.valor_f()));
	}
}

void Boton::dibujar()
{
	//Dibuja el fondo
	if(m_textura_boton != nullptr)
	{
		m_textura_boton->activar();
		m_rectangulo->textura(true);
		m_rectangulo->dibujar(this->x(), this->y(), this->ancho(), this->alto(), m_color_boton_actual);
	}

	//Dibuja el texto
	if(m_texto != nullptr)
		m_texto->dibujar();
}

void Boton::evento_raton(Raton *raton)
{
	if(!m_habilitado)
		return;

	bool cambio_estado = false;
	if(raton->esta_sobre(this->x(), this->y(), this->ancho(), this->alto()))
	{
		if(	raton->tipo_evento() == EventoRaton::Clic &&
			raton->activado(BotonRaton::Izquierdo) && m_sobre_boton)
		{
			//Solo se aceptan el clic cuando estaba sobre el boton antes del evento
			if(m_estado != 2)
			{
				m_estado = 2;
				cambio_estado = true;
				m_boton_pre_activado = true;
				m_color_objetivo = m_color_boton_activado;
			}
		}
		else if(!raton->activado(BotonRaton::Izquierdo))
		{
			//Se confirma que esta sobre el boton antes del evento
			if(m_estado != 1)
			{
				m_estado = 1;
				cambio_estado = true;
				m_color_objetivo = m_color_boton_sobre;
				m_sobre_boton = true;
				if(m_boton_pre_activado)
				{
					//Se activa al soltar el clic sobre el boton
					m_boton_activado = true;
					m_boton_pre_activado = false;
				}
			}
		}
	}
	else
	{
		//Se borra todo
		if(m_estado != 0)
		{
			m_estado = 0;
			cambio_estado = true;
			m_color_objetivo = m_color_boton_normal;
			m_sobre_boton = false;
			m_boton_pre_activado = false;
			m_boton_activado = false;
		}
	}

	if(cambio_estado == true)
	{
		m_animacion.reiniciar();
		m_animacion.iniciar();

		m_color_origen = m_color_boton_actual;
		m_color_rojo = m_color_objetivo.rojo() - m_color_boton_actual.rojo();
		m_color_verde = m_color_objetivo.verde() - m_color_boton_actual.verde();
		m_color_azul = m_color_objetivo.azul() - m_color_boton_actual.azul();
	}
}

void Boton::posicion(float x, float y)
{
	this->_posicion(x, y);
	if(m_texto != nullptr)
		m_texto->posicion(this->x(), this->y());
}

void Boton::dimension(float ancho, float alto)
{
	this->_dimension(ancho, alto);
	if(m_texto != nullptr)
		m_texto->dimension(this->ancho(), this->alto());
}

void Boton::textura(Textura2D *textura)
{
	m_textura_boton = textura;
}

void Boton::color_boton(Color color)
{
	m_color_boton_normal = color;
	m_color_boton_sobre = color - 0.1f;
	m_color_boton_activado = color - 0.15f;

	if(m_estado == 0)
		m_color_boton_actual = m_color_boton_normal;
	else if(m_estado == 1)
		m_color_boton_actual = m_color_boton_sobre;
	else if(m_estado == 2)
		m_color_boton_actual = m_color_boton_activado;

	m_color_origen = m_color_boton_actual;
	m_color_rojo = 0.0f;
	m_color_verde = 0.0f;
	m_color_azul = 0.0f;
	m_animacion.detener();
}

void Boton::color_texto(Color color)
{
	m_color_texto = color;
	if(m_texto != nullptr)
		m_texto->color(m_color_texto);
}

void Boton::tipografia(Tipografia *tipografia)
{
	m_tipografia_actual = tipografia;
	if(m_texto != nullptr)
		m_texto->tipografia(tipografia);
}

void Boton::texto(const std::string &texto)
{
	if(texto.length() > 0)
	{
		if(m_texto == nullptr)
		{
			m_texto = new Etiqueta(m_recursos);
			m_texto->tipografia(m_tipografia_actual);
			m_texto->margen(m_margen);
			m_texto->alineacion_horizontal(m_alineacion_horizontal);
			m_texto->alineacion_vertical(m_alineacion_vertical);
			m_texto->posicion(this->x(), this->y());
		}
		m_texto->texto(texto);

		if(m_ancho > 0)
			m_texto->dimension(m_ancho, m_alto);
		else
		{
			m_texto->dimension(0, m_alto);
			m_texto->ancho_automatico(true);
			m_ancho = m_texto->ancho();
		}
		m_texto->color(m_color_texto);
	}
	else
	{
		if(m_texto != nullptr)
			delete m_texto;

		m_texto = nullptr;
	}
}

void Boton::margen(float margen)
{
	m_margen = margen;
	if(m_texto != nullptr)
		m_texto->margen(margen);
}

void Boton::alineacion_horizontal(Alineacion_H horizontal)
{
	m_alineacion_horizontal = horizontal;
	if(m_texto != nullptr)
		m_texto->alineacion_horizontal(horizontal);
}

void Boton::alineacion_vertical(Alineacion_V vertical)
{
	m_alineacion_vertical = vertical;
	if(m_texto != nullptr)
		m_texto->alineacion_vertical(vertical);
}

bool Boton::esta_activado()
{
	bool estado = m_boton_activado;
	m_boton_activado = false;
	return estado;
}

void Boton::habilitado(bool estado)
{
	if(m_habilitado != estado)
	{
		m_habilitado = estado;
		if(m_habilitado)
			m_color_texto = m_color_texto - 0.5f;
		else
		{
			m_color_texto = m_color_texto + 0.5f;
			m_color_boton_actual = m_color_boton_normal;
		}
		if(m_texto != nullptr)
			m_texto->color(m_color_texto);
	}
}
