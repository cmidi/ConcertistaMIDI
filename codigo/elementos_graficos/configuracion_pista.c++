#include "configuracion_pista.h++"

Configuracion_Pista::Configuracion_Pista(float x, float y, float ancho, float alto, Pista pista, Administrador_Recursos *recursos)
: Elemento(x, y, ancho, alto), m_texto_instrumento(recursos), m_texto_datos(recursos), m_texto_sonido(recursos), m_seleccion_modo(20, 80, 100, 50, false, recursos), m_seleccion_color(125, 80, 100, 50, false, recursos), m_datos_pista(pista)
{
	m_rectangulo = recursos->figura(FiguraGeometrica::Rectangulo);

	m_texto_instrumento.texto(m_datos_pista.pista_midi()->nombre_instrumento());
	m_texto_instrumento.tipografia(recursos->tipografia(ModoLetra::Mediana));
	m_texto_instrumento.color(Color(1.0f, 1.0f, 1.0f));
	m_texto_instrumento.posicion(20, 20);
	m_texto_instrumento.dimension(310, 14);

	m_texto_datos.texto(T_("{0} notas - Grupo {1} - Canal {2} - Programa {3} ({4}, {5})",
						   m_datos_pista.pista_midi()->numero_notas(),
						   m_datos_pista.pista_midi()->grupo()+1,
						   m_datos_pista.pista_midi()->canal()+1,
						   m_datos_pista.pista_midi()->programa()+1,
						   m_datos_pista.pista_midi()->banco_izquierdo(),
						   m_datos_pista.pista_midi()->banco_derecho()));
	m_texto_datos.tipografia(recursos->tipografia(ModoLetra::MuyChica));
	m_texto_datos.color(Color(1.0f, 1.0f, 1.0f));
	m_texto_datos.posicion(20, 42);
	m_texto_datos.dimension(310, 8);

	m_texto_sonido.tipografia(recursos->tipografia(ModoLetra::MuyChica));
	m_texto_sonido.color(Color(1.0f, 1.0f, 1.0f));
	m_texto_sonido.dimension(100, m_texto_sonido.alto_texto());
	m_texto_sonido.alineacion_horizontal(Alineacion_H::Centrado);
	m_texto_sonido.alineacion_vertical(Alineacion_V::Centrado);
	m_texto_sonido.posicion(230, 122);

	m_textura_fondo = recursos->textura(Textura::Circulo);

	m_textura_reproducir = recursos->textura(Textura::Reproducir);
	m_textura_pausar = recursos->textura(Textura::Pausar);

	m_textura_sonido_activado = recursos->textura(Textura::Sonido_3);
	m_textura_sonido_desactivado = recursos->textura(Textura::Sonido_0);

	std::vector<std::string> opcion_modo;
	//TR Musica de Fondo
	opcion_modo.push_back(T("Fondo"));
	//TR Musica tocada por el jugador
	opcion_modo.push_back(T("Tocar"));
	//TR La musica espera al jugador
	opcion_modo.push_back(T("Aprender"));

	std::vector<std::string> opcion_color;
	for(unsigned int nc=0; nc<NUMERO_COLORES_PISTA+1; nc++)
		opcion_color.push_back(T(Pista::Nombre_colores[nc]));

	std::vector<Textura2D*> icono_modos;
	icono_modos.push_back(recursos->textura(Textura::MusicaFondo));
	icono_modos.push_back(recursos->textura(Textura::Tocar));
	icono_modos.push_back(recursos->textura(Textura::Aprender));

	std::vector<Textura2D*> icono_color;
	for(unsigned char cp=0; cp<NUMERO_COLORES_PISTA+1; cp++)
		icono_color.push_back(recursos->textura(Textura::ColorPista));

	m_seleccion_modo.dimension_icono(40, 40);
	m_seleccion_modo.opciones_textos(opcion_modo);
	m_seleccion_modo.opciones_iconos(icono_modos);
	m_seleccion_modo.color_texto(Color(1.0f, 1.0f, 1.0f));
	m_seleccion_modo.tipografia(recursos->tipografia(ModoLetra::MuyChica));

	m_seleccion_color.dimension_icono(40, 40);
	m_seleccion_color.opciones_textos(opcion_color);
	m_seleccion_color.opciones_iconos(icono_color);
	m_seleccion_color.color_texto(Color(1.0f, 1.0f, 1.0f));
	m_seleccion_color.tipografia(recursos->tipografia(ModoLetra::MuyChica));
	m_seleccion_color.opcion_predeterminada(1);

	m_vista_previa = new Boton(300, 50, 30, 30, "", recursos);
	m_vista_previa->color_boton(Color(1.0f, 1.0f, 1.0f));
	m_vista_previa->textura(m_textura_reproducir);

	m_boton_sonido = new Boton(260, 80, 40, 40, "", recursos);
	m_boton_sonido->color_boton(Color(1.0f, 1.0f, 1.0f));

	//Se lee la configuracion del color
	for(unsigned int i=0; i<NUMERO_COLORES_PISTA+1; i++)
	{
		//Se busca el color de la pista
		if(Pista::Colores_pista[i] == m_datos_pista.color())
		{
			m_seleccion_color.opcion_predeterminada(i);
			i = NUMERO_COLORES_PISTA;//Termina el ciclo
		}
	}

	//Se lee la configuracion del modo
	if(m_datos_pista.modo() == Modo::Fondo)
		m_seleccion_modo.opcion_predeterminada(0);
	else if(m_datos_pista.modo() == Modo::Tocar)
		m_seleccion_modo.opcion_predeterminada(1);
	else if(m_datos_pista.modo() == Modo::Aprender)
		m_seleccion_modo.opcion_predeterminada(2);

	//Se lee la configuracion del sonido
	if(m_datos_pista.sonido())
	{
		m_boton_sonido->textura(m_textura_sonido_activado);
		//TR La pista MIDI se escucha
		m_texto_sonido.texto(T("Sonido activado"));
	}
	else
	{
		m_boton_sonido->textura(m_textura_sonido_desactivado);
		//TR La pista MIDI no se escucha
		m_texto_sonido.texto(T("Sin sonido"));
	}

	m_cambio_estado_vista_previa = false;
	m_estado_vista_previa = false;
}

Configuracion_Pista::~Configuracion_Pista()
{
	delete m_vista_previa;
	delete m_boton_sonido;
}

void Configuracion_Pista::actualizar(unsigned int diferencia_tiempo)
{
	m_seleccion_modo.actualizar(diferencia_tiempo);
	m_seleccion_color.actualizar(diferencia_tiempo);
	m_vista_previa->actualizar(diferencia_tiempo);
	m_boton_sonido->actualizar(diferencia_tiempo);
}

void Configuracion_Pista::dibujar()
{
	//Dibuja el fondo
	m_textura_fondo->activar();
	m_rectangulo->textura(true);
	m_rectangulo->extremos_fijos(true, true);
	m_rectangulo->color(m_datos_pista.color());
	m_rectangulo->dibujar_estirable(this->x(), this->y(), this->ancho(), this->alto(), 8, 8);
	m_rectangulo->extremos_fijos(false, false);

	//Dibuja los componentes
	m_vista_previa->dibujar();
	m_boton_sonido->dibujar();

	m_seleccion_modo.dibujar();
	m_seleccion_color.dibujar();

	m_texto_instrumento.dibujar();
	m_texto_datos.dibujar();
	m_texto_sonido.dibujar();
}

void Configuracion_Pista::evento_raton(Raton *raton)
{
	m_seleccion_modo.evento_raton(raton);
	m_seleccion_color.evento_raton(raton);
	m_vista_previa->evento_raton(raton);
	m_boton_sonido->evento_raton(raton);

	if(m_seleccion_modo.cambio_opcion_seleccionada())
	{
		unsigned int modo_seleccionado = m_seleccion_modo.opcion_seleccionada();
		if(modo_seleccionado == 0)
			m_datos_pista.modo(Modo::Fondo);
		else if(modo_seleccionado == 1)
			m_datos_pista.modo(Modo::Tocar);
		else if(modo_seleccionado == 2)
			m_datos_pista.modo(Modo::Aprender);
	}

	if(m_seleccion_color.cambio_opcion_seleccionada())
	{
		m_datos_pista.color(Pista::Colores_pista[m_seleccion_color.opcion_seleccionada()]);

		if(m_seleccion_color.opcion_seleccionada() == 0)
			m_datos_pista.visible(false);
		else
			m_datos_pista.visible(true);
	}

	if(m_vista_previa->esta_activado())
	{
		m_estado_vista_previa = !m_estado_vista_previa;
		m_cambio_estado_vista_previa = true;
		if(m_estado_vista_previa)
			m_vista_previa->textura(m_textura_pausar);
		else
			m_vista_previa->textura(m_textura_reproducir);
	}

	if(m_boton_sonido->esta_activado())
	{

		bool estado = m_datos_pista.sonido();
		estado = !estado;
		m_datos_pista.sonido(estado);
		if(estado)
		{
			m_texto_sonido.texto(T("Sonido activado"));
			m_boton_sonido->textura(m_textura_sonido_activado);
		}
		else
		{
			m_texto_sonido.texto(T("Sin sonido"));
			m_boton_sonido->textura(m_textura_sonido_desactivado);
		}
	}
}

bool Configuracion_Pista::cambio_estado_vista_previa()
{
	bool estado = m_cambio_estado_vista_previa;
	m_cambio_estado_vista_previa = false;
	return estado;
}

bool Configuracion_Pista::vista_previa()
{
	return m_estado_vista_previa;
}

void Configuracion_Pista::posicion(float x, float y)
{
	this->_posicion(x, y);

	m_seleccion_modo.posicion(x+20, y+80);
	m_seleccion_color.posicion(x+125, y+80);
	m_vista_previa->posicion(x+300, y+50);
	m_boton_sonido->posicion(x+260, y+80);
	m_texto_instrumento.posicion(x+20, y+20);
	m_texto_datos.posicion(x+20, y+42);
	m_texto_sonido.posicion(x+230, y+122);
}

Pista Configuracion_Pista::pista()
{
	return m_datos_pista;
}
