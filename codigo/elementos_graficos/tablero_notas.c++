#include "tablero_notas.h++"

Tablero_Notas::Tablero_Notas(float x, float y, float ancho, float alto, Rango_Organo *teclado_visible, Rango_Organo *teclado_util, TiempoTocado *tiempo_tocado, Administrador_Recursos *recursos)
: Elemento(x, y, ancho, alto)
{
	m_textura_nota = recursos->textura(Textura::Nota);
	m_textura_nota_resaltada = recursos->textura(Textura::NotaResaltada);

	m_textura_marcador_1 = recursos->textura(Textura::Marcador_1);
	m_textura_marcador_2 = recursos->textura(Textura::Marcador_2);
	m_textura_marcador_3 = recursos->textura(Textura::Marcador_3);
	m_textura_marcador_4 = recursos->textura(Textura::Marcador_4);

	m_rectangulo = recursos->figura(FiguraGeometrica::Rectangulo);
	m_tipografia = recursos->tipografia(ModoLetra::MuyChica);
	m_tipografia_chica = recursos->tipografia(ModoLetra::Chica);
	m_recursos = recursos;

	m_informacion_nota = new Informacion_Nota(recursos);
	m_informacion_nota->limites(x, y, ancho, alto);

	m_desplazamiento = Direccion::Bajar;
	m_opcion_etiqueta = TABLERO_NOTAS_OCULTO;
	m_mosrar_reproduccion_previa = false;

	m_nota_seleccionada = false;
	m_raton_x = -1.0f;
	m_raton_y = -1.0f;
	m_teclado_visible = teclado_visible;
	m_teclado_util = teclado_util;
	m_tiempo_actual_midi = 0;
	m_duracion_nota = 6500;
	m_pistas_midi = nullptr;
	m_lineas = nullptr;
	m_marcadores = nullptr;
	m_pistas = nullptr;
	m_tiempo_tocado = tiempo_tocado;

	this->calcular_tamannos();
}

Tablero_Notas::~Tablero_Notas()
{
	for(std::map<int, Etiqueta*>::iterator i = m_texto_numeros.begin(); i != m_texto_numeros.end(); i++)
		delete i->second;
	m_texto_numeros.clear();

	for(std::map<std::int64_t, Etiqueta*>::iterator i = m_texto_marcador.begin(); i != m_texto_marcador.end(); i++)
		delete i->second;
	m_texto_marcador.clear();

	if(m_dinamicas.size() > 0)
	{
		for(unsigned char n=0; n<m_dinamicas.size(); n++)
			delete m_dinamicas[n];
		m_dinamicas.clear();
	}

	if(m_nombre_notas.size() > 0)
	{
		for(unsigned char x=0; x<m_nombre_notas.size(); x++)
			delete m_nombre_notas[x];
		m_nombre_notas.clear();
	}

	delete m_informacion_nota;
}

void Tablero_Notas::calcular_tamannos()
{
	m_ancho_blanca = (this->ancho() / static_cast<float>(Octava::numero_blancas(m_teclado_visible->tecla_inicial(), m_teclado_visible->numero_teclas())));
	m_ancho_negra = m_ancho_blanca * PROPORCION_ANCHO_NEGRA;
}

void Tablero_Notas::dibujar_lineas_verticales()
{
	float posicion_x = 0;
	bool primera_parte = false;//La primera parte corresponde a la linea de DO
	unsigned char posicion_inicial = m_teclado_visible->tecla_inicial() % 12;
	if(posicion_inicial == 0 || posicion_inicial > 5)
	{
		//Dibuja linea de DO
		primera_parte = true;
		//Ajusta la posicion_x si no empieza en DO
		if(posicion_inicial != 0)
			posicion_x = m_ancho_blanca * static_cast<float>(8 - Octava::blancas_desde_inicio(posicion_inicial-1) - 1);
	}
	else
	{
		//Dibuja linea de FA
		//Ajusta la posicion_x si no empieza en FA
		if(posicion_inicial != 5)
			posicion_x = m_ancho_blanca * static_cast<float>(4 - Octava::blancas_desde_inicio(posicion_inicial-1) - 1);
	}
	for(unsigned char i=0; i<22 && posicion_x < this->ancho(); i++)
	{
		//Se dibuja la linea vertical
		m_rectangulo->dibujar(this->x()+posicion_x, this->y(), 1, this->alto());
		if(primera_parte)
			posicion_x += m_ancho_blanca * 3;
		else
			posicion_x += m_ancho_blanca * 4;
		primera_parte = !primera_parte;
	}
}

void Tablero_Notas::dibujar_lineas_horizontales()
{
	int numero_linea = 0;
	float posicion_y = 0;
	Etiqueta *numero_temporal;

	for(unsigned int i=0; i<m_lineas->size(); i++)
	{
		numero_linea++;

		//Dibuja solo las lineas que seran visibles
		if(m_desplazamiento == Direccion::Bajar)
		{
			posicion_y = (static_cast<float>(m_tiempo_actual_midi) - static_cast<float>((*m_lineas)[i])) / static_cast<float>(m_duracion_nota);
			if(posicion_y < -this->alto())
				break;
			else if(posicion_y > static_cast<float>(m_tipografia->alto_texto() + 2))
				continue;
		}
		else if(m_desplazamiento == Direccion::Subir)
		{
			posicion_y = (static_cast<float>((*m_lineas)[i]) - static_cast<float>(m_tiempo_actual_midi)) / static_cast<float>(m_duracion_nota);
			if(posicion_y > static_cast<float>(m_tipografia->alto_texto() + 2))
				break;
			else if(posicion_y < -this->alto())
				continue;
		}

		float y = this->y() + posicion_y + this->alto();

		//Se carga el numero de linea a mostrar
		numero_temporal = m_texto_numeros[numero_linea];
		if(!numero_temporal)
		{
			//Si no existe se crea
			numero_temporal = new Etiqueta(this->x()+10, y, std::to_string(numero_linea), m_tipografia, m_recursos);
			m_texto_numeros[numero_linea] = numero_temporal;
		}

		//Dibuja la linea horizontal
		m_rectangulo->textura(false);
		m_rectangulo->dibujar(this->x(), y, this->ancho(), 1);

		//Dibuja el numero de linea
		numero_temporal->posicion(this->x()+10, y - static_cast<float>(m_tipografia->alto_texto() + 2));
		numero_temporal->dibujar();
	}
}

void Tablero_Notas::dibujar_marcadores()
{
	float posicion_y = 0;
	Etiqueta *texto_marcador;

	for(unsigned int i=0; i<m_marcadores->size(); i++)
	{
		std::int64_t tiempo = std::get<0>((*m_marcadores)[i]);

		//Dibuja solo los marcadores que seran visibles
		if(m_desplazamiento == Direccion::Bajar)
		{
			posicion_y = (static_cast<float>(m_tiempo_actual_midi) - static_cast<float>(tiempo)) / static_cast<float>(m_duracion_nota);

			if(posicion_y < -this->alto()-10)//10 es el alto de la mitad de la textura
				break;
			else if(posicion_y > static_cast<float>(m_tipografia_chica->alto_texto()/2))
				continue;
		}
		else if(m_desplazamiento == Direccion::Subir)
		{
			posicion_y = (static_cast<float>(tiempo) - static_cast<float>(m_tiempo_actual_midi)) / static_cast<float>(m_duracion_nota);
			if(posicion_y > static_cast<float>(m_tipografia_chica->alto_texto()/2))
				break;
			else if(posicion_y < -this->alto()-10)
				continue;
		}

		unsigned char tipo = std::get<1>((*m_marcadores)[i]);

		//Se carga el numero de linea a mostrar
		texto_marcador = m_texto_marcador[tiempo];
		if(!texto_marcador)
		{
			std::string texto = std::get<2>((*m_marcadores)[i]);

			//Si no existe se crea
			texto_marcador = new Etiqueta(this->x()+10, this->y()+posicion_y + this->alto(), texto, m_tipografia_chica, m_recursos);
			texto_marcador->color(Color(0.3f, 0.3f, 0.3f));
			m_texto_marcador[tiempo] = texto_marcador;
		}

		//Selecciona el color del marcador
		if(tipo == 0)
			m_textura_marcador_1->activar();
		else if(tipo == 1)
			m_textura_marcador_2->activar();
		else if(tipo == 2)
			m_textura_marcador_3->activar();
		else
			m_textura_marcador_4->activar();

		float posicion_marcador = this->y()+posicion_y + this->alto();//Posicion exacta del marcador
		float ancho_texto = static_cast<float>(texto_marcador->largo_texto());
		float alto_texto = static_cast<float>(m_tipografia_chica->alto_texto());

		//Dibuja el icono del marcador
		m_rectangulo->textura(true);
		m_rectangulo->color(Color(1.0f, 1.0f, 1.0f));
		m_rectangulo->dibujar(this->x(), posicion_marcador, 35, 12);

		//Fondo blanco del texto
		m_rectangulo->textura(false);
		m_rectangulo->color(Color(0.95f, 0.95f, 0.95f));
		m_rectangulo->dibujar(this->x()+35, posicion_marcador-alto_texto/2.0f, ancho_texto, alto_texto);

		//Texto
		texto_marcador->posicion(this->x()+35, posicion_marcador-alto_texto/2.0f);
		texto_marcador->dibujar();

	}
}

void Tablero_Notas::dibujar_notas(unsigned short pista)
{
	float largo_nota = 0;
	float posicion_y = 0;//Es negativo hasta que la tota sale de la pantalla

	//Datos para el dibujo final
	unsigned char numero_nota = 0;//Id de la nota desde 0 hasta 127
	unsigned char numero_notas_omitir = 0;
	if(m_teclado_visible->tecla_inicial() > 0)
		numero_notas_omitir = Octava::blancas_desde_inicio(m_teclado_visible->tecla_inicial()-1);
	float ancho_tecla = 0;//El ancho puede cambiar si es blanca o es negra
	float ajuste_negra = 0;//Permite desplazar la nota negra un poco en relacion a la blanca
	std::vector<Nota_Midi*> &notas = (*m_pistas_midi)[pista]->notas();
	Color color_nota_blanca = (*m_pistas)[pista].color();
	Color color_nota_negra(color_nota_blanca);
	color_nota_negra.cambiar_luminosidad(-0.15f);

	for(unsigned int n=m_ultima_nota[pista]; n<notas.size(); n++)
	{
		m_rectangulo->textura(true);
		//Numero_nota incluye blancas y negras
		numero_nota = notas[n]->id_nota();

		//Se salta las notas fuera de la pantalla hacia los lados
		if(numero_nota < m_teclado_visible->tecla_inicial() || numero_nota >= m_teclado_visible->tecla_inicial() + m_teclado_visible->numero_teclas())
		{
			//Se actualiza si es la ultima nota que salio de la pantalla aunque no se vea
			if(n == m_ultima_nota[pista])
				m_ultima_nota[pista] = n+1;
			continue;
		}

		//Solo cuando se graba, el evento del inicio es igual al final, porque aun no existe el evento final
		if(notas[n]->inicio != notas[n]->fin)
			largo_nota = static_cast<float>(notas[n]->fin->microsegundos() - notas[n]->inicio->microsegundos()) / static_cast<float>(m_duracion_nota);
		else
			largo_nota = static_cast<float>(m_tiempo_actual_midi - notas[n]->inicio->microsegundos()) / static_cast<float>(m_duracion_nota);

		//Largo minimo de nota a 20
		if(largo_nota < 20)
			largo_nota = 20;

		if(m_desplazamiento == Direccion::Bajar)
		{
			posicion_y = static_cast<float>(m_tiempo_actual_midi - notas[n]->inicio->microsegundos()) / static_cast<float>(m_duracion_nota);
			//No se dibujan las notas que aun no entran en la pantalla
			if(posicion_y < -this->alto())
				break;

			//El alto minimo de la nota a mostrar es de 20 pixeles
			if(posicion_y-largo_nota > 0)//La nota n salio de la pantalla
			{
				if(n == m_ultima_nota[pista])
					m_ultima_nota[pista] = n+1;
				//No se dibujan las notas que ya salieron de la pantalla o son invisibles (largo igual a cero)
				continue;
			}
		}
		else if(m_desplazamiento == Direccion::Subir)
		{
			posicion_y = static_cast<float>(notas[n]->inicio->microsegundos() - m_tiempo_actual_midi) / static_cast<float>(m_duracion_nota);

			//No se dibujan las notas que aun no entran en pantalla
			if(posicion_y > 0)
				break;

			//No se dibujan las notas que ya salieron de la pantalla
			if(posicion_y+largo_nota < -this->alto())
			{
				//Se actualiza si es la ultima nota que salio de la pantalla
				if(n == m_ultima_nota[pista])
					m_ultima_nota[pista] = n+1;
				continue;
			}
		}

		//Notas tocadas por el jugador
		Color color_actual;
		if(Octava::es_blanca(numero_nota))
		{
			//Dibuja las notas blancas
			ancho_tecla = m_ancho_blanca;
			ajuste_negra = 0;

			//Se establece el color de la nota
			m_rectangulo->color(color_nota_blanca);
			color_actual = color_nota_blanca;
		}
		else
		{
			//Dibuja las notas negras
			ancho_tecla = m_ancho_negra;

			//Mueve la tecla un poco dependiendo de su posicion
			ajuste_negra = m_ancho_blanca + m_ancho_negra * Octava::desplazamiento_negra(numero_nota);

			//La nota negra es un poco mas oscura
			m_rectangulo->color(color_nota_negra);
			color_actual = color_nota_negra;
		}

		if(m_tiempo_tocado != nullptr)
		{
			if((*m_pistas)[pista].modo() != Modo::Fondo && numero_nota >= m_teclado_util->tecla_inicial() && numero_nota <= m_teclado_util->tecla_final())
			{
				//Cambia de color a la nota tocada correctamente
				TiempoTocado::iterator pista_buscada = m_tiempo_tocado->find(pista);
				if(pista_buscada != m_tiempo_tocado->end() && pista_buscada->second.size() > n)
				{
					std::vector<Tiempos_Nota> &pista_actual = pista_buscada->second;
					if(pista_actual[n].tocada && pista_actual[n].fin_tocado == std::numeric_limits<std::int64_t>::min())
					{
						color_actual.cambiar_luminosidad(0.07f);
						m_rectangulo->color(color_actual);
					}
					//Nota ploma al no tocarla cuando se termina el tiempo
					else if (!pista_actual[n].tocada && pista_actual[n].inicio + TIEMPO_DETECCION_FINAL < m_tiempo_actual_midi)
						m_rectangulo->color(Pista::Colores_pista[0]);
				}
			}
		}

		unsigned char numero_blancas = Octava::blancas_desde_inicio(numero_nota) - numero_notas_omitir;
		if(numero_blancas > 0)
			numero_blancas--;
		else
			ajuste_negra -= m_ancho_blanca;//Esto ocurre cuando comienza con una negra, se le quita el ancho de la blanca

		float dibujar_x = this->x() + static_cast<float>(numero_blancas) * m_ancho_blanca + ajuste_negra;
		float dibujar_y = 0;
		if(m_desplazamiento == Direccion::Bajar)
			dibujar_y = this->y()+this->alto()+posicion_y-largo_nota;
		else if(m_desplazamiento == Direccion::Subir)
			dibujar_y = this->y()+this->alto()+posicion_y;
		if(	m_raton_x >= dibujar_x && m_raton_x <= dibujar_x + ancho_tecla &&
			m_raton_y >= dibujar_y && m_raton_y <= dibujar_y + largo_nota)
		{
			m_informacion_nota->actualizar_nota(notas[n], color_actual, (*m_pistas_midi)[pista]->nombre_instrumento(), pista, n);
			m_nota_seleccionada = true;
			color_actual.cambiar_luminosidad(0.07f);
			m_rectangulo->color(color_actual);
		}

		m_textura_nota->activar();

		if(m_desplazamiento == Direccion::Bajar)
			m_rectangulo->dibujar_estirable(dibujar_x, dibujar_y, ancho_tecla, largo_nota, 0, 10);
		else if(m_desplazamiento == Direccion::Subir)
			m_rectangulo->dibujar_estirable(dibujar_x, dibujar_y, ancho_tecla, largo_nota, 0, 10);

		if(m_opcion_etiqueta == TABLERO_NOTAS_DINAMICA && notas[n]->dinamica != Dinamica::SinCambio && static_cast<unsigned char>(notas[n]->dinamica) < 8)
		{
			Etiqueta *actual = m_dinamicas[static_cast<unsigned char>(notas[n]->dinamica)];

			float px = this->x() + static_cast<float>(numero_blancas) * m_ancho_blanca + ajuste_negra;
			float py = 0;
			if(m_desplazamiento == Direccion::Bajar)
				py = this->y() + this->alto() + posicion_y - actual->alto();
			else if(m_desplazamiento == Direccion::Subir)
				py = this->y() + this->alto() + posicion_y + 5;

			actual->posicion(px+(ancho_tecla*0.12f), py);
			actual->dimension(ancho_tecla*0.8f, actual->alto_texto());
			actual->dibujar();
		}
		else if(m_opcion_etiqueta != TABLERO_NOTAS_DINAMICA && m_opcion_etiqueta != TABLERO_NOTAS_OCULTO)
		{
			unsigned char desplazamiento = 0;
			//Se calcula el desplazamiento solo para el sistema de nombre de notas que depede de la armadura y la escala
			if(m_opcion_etiqueta == TABLERO_NOTAS_DOVARIABLE || m_opcion_etiqueta == TABLERO_NOTAS_NUMERICO)
				desplazamiento = Funciones_Midi::desplazamiento_notas(notas[n]->armadura, notas[n]->escala);

			Etiqueta *actual = m_nombre_notas[static_cast<unsigned char>(notas[n]->id_nota()+desplazamiento)%12];

			float px = this->x() + static_cast<float>(numero_blancas) * m_ancho_blanca + ajuste_negra;
			float py = 0;
			if(m_desplazamiento == Direccion::Bajar)
				py = this->y() + this->alto() + posicion_y - (actual->alto()+5);
			else if(m_desplazamiento == Direccion::Subir)
				py = this->y() + this->alto() + posicion_y + 5;

			actual->posicion(px+(ancho_tecla*0.1f), py);
			actual->dimension(ancho_tecla*0.8f, actual->alto_texto());
			actual->dibujar();
		}
/*
		//Muestra el rango de deteccion
		m_rectangulo->color(Color(0.0f, 0.7f, 0.2f, 0.5f));
		float posicion_y_d = static_cast<float>(m_tiempo_actual_midi - (m_notas[pista][n].start-TIEMPO_TOLERANCIA)) / static_cast<float>(m_duracion_nota);
		float largo_nota_d = static_cast<float>(TIEMPO_TOLERANCIA) / static_cast<float>(m_duracion_nota);
		m_rectangulo->dibujar_estirable(this->x() + static_cast<float>(numero_blancas) * m_ancho_blanca + ajuste_negra, this->y()+this->alto()+posicion_y_d-largo_nota_d, ancho_tecla, largo_nota_d, 0, 10);
*/

		//Muestra lo que realmente toco el jugador
		if(m_mosrar_reproduccion_previa && (*m_pistas)[pista].modo() != Modo::Fondo)
		{
			//AVISO no se esta verificando si (*m_evaluacion)[pista][n] existe
			std::int64_t inicio_tocado = (*m_tiempo_tocado)[pista][n].inicio_tocado;
			std::int64_t fin_tocado = (*m_tiempo_tocado)[pista][n].fin_tocado;

			if(inicio_tocado > 0 || fin_tocado > 0)
			{
				//Dibuja de otro color lo tocado por el jugador
				m_rectangulo->color(Color(color_actual.rojo()+0.4f, color_actual.verde(), color_actual.azul(), 0.5f));
				float posicion_y_tocado = static_cast<float>(m_tiempo_actual_midi - inicio_tocado) / static_cast<float>(m_duracion_nota);
				float largo_nota_tocado = static_cast<float>(fin_tocado - inicio_tocado) / static_cast<float>(m_duracion_nota);
				m_rectangulo->dibujar_estirable(this->x() + static_cast<float>(numero_blancas) * m_ancho_blanca + ajuste_negra, this->y()+this->alto()+posicion_y_tocado-largo_nota_tocado, ancho_tecla, largo_nota_tocado, 0, 10);
			}
		}
	}
}

void Tablero_Notas::actualizar(unsigned int diferencia_tiempo)
{
	m_informacion_nota->actualizar(diferencia_tiempo);
}

void Tablero_Notas::dibujar()
{
	//Dibuja el fondo
	m_rectangulo->textura(false);
	m_rectangulo->dibujar(this->x(), this->y(), this->ancho(), this->alto(), Color(0.95f, 0.95f, 0.95f));

	//Dibuja las lineas
	m_rectangulo->color(Color(0.7f, 0.7f, 0.7f));
	this->dibujar_lineas_verticales();
	this->dibujar_lineas_horizontales();
	this->dibujar_marcadores();

	//Dibuja las notas por pistas
	m_rectangulo->extremos_fijos(false, true);
	m_nota_seleccionada = false;
	for(unsigned short pista=0; pista<m_pistas_midi->size(); pista++)
	{
		//Dibuja solo las pistas que tienen notas, hay pistas vacias
		if((*m_pistas_midi)[pista]->numero_notas() > 0)
		{
			//Dibuja solo las pistas visibles
			if((*m_pistas)[pista].visible())
				this->dibujar_notas(pista);
		}
	}
	m_informacion_nota->mostrar(m_nota_seleccionada);
	m_informacion_nota->construir();
	m_informacion_nota->dibujar();
	m_rectangulo->extremos_fijos(false, false);
}

void Tablero_Notas::evento_raton(Raton *raton)
{
	if(raton->esta_sobre(this->x(), this->y(), this->ancho(), this->alto()))
	{
		m_raton_x = static_cast<float>(raton->x());
		m_raton_y = static_cast<float>(raton->y());
		m_informacion_nota->evento_raton(raton);
	}
	else
	{
		m_raton_x = -1.0f;
		m_raton_y = -1.0f;
	}
}

void Tablero_Notas::dimension(float ancho, float alto)
{
	m_raton_x = -1.0f;
	m_raton_y = -1.0f;
	this->_dimension(ancho, alto);
	this->calcular_tamannos();
	m_informacion_nota->limites(this->x(), this->y(), this->ancho(), this->alto());
}

void Tablero_Notas::tiempo_tocado(TiempoTocado *tiempo_tocado)
{
	m_tiempo_tocado = tiempo_tocado;
}

void Tablero_Notas::tiempo(std::int64_t tiempo)
{
	m_tiempo_actual_midi = tiempo;
}

void Tablero_Notas::pistas_midi(std::vector<Pista_Midi*> *pistas_midi)
{
	m_pistas_midi = pistas_midi;
}

void Tablero_Notas::lineas(std::vector<std::int64_t> *lineas)
{
	m_lineas = lineas;
}

void Tablero_Notas::marcadores(std::vector<std::tuple<std::int64_t, unsigned char, std::string>> *marcadores)
{
	m_marcadores = marcadores;
}

void Tablero_Notas::pistas(std::vector<Pista> *pistas)
{
	m_pistas = pistas;
	for(unsigned short i=0; i<pistas->size(); i++)
		m_ultima_nota.push_back(0);//Se inician todas las pistas en 0
}

unsigned short Tablero_Notas::duracion_nota()
{
	return m_duracion_nota;
}

void Tablero_Notas::duracion_nota(short valor)
{
	if(valor < 0)
		m_duracion_nota = m_duracion_nota - 100;
	else
		m_duracion_nota = m_duracion_nota + 100;

	if(m_duracion_nota < 1500)
		m_duracion_nota = 1500;
	else if(m_duracion_nota > 14000)
		m_duracion_nota = 14000;
}

void Tablero_Notas::modificar_duracion_nota(unsigned short valor)
{
	m_duracion_nota = valor;

	if(m_duracion_nota < 1500)
		m_duracion_nota = 1500;
	else if(m_duracion_nota > 14000)
		m_duracion_nota = 14000;
}

void Tablero_Notas::reiniciar()
{
	for(unsigned short i=0; i<m_ultima_nota.size(); i++)
		m_ultima_nota[i] = 0;
}

void Tablero_Notas::desplazamiento(Direccion direccion)
{
	m_desplazamiento = direccion;
}

void Tablero_Notas::opcion_etiqueta(unsigned char opcion)
{
	if(opcion < this->numero_etiquetas())
	{
		//Opcion sin cambio
		if(m_opcion_etiqueta == opcion)
			return;

		m_opcion_etiqueta = opcion;
		if(opcion == TABLERO_NOTAS_DINAMICA && m_dinamicas.size() == 0)
		{
			m_dinamicas.reserve(8);
			Tipografia *letra_notas = m_recursos->tipografia(ModoLetra::Mediana);
			for(unsigned char n=0; n<8; n++)
			{
				Etiqueta *nueva = new Etiqueta(0, 0, Nombre_Dinamica(static_cast<Dinamica>(n)), letra_notas, m_recursos);
				nueva->alineacion_horizontal(Alineacion_H::Centrado);
				nueva->color(Color(1.0f, 1.0f, 1.0f));
				m_dinamicas.push_back(nueva);
			}
		}
		else if(opcion != TABLERO_NOTAS_OCULTO)
		{
			if(m_nombre_notas.size() == 0)
			{
				m_nombre_notas.reserve(12);
				//Crea las etiquetas para los nombres de las notas
				Tipografia *letra_notas = m_recursos->tipografia(ModoLetra::Chica);
				for(unsigned char n=0; n<12; n++)
				{
					Etiqueta *nueva = new Etiqueta(0, 0, "", letra_notas, m_recursos);
					nueva->alineacion_horizontal(Alineacion_H::Centrado);
					nueva->color(Color(1.0f, 1.0f, 1.0f));
					m_nombre_notas.push_back(nueva);
				}
			}

			SistemaNombreNotas sistema_actual;

			if(m_opcion_etiqueta == TABLERO_NOTAS_DOFIJO)
				sistema_actual = SistemaNombreNotas::DoFijo;
			else if(m_opcion_etiqueta == TABLERO_NOTAS_DOVARIABLE)
				sistema_actual = SistemaNombreNotas::DoVariable;
			else if(m_opcion_etiqueta == TABLERO_NOTAS_INGLES)
				sistema_actual = SistemaNombreNotas::Ingles;
			else if(m_opcion_etiqueta == TABLERO_NOTAS_NUMERICO)
				sistema_actual = SistemaNombreNotas::Numerico;

			for(unsigned char n=0; n<m_nombre_notas.size(); n++)
				m_nombre_notas[n]->texto(Nombre_Notas(sistema_actual, n));
		}
	}
}

unsigned char Tablero_Notas::numero_etiquetas()
{
	return TABLERO_NOTAS_NUMERO_ETIQUETAS;
}

void Tablero_Notas::mosrar_reproduccion_previa(bool estado)
{
	m_mosrar_reproduccion_previa = estado;
}

bool Tablero_Notas::mosrar_reproduccion_previa()
{
	return m_mosrar_reproduccion_previa;
}
