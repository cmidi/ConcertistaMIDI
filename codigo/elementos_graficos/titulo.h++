#ifndef TITULO_H
#define TITULO_H

#include "elemento.h++"
#include "etiqueta.h++"
#include "../control/datos_musica.h++"

class Titulo : public Elemento
{
private:
	Rectangulo *m_rectangulo;
	Textura2D *m_fondo;
	Datos_Musica *m_datos;

	Etiqueta m_titulo, m_autor;

	unsigned short m_estado;
	unsigned int m_tiempo;
	float m_posicion_texto;
	float m_texto_largo;
	float m_alfa;
	float m_velocidad_texto;

public:
	Titulo(float x, float y, float ancho, float alto, Administrador_Recursos *recursos);
	~Titulo();

	void actualizar(unsigned int diferencia_tiempo) override;
	void dibujar() override;
	void evento_raton(Raton *raton) override;

	void dimension(float ancho, float alto) override;
	float posicion_y_dibujo();

	void datos(Datos_Musica *datos_musica);
};

#endif
