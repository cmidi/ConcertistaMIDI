#include "partitura.h++"
#include "../util/octava.h++"

Partitura::Partitura(float x, float y, float ancho, float alto, Administrador_Recursos *recursos) : Elemento(x, y, ancho, alto)
{
	m_rectangulo = recursos->figura(FiguraGeometrica::Rectangulo);
	m_clave = new Etiqueta(0.0f, 0.0f, 40.0f, 20.0f * 4.0f, "𝄞", ModoLetra::MuyGrande, recursos);//𝄞𝄢
	m_numerador = new Etiqueta(0.0f, 0.0f, 10.0f, 10.0f, "4", ModoLetra::MuyChica, recursos);
	m_denumerador = new Etiqueta(0.0f, 0.0f, 10.0f, 10.0f, "4", ModoLetra::MuyChica, recursos);

	m_numerador->alineacion_horizontal(Alineacion_H::Centrado);
	m_numerador->alineacion_vertical(Alineacion_V::Centrado);

	m_denumerador->alineacion_horizontal(Alineacion_H::Centrado);
	m_denumerador->alineacion_vertical(Alineacion_V::Centrado);

	m_pista = nullptr;
	m_tempos = nullptr;
	m_compas = nullptr;
	m_armadura = nullptr;
	m_barras_compas = nullptr;
}

Partitura::~Partitura()
{
	delete m_clave;
	delete m_numerador;
	delete m_denumerador;
}

void Partitura::actualizar(unsigned int /*diferencia_tiempo*/)
{
}

void Partitura::dibujar()
{
	if(m_pista == nullptr || m_barras_compas == nullptr)
		return;

	m_rectangulo->textura(false);
	m_rectangulo->dibujar(this->x(), this->y(), this->ancho(), this->alto(), Color(0.9f, 0.9f, 0.9f, 0.5f));

	float medio = this->alto() / 2.0f;
	float separacion = 10.0f;
	float alto_pentagrama = separacion * 4;
	m_rectangulo->color(Color(0.0f, 0.0f, 0.0f));

	//Pentagrama
	m_rectangulo->dibujar(this->x(), this->y() + (medio - (separacion*2.0f)), this->ancho(), 1.0f);
	m_rectangulo->dibujar(this->x(), this->y() + (medio - separacion), this->ancho(), 1.0f);
	m_rectangulo->dibujar(this->x(), this->y() + medio, this->ancho(), 1.0f);
	m_rectangulo->dibujar(this->x(), this->y() + (medio + separacion), this->ancho(), 1.0f);
	m_rectangulo->dibujar(this->x(), this->y() + (medio + (separacion*2.0f)), this->ancho(), 1.0f);

	m_clave->posicion(20, this->y() + 5.0f + (medio - (separacion*2.0f)));
	m_numerador->posicion(70.0f, this->y() + (medio - separacion));
	m_denumerador->posicion(70.0f, this->y() + medio);

	//Dibuja la clave y el compas
	m_clave->dibujar();
	m_numerador->dibujar();
	m_denumerador->dibujar();

	m_rectangulo->dibujar(this->x()+100.0f, this->y() + (medio - (separacion*2.0f)), 1.0f, alto_pentagrama);

	float ta = 0;
	for(unsigned char c = 0; c<=m_barras_compas->size(); c++)
	{
		ta = static_cast<float>((*m_barras_compas)[c]) / static_cast<float>(m_tiempo_total);
		m_rectangulo->dibujar(this->x()+100.0f + ((this->ancho()-100.0f) * ta), this->y() + (medio - (separacion*2.0f)), 1.0f, alto_pentagrama);
	}

	//Dibujar Notas
	float p_nota = 0;
	std::vector<Nota_Midi*> notas = m_pista->notas();
	Color color_actual;
	for(unsigned int x=0; x<notas.size(); x++)
	{
		Nota_Midi *actual = notas[x];
		unsigned char id_nota = actual->id_nota();
		if(id_nota == 55 || id_nota == 56)//SOL
			p_nota = medio + separacion*4.5f;
		else if(id_nota == 57 || id_nota == 58)
			p_nota = medio + separacion*4.0f;
		else if(id_nota == 59)
			p_nota = medio + separacion*3.5f;
		else if(id_nota == 60 || id_nota == 61)
			p_nota = medio + separacion*3.0f;
		else if(id_nota == 62 || id_nota == 63)
			p_nota = medio + separacion*2.5f;
		else if(id_nota == 64)
			p_nota = medio + separacion*2.0f;
		else if(id_nota == 65 || id_nota == 66)
			p_nota = medio + separacion*1.5f;
		else if(id_nota == 67 || id_nota == 68)
			p_nota = medio + separacion*1.0f;
		else if(id_nota == 69 || id_nota == 70)
			p_nota = medio + separacion*0.5f;
		else if(id_nota == 71)
			p_nota = medio;
		else if(id_nota == 72 || id_nota == 73)
			p_nota = medio - separacion*0.5f;
		else if(id_nota == 74 || id_nota == 75)
			p_nota = medio - separacion*1.0f;
		else if(id_nota == 76)
			p_nota = medio - separacion*1.5f;
		else if(id_nota == 77 || id_nota == 78)
			p_nota = medio - separacion*2.0f;
		else if(id_nota == 79 || id_nota == 80)
			p_nota = medio - separacion*2.5f;
		else if(id_nota == 81 || id_nota == 82)
			p_nota = medio - separacion*3.0f;
		else if(id_nota == 83)
			p_nota = medio - separacion*3.5f;
		else if(id_nota == 84 || id_nota == 85)
			p_nota = medio - separacion*4.0f;
		else if(id_nota == 86 || id_nota == 87)
			p_nota = medio - separacion*4.5f;

		if(Octava::es_blanca(id_nota))
			color_actual = Color(0.0f, 0.0f, 1.0f);
		else
			color_actual = Color(1.0f, 0.0f, 0.0f);

		ta = static_cast<float>(actual->inicio->microsegundos()) / static_cast<float>(m_tiempo_total);
		m_rectangulo->dibujar(this->x()+100.0f + ((this->ancho()-100.0f) * ta), this->y() + p_nota - 5.0f, 4.0f, 10.0f, color_actual);
	}

	float porcentaje_avance = static_cast<float>(m_tiempo_actual) / static_cast<float>(m_tiempo_total);
	if(m_barras_compas->size() > 0)
	{
		if(porcentaje_avance > 0)
		{
			m_rectangulo->color(Color(0.3f, 0.3f, 0.3f, 0.7f));
			m_rectangulo->dibujar(this->x()+100.0f + ((this->ancho()-100.0f) * porcentaje_avance), this->y() + (medio - (separacion*2.0f)), 5.0f, alto_pentagrama);
		}
	}
}

void Partitura::evento_raton(Raton */*raton*/)
{
}

void Partitura::pista(Pista_Midi *pista)
{
	m_pista = pista;
}

void Partitura::tempos(std::vector<Datos_Evento*> *tempos)
{
	m_tempos = tempos;
}

void Partitura::compas(std::vector<Datos_Evento*> *compas)
{
	m_compas = compas;
	if(m_compas->size() > 0)
	{
		Datos_Evento *d = (*m_compas)[0];
		if(d->tics() == 0)//Inicial
		m_numerador->texto(std::to_string(static_cast<unsigned int>(d->evento()->compas_numerador())));
		m_denumerador->texto(std::to_string(static_cast<unsigned int>(d->evento()->compas_denominador())));
	}
}

void Partitura::armadura(std::vector<Datos_Evento*> *armadura)
{
	m_armadura = armadura;
}

void Partitura::barras_compas(std::vector<std::int64_t> *barras_compas)
{
	m_barras_compas = barras_compas;
}

void Partitura::tiempo_total(std::int64_t tiempo_total)
{
	m_tiempo_total = tiempo_total;
}

void Partitura::tiempo(std::int64_t tiempo_actual)
{
	m_tiempo_actual = tiempo_actual;
}
