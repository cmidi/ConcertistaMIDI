#include "barra_carga.h++"

Barra_Carga::Barra_Carga(float x, float y, float ancho, float alto, Administrador_Recursos *recurso) : Elemento(x, y, ancho, alto)
{
	m_rectangulo = recurso->figura(FiguraGeometrica::Rectangulo);
	m_textura_fondo = recurso->textura(Textura::ControlDeslizante_H_Fondo);
	m_textura_progreso = recurso->textura(Textura::ControlDeslizante_H_Relleno);

	m_etiqueta = new Etiqueta(recurso);
	m_etiqueta->tipografia(recurso->tipografia(ModoLetra::Chica));
	m_etiqueta->color(Color(0.2f, 0.2f, 0.2f));
	m_etiqueta->posicion(x, y);
	m_etiqueta->dimension(ancho, alto);
	m_etiqueta->alineacion_horizontal(Alineacion_H::Centrado);
	m_etiqueta->alineacion_vertical(Alineacion_V::Centrado);

	m_valor_actual = 0;
	m_valor_maximo = 0;
	m_cambio_valor = false;

	m_animacion.direccion(Direccion_Animacion::Decremento);
	m_animacion.tiempo(100000000);
}

Barra_Carga::~Barra_Carga()
{
	delete m_etiqueta;
}

void Barra_Carga::actualizar(unsigned int diferencia_tiempo)
{
	if(m_cambio_valor)
	{
		m_etiqueta->texto(m_texto + " " + std::to_string(m_valor_actual) + "/" + std::to_string(m_valor_maximo));
		m_etiqueta->color(Color(0.2f, 0.2f, 0.2f, 1.0f));
		m_animacion.reiniciar();
		m_cambio_valor = false;
	}

	m_animacion.actualizar(diferencia_tiempo);
	if(m_animacion.iniciado())
	{
		m_etiqueta->color(Color(0.2f, 0.2f, 0.2f, m_animacion.valor_f()));
		if(m_animacion.valor() <= 0.0 || m_valor_actual != m_valor_maximo)
			m_animacion.detener();
	}
	else
	{
		if(m_animacion.valor() > 0.0 && m_valor_actual == m_valor_maximo)
			m_animacion.iniciar();
	}
}

void Barra_Carga::dibujar()
{
	if(!this->visible())
		return;

	float progreso = static_cast<float>(m_valor_actual) / static_cast<float>(m_valor_maximo);
	m_rectangulo->textura(true);
	m_rectangulo->color(Color(1.0f, 1.0f, 1.0f, m_animacion.valor_f()));

	m_rectangulo->extremos_fijos(true, false);
	m_textura_fondo->activar();
	m_rectangulo->dibujar_estirable(this->x(), this->y(), this->ancho(), this->alto(), 40.0f, 0.0f);
	m_textura_progreso->activar();
	m_rectangulo->dibujar_estirable(this->x()+1, this->y()+1, (this->ancho()-2)*progreso, this->alto()-2, 40.0f, 0.0f);
	m_rectangulo->extremos_fijos(false, false);

	m_etiqueta->dibujar();
}

void Barra_Carga::evento_raton(Raton */*raton*/)
{
}

void Barra_Carga::posicion(float x, float y)
{
	m_etiqueta->posicion(x, y);
	this->_posicion(x, y);
}

void Barra_Carga::dimension(float ancho, float alto)
{
	m_etiqueta->dimension(ancho, alto);
	this->_dimension(ancho, alto);
}

void Barra_Carga::texto(const std::string &texto)
{
	if(m_texto != texto)
		m_cambio_valor = true;

	m_texto = texto;
}

void Barra_Carga::valor_actual(unsigned int valor)
{
	if(m_valor_actual != valor)
		m_cambio_valor = true;

	m_valor_actual = valor;
}

void Barra_Carga::valor_maximo(unsigned int valor)
{
	if(m_valor_maximo != valor)
		m_cambio_valor = true;

	m_valor_maximo = valor;
}

bool Barra_Carga::visible()
{
	if(m_animacion.valor() > 0)
		return true;
	return false;
}
