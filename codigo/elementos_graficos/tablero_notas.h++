#ifndef TABLERO_NOTAS_H
#define TABLERO_NOTAS_H

#include "elemento.h++"
#include "etiqueta.h++"
#include "informacion_nota.h++"
#include "../recursos/rectangulo.h++"
#include "../util/octava.h++"
#include "../control/datos_musica.h++"
#include "../control/pista.h++"
#include "../control/rango_organo.h++"
#include "../control/tiempos_nota.h++"
#include "../libreria_midi/funciones_midi.h++"

#include <vector>
#include <map>

#define TABLERO_NOTAS_OCULTO 0
#define TABLERO_NOTAS_DINAMICA 1
#define TABLERO_NOTAS_DOFIJO 2
#define TABLERO_NOTAS_DOVARIABLE 3
#define TABLERO_NOTAS_INGLES 4
#define TABLERO_NOTAS_NUMERICO 5
#define TABLERO_NOTAS_NUMERO_ETIQUETAS 6

enum class Direccion : unsigned char
{
	Bajar,
	Subir,
};

class Tablero_Notas : public Elemento
{
private:
	Administrador_Recursos *m_recursos;
	Textura2D *m_textura_nota, *m_textura_nota_resaltada;
	Textura2D *m_textura_marcador_1, *m_textura_marcador_2, *m_textura_marcador_3, *m_textura_marcador_4;
	Rectangulo *m_rectangulo;
	Tipografia *m_tipografia, *m_tipografia_chica;
	Informacion_Nota *m_informacion_nota;
	std::map<int, Etiqueta*> m_texto_numeros;
	std::map<std::int64_t, Etiqueta*> m_texto_marcador;
	std::vector<Etiqueta*> m_dinamicas;
	std::vector<Etiqueta*> m_nombre_notas;

	bool m_nota_seleccionada;
	float m_raton_x, m_raton_y;
	Rango_Organo *m_teclado_visible, *m_teclado_util;
	std::int64_t m_tiempo_actual_midi;
	float m_ancho_blanca, m_ancho_negra;
	unsigned short m_duracion_nota;
	std::vector<Pista_Midi*> *m_pistas_midi;
	std::vector<std::int64_t> *m_lineas;
	std::vector<std::tuple<std::int64_t, unsigned char, std::string>> *m_marcadores;

	std::vector<unsigned int> m_ultima_nota;//Ultima nota por cada pista
	std::vector<Pista> *m_pistas;
	TiempoTocado *m_tiempo_tocado;

	Direccion m_desplazamiento;
	unsigned char m_opcion_etiqueta;
	bool m_mosrar_reproduccion_previa;

	void calcular_tamannos();

	void dibujar_lineas_verticales();
	void dibujar_lineas_horizontales();
	void dibujar_marcadores();
	void dibujar_notas(unsigned short pista);

public:
	Tablero_Notas(float x, float y, float ancho, float alto, Rango_Organo *teclado_visible, Rango_Organo *teclado_util, TiempoTocado *tiempo_tocado, Administrador_Recursos *recursos);
	~Tablero_Notas();

	void actualizar(unsigned int diferencia_tiempo) override;
	void dibujar() override;
	void evento_raton(Raton *raton) override;

	void dimension(float ancho, float alto) override;

	void tiempo_tocado(TiempoTocado *tiempo_tocado);
	void tiempo(std::int64_t tiempo);
	void pistas_midi(std::vector<Pista_Midi*> *pistas_midi);
	void lineas(std::vector<std::int64_t> *lineas);
	void marcadores(std::vector<std::tuple<std::int64_t, unsigned char, std::string>> *marcadores);
	void pistas(std::vector<Pista> *pistas);
	unsigned short duracion_nota();
	void duracion_nota(short valor);
	void modificar_duracion_nota(unsigned short valor);
	void reiniciar();

	void desplazamiento(Direccion direccion);
	void opcion_etiqueta(unsigned char opcion);
	unsigned char numero_etiquetas();
	void mosrar_reproduccion_previa(bool estado);
	bool mosrar_reproduccion_previa();
};

#endif
