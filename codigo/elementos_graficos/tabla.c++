#include "tabla.h++"
#include "etiqueta.h++"
#include "imagen.h++"

Tabla::Tabla(float x, float y, float ancho, float alto, float alto_fila, Administrador_Recursos *recursos) : Elemento(x, y, ancho, alto), m_color_fondo(0.9f, 0.9f, 0.9f)
{
	m_recursos = recursos;
	m_rectangulo = recursos->figura(FiguraGeometrica::Rectangulo);

	m_alto_fila = alto_fila+1.0f;
	m_fila_seleccionada = 0;//Primera fila predeterminada
	m_seleccion = false;
	m_seleccion_activada = false;

	m_espacio_total_columnas = 0;

	//El alto del panel de desplazamiento = alto-(m_alto_fila - 1) y (-1) del borde inferior se neutralizan los -1 entre si
	m_panel_desplazamiento = new Panel_Desplazamiento(x+1.0f, y+(m_alto_fila-1.0f), ancho-2.0f, alto-m_alto_fila, 0, recursos);

	m_texto_tabla_vacia = new Etiqueta(recursos);
	m_texto_tabla_vacia->posicion(x+1.0f, y+(m_alto_fila-1.0f));
	m_texto_tabla_vacia->dimension(ancho-2.0f, alto-m_alto_fila);
	m_texto_tabla_vacia->tipografia(recursos->tipografia(ModoLetra::Chica));
	m_texto_tabla_vacia->alineacion_horizontal(Alineacion_H::Centrado);
	m_texto_tabla_vacia->alineacion_vertical(Alineacion_V::Centrado);
	m_texto_tabla_vacia->color(Color(0.7f, 0.7f, 0.7f));
	m_texto_tabla_vacia->margen(10);
}

Tabla::~Tabla()
{
	for(CeldaTitulo c : m_fila_titulo)
		delete c.texto;

	for(Fila *f : m_filas)
		delete f;

	delete m_panel_desplazamiento;
	delete m_texto_tabla_vacia;
}

void Tabla::actualizar_ancho_columnas()
{
	float posicion_actual = this->x()+1.0f;
	float ancho_actual = 0;
	for(CeldaTitulo c : m_fila_titulo)
	{
		c.texto->posicion(posicion_actual, this->y());
		ancho_actual = (static_cast<float>(c.numero_espacio) * (this->ancho()-2.0f)) / static_cast<float>(m_espacio_total_columnas);
		c.texto->dimension(ancho_actual, m_alto_fila-1.0f);
		posicion_actual += ancho_actual;
	}

	for(Fila *f : m_filas)
	{
		f->dimension(this->ancho()-2.0f, m_alto_fila);

		std::vector<Elemento *> *celdas = f->celdas();
		for(unsigned int x = 0; x<m_fila_titulo.size(); x++)
		{
			(*celdas)[x]->posicion(m_fila_titulo[x].texto->x(), m_fila_titulo[x].texto->y());
			(*celdas)[x]->dimension(m_fila_titulo[x].texto->ancho(), m_fila_titulo[x].texto->alto());
		}
	}
}

void Tabla::actualizar(unsigned int diferencia_tiempo)
{
	m_panel_desplazamiento->actualizar(diferencia_tiempo);
}

void Tabla::dibujar()
{
	//Dibuja el color de fondo
	m_rectangulo->textura(false);
	m_rectangulo->dibujar(this->x(), this->y(), this->ancho(), m_alto_fila-1.0f, m_color_fondo);

	//Dibuja las filas
	for(unsigned int x=0; x<m_fila_titulo.size(); x++)
		m_fila_titulo[x].texto->dibujar();

	//Dibuja los bordes laterales
	if(m_filas.size() > 0)
		m_panel_desplazamiento->dibujar();
	else
		m_texto_tabla_vacia->dibujar();

	m_rectangulo->textura(false);
	m_rectangulo->dibujar(this->x(), this->y(), 1.0f, this->alto(), m_color_fondo);
	m_rectangulo->dibujar(this->x()+this->ancho()-1.0f, this->y(), 1.0f, this->alto(), m_color_fondo);
	m_rectangulo->dibujar(this->x(), this->y()+this->alto()-1.0f, this->ancho(), 1.0f, m_color_fondo);
}

void Tabla::evento_raton(Raton *raton)
{
	m_panel_desplazamiento->evento_raton(raton);
	if(!raton->esta_sobre(this->x(), this->y(), this->ancho(), this->alto()))
		return;//Ignora eventos fuera de la tabla

	for(unsigned int x=0; x<m_filas.size(); x++)
	{
		if(m_filas[x]->esta_seleccionado() && x != m_fila_seleccionada)
		{
			m_filas[m_fila_seleccionada]->deseleccionar();
			m_fila_seleccionada = x;
			m_seleccion_activada = false;
		}
		if(m_filas[x]->esta_seleccionado())
		{
			m_seleccion = true;
			if(raton->tipo_evento() == EventoRaton::Clic && raton->activado(BotonRaton::Izquierdo) && raton->numero_clics() == 2)
			{
				m_seleccion_activada = true;
			}
		}
	}
}

void Tabla::posicion(float x, float y)
{
	this->_posicion(x, y);

	float posicion_actual = this->x();
	float ancho_actual = 0;
	for(CeldaTitulo c : m_fila_titulo)
	{
		c.texto->posicion(posicion_actual, this->y());
		ancho_actual = (static_cast<float>(c.numero_espacio) * this->ancho()) / static_cast<float>(m_espacio_total_columnas);
		posicion_actual += ancho_actual;
	}
	m_panel_desplazamiento->posicion(x+1.0f, y + (m_alto_fila-1.0f));
	m_texto_tabla_vacia->posicion(x+1.0f, y + (m_alto_fila-1.0f));
}

void Tabla::dimension(float ancho, float alto)
{
	this->_dimension(ancho, alto);
	this->actualizar_ancho_columnas();
	//La barra de desplazamiento esta despues de la fila de titulo
	m_panel_desplazamiento->dimension(ancho-2.0f, alto-m_alto_fila);
	m_texto_tabla_vacia->dimension(ancho-2.0f, alto-m_alto_fila);
}

void Tabla::agregar_columna(TipoCelda tipo, std::string texto, bool centrado, unsigned int numero_espacio)
{
	//numero_espacio representa el espacio que ocupa la columna actual en relacion con el espacio total de todas las columnas
	if(numero_espacio == 0)
		numero_espacio = 1;
	m_espacio_total_columnas += numero_espacio;

	Etiqueta *titulo_celda = new Etiqueta(0.0f, 0.0f, texto, ModoLetra::Chica, m_recursos);
	if(centrado)
		titulo_celda->alineacion_horizontal(Alineacion_H::Centrado);
	titulo_celda->alineacion_vertical(Alineacion_V::Centrado);
	titulo_celda->margen(10);
	CeldaTitulo celda_actual = {tipo, titulo_celda, centrado, numero_espacio};

	m_fila_titulo.push_back(celda_actual);

	this->actualizar_ancho_columnas();
}

void Tabla::insertar_fila(const std::vector<CeldaContenido> &celdas, Color color)
{
	if(celdas.size() != m_fila_titulo.size())
	{
		Registro::Depurar("No coincide el numero de columnas.");
		return;
	}

	Fila *f = new Fila(this->x()+1.0f, 0.0f, this->ancho()-2.0f, m_alto_fila, color, m_recursos);
	Elemento *nueva_celda;
	for(unsigned int x=0; x<celdas.size(); x++)
	{
		//Se crean las celdas de la fila
		if(m_fila_titulo[x].tipo == TipoCelda::Etiqueta)
		{
			Etiqueta *e = new Etiqueta(m_fila_titulo[x].texto->x(), 0.0f, celdas[x].texto, ModoLetra::Chica, m_recursos);
			if(m_fila_titulo[x].centrado)
				e->alineacion_horizontal(Alineacion_H::Centrado);
			e->alineacion_vertical(Alineacion_V::Centrado);
			e->margen(10.0f);
			nueva_celda = e;
		}
		else if(m_fila_titulo[x].tipo == TipoCelda::Imagen)
		{
			Textura2D *actual = m_recursos->textura(celdas[x].imagen);
			Imagen *e = new Imagen(m_fila_titulo[x].texto->x(), 0.0f, m_fila_titulo[x].centrado, actual, m_recursos);
			e->margen(1.0f);
			nueva_celda = e;
		}
		nueva_celda->dimension(m_fila_titulo[x].texto->ancho(), m_alto_fila-1.0f);
		f->agregar_celda(nueva_celda);
	}

	m_seleccion = false;
	m_fila_seleccionada = 0;
	m_filas.push_back(f);
	m_panel_desplazamiento->agregar_elemento(f);
}

void Tabla::insertar_fila(const std::vector<CeldaContenido> &celdas)
{
	this->insertar_fila(celdas, Color(0.95f, 0.95f, 0.95f));
}

void Tabla::vaciar()
{
	//Se eliminan las filas al cambiar de carpeta
	m_panel_desplazamiento->vaciar();
	for(Fila *m : m_filas)
		delete m;

	m_filas.clear();
	m_fila_seleccionada = 0;
	m_seleccion = false;
}

void Tabla::texto_tabla_vacia(const std::string &texto)
{
	m_texto_tabla_vacia->texto(texto);
}

void Tabla::cambiar_seleccion(int cambio)
{
	if(m_filas.size() == 0)
		return;
	m_seleccion = true;
	m_seleccion_activada = false;
	//Se deselecciona la fila anterior y se marca la siguiente
	//Si llega arriba comienza abajo de nuevo
	if(!m_filas[m_fila_seleccionada]->esta_seleccionado())
	{
		//La fila no esta seleccionada visiblemente
		//hasta que se cambia la seleccion
		m_filas[m_fila_seleccionada]->seleccionar();
	}
	else
	{
		m_filas[m_fila_seleccionada]->deseleccionar();

		if(cambio > 0)
		{
			m_fila_seleccionada++;
			if(m_fila_seleccionada >= m_filas.size())
				m_fila_seleccionada = 0;
		}
		else
		{
			if(m_fila_seleccionada == 0)
				m_fila_seleccionada = static_cast<unsigned int>(m_filas.size() - 1);
			else
				m_fila_seleccionada--;
		}
		m_filas[m_fila_seleccionada]->seleccionar();
	}

	if(m_filas[m_fila_seleccionada]->y() < this->y()+m_alto_fila)
	{
		//Desplaza la barra cuando se sale por arriba
		float inicio_tabla = this->y()+m_alto_fila;
		float inicio_fila = m_filas[m_fila_seleccionada]->y();
		m_panel_desplazamiento->desplazar_y(static_cast<int>(inicio_tabla - inicio_fila));
	}
	else if(m_filas[m_fila_seleccionada]->y() + m_filas[m_fila_seleccionada]->alto() > this->y()+this->alto())
	{
		//Desplaza la barra cuando se sale por abajo
		float final_tabla = this->y()+this->alto();
		float final_fila = m_filas[m_fila_seleccionada]->y() + m_filas[m_fila_seleccionada]->alto();
		m_panel_desplazamiento->desplazar_y(static_cast<int>(final_tabla - final_fila));
	}
}

void Tabla::seleccionar(unsigned int seleccion)
{
	//Actualiza las posiciones de los elementos antes de moverlos, normalmente esto lo hace
	//el panel de desplazamiento una vez al actualizar, pero la llamada a esta función
	//puede ser realizada antes de actualizar
	m_panel_desplazamiento->actualizar_dimension();
	if(seleccion < m_filas.size())
	{
		//Deselecciona la anterior y selecciona la nueva
		m_filas[m_fila_seleccionada]->deseleccionar();
		m_fila_seleccionada = seleccion;
		m_filas[m_fila_seleccionada]->seleccionar();

		//Mueve la barra de desplazamiento para mostrar la file seleccionada
		if(m_filas[m_fila_seleccionada]->y() < this->y()+m_alto_fila)
		{
			//Desplaza la barra cuando se sale por arriba
			float inicio_tabla = this->y()+m_alto_fila;
			float inicio_fila = m_filas[m_fila_seleccionada]->y();
			m_panel_desplazamiento->desplazar_y(static_cast<int>(inicio_tabla - inicio_fila));
		}
		else if(m_filas[m_fila_seleccionada]->y() + m_filas[m_fila_seleccionada]->alto() > this->y()+this->alto())
		{
			//Desplaza la barra cuando se sale por abajo
			float final_tabla = this->y()+this->alto();
			float final_fila = m_filas[m_fila_seleccionada]->y() + m_filas[m_fila_seleccionada]->alto();
			m_panel_desplazamiento->desplazar_y(static_cast<int>(final_tabla - final_fila));
		}
	}
}

unsigned int Tabla::obtener_seleccion()
{
	m_seleccion_activada = false;
	return m_fila_seleccionada;
}

bool Tabla::seleccion_activada()
{
	return m_seleccion_activada;
}

bool Tabla::seleccion()
{
	return m_seleccion;
}

bool Tabla::seleccion_visible()
{
	if(m_filas.size() == 0 || m_filas.size() < m_fila_seleccionada)
		return false;

	//Se sale por arriba
	if(m_filas[m_fila_seleccionada]->y() < this->y()+m_alto_fila)
		return false;
	//Se sale por abajo
	else if(m_filas[m_fila_seleccionada]->y() + m_filas[m_fila_seleccionada]->alto() > this->y()+this->alto())
		return false;

	return true;
}
