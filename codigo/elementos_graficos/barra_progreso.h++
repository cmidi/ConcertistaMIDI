#ifndef BARRA_PROGRESO_H
#define BARRA_PROGRESO_H

#include "elemento.h++"
#include "etiqueta.h++"

class Barra_Progreso : public Elemento
{
private:
	std::vector<std::int64_t> *m_indicadores_compas;
	std::vector<std::tuple<std::int64_t, unsigned char, std::string>> *m_marcadores;

	std::int64_t m_tiempo_total;
	std::int64_t m_tiempo_actual;
	std::int64_t m_tiempo_anterior;
	float m_progreso;
	bool m_sobre_barra;
	bool m_cambiando_tiempo;
	bool m_mostrar_marcador;
	unsigned int m_marcador_mostrado = 0;
	int m_direccion_cambio;

	Textura2D *m_frente, *m_textura_sombra;
	Textura2D *m_minimarcador_1, *m_minimarcador_2, *m_minimarcador_3, *m_minimarcador_4;

	Etiqueta m_texto_inicial, m_texto_final, m_texto_marcador;
	Rectangulo *m_rectangulo;

	Color m_color_fondo;
	Color m_color_progreso;

	void dibujar_marcadores();
public:
	Barra_Progreso(float x, float y, float ancho, float alto, std::int64_t tiempo_total, Administrador_Recursos *recursos);
	~Barra_Progreso();

	void actualizar(unsigned int diferencia_tiempo) override;
	void dibujar() override;
	void evento_raton(Raton *raton) override;

	void dimension(float ancho, float alto) override;

	void tiempo_total(std::int64_t tiempo_total);
	void tiempo(std::int64_t tiempo_actual);
	std::int64_t tiempo();
	bool cambiado_tiempo();
	int direccion_cambio();

	void indicadores_compas(std::vector<std::int64_t> *compas);
	void marcadores(std::vector<std::tuple<std::int64_t, unsigned char, std::string>> *marcadores);
};

#endif
