#include <GL/glew.h>
#include <SDL2/SDL.h>
#include <unicode/uclean.h>

#include "recursos/administrador_recursos.h++"
#include "dispositivos/pantalla.h++"
#include "dispositivos_midi/sintetizador_midi.h++"
#include "archivos/imagen_tga.h++"
#include "util/usuario.h++"
#include "util/traduccion.h++"
#include "controlador_juego.h++"

#include "registro.h++"
#include "version.h++"
#include "configuracion_cmake.h++"

#define MINIMO_ANCHO 780
#define MINIMO_ALTO 500

void configurar_gl();
void ajustar_ventana(Controlador_Juego *controlador, int ancho_nuevo, int alto_nuevo);
void eventos_raton_rueda(Controlador_Juego *controlador, int desplazamiento_x, int desplazamiento_y);
void eventos_raton_botones(Controlador_Juego *controlador, int boton, int accion, unsigned char numero_clics);
void eventos_raton_posicion(Controlador_Juego *controlador, int x, int y);
void eventos_taclado(Controlador_Juego *controlador, int tecla, bool estado);
void controlar_eventos(Controlador_Juego *controlador, SDL_Event *evento, SDL_Window *ventana);
bool ventana_tiene_el_foco(SDL_Window *ventana);
void crear_carpeta_configuracion();
void actualizar_dimension_ventana(SDL_Window *ventana, Controlador_Juego *controlador);
void guardad_configuracion_pantalla(SDL_Window *ventana, Controlador_Juego *controlador, int *x, int *y, int *ancho, int *alto);
void mostrar_detalles();

int main (int n, char **argumentos)
{
	Sintetizador_Midi sintetizador;
	sintetizador.iniciar();

	configurar_traduccion();
	crear_carpeta_configuracion();

	Registro::Nota("Concertista MIDI " + std::to_string(CONCERTISTAMIDI_VERSION_MAYOR) + "." + std::to_string(CONCERTISTAMIDI_VERSION_MENOR) + "." + std::to_string(CONCERTISTAMIDI_VERSION_PARCHE));

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
	SDL_SetHint(SDL_HINT_VIDEO_X11_NET_WM_BYPASS_COMPOSITOR, "0");//No suspende el compositor de ventana (No funciona en devuan mate)
	//error: ‘SDL_HINT_VIDEO_X11_NET_WM_BYPASS_COMPOSITOR’ was not declared in this scope (devuan mate)
	SDL_SetHint(SDL_HINT_VIDEO_MINIMIZE_ON_FOCUS_LOSS, "0");

	//Se carga el icono
	Archivo::Tga *icono_tga = new Archivo::Tga(std::string(RUTA_ARCHIVOS) + "/texturas/icono.tga");
	SDL_Surface *icono = SDL_CreateRGBSurfaceFrom(icono_tga->imagen(),
												  static_cast<int>(icono_tga->ancho()),
												  static_cast<int>(icono_tga->alto()),
												  static_cast<int>(icono_tga->bytes()),
												  static_cast<int>(icono_tga->ancho())*4,
												  0x000000ff, 0x0000ff00, 0x00ff0000, 0xff000000);

	Controlador_Juego controlador;
	int ventana_x = 0, ventana_y = 0, ventana_ancho = 0, ventana_alto = 0;
	controlador.configuracion()->posicion_ventana(&ventana_x, &ventana_y);
	controlador.configuracion()->dimension_ventana(&ventana_ancho, &ventana_alto);
	Pantalla::Ancho = static_cast<float>(ventana_ancho);
	Pantalla::Alto = static_cast<float>(ventana_alto);

	unsigned int bandera_pantalla = 0;
	if(controlador.es_pantalla_completa())
	{
		bandera_pantalla = SDL_WINDOW_FULLSCREEN_DESKTOP;
		Pantalla::PantallaCompleta = true;
	}

	std::string nombre_ventana = "Concertista MIDI "
								"" + std::to_string(CONCERTISTAMIDI_VERSION_MAYOR) + "."
								"" + std::to_string(CONCERTISTAMIDI_VERSION_MENOR) + "."
								"" + std::to_string(CONCERTISTAMIDI_VERSION_PARCHE);

	SDL_Window *ventana = SDL_CreateWindow(nombre_ventana.c_str(), ventana_x, ventana_y, ventana_ancho, ventana_alto,
										   SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE | bandera_pantalla);
	SDL_EnableScreenSaver();
	SDL_SetWindowIcon(ventana, icono);
	SDL_SetWindowMinimumSize(ventana, MINIMO_ANCHO, MINIMO_ALTO);

	SDL_FreeSurface(icono);
	delete icono_tga;

	/*SDL_GLContext contexto = */SDL_GL_CreateContext(ventana);
	SDL_GL_SetSwapInterval(0);//Limita a 60 fps

	glewExperimental = GL_TRUE;
	GLenum estado = glewInit();
	if(estado != GLEW_OK)
	{
		Registro::Error(T_("Error al iniciar GLEW: {0}", std::string(reinterpret_cast<const char*>(glewGetErrorString(estado)))));
		//return 0; //Aveces puede funcionar cuando falla glew
	}

	mostrar_detalles();
	configurar_gl();

	Administrador_Recursos recursos;
	controlador.asignar_administrador_recursos(&recursos);

	//Carga el archivo pasado como parametro
	if(n > 1)
		controlador.abrir_archivo(argumentos[1]);

	//Carga la primera ventana
	controlador.crear_ventana_inicial();
	Tiempo tiempo_inicio, tiempo_final;
	std::int64_t tiempo_procesamiento, tiempo_espera;
	try
	{
		//Consume los eventos que hay hasta aqui, se hace esto
		//porque en wayland hay eventos de posicion que de alguna forma
		//permanecen de la ejecución anterior, se ve al precionar el boton salir,
		//al volver a iniciar, el boton esta seleccionado, porque esa posicion tenia
		//el raton la ultima vez.
		SDL_Event evento;
		while (SDL_PollEvent(&evento));

		while (!controlador.terminar())
		{
			tiempo_inicio = Reloj::now();

			while (SDL_PollEvent(&evento))
				controlar_eventos(&controlador, &evento, ventana);

			glClear(GL_COLOR_BUFFER_BIT);

			controlador.actualizar();

			//Pantalla Completa
			if(controlador.es_pantalla_completa() && !Pantalla::PantallaCompleta)
			{
				//Guarda la posicion antes de ponerse en pantalla completa
				guardad_configuracion_pantalla(ventana, &controlador, &ventana_x, &ventana_y, &ventana_ancho, &ventana_alto);
				SDL_SetWindowFullscreen(ventana, SDL_WINDOW_FULLSCREEN_DESKTOP);
				Pantalla::PantallaCompleta = true;
				actualizar_dimension_ventana(ventana, &controlador);
			}
			else if(!controlador.es_pantalla_completa() && Pantalla::PantallaCompleta)
			{
				SDL_SetWindowFullscreen(ventana, 0);
				SDL_RestoreWindow(ventana);
				SDL_SetWindowPosition(ventana, ventana_x, ventana_y);
				SDL_SetWindowSize(ventana, ventana_ancho, ventana_alto);
				Pantalla::PantallaCompleta = false;
				SDL_SetWindowMinimumSize(ventana, MINIMO_ANCHO, MINIMO_ALTO);
				actualizar_dimension_ventana(ventana, &controlador);
			}

			//Modo Alambre
			if(controlador.modo_alambre_activado() && !Pantalla::ModoAlambre)
			{
				glPolygonMode(GL_FRONT, GL_LINE);
				Pantalla::ModoAlambre = true;
			}
			else if(!controlador.modo_alambre_activado() && Pantalla::ModoAlambre)
			{
				glPolygonMode(GL_FRONT, GL_FILL);
				Pantalla::ModoAlambre = false;
			}

			SDL_GL_SwapWindow(ventana);
			tiempo_final = Reloj::now();
			//Se agregan 70 microsegundos para intentar compensar el error de std::this_thread::sleep_for
			tiempo_procesamiento = std::chrono::duration_cast<std::chrono::nanoseconds>(tiempo_final - tiempo_inicio).count() + 70000;
			if(tiempo_procesamiento < controlador.tiempo_fotograma())
			{
				tiempo_espera = controlador.tiempo_fotograma() - tiempo_procesamiento;
				std::this_thread::sleep_for(std::chrono::nanoseconds(tiempo_espera));
			}
		}
	}
	catch(std::exception & e)
	{
		//Esto nunca deberia ocurrir
		Registro::Error("Error grave: " + std::string(e.what()));
	}
	catch(...)
	{
		Registro::Error("Error grave desconocido");
	}

	sintetizador.detener();

	//Se guarda la nueva posicion y dimencion de la pantalla si no esta en pantalla completa
	if(!controlador.configuracion()->pantalla_completa())
		guardad_configuracion_pantalla(ventana, &controlador, &ventana_x, &ventana_y, &ventana_ancho, &ventana_alto);

	u_cleanup();
	SDL_DestroyWindow(ventana);
	SDL_Quit();

	return 0;
}

void configurar_gl()
{
	glEnable (GL_CULL_FACE);
	glViewport(0, 0, static_cast<int>(Pantalla::Ancho), static_cast<int>(Pantalla::Alto));
	glClearColor(0.95f, 0.95f, 0.95f, 1.0f);
	glActiveTexture(GL_TEXTURE0);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_SCISSOR_TEST);
	glScissor(0, 0, static_cast<int>(Pantalla::Ancho), static_cast<int>(Pantalla::Alto));
}

void controlar_eventos(Controlador_Juego *controlador, SDL_Event *evento, SDL_Window *ventana)
{
	if (evento->type == SDL_KEYDOWN)
		eventos_taclado(controlador, evento->key.keysym.sym, true);
	else if (evento->type == SDL_KEYUP)
		eventos_taclado(controlador, evento->key.keysym.sym, false);
	else if(evento->type == SDL_MOUSEBUTTONDOWN)
	{
		SDL_CaptureMouse(SDL_TRUE);
		eventos_raton_botones(controlador, evento->button.button, evento->button.state, evento->button.clicks);
	}
	else if(evento->type == SDL_MOUSEBUTTONUP)
	{
		SDL_CaptureMouse(SDL_FALSE);
		eventos_raton_botones(controlador, evento->button.button, evento->button.state, evento->button.clicks);
	}
	else if(evento->type == SDL_MOUSEMOTION)
	{
		//Se ignora este evento si la ventana no tiene el foco
		if(ventana_tiene_el_foco(ventana))
			eventos_raton_posicion(controlador, evento->button.x, evento->button.y);
	}
	else if(evento->type == SDL_MOUSEWHEEL)
	{
		//Se ignora este evento si la ventana no tiene el foco
		if(ventana_tiene_el_foco(ventana))
			eventos_raton_rueda(controlador, evento->wheel.x, evento->wheel.y);
	}
	else if (evento->type == SDL_WINDOWEVENT)
	{
		if(evento->window.event == SDL_WINDOWEVENT_SIZE_CHANGED)
			ajustar_ventana(controlador, evento->window.data1, evento->window.data2);
		else if(evento->window.event == SDL_WINDOWEVENT_CLOSE)
			controlador->evento_salir();
		else if(evento->window.event == SDL_WINDOWEVENT_FOCUS_LOST)
			//Saca el raton a una esquina, esto evita que se haga clic en algo,
			//puede ocurrir al volver a ganar el foco usando el gestor de tareas
			//sin hacer clic en la ventana directamente
			eventos_raton_posicion(controlador, -1, -1);
		else if(evento->window.event == SDL_WINDOWEVENT_FOCUS_GAINED)
		{
			//Se pierde el evento del clic cuando gana el foco y tengo que recuperar la posicion,
			//porque arriba ignoro los eventos de posicion cuando esta fuera de foco,
			//se crean 3 eventos falsos en su lugar, util por ejemplo al hacer clic en la tabla,
			//sin esto simplemente gana el foco pero no selecciona la fila
			int x=-1, y=-1;
			SDL_GetMouseState(&x, &y);
			eventos_raton_posicion(controlador, x, y);
			eventos_raton_botones(controlador, SDL_BUTTON_LEFT, SDL_PRESSED, 1);
			eventos_raton_botones(controlador, SDL_BUTTON_LEFT, SDL_RELEASED, 1);
		}
	}
	else if (evento->type == SDL_DROPFILE)
		controlador->evento_soltar_contenido(true, std::string(evento->drop.file));
	else if (evento->type == SDL_DROPTEXT)
		controlador->evento_soltar_contenido(false, std::string(evento->drop.file));
	else if (evento->type == SDL_DROPBEGIN)
		controlador->evento_soltar_inicio();
	else if (evento->type == SDL_DROPCOMPLETE)
		controlador->evento_soltar_fin();
}

bool ventana_tiene_el_foco(SDL_Window *ventana)
{
	unsigned int bandera = SDL_GetWindowFlags(ventana);

	if((bandera & SDL_WINDOW_INPUT_FOCUS) == SDL_WINDOW_INPUT_FOCUS)
		return true;

	return false;
}

void ajustar_ventana(Controlador_Juego *controlador, int ancho_nuevo, int alto_nuevo)
{
	glViewport(0, 0, ancho_nuevo, alto_nuevo);
	controlador->evento_ventana(static_cast<float>(ancho_nuevo), static_cast<float>(alto_nuevo));
}

void eventos_raton_botones(Controlador_Juego *controlador, int boton, int accion, unsigned char numero_clics)
{
	Raton *raton = controlador->raton();

	bool estado = false;
	if(accion == SDL_PRESSED)
		estado = true;
	else if(accion == SDL_RELEASED)
		estado = false;

	if(boton == SDL_BUTTON_LEFT)
		raton->actualizar_boton(BotonRaton::Izquierdo, estado, numero_clics);
	else if(boton == SDL_BUTTON_MIDDLE)
		raton->actualizar_boton(BotonRaton::Central, estado, numero_clics);
	else if(boton == SDL_BUTTON_RIGHT)
		raton->actualizar_boton(BotonRaton::Derecho, estado, numero_clics);
	controlador->eventos_raton();
}
void eventos_raton_posicion(Controlador_Juego *controlador, int x, int y)
{
	Raton *raton = controlador->raton();
	raton->actualizar_posicion(x, y);
	controlador->eventos_raton();
}

void eventos_raton_rueda(Controlador_Juego *controlador, int desplazamiento_x, int desplazamiento_y)
{
	Raton *raton = controlador->raton();
	raton->actualizar_desplazamiento(desplazamiento_x, desplazamiento_y);
	controlador->eventos_raton();
	raton->actualizar_desplazamiento(0, 0);//Elimina el evento
}

void eventos_taclado(Controlador_Juego *controlador, int tecla, bool estado)
{
	switch(tecla)
	{
		case SDLK_0: controlador->eventos_teclado(Tecla::Numero_0, estado); break;
		case SDLK_1: controlador->eventos_teclado(Tecla::Numero_1, estado); break;
		case SDLK_2: controlador->eventos_teclado(Tecla::Numero_2, estado); break;
		case SDLK_3: controlador->eventos_teclado(Tecla::Numero_3, estado); break;
		case SDLK_4: controlador->eventos_teclado(Tecla::Numero_4, estado); break;
		case SDLK_5: controlador->eventos_teclado(Tecla::Numero_5, estado); break;
		case SDLK_6: controlador->eventos_teclado(Tecla::Numero_6, estado); break;
		case SDLK_7: controlador->eventos_teclado(Tecla::Numero_7, estado); break;
		case SDLK_8: controlador->eventos_teclado(Tecla::Numero_8, estado); break;
		case SDLK_9: controlador->eventos_teclado(Tecla::Numero_9, estado); break;

		case SDLK_a: controlador->eventos_teclado(Tecla::A, estado); break;
		case SDLK_b: controlador->eventos_teclado(Tecla::B, estado); break;
		case SDLK_c: controlador->eventos_teclado(Tecla::C, estado); break;
		case SDLK_d: controlador->eventos_teclado(Tecla::D, estado); break;
		case SDLK_e: controlador->eventos_teclado(Tecla::E, estado); break;
		case SDLK_f: controlador->eventos_teclado(Tecla::F, estado); break;
		case SDLK_g: controlador->eventos_teclado(Tecla::G, estado); break;
		case SDLK_h: controlador->eventos_teclado(Tecla::H, estado); break;
		case SDLK_i: controlador->eventos_teclado(Tecla::I, estado); break;
		case SDLK_j: controlador->eventos_teclado(Tecla::J, estado); break;
		case SDLK_k: controlador->eventos_teclado(Tecla::K, estado); break;
		case SDLK_l: controlador->eventos_teclado(Tecla::L, estado); break;
		case SDLK_m: controlador->eventos_teclado(Tecla::M, estado); break;
		case SDLK_n: controlador->eventos_teclado(Tecla::N, estado); break;
		case 241: controlador->eventos_teclado(Tecla::NN, estado); break;//Ñ
		case SDLK_o: controlador->eventos_teclado(Tecla::O, estado); break;
		case SDLK_p: controlador->eventos_teclado(Tecla::P, estado); break;
		case SDLK_q: controlador->eventos_teclado(Tecla::Q, estado); break;
		case SDLK_r: controlador->eventos_teclado(Tecla::R, estado); break;
		case SDLK_s: controlador->eventos_teclado(Tecla::S, estado); break;
		case SDLK_t: controlador->eventos_teclado(Tecla::T, estado); break;
		case SDLK_u: controlador->eventos_teclado(Tecla::U, estado); break;
		case SDLK_v: controlador->eventos_teclado(Tecla::V, estado); break;
		case SDLK_w: controlador->eventos_teclado(Tecla::W, estado); break;
		case SDLK_x: controlador->eventos_teclado(Tecla::X, estado); break;
		case SDLK_y: controlador->eventos_teclado(Tecla::Y, estado); break;
		case SDLK_z: controlador->eventos_teclado(Tecla::Z, estado); break;

		case SDLK_LEFT: controlador->eventos_teclado(Tecla::FlechaIzquierda, estado); break;
		case SDLK_RIGHT: controlador->eventos_teclado(Tecla::FlechaDerecha, estado); break;
		case SDLK_UP: controlador->eventos_teclado(Tecla::FlechaArriba, estado); break;
		case SDLK_DOWN: controlador->eventos_teclado(Tecla::FlechaAbajo, estado); break;

		case SDLK_SPACE: controlador->eventos_teclado(Tecla::Espacio, estado); break;
		case SDLK_BACKSPACE: controlador->eventos_teclado(Tecla::Borrar, estado); break;
		case SDLK_PAUSE: controlador->eventos_teclado(Tecla::Pausa, estado); break;
		case SDLK_ESCAPE: controlador->eventos_teclado(Tecla::Escape, estado); break;
		case SDLK_RETURN: controlador->eventos_teclado(Tecla::Entrar, estado); break;
		case SDLK_KP_ENTER: controlador->eventos_teclado(Tecla::Entrar, estado); break;
		case SDLK_TAB: controlador->eventos_teclado(Tecla::Tabulador, estado); break;
		case SDLK_QUOTE: controlador->eventos_teclado(Tecla::Apostrofe, estado); break;
		case 1073741824: controlador->eventos_teclado(Tecla::AcentoGrave, estado); break;
		case 161: controlador->eventos_teclado(Tecla::ExclamacionInicio, estado); break;
		case SDLK_PLUS: controlador->eventos_teclado(Tecla::Suma, estado); break;

		case SDLK_KP_MINUS: controlador->eventos_teclado(Tecla::RestaNumerico, estado); break;
		case SDLK_KP_PLUS: controlador->eventos_teclado(Tecla::SumaNumerico, estado); break;

		case SDLK_F1: controlador->eventos_teclado(Tecla::F1, estado); break;
		case SDLK_F2: controlador->eventos_teclado(Tecla::F2, estado); break;
		case SDLK_F3: controlador->eventos_teclado(Tecla::F3, estado); break;
		case SDLK_F4: controlador->eventos_teclado(Tecla::F4, estado); break;
		case SDLK_F5: controlador->eventos_teclado(Tecla::F5, estado); break;
		case SDLK_F6: controlador->eventos_teclado(Tecla::F6, estado); break;
		case SDLK_F7: controlador->eventos_teclado(Tecla::F7, estado); break;
		case SDLK_F8: controlador->eventos_teclado(Tecla::F8, estado); break;
		case SDLK_F9: controlador->eventos_teclado(Tecla::F9, estado); break;
		case SDLK_F10: controlador->eventos_teclado(Tecla::F10, estado); break;
		case SDLK_F11: controlador->eventos_teclado(Tecla::F11, estado); break;
		case SDLK_F12: controlador->eventos_teclado(Tecla::F12, estado); break;
		case SDLK_INSERT: controlador->eventos_teclado(Tecla::Insertar, estado); break;
		case SDLK_DELETE: controlador->eventos_teclado(Tecla::Suprimir, estado); break;
		case SDLK_HOME: controlador->eventos_teclado(Tecla::Inicio, estado); break;
		case SDLK_END: controlador->eventos_teclado(Tecla::Fin, estado); break;
		case SDLK_PAGEUP: controlador->eventos_teclado(Tecla::Repag, estado); break;
		case SDLK_PAGEDOWN: controlador->eventos_teclado(Tecla::Avpag, estado); break;
		default: return;
	}
}

void crear_carpeta_configuracion()
{
	try
	{
		//Crear carpetas de datos si no existe
		if(!std::filesystem::exists(Usuario::carpeta_juego()))
		{
			bool carpeta_creada = std::filesystem::create_directories(Usuario::carpeta_juego());
			if(!carpeta_creada)
				Registro::Error(T_("No se puede crear la carpeta \"{0}\", no se guardara la configuracion ni el registro", Usuario::carpeta_juego()));
		}

		//Mueve los archivos de configuración a la nueva posicion
		if(std::filesystem::exists(Usuario::carpeta_juego()))
		{
			if(std::filesystem::exists(Usuario::carpeta_personal() + "/.concertista.db"))
				std::filesystem::rename(Usuario::carpeta_personal() + "/.concertista.db", Usuario::carpeta_juego() + "concertista.db");
			if(std::filesystem::exists(Usuario::carpeta_personal() + "/.registros_concertista_midi.txt"))
				std::filesystem::rename(Usuario::carpeta_personal() + "/.registros_concertista_midi.txt", Usuario::carpeta_juego() + "registro.txt");
		}
	}
	catch(std::filesystem::filesystem_error const &e)
	{
		Registro::Error(T_("Fallo al crear el archivo de configuración {0}", std::string(e.what())));
	}
}

void actualizar_dimension_ventana(SDL_Window *ventana, Controlador_Juego *controlador)
{
	//Por algun error en sdl al cambiar a pantalla completa no se genera un evento de SDL_WINDOWEVENT_SIZE_CHANGED
	//esta funcion es llamada al cambiar o salir de pantalla completa
	int nuevo_ancho, nuevo_alto;
	SDL_GetWindowSize(ventana, &nuevo_ancho, &nuevo_alto);
	ajustar_ventana(controlador, nuevo_ancho, nuevo_alto);
}

void guardad_configuracion_pantalla(SDL_Window *ventana, Controlador_Juego *controlador, int *x, int *y, int *ancho, int *alto)
{
	int nueva_x = 0, nueva_y = 0, nuevo_ancho = 0, nuevo_alto = 0;
	int arriba_borde_ventana, abajo_borde_ventana, izquierda_borde_ventana, derecha_borde_ventana;

	SDL_GetWindowPosition(ventana, &nueva_x, &nueva_y);
	SDL_GetWindowSize(ventana, &nuevo_ancho, &nuevo_alto);
	SDL_GetWindowBordersSize(ventana, &arriba_borde_ventana, &izquierda_borde_ventana, &abajo_borde_ventana, &derecha_borde_ventana);

	nueva_x = nueva_x - izquierda_borde_ventana;
	nueva_y = nueva_y - arriba_borde_ventana;

	if(*x != nueva_x || *y != nueva_y)
	{
		controlador->configuracion()->guardar_posicion_ventana(nueva_x, nueva_y);
		*x = nueva_x;
		*y = nueva_y;
	}

	if(*ancho != nuevo_ancho || *alto != nuevo_alto)
	{
		controlador->configuracion()->guardar_dimension_ventana(nuevo_ancho, nuevo_alto);
		*ancho = nuevo_ancho;
		*alto = nuevo_alto;
	}
}

void mostrar_detalles()
{
	const GLubyte *marca = glGetString(GL_VENDOR);
	const GLubyte *tarjeta_grafica = glGetString(GL_RENDERER);
	const GLubyte *version = glGetString(GL_VERSION);
	const GLubyte *version_glsl = glGetString(GL_SHADING_LANGUAGE_VERSION);

	Registro::Nota(T_("Compañia: {0}", std::string(reinterpret_cast<const char*>(marca))));
	Registro::Nota(T_("Tarjeta Grafica: {0}", std::string(reinterpret_cast<const char*>(tarjeta_grafica))));
	Registro::Nota(T_("Versión OpenGl: {0}", std::string(reinterpret_cast<const char*>(version))));
	Registro::Nota(T_("Versión GLSL: {0}", std::string(reinterpret_cast<const char*>(version_glsl))));

	GLenum parametros[] = {
		GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS,
		GL_MAX_CUBE_MAP_TEXTURE_SIZE,
		GL_MAX_DRAW_BUFFERS,
		GL_MAX_FRAGMENT_UNIFORM_COMPONENTS,
		GL_MAX_TEXTURE_IMAGE_UNITS,
		GL_MAX_TEXTURE_SIZE,
		GL_MAX_VARYING_FLOATS,
		GL_MAX_VERTEX_ATTRIBS,
		GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS,
		GL_MAX_VERTEX_UNIFORM_COMPONENTS,
		GL_MAX_GEOMETRY_OUTPUT_VERTICES,
		GL_MAX_GEOMETRY_TOTAL_OUTPUT_COMPONENTS,
	};
	const char* nombres[] = {
		"GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS",
		"GL_MAX_CUBE_MAP_TEXTURE_SIZE",
		"GL_MAX_DRAW_BUFFERS",
		"GL_MAX_FRAGMENT_UNIFORM_COMPONENTS",
		"GL_MAX_TEXTURE_IMAGE_UNITS",
		"GL_MAX_TEXTURE_SIZE",
		"GL_MAX_VARYING_FLOATS",
		"GL_MAX_VERTEX_ATTRIBS",
		"GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS",
		"GL_MAX_VERTEX_UNIFORM_COMPONENTS",
		"GL_MAX_GEOMETRY_OUTPUT_VERTICES",
		"GL_MAX_GEOMETRY_TOTAL_OUTPUT_COMPONENTS",
	};

	Registro::Depurar("---------------------------------------");
	Registro::Depurar(T("Parametros del Contexto GL:"));

	int valor = 0;
	for (int x = 0; x < 12; x++)
	{
		glGetIntegerv(parametros[x], &valor);
		Registro::Depurar(std::string(nombres[x]) + " " + std::to_string(valor));
	}

	int valor_doble[2];
	valor_doble[0] = valor_doble[1] = 0;
	glGetIntegerv(GL_MAX_VIEWPORT_DIMS, valor_doble);
	Registro::Depurar("GL_MAX_VIEWPORT_DIMS " + std::to_string(valor_doble[0]) + " " + std::to_string(valor_doble[1]));

	unsigned char estereo = 0;
	glGetBooleanv(GL_STEREO, &estereo);
	Registro::Depurar("GL_STEREO " + std::to_string(static_cast<unsigned int>(estereo)));
	Registro::Depurar("---------------------------------------");
}
