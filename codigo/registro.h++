#ifndef REGISTRO_H
#define REGISTRO_H

#include <string>
#include <mutex>

enum class CodigoEstado : unsigned char
{
	Error = 0,
	Aviso = 1,
	Nota = 2,
	Depurar = 3,
	Ninguno = 4
};

class Registro
{
private:
	static std::mutex Bloqueo;
	static void Escribir_registro(CodigoEstado estado, std::string texto);
public:
	static void Error(std::string texto);
	static void Aviso(std::string texto);
	static void Nota(std::string texto);
	static void Depurar(std::string texto);
};

#endif
